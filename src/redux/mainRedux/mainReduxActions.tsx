import * as actionTypes from './mainReduxActionTypes'; 


export function update_user(data : any) {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    data
  }
}

export function login_error(err:any) {
  return {
    type: actionTypes.LOGIN_ERROR,
    err
  }
}

export function update_name(data:any) {
  return {
    type: actionTypes.UPDATE_NAME,
    data
  }
}

export function add_files(data:any) {
  return {
    type: actionTypes.ADD_FILES,
    data
  }
}

export function update_files(data:any) {
  return {
    type: actionTypes.UPDATE_FILES,
    data
  }
}

export function update_customers_bills(data:any) {
  return {
    type: actionTypes.UPDATE_CUSTOMERS_BILLS,
    data
  }
}

export function update_providers_bills(data:any) {
  return {
    type: actionTypes.UPDATE_PROVIDERS_BILLS,
    data
  }
}


export function update_customers_processable_bills(data:any) {
  return {
    type: actionTypes.UPDATE_CUSTOMERS_PROCESSABLE_BILLS,
    data
  }
}

export function update_providers_processable_bills(data:any) {
  return {
    type: actionTypes.UPDATE_PROVIDERS_PROCESSABLE_BILLS,
    data
  }
}

export function update_computed_bills(data:any) {
  return {
    type: actionTypes.UPDATE_COMPUTED_BILLS,
    data
  }
}

export function update_computed_quotes(data:any) {
  return {
    type: actionTypes.UPDATE_COMPUTED_QUOTES,
    data
  }
}

export function update_ged_map(data:any) {
  return {
    type: actionTypes.UPDATE_GED_MAP,
    data
  }
}



export function update_exercise_year(data:any) {
  return {
    type: actionTypes.UPDATE_EXERCISE_YEAR,
    data
  }
}

export function init_process_files(data:any) {
  return {
    type: actionTypes.INIT_PROCESS_FILES,
    data
  }
}


export function update_process_files(data:any) {
  return {
    type: actionTypes.UPDATE_PROCESS_FILES,
    data
  }
}

export function remove_bill(data:any) {
  return {
    type: actionTypes.REMOVE_BILL,
    data
  }
}

export function edit_bill(data:any) {
  return {
    type: actionTypes.EDIT_BILL,
    data
  }
}


export function update_tab(data:any) {
  return {
    type: actionTypes.UPDATE_TAB,
    data
  }
}


export function update_company(data:any) {
  return {
    type: actionTypes.UPDATE_CREATED_COMPANY,
    data
  }
}

 
export function update_customers_list(data:any) {
  return {
    type: actionTypes.UPDATE_CUSTOMERS_LIST,
    data
  }
}

export function update_selected_company(data:any) {
  return {
    type: actionTypes.UPDATE_SELECTED_COMPANY,
    data
  }
}
 
export function remove_box_items() {
  return {
    type: actionTypes.REMOVE_BOX_ITEMS
  }
}


export function edit_box_items() {
  return {
    type: actionTypes.EDIT_BOX_ITEMS
  }
}

export function change_month(data:any) {
  return {
    type: actionTypes.CHANGE_MONTH,
    data
  }
}