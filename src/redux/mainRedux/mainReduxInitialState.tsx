const initialState = {
    mainReduxState: {
      user : {
         id: null,
         name : "Default",
      },
      
      active_tab : "main",

      files : [],

      customers_bills : {files : [], page:1},
      providers_bills : {files : [], page:1},

      customers_processable_bills : {files : [], page:1},
      providers_processable_bills : {files : [], page:1},      

      customers : [],

      selected_company : {},

      gedMap: [],

      // computed bills
      computed_bills_all : {files : [], page:1},
      computed_bills_payed : {files : [], page:1},
      computed_bills_suffering : {files : [], page:1}, // half payed
      computed_bills_unpayed : {files : [], page:1},
      computed_bills_sent : {files : [], page:1},


      // computed quotes
      computed_quotes_all : {files : [], page:1},
      computed_quotes_payed : {files : [], page:1},
      computed_quotes_suffering : {files : [], page:1}, // half payed
      computed_quotes_unpayed : {files : [], page:1},
      computed_quotes_sent : {files : [], page:1},

      //------------

      created_company : [], // created company

      remove_box_items : false,
      edit_box_items : false,

      active_month : new Date().getMonth(),

      exercise_year : new Date().getFullYear().toString(),

      error: '',
    }
  }


  export default initialState;