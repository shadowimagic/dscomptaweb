import * as types from './mainReduxActionTypes';
import initialState from './mainReduxInitialState';

import {reactLocalStorage} from 'reactjs-localstorage';


export default function mainReduxReducer(state = initialState.mainReduxState, action:any) {
  switch (action.type) {
    case types.LOGIN_ERROR:
      return Object.assign({}, state, { username: 'Nooooo' });
    case types.LOGIN_SUCCESS:

      reactLocalStorage.setObject('ls_loggedUser', action.data);

      return {
        ...state,
        user: {...state.user, ...action.data},
      };

    case types.UPDATE_NAME:
    return {
      ...state,
      user: {...state.user, ...action.data},
    };

    // Adding content into files
    case types.ADD_FILES:
      return {
        ...state,
        files: [...state.files, ...action.data],
      };
   
    // Adding content into customers_bills
    case types.UPDATE_CUSTOMERS_BILLS:
      return {
        ...state,
        customers_bills: action.data,
      };

    // Adding content into providers_bills
    case types.UPDATE_PROVIDERS_BILLS:
      return {
        ...state,
        providers_bills: action.data,
      };      

   
    // Adding content into customers_bills
    case types.UPDATE_CUSTOMERS_PROCESSABLE_BILLS:
      return {
        ...state,
        customers_processable_bills: action.data,
      };

    // Adding content into providers_processable_bills
    case types.UPDATE_PROVIDERS_PROCESSABLE_BILLS:
      return {
        ...state,
        providers_processable_bills: action.data,
      };      

    // Adding content into computed bills
    case types.UPDATE_COMPUTED_BILLS:

      switch (action.data.status) {
        case "all":

          return {
            ...state,
            computed_bills_all: action.data.data
          }; 
           

        case "payed":
          return {
            ...state,
            computed_bills_payed: action.data.data
          }; 

        case "suffering":
          return {
            ...state,
            computed_bills_suffering: action.data.data
          };  

        case "unpayed":
          return {
            ...state,
            computed_bills_unpayed: action.data.data
          }; 

        case "sent":
          return {
            ...state,
            computed_bills_sent: action.data.data
          }; 

    }
    

    // Adding content into computed quotes
    case types.UPDATE_COMPUTED_QUOTES:

      switch (action.data.status) {
        case "all":

          return {
            ...state,
            computed_quotes_all: action.data.data
          }; 
           

        case "payed":
          return {
            ...state,
            computed_quotes_payed: action.data.data
          }; 

        case "suffering":
          return {
            ...state,
            computed_quotes_suffering: action.data.data
          };  

        case "unpayed":
          return {
            ...state,
            computed_quotes_unpayed: action.data.data
          }; 

        case "sent":
          return {
            ...state,
            computed_quotes_sent: action.data.data
          }; 

    }
    

      // Adding content into files
    case types.UPDATE_FILES:
      
      // let _files = [...state.files];
      
      return {
        ...state,
        files: action.data,
      };

    
      case types.UPDATE_TAB:
       
        return {
          ...state,
          active_tab: action.data,
        };

      // UPDATE CREATED COMPANY
      case types.UPDATE_CREATED_COMPANY:
    
        reactLocalStorage.setObject('ls_loggedCompany', action.data);

        return {
          ...state,
          created_company: action.data,
        };

      // UPDATE CUSTOMERS LIST
      case types.UPDATE_CUSTOMERS_LIST:
    
        // reactLocalStorage.setObject('ls_customersList', action.data);

        return {
          ...state,
          customers: action.data,
        };


      // UPDATE SELECTED COMPANY
      case types.UPDATE_SELECTED_COMPANY:
    
        reactLocalStorage.setObject('ls_selectedCompany', action.data);

        return {
          ...state,
          selected_company: action.data,
        };

      // Update ged files
      case types.UPDATE_GED_MAP:
   
        return {
          ...state,
          gedMap : action.data,
        };

    
        // Update company exercise year
    case types.UPDATE_EXERCISE_YEAR:
  
      return {
        ...state,
        exercise_year : action.data,
      };

    // Setting all files with "none" status to "progress"
    case types.INIT_PROCESS_FILES:
    
      switch (action.data) {
        case "customer":
          
          // console.log("UPDATING")
          var init_tmp = {...state.customers_bills};
          
          //@ts-ignore
          var init_filesTemp = init_tmp.files.map(el=> el.status == "none" ? {...el, status: "progress" } : el)
          
          //@ts-ignore
          init_tmp.files = init_filesTemp;
  
          // Check for processable customers bills
          var init_tmp_processable = {...state.customers_processable_bills};
          //@ts-ignore
          var init_processableFilesTemp = init_tmp_processable.files.map(el=> el.status == "none" ? {...el, status: "progress" } : el)
          
          //@ts-ignore
          init_tmp_processable.files = init_processableFilesTemp;
  
          return { ...state, customers_bills : init_tmp, customers_processable_bills : init_tmp_processable };
        
        case "provider":
        
          var init_tmp = {...state.providers_bills};
          
          //@ts-ignore
          var init_filesTemp = init_tmp.files.map(el=> el.status == "none" ? {...el, status: "progress" } : el)
          
          //@ts-ignore
          init_tmp.files = init_filesTemp;
  
          // Check for processable providers bills
          var init_tmp_processable = {...state.providers_processable_bills};
          //@ts-ignore
          var init_processableFilesTemp = init_tmp_processable.files.map(el=> el.status == "none" ? {...el, status: "progress" } : el)
          
          //@ts-ignore
          init_tmp_processable.files = init_processableFilesTemp;
  
          return { ...state, providers_bills : init_tmp, providers_processable_bills : init_tmp_processable };
        
      }
  

    // Setting all files with "none" status to "progress"
    case types.UPDATE_PROCESS_FILES:
      
      var files = action.data;
      // ------------ Update processable bills : customers ---------------//
      var customers_processable_bills_tmp = {...state.customers_processable_bills};
      var customers_processable_bills_tmp_files = customers_processable_bills_tmp.files;
      for (let i = 0; i < files.length; i++) {
        
        const el = files[i];
        //@ts-ignore
        customers_processable_bills_tmp_files = customers_processable_bills_tmp_files.map((u)=> el.id == u.id ? el : u);
      }
      customers_processable_bills_tmp.files = customers_processable_bills_tmp_files;
      //-----------------------------------------------------------------//

      // ------------ Update processable bills : providers ---------------//
      var providers_processable_bills_tmp = {...state.providers_processable_bills};
      var providers_processable_bills_tmp_files = providers_processable_bills_tmp.files;
      for (let i = 0; i < files.length; i++) {
        
        const el = files[i];
        //@ts-ignore
        providers_processable_bills_tmp_files = providers_processable_bills_tmp_files.map((u)=> el.id == u.id ? el : u);
      }
      providers_processable_bills_tmp.files = providers_processable_bills_tmp_files;
      //-----------------------------------------------------------------//

      // ------------ Update bills : customers ---------------//
      var customers_bills_tmp = {...state.customers_bills};
      var customers_bills_tmp_files = customers_bills_tmp.files;
      for (let i = 0; i < files.length; i++) {
        
        const el = files[i];
        //@ts-ignore
        customers_bills_tmp_files = customers_bills_tmp_files.map((u)=> el.id == u.id ? el : u);
      }
      customers_bills_tmp.files = customers_bills_tmp_files;
      //-----------------------------------------------------------------//


      // ------------ Update bills : providers ---------------//
      var providers_bills_tmp = {...state.providers_bills};
      var providers_bills_tmp_files = providers_bills_tmp.files;
      for (let i = 0; i < files.length; i++) {
        
        const el = files[i];
        //@ts-ignore
        providers_bills_tmp_files = providers_bills_tmp_files.map((u)=> el.id == u.id ? el : u);
      }
      providers_bills_tmp.files = providers_bills_tmp_files;
      //-----------------------------------------------------------------//



      // RETURN
      return { ...state, 
        customers_processable_bills : customers_processable_bills_tmp,
        providers_processable_bills : providers_processable_bills_tmp,
        customers_bills : customers_bills_tmp,
        providers_bills : providers_bills_tmp
      };

    // Setup Option : REMOVE
    case types.REMOVE_BOX_ITEMS:
     
      return {
        ...state,
        //@ts-ignore
        remove_box_items: !state.remove_box_items,
        edit_box_items: false
      };  
      
        // Setup Option :EDIT
    case types.EDIT_BOX_ITEMS:
  
      return {
        ...state,
        //@ts-ignore
        edit_box_items: !state.edit_box_items,
        remove_box_items: false
      };  

    // Remove bill
    case types.REMOVE_BILL:

    var ret = state;

    // console.log(state);

    switch (action.data.type) {
      case "customer":
        
        // console.log("REMOVING")
        //@ts-ignore
        var indexToDelete = state.customers_bills.files.findIndex(e=> e.id == action.data.id)
        var tmp = {...state.customers_bills};
        var filesTemp = [
          ...tmp.files.slice(0, indexToDelete),
          ...tmp.files.slice(indexToDelete + 1)
        ];
        tmp.files = filesTemp;

        // Check for processable customers bills
        var tmp_processable = {...state.customers_processable_bills};
        //@ts-ignore
        var processableIndexToDelete = state.customers_processable_bills.files.findIndex(e=> e.id == action.data.id)
        if(processableIndexToDelete != -1)
        {
          var processableFilesTemp = [
            ...tmp_processable.files.slice(0, processableIndexToDelete),
            ...tmp_processable.files.slice(processableIndexToDelete + 1)
          ];
          tmp_processable.files = processableFilesTemp;
        }

        return { ...state, customers_bills : tmp, customers_processable_bills : tmp_processable };
      
      case "provider":
      
        //@ts-ignore
        var indexToDelete = state.providers_bills.files.findIndex(e=> e.id == action.data.id)

        var tmp = {...state.providers_bills};
        var filesTemp = [
          ...tmp.files.slice(0, indexToDelete),
          ...tmp.files.slice(indexToDelete + 1)
        ];
        tmp.files = filesTemp;

       // Check for processable providers bills
       var tmp_processable = {...state.providers_processable_bills};
       //@ts-ignore
       var processableIndexToDelete = state.providers_processable_bills.files.findIndex(e=> e.id == action.data.id)
       if(processableIndexToDelete != -1)
       {
         var processableFilesTemp = [
           ...tmp_processable.files.slice(0, processableIndexToDelete),
           ...tmp_processable.files.slice(processableIndexToDelete + 1)
         ];
         tmp_processable.files = processableFilesTemp;
       }


       return { ...state, providers_bills : tmp, providers_processable_bills : tmp_processable};
      
    }


    // Setting all files with "none" status to "progress"
    case types.CHANGE_MONTH:

    return {
      ...state,
      //@ts-ignore
      active_month: action.data
    };


    default:
      return state;
  }
}