// src/redux/store.ts

import { configureStore, combineReducers } from '@reduxjs/toolkit'
import authReducer from '../features/Authentication/authenticationSlice'
import { createHashHistory} from 'history';

export const history = createHashHistory();

const store = configureStore({
  reducer: combineReducers({
    authentication: authReducer,
  }),
})

export default store
