"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExtraActions = void 0;
var icons_types_1 = require("../types/icons.types");
var helpers_1 = require("../util/helpers");
exports.ExtraActions = {
    /**
     * Action that adds a button and shortcut to copy files.
     */
    CopyFiles: helpers_1.defineFileAction({
        id: 'copy_files',
        requiresSelection: true,
        hotkeys: ['ctrl+c'],
        button: {
            name: 'Copy selection',
            toolbar: true,
            contextMenu: true,
            group: 'Actions',
            icon: icons_types_1.ChonkyIconName.copy,
        },
    }),
    /**
     * Action that adds a button to create a new folder.
     */
    CreateFolder: helpers_1.defineFileAction({
        id: 'create_folder',
        button: {
            name: 'Nouveau dossier',
            toolbar: true,
            tooltip: 'Create a folder',
            icon: icons_types_1.ChonkyIconName.folderCreate,
        },
    }),
    /**
     * Action that adds a button to Uploader.
     */
    UploadFiles: helpers_1.defineFileAction({
        id: 'upload_files',
        button: {
            name: 'Uploader',
            toolbar: true,
            tooltip: 'Uploader',
            icon: icons_types_1.ChonkyIconName.upload,
        },
    }),
    /**
     * Action that adds a button to Telecharger.
     */
    DownloadFiles: helpers_1.defineFileAction({
        id: 'download_files',
        requiresSelection: true,
        button: {
            name: 'Telecharger',
            toolbar: true,
            contextMenu: true,
            group: 'Actions',
            icon: icons_types_1.ChonkyIconName.download,
        },
    }),
    /**
     * Action that adds a button and shortcut to Supprimer.
     */
    DeleteFiles: helpers_1.defineFileAction({
        id: 'delete_files',
        requiresSelection: true,
        hotkeys: ['delete'],
        button: {
            name: 'Supprimer',
            toolbar: true,
            contextMenu: true,
            group: 'Actions',
            icon: icons_types_1.ChonkyIconName.trash,
        },
    }),
};
