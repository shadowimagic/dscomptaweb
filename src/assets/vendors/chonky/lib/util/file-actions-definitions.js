"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OldChonkyActions = void 0;
var icons_types_1 = require("../types/icons.types");
var validateActionTypes = function (actionMap) { return actionMap; };
exports.OldChonkyActions = validateActionTypes({
    // Optional actions
    CopyFiles: {
        id: 'copy_files',
        requiresSelection: true,
        hotkeys: ['ctrl+c'],
        button: {
            name: 'Copier la selection',
            toolbar: true,
            contextMenu: true,
            group: 'Actions',
            dropdown: true,
            icon: icons_types_1.ChonkyIconName.copy,
        },
    },
    CreateFolder: {
        id: 'create_folder',
        button: {
            name: 'Nouveau dossier',
            toolbar: true,
            contextMenu: true,
            tooltip: 'Nouveau dossier',
            icon: icons_types_1.ChonkyIconName.folderCreate,
        },
    },
    UploadFiles: {
        id: 'upload_files',
        button: {
            name: 'Uploader',
            toolbar: true,
            contextMenu: true,
            tooltip: 'Uploader',
            icon: icons_types_1.ChonkyIconName.upload,
        },
    },
    DownloadFiles: {
        id: 'download_files',
        requiresSelection: true,
        button: {
            name: 'Telecharger',
            toolbar: true,
            contextMenu: true,
            group: 'Actions',
            tooltip: 'Telecharger',
            dropdown: true,
            icon: icons_types_1.ChonkyIconName.download,
        },
    },
    DeleteFiles: {
        id: 'delete_files',
        requiresSelection: true,
        hotkeys: ['delete'],
        button: {
            name: 'Supprimer',
            toolbar: true,
            contextMenu: true,
            group: 'Actions',
            tooltip: 'Supprimer',
            dropdown: true,
            icon: icons_types_1.ChonkyIconName.trash,
        },
    },
});
