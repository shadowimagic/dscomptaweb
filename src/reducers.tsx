import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import mainReduxState from './redux/mainRedux/mainReduxReducer';
export default (history) => combineReducers({
  router: connectRouter(history),
  mainReduxState
})