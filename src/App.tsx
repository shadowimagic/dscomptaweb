// src/App.tsx

import React from 'react'
import './App.scss';
import Emitter from './services/emitter';
import './blueprint.scss';
// import './tabler.scss';
import 'semantic-ui-css/semantic.css';

import routes from './constants/routes.json';

import SplashPage from './containers/SplashPage';
import SignupPage from './containers/SignupPage';
import WelcomePage from './containers/WelcomePage';
import SigninPage from './containers/SigninPage';
import SignupOwnerPage from './containers/SignupOwnerPage';
import HomePage from './containers/HomePage';
import Dashboard from './containers/Dashboard';


import {  Route, BrowserRouter as Router, Switch  } from 'react-router-dom'
import {  Provider  } from 'react-redux'
import {  ConnectedRouter } from 'connected-react-router'
import configureStore, { history } from './configureStore'
import CheckoutPage from './containers/CheckoutPage';

const store = configureStore();
 
const routing = (
    <Router>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Switch>
                    <Route path={routes.DASHBOARD} component={Dashboard} />
                    <Route path={routes.HOME} component={HomePage} />
                    <Route path={routes.CHECKOUT} component={CheckoutPage} />
                    <Route path={routes.WELCOME} component={WelcomePage} />
                    <Route path={routes.SIGNIN} component={SigninPage} />
                    <Route path={routes.SIGNUPOWNER} component={SignupOwnerPage} />
                    <Route path={routes.SIGNUP} component={SignupPage} />
                    <Route exact path={routes.SPLASH} component={SplashPage} />
                </Switch>
            </ConnectedRouter>
        </Provider>
    </Router>
)

function App() {

  return (
    routing
  )
}

export default App
