import React from 'react';
import SigninScreen from '../components/SigninScreen/SigninScreen';

export default function SigninPage() {
  return <SigninScreen />;
}
