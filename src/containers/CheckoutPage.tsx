import React from 'react';
import CheckoutScreen from '../components/CheckoutScreen/CheckoutScreen';

export default function CheckoutPage() {
  return <CheckoutScreen />;
}
