import React from 'react';
import SignupScreen from '../components/SignupScreen/SignupScreen';

export default function SignupPage() {
  return <SignupScreen />;
}
