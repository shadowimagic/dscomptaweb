import React from 'react';
import SplashScreen from '../components/SplashScreen/SplashScreen';

export default function SplashPage() {
  return <SplashScreen />;
}
