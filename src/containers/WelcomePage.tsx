import React from 'react';
import WelcomeScreen from '../components/WelcomeScreen/WelcomeScreen';

export default function WelcomePage() {
  return <WelcomeScreen />;
}
