import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../global.module.css';
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../icons';

import routes from '../../constants/routes.json';

import UserServices from '../../services/UserServices';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';
import {history} from '../../redux/store';
import {reactLocalStorage} from 'reactjs-localstorage';

const height = window.innerHeight;


 
class SigninScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {
      screen: "splash",
      splash_fade : 1,
      input_user_type : 1,
      input_usermail : "",
      input_password : "",
      display_errors : false,
      disable_signin_btn : false,
      connexion_status : "default",

      // Test redux 
      username : "admin@ds.com",
      password : "admin"
      
    }

  }
   
 
  componentDidMount()
  {
    // this.setState({splash_fade : 0});
    // setTimeout(()=>{
    //     history.replace(routes.HOME)

    // }, 1200)
  }

  handleUsermail(value : string){

    this.setState({input_usermail : value})
  }
  handlePassword(value : string){

    this.setState({input_password : value})
  }

  
  signin = ()=>
  {
      var data = {
        email: this.state.input_usermail,
        password: this.state.input_password
      }

    if(data.email != "" && data.password != "")
    {
       this.setState({
         display_errors : false,
         disable_signin_btn :true,
         connexion_status : "connecting"
      });

      //  console.log(data);
       UserServices._signin(data.email, data.password).then(resp =>{

        if(resp.data.length > 0 && resp.data[0].password == data.password )
        {
          this.setState({
            disable_signin_btn :false,
            connexion_status : "success"
          });

          // localStorage.setItem('user', JSON.stringify(resp.data[0]));
          let loggedUser = resp.data[0];
          this.props.mainReduxActions.update_user(loggedUser);
          reactLocalStorage.setObject('ls_loggedUser', loggedUser);

          UserServices._getCompanyById(loggedUser.companyId)
          .then(resp =>{
            
            let currentCompany = resp.data;
            this.props.mainReduxActions.update_company(currentCompany);

          });

          setTimeout(()=>{
              history.replace(routes.HOME)
          },1100)
          
          console.log(loggedUser);
        }
        else
        {
          this.setState({
            disable_signin_btn :false,
            connexion_status : "notexist"
          });
        }

        }).catch((err)=>{
          
          this.setState({
            disable_signin_btn :false,
            connexion_status : "error"
          });
          console.log(err);

        });
    }
    else
    {
      this.setState({display_errors : true});
    }

  }


  // onUpdateName = () => {
  //   this.props.mainReduxActions.update_name({name : this.state.input_password});
  // }

 render() {

// ANIMATIONS PROPS


  return (
    <div className={styles.container} data-tid="container" style={{height : (height ) , backgroundImage: `url(${AppIcons.ds_backg_1})`, backgroundSize: "cover", backgroundRepeat:"no-repeat" }}>
       <div className={styles.appContentContainerRight} style={{backgroundColor : "#ffffffe0"}} >
         <span style={{fontSize : 12, color : "#AB3134",fontWeight:"bolder" }}>CONNEXION</span>
         <div style={{width : 30, height:4, backgroundColor : "#8E295F", marginBottom : 20}}></div>

        <div className={styles.appContainerRightFields} >
          
          {/* <span style={{color :"black"}}>{this.props.mainReduxState.user.name}</span>
          <button onClick={this.onUpdateName} >SE CONNECTER</button> */}

          {/* Field */}
          <div className={styles.appContainerRightField}>
            {/* <span>Date</span> */}
            <input className={globalStyles.appInputBase} placeholder="Votre identifiant" value={this.state.input_usermail} onChange={(e:any)=>this.handleUsermail(e.target.value)} type="text" style={{backgroundColor : "#ffffff85"}}
  />
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            {/* <span>N° de Piéce</span> */}
            <input className={globalStyles.appInputBase} placeholder="Mot de passe" value={this.state.input_password} onChange={(e:any)=>this.handlePassword(e.target.value)} type="password" style={{backgroundColor : "#ffffff85"}}/>

          </div>

          
          <div className={styles.appContainerBtn}>
            <button className={(this.state.disable_signin_btn ? styles.signinDisabled : styles.signinEnabled)} onClick={this.signin} >SE CONNECTER</button>
          </div>

          {this.state.connexion_status == "default" &&
            <span style={{color : "#111111",fontFamily:"Poppins", fontSize:14, fontWeight : "bolder"}}>Pas encore de compte ? <span onClick={()=>{history.replace(routes.SIGNUP) }} style={{color:"#AB3134", cursor:"pointer"}}> Vous inscrire</span> </span> 
          }
          <div style={{display : "flex", flexDirection : "column", alignContent:"center", alignItems:"center" }}>
            
            {this.state.connexion_status == "connecting" &&
              <img src={AppIcons.ds_signup_loading} style={{width : 100, opacity : 0.6}}  alt="" />
            }
            {this.state.connexion_status == "connecting" &&
              <span style={{color : "#111111",fontFamily:"Poppins", fontSize:12, fontWeight : "bolder"}}>Connexion en cours... </span> 
            }
            {this.state.connexion_status == "error" &&
              <span style={{color : "#D63737",fontFamily:"Poppins", fontSize:12, fontWeight : "bolder"}}>Une erreur est survenue. Réessayez </span> 
            }
            {this.state.connexion_status == "notexist" &&
              <span style={{color : "#D63737",fontFamily:"Poppins", fontSize:12, fontWeight : "bolder"}}>Compte inexistant </span> 
            }
            {this.state.connexion_status == "success" &&
              <span style={{fontFamily:"Poppins", fontSize:12, fontWeight : "bolder", color : "#43C057"}}>Connexion établie ! </span> 
            }
            </div>

        </div>

      </div>

    </div>
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SigninScreen);
