import React, { Component } from 'react';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../icons';

import routes from '../../constants/routes.json';
import {history} from '../../redux/store';

const height = window.innerHeight;


 
class SplashScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {
      screen: "splash",
      splash_fade : 1
    }

  }
   
 
  componentDidMount()
  {
    this.setState({splash_fade : 0});
    setTimeout(()=>{

        history.replace(routes.WELCOME);

    }, 1200)
  }

 

 render() {

// ANIMATIONS PROPS


  return (
    <motion.div animate={{opacity : this.state.splash_fade}} transition={{ duration: 1.2 }} className={styles.container} data-tid="container" style={{height : (height ) }}>
      
      <img src={AppIcons.ds_logo_big} className={styles.splashImg}  alt="" />
       
    </motion.div>
  );
  
 }
}

export default SplashScreen;
