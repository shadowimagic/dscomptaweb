import React, { Component } from 'react';
import styles from './style.module.css';
import { motion } from "framer-motion";
import AppIcons from '../../../icons';
import TesseractServices from '../../../services/TesseractServices';
import { Icon, Intent, Label, Position, Toaster } from "@blueprintjs/core";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import Emitter from '../../../services/emitter';


class SubmenuPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      user : {},
      status : "success",
      upload_files_count : 0,
      upload_success_counter : 0,
      upload_error_counter : 0,
      uploading : false,
      active_index : 0,
      context_menu_index : -1,
      types : ["customer", "provider"],

    };

    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);

  }

componentDidMount()
{
  //@ts-ignore

  // window.addEventListener('click', (e) =>{ 
  //   // do something here... 
  //   e.preventDefault();
  //   e.stopPropagation();
  //   this.setState({context_menu_index : -1})

  // });
  
  Emitter.on(Emitter.types.CLOSE_POPOVER, (src) => this.onClose());
   
}

onClose()
{
  // e.stopPropagation()
  this.setState({context_menu_index : -1})
}  
  
onSubmenuClicked = (e:Event, index : number)=>{
  e.stopPropagation();
  
  this.setState({active_index : index})
  this.props.onSubmenuClicked(index)

  Emitter.emit(Emitter.types.FILES_TYPE_CHANGED, this.props.submenu[index].route);

}  

onFilesAdded = () =>{
  this.props.onFilesAdded()
}  


onContextMenuClicked = (index : number)=>{
  if(this.props.denyContext != true)
  // console.log("Context Menu")
    this.setState({context_menu_index : index});
}  

onOptClicked = (e : Event,type : string, index : number)=>
{
    e.preventDefault();
    e.stopPropagation();
    switch (type) {
      case "ADD":
        // this.openUploadFileContext();
        break;
    
      case "REMOVE":
        break;
      
      case "EXPORT":
      
      break;
       
    }
}  

_onUploadClicked = (event :Event)=>{
  event.stopPropagation();
}

_uploadFile = (event : any) =>{

    this.setState({context_menu_index : -1, uploading : true})
    
    event.preventDefault();
    event.stopPropagation();
    // console.log(event.target);

    var tgt = event.target,
    files = tgt.files;
    
    this.setState({upload_files_count : files.length, upload_success_counter : 0, upload_error_counter : 0});

    for (let index = 0; index < files.length; index++) {
      
          if (FileReader && files && files.length) {
            var fr = new FileReader();
            fr.onload = () => {
                
                var file = files[index];
                var filename = files[index].name.split('.').slice(0, -1).join('.');
                var base64 : any;
                base64 = fr.result;
                var fileType = files[index].type;
 
                // Set date according to selected month
                // var currentYear = new Date().getFullYear();
                var selectedCompany = this.props.mainReduxState.selected_company;
                var currentYear = selectedCompany.exercise_year;

                var realMonth = this.props.mainReduxState.active_month + 1;
                var realMonthString = realMonth.toString();
                
                if(realMonthString.length == 1) realMonthString = "0"+realMonthString;
                var dateString = realMonthString+"/01/"+currentYear.toString(); // Arbitrary day 
                var _date = new Date(dateString);
                
                var data = {uri : base64, name : filename, libelle : filename, date : _date, type : this.state.types[this.state.active_index], companyId : this.props.mainReduxState.selected_company.id/* this.props.mainReduxState.user.companyId */};
                
                var formData = new FormData();
                formData.append('files',file);

                if(fileType != "application/pdf") // Not pdf
                {
                    // console.log("not pdf");
                    TesseractServices._uploadFile(formData).then(resp =>{

                      var savedFile = resp.data.files[0];
                      data.uri = savedFile.filename;
        
                      TesseractServices._uploadBill(data).then(resp =>{
         
                          let fileArray = [resp.data];
                          this.props.mainReduxActions.add_files(fileArray);
                          this.onFilesAdded();  
                          
                          this.setState({uploading : false, upload_success_counter : (this.state.upload_success_counter+1)}) 
                          this._checkUploadStatus();
                          
        
                        }).catch((err)=>{
                          
                          this.setState({uploading : false, upload_error_counter : (this.state.upload_error_counter+1)}) 
                          this._checkUploadStatus();
                          console.log(err);
        
                        });
        
                    }).catch((err)=>{
                     
                      this.setState({uploading : false, upload_error_counter : (this.state.upload_error_counter+1)}) 
                      this._checkUploadStatus();
                      console.log(err);
        
                    });

                }
                else // IS PDF
                {
                  // console.log("is pdf");
                  this._render(data);
                }

            
            }


            fr.readAsDataURL(files[index]);

        }

    }

}

_checkUploadStatus(){

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

  if((this.state.upload_success_counter + this.state.upload_error_counter) == this.state.upload_files_count)
  {
      // console.log("UPLOAD DONE")
      if(this.state.upload_success_counter == this.state.upload_files_count)
      {
        // console.log("FULL SUCCESS")
        AppToaster.show({ intent:Intent.SUCCESS, message: "Facture(s) ajoutée(s) avec succés !" });

      }
      else if(this.state.upload_error_counter == this.state.upload_files_count)
      {
          // console.log("FULL ERROR")
          AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'ajout.Réessayez svp !" });

      }
      else
      {
        // console.log("ERREUR SUR CERTAINS FICHIERS")
        AppToaster.show({ intent:Intent.WARNING, message: "Certains fichiers n'ont pas pu etre ajoutés" });

      }
  }

}


_dataURLtoFile = (dataurl, filename)  => {
 
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), 
      n = bstr.length, 
      u8arr = new Uint8Array(n);
      
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  
  return new File([u8arr], filename, {type:mime});
}

_render = (_data) =>
{
    let self = this;
    var url = _data.uri;
    var PDFJS = window['pdfjs-dist/build/pdf'];

    PDFJS.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';

    var loadingTask = PDFJS.getDocument(url);
    
    loadingTask.promise.then(function(pdf) {
    
    var canvasdiv = document.getElementById("canvas_renderer");
    var totalPages = pdf.numPages

    // for (let pageNumber = 1; pageNumber <= totalPages; pageNumber++) {
    let pageNumber = 1;    
    pdf.getPage(pageNumber).then((page) => {

        var scale = 3;
        var viewport = page.getViewport({ scale: scale });

        var canvas = document.createElement('canvas');
        canvas.classList.add("renderer")
        canvasdiv?.appendChild(canvas);

        // Prepare canvas using PDF page dimensions
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = { canvasContext: context, viewport: viewport };

        var renderTask = page.render(renderContext);
        renderTask.promise.then(() =>{
            // console.log(canvas.toDataURL('image/png'));
            var _uri = canvas.toDataURL('image/png');

            var file = self._dataURLtoFile(_uri, _data.name+'.png');
            var formData = new FormData();
            formData.append('files',file);
            

            TesseractServices._uploadFile(formData).then(resp =>{

              var savedFile = resp.data.files[0];
              _data.uri = savedFile.filename;

              console.log(resp);

              TesseractServices._uploadBill(_data).then(resp =>{
 
                  let fileArray = [resp.data];
                  self.props.mainReduxActions.add_files(fileArray);
                  self.onFilesAdded();
                  
                  self.setState({uploading : false, upload_success_counter : (self.state.upload_success_counter+1)})                          
                  self._checkUploadStatus();

                }).catch((err)=>{
                   
                  self.setState({uploading : false, upload_error_counter : (self.state.upload_error_counter+1)}) 
                  self._checkUploadStatus();

                  console.log(err);

                });

            }).catch((err)=>{
            
              self.setState({uploading : false, upload_error_counter : (self.state.upload_error_counter+1)})                    
              self._checkUploadStatus();
              console.log(err);

            });


        });
      });
    // }

    }, function(reason) {
      // PDF loading error
      console.error(reason);
    });
}


 render() {

// ANIMATIONS PROPS

  const submenuItems = this.props.submenu.map((item :any, index:number) =>
  <div  key={index} className={(this.state.active_index == index ? styles.submenuItemActive : styles.submenuItem )}
    onClick={(e:any)=>this.onSubmenuClicked(e,index)}    onContextMenu={()=>this.onContextMenuClicked(index)} onMouseEnter={()=>this.onContextMenuClicked(index)} onMouseLeave={()=>this.onClose()} >
      <span>{item.title}</span>
      <span> &#62; </span>

      {this.state.context_menu_index == index &&
        <div className={styles.contextMenuBox}>
          {/* <button onClick={(e:any)=>this.onOptClicked(e,"ADD", index)}     >Ajouter</button> */}
          <form id="uploadForm" style={{textAlign:'left'}} >
            <input type="file" name={"files"} id={"files"} multiple accept=".png, .jpeg, .pdf" onClick={(e:any)=>this._onUploadClicked(e)} onChange={this._uploadFile} />
            <label style={{display:'flex'}} htmlFor={"files"}  onClick={(e:any)=>this._onUploadClicked(e)}  >Ajouter</label>
          </form>
          {/* <button onClick={(e:any)=>this.onOptClicked(e,"REMOVE", index)}  >Supprimer</button>
          <button onClick={(e:any)=>this.onOptClicked(e,"EXPORT", index)}  >Exporter</button> */}
        </div>
      }
  </div>
  );

  return (
    
    <div className={styles.appContentContainerLeftSubmenu}>
      {submenuItems}

      {this.state.uploading &&
        <div style={{display:'flex', flexDirection:'column', alignItems:'center', fontSize:12, color:"#4caf50"}}>
          <img src={AppIcons.ds_spinner_icon} style={{width:36, height:"auto", marginTop:20}} alt="" />
          <span>Ajout en cours</span>
        </div>
       }
      

    </div>
 

  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmenuPartial);
