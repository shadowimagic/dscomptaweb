import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../../global.module.css';

import { motion } from "framer-motion";

import AppIcons from '../../../icons';


class ActivitiesPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",

    };
  }
    


 render() {

  // ANIMATIONS PROPS
    const activities = this.props.activities.map((item :any, index : number) =>
    <div key={index} className={styles.appActivityItem}>
      <span>Lorem ipsum dolore sit amet </span> 
      <span> <em>Facture Mosseya</em></span> 
      <span>Aujourd'hui 10:40</span>
    </div>
    );

  

  return (
    
    <div className={styles.appContentContainerLeftActivities}>
        <div className={globalStyles.appContainerTitle}>
          Activités récentes
        </div>
        {/* Activities */}
        <div className={styles.appActivitiesContainer}>
          {this.props.activities.length > 0 &&
             activities
          }
          {this.props.activities.length == 0 &&
            <span style={{fontSize:12, color:'#111111', marginTop:'20%'}}>Aucun historique</span>
          }
        </div>

    </div>
 
  );
  
 }
}

export default ActivitiesPartial;
