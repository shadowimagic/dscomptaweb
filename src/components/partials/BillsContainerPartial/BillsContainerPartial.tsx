import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../../global.module.css';

import { motion } from "framer-motion"; 
import AppIcons from '../../../icons';

// Partials
import FileBaseItem from "../FileBaseItem/FileBaseItem";
import Emitter from '../../../services/emitter';
import {reactLocalStorage} from 'reactjs-localstorage';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import UserServices from '../../../services/UserServices';
import InfiniteScroll from 'react-infinite-scroller';
import {Intent, Position, Toaster } from "@blueprintjs/core";
import {Button, Icon,Dimmer, Loader ,Grid, Popup, Table} from 'semantic-ui-react';


const MAX_FILES = 30;

class BillsContainerPartial extends Component<any, any> {
  _isMounted = false;

  constructor(props:any) {
    super(props);
    this._isMounted = false;

    this.state = {
      status : "success",
      preview_right_pos : -110,
      files : [],
      data_loading : true,
      has_more : false,
      can_load_more : false,
      current_page : 1,
      is_updating : false,
      active_month:this.props.mainReduxState.active_month,
      selected_file : {uri : AppIcons.ds_facture_model},
      selected_uri : "",
      mediahost: "https://dscompta-api.herokuapp.com/files/"
    };

    
  }
    
  componentDidMount(){
  
    this._isMounted = true;

    Emitter.on(Emitter.types.UPDATE_COMPUTED_BILLS_TAB, (type)=> this._getBills(type))

  }

  _loadLocal = (type : string) => {

    var files;
    switch (type) {
      case "all":
        if(this.props.tab == "bills")
          files = this.props.mainReduxState.computed_bills_all.files
        else
          files = this.props.mainReduxState.computed_quotes_all.files
        break;

      case "payed":
        if(this.props.tab == "bills")
          files = this.props.mainReduxState.computed_bills_payed.files
        else
          files = this.props.mainReduxState.computed_quotes_payed.files
        break; 

      case "suffering":
        if(this.props.tab == "bills")
          files = this.props.mainReduxState.computed_bills_suffering.files
        else
          files = this.props.mainReduxState.computed_quotes_suffering.files
        break;

      case "unpayed":
        if(this.props.tab == "bills")
          files = this.props.mainReduxState.computed_bills_unpayed.files
        else
          files = this.props.mainReduxState.computed_quotes_unpayed.files
        break;

      case "sent":
        if(this.props.tab == "bills")
          files = this.props.mainReduxState.computed_bills_sent.files
        else
          files = this.props.mainReduxState.computed_quotes_sent.files
        break; 
    }

      if(this._isMounted)
        this.setState({files : files});
  
  };


_getBills(type : string){


    if(this._isMounted)
      this.setState({has_more : true});
    // Load Local 
    this._loadLocal(type);

    var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
    var companyId = loggedCompany.id;
    // var selectedCompany = this.props.mainReduxState.selected_company;
    // var companyId = selectedCompany.id;

    var page = 1;
    if(this.props.tab == "bills")
      var initialBills = this.props.mainReduxState.computed_bills_all;
    else
      var initialBills = this.props.mainReduxState.computed_quotes_all;


    switch (type) {
      case "all":

        if(this.props.tab == "bills")
        {
          page = this.props.mainReduxState.computed_bills_all.page
          initialBills = this.props.mainReduxState.computed_bills_all
        }
        else
        {
          page = this.props.mainReduxState.computed_quotes_all.page
          initialBills = this.props.mainReduxState.computed_quotes_all 
        }
        break;

      case "payed":
        if(this.props.tab == "bills")
        {
          page = this.props.mainReduxState.computed_bills_payed.page
          initialBills = this.props.mainReduxState.computed_bills_payed
        }
        else
        {
          page = this.props.mainReduxState.computed_quotes_payed.page
          initialBills = this.props.mainReduxState.computed_quotes_payed 
        }
        break; 

      case "suffering":
        if(this.props.tab == "bills")
        {
          page = this.props.mainReduxState.computed_bills_suffering.page
          initialBills = this.props.mainReduxState.computed_bills_suffering
        }
        else
        {
          page = this.props.mainReduxState.computed_quotes_suffering.page
          initialBills = this.props.mainReduxState.computed_quotes_suffering 
        }
        break;

      case "unpayed":
        if(this.props.tab == "bills")
        {
          page = this.props.mainReduxState.computed_bills_unpayed.page
          initialBills = this.props.mainReduxState.computed_bills_unpayed
        }
        else
        {
          page = this.props.mainReduxState.computed_quotes_unpayed.page
          initialBills = this.props.mainReduxState.computed_quotes_unpayed 
        }
        break;

      case "sent":
        if(this.props.tab == "bills")
        {
          page = this.props.mainReduxState.computed_bills_sent.page
          initialBills = this.props.mainReduxState.computed_bills_sent
        }
        else
        {
          page = this.props.mainReduxState.computed_quotes_sent.page
          initialBills = this.props.mainReduxState.computed_quotes_sent 
        }
        break; 
    }

    // console.log(type);


    UserServices._getComputedBills(companyId, this.props.tab ,type, page).then((resp)=> {
      
      var newcomputedBills = {...initialBills};
      var newFiles = resp.data;
      var newPage = newcomputedBills.page;
      
      if(resp.data.length > 0) newPage = newPage + 1;
      
      // Set loader status
      if(newFiles.length < MAX_FILES)
      {
          if(this._isMounted)
            this.setState({has_more : false});
      }
      if(newFiles.length == MAX_FILES)
      {
          if(this._isMounted)
            this.setState({can_load_more : true});
      }
       
      
      newcomputedBills = {files : [...newcomputedBills.files, ...newFiles] , page : newPage };
 
      var data = {data:newcomputedBills, status : type};
           
      // console.log(data);

      if(this.props.tab == 'bills')
        this.props.mainReduxActions.update_computed_bills(data);
      else
        this.props.mainReduxActions.update_computed_quotes(data);

      
        if(this._isMounted)
          this.setState({files : newcomputedBills.files, has_more:false});
    
    }).catch((err)=> {

        if(this._isMounted)
          this.setState({has_more : false});

    });

  }

  _loadMore(){
    if(this.state.can_load_more)
    {
      // console.log("More...")
      this._getBills(this.props.type);
    }
  }


  _updateBillState = (id : string, status : string) => {

    if(status == 'delete') // Delete
    {
      this._deleteBill(id);
    }
    else // Update state
    {
      this._changeBillStatus(id, status);
    }
     
  }


  _changeBillStatus = (id : string, status: string) => {

    var data = {"type" : this.props.tab == "bills" ? "bill" : "quote" , "status" : status};
      if(this._isMounted)
        this.setState({is_updating : true});

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    UserServices._updateComputedBill(id, data).then((resp)=> {
      
      // Update current type local state

      if(this.props.tab == "bills")
        var initialBills = this.props.mainReduxState.computed_bills_all;
      else
        var initialBills = this.props.mainReduxState.computed_quotes_all;

      initialBills.page = 1;
      initialBills.files = [];
      
      var types = ["all", "payed", "suffering", "unpayed", "sent"];

      // Update All Pans
      types.forEach(t => {
        
        var data = {data:initialBills, status : t};
        // console.log(data);
        if(this.props.tab == "bills")
          this.props.mainReduxActions.update_computed_bills(data);
        else
          this.props.mainReduxActions.update_computed_quotes(data);
        
      });
       

      // Update this tab data
      this._getBills(this.props.type);

      AppToaster.show({ intent:Intent.SUCCESS, message: this.props.tab == "bills" ? "Facture modifiée !" : "Devis modifié !" });
    
        if(this._isMounted)
          this.setState({is_updating : false});

    }).catch((err)=> {

      AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors du traitement !" });

        if(this._isMounted)
          this.setState({is_updating : false});

    });

  }



  _deleteBill = (id : string) => {

      if(this._isMounted)
        this.setState({is_updating : true});

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    UserServices._deleteComputedBill(id).then((resp)=> {
      
      // Update current type local state


      if(this.props.tab == "bills")
        var initialBills = this.props.mainReduxState.computed_bills_all;
      else
        var initialBills = this.props.mainReduxState.computed_quotes_all;

      initialBills.page = 1;
      initialBills.files = [];
      
      var types = ["all", "payed", "suffering", "unpayed", "sent"];

      // Update All Pans
      types.forEach(t => {
        
        var data = {data:initialBills, status : t};
        // console.log(data);
        if(this.props.tab == "bills")
          this.props.mainReduxActions.update_computed_bills(data);
        else
          this.props.mainReduxActions.update_computed_quotes(data);
        
      });
       

      // Update this tab data
      this._getBills(this.props.type);

      AppToaster.show({ intent:Intent.SUCCESS, message: this.props.tab == "bills" ? "Facture supprimée !" : "Devis supprimé !" });
    
        if(this._isMounted)
          this.setState({is_updating : false});

    }).catch((err)=> {

      AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors du traitement !" });

        if(this._isMounted)
          this.setState({is_updating : false});

    });

  }



  onBillClicked(value:any) {
     
    this.props.onBillClicked(value);
  }
 
  formatDate(item:any)
  {
    var real_date = new Date(item.date);
    var real_date_string = real_date.toLocaleDateString();
    return real_date_string.replaceAll('/', '-');
    
  }

  formatPrice(price : string)
  {
    if(!price) return "0.000";
    return price.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
  }


  _getStatus = (type : string) => {
 
    switch (type) {
      case "all":
        return this.props.tab == "bills" ? "..." : "...";

      case "payed":
        return this.props.tab == "bills" ? "payée" : "..."; 

      case "suffering":
        return this.props.tab == "bills" ? "En souffrance" : "...";

      case "unpayed":
        return this.props.tab == "bills" ? "impayée" : "..." ;

      case "sent":
        return this.props.tab == "bills" ? "envoyée" : "...";
    }

  }


 render() {

  // ANIMATIONS PROPS
  
    const filesItems = this.state.files.map((item :any, index : number) =>

        <Table.Row onClick={() =>this.onBillClicked(item)} style={{cursor:"pointer"}} >
          <Table.Cell>{item.to_name}</Table.Cell>
          <Table.Cell>{item.name}</Table.Cell>
          <Table.Cell>{item.no_ref}</Table.Cell>
          <Table.Cell>{this.formatDate(item)}</Table.Cell>
          <Table.Cell>{this._getStatus(item.status)}</Table.Cell>
          <Table.Cell style={{fontWeight:'bolder', fontSize:16}}>{this.formatPrice(item.total)}</Table.Cell>
          <Table.Cell> 
             <Popup trigger={<Button onClick={(e)=>e.stopPropagation()} style={{backgroundColor : '#ffffff',border : 'solid 1px #AB3134', color : '#AB3134'}} icon='ellipsis horizontal' />} on='click' >
                <Button.Group basic size='small'>
                  
                  {item.status != "payed" && 
                    <Popup content='Marquée comme payée' inverted trigger={<Button color='green' icon='check square' onClick={() => this._updateBillState(item.id, 'payed' ) } />} />
                  }
                  {item.status != "unpayed" && 
                    <Popup content='Marquée comme impayée' inverted trigger={<Button color='yellow' icon='exclamation triangle' onClick={() => this._updateBillState(item.id, 'unpayed' ) } />} />
                  }


                  {/* TRASH */}
                  <Popup content='Supprimer' inverted trigger={<Button color='red' icon='trash' onClick={() => this._updateBillState(item.id, 'delete' ) } />} />
                </Button.Group>
               
             </Popup>
          </Table.Cell>
        </Table.Row>
    );


  return (

    <div className={styles.appInnerMiddleContainer}>

      <Dimmer active={this.state.is_updating}>
        <Loader content='Mis à jour...' />
      </Dimmer>

      <div className={styles.appInnerMiddleFilesContainer}>
        
          <div style={{display:'flex', width: '100%',flexDirection:'column', alignItems:'center', height:'80%', overflowY:'scroll', paddingBottom:0}}>
            <InfiniteScroll
                style={{width:'100%'}}
                pageStart={0}
                loadMore={this._loadMore()}
                hasMore={this.state.has_more}
                loader={<img src={AppIcons.ds_spinner_icon} style={{width:46, height:"auto", marginTop:20}} alt="" />}
                useWindow={false}
                >
                  <Table singleLine>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Client</Table.HeaderCell>
                        <Table.HeaderCell>Libellé</Table.HeaderCell>
                        <Table.HeaderCell>Numéro</Table.HeaderCell>
                        <Table.HeaderCell>Date</Table.HeaderCell>
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell>Montant</Table.HeaderCell>
                        <Table.HeaderCell> <Icon name='th list' /> </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {filesItems}
                    </Table.Body>
                  </Table>
           
                </InfiniteScroll>
                {!this.state.has_more && this.state.files.length == 0 &&
                  <span style={{marginTop:'16%', marginBottom:'16%'}}>{ this.props.tab == "bills" ? "Aucune facture à afficher" : "Aucun devis à afficher"}</span>
                }
              </div>


            
      </div>

    </div>
  
 
  );
  
 }
}
 

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BillsContainerPartial);
