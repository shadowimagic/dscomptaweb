import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../../global.module.css';

import { motion } from "framer-motion";

import AppIcons from '../../../icons';
import Emitter from '../../../services/emitter';


class DetailsPanelPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      display:false,
      displayed_file:[],

    };
  }
    

  componentDidMount(){

    Emitter.on(Emitter.types.FILE_TOGGLED, (file) => this.onFileToggled(file));

  }

  onFileToggled(file:any)
  {
    var display = file.id ? true : false;
    this.setState({display:display, displayed_file : file})
  }
  
  formatDate(item:any)
  {
    var real_date = new Date(item.date);
    var real_date_string = real_date.toLocaleDateString();
    if(item.status == "success")
      return real_date_string.replaceAll('/', '-');
    else
      return "-"
  }

 render() {

  // ANIMATIONS PROPS
  

  return (

      <div className={styles.appContentContainerRight} style={{display : this.state.display ? "flex" : "none"}}>
        <div className={globalStyles.appContainerTitle}>
          Details
        </div>

        <div className={styles.appContainerRightFields}>
          {/* Field */}
          <div className={styles.appContainerRightField}>
            <span>Ref</span>
            <input className={globalStyles.appInputBase} disabled value={"#"+this.state.displayed_file.id} type="text"/>
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            <span>Date</span>
            <input className={globalStyles.appInputBase} disabled value={this.formatDate(this.state.displayed_file)} type="text"/>
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            <span>N° de Piéce</span>
            <input className={globalStyles.appInputBase} disabled value={this.state.displayed_file.no_piece} type="text"/>
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            <span>Libellé</span>
            <input className={globalStyles.appInputBase} disabled value={this.state.displayed_file.libelle} type="text"/>
          </div>

          {/* TTC , HT, TVA */}
          <div style={{display:"flex", width:'94%', alignSelf: 'center'}}>
            
            {/* Field */}
            <div className={styles.appContainerRightField} style={{flex : 2}}>
              <span>TTC</span>
              <input className={globalStyles.appInputBase} style={{fontSize:12}} disabled value={this.state.displayed_file.status == "success" ? this.state.displayed_file.ttc : "-"} type="text"/>
            </div>
            
            {/* Field */}
            <div className={styles.appContainerRightField} style={{flex : 2}}>
              <span>HT</span>
              <input className={globalStyles.appInputBase} style={{fontSize:12}} disabled value={this.state.displayed_file.status == "success" ? this.state.displayed_file.ht : "-"}  type="text"/>
            </div>

            {/* Field */}
            <div className={styles.appContainerRightField} style={{flex : 1}}>
              <span>TVA</span>
              <input className={globalStyles.appInputBase} style={{fontSize:12}} disabled value={this.state.displayed_file.status == "success" ? this.state.displayed_file.tva+"%" : "-"}  type="text"/>
            </div>
          </div>


        </div>

      </div>
  
 
  );
  
 }
}

export default DetailsPanelPartial;
