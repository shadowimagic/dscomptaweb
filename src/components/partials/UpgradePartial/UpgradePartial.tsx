import React, { Component } from 'react';
import styles from './style.module.css';

import AppIcons from '../../../icons';
import Emitter from '../../../services/emitter';
import { Form, Segment, Modal,Button, Card, Label,List, Image, Item, Icon } from 'semantic-ui-react'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import packs from '../../../constants/packs';
import { loadStripe } from '@stripe/stripe-js';
 
const packsInitial = packs; 

const stripePromise = loadStripe('pk_test_51Iw7QaHjs6zu68wd4D6DfA1VLzjSwqV6SYPiecrqszgxtAEG9DorNldPe8F6VaSOqXon0KTlQPcjhrFC0jtgZbY6001hzIudEK');

class UpgradePartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      packs : []
    };
     
  }

  
  componentDidMount()
  {


        // Packs
        var _packs : any[] = [];
        for (let i = 1; i < packsInitial.length; i++) {
          const el = packsInitial[i];
          _packs.push(el);
        }
    
        this.setState({packs : _packs});
    
     
  }
  
 
  onClose()
  {
    var visibility = "none";
    this.setState({clicked_toolbar_item :visibility, clicked_toolbar_item_option : visibility});
  } 

  
 
  handleUpgradeClick = async (event,packId, priceId) => {

    console.log('CLICKED');

    const stripe = await stripePromise;
 
    let _data = {
      id:"1234", 
      companyId:"" ,
      currency:"xaf",
      packName:"Pack Standard",
      packId:packId,// Useful prop
      packPriceId:priceId // Useful prop
    }
    
    const response = await fetch('http://dscompta-api.herokuapp.com/upgrade', {
      method: "POST",
      body: JSON.stringify(_data),
      headers: {"Content-type": "application/json"}
    })
 
    const session = await response.json().catch((err)=>{})

    // console.log(session);
    // When the customer clicks on the button, redirect them to Checkout.
    //@ts-ignore
    const result = await stripe.redirectToCheckout({
      sessionId: session.id,
    });

    if (result.error) {
      // If `redirectToCheckout` fails due to a browser or network
      // error, display the localized error message to your customer
      // using `result.error.message`.
      console.log("ERROR");
    }
    else{

      // console.log(session);
    }

  };



  render() {

    
  const packsList = this.state.packs.map((item :any, index : number) =>

  <Card style={{flex:1, transform :'scale(0.95)'}}>
    {item.name == "Standard" &&
      <Image src={AppIcons.ds_pack_1_icon} wrapped ui={false} />
    }
    {item.name == "Premium" &&
      <Image src={AppIcons.ds_pack_2_icon} wrapped ui={false} />
    }
    {item.name == "Entreprise" &&
      <Image src={AppIcons.ds_pack_3_icon} wrapped ui={false} />
    }
    <Card.Content>
      <Card.Header>{item.name}</Card.Header>
      <Card.Meta>
        <span style={{fontWeight:'bolder', fontSize:18, color:"#ab3134"}}>{item.price ? item.price + " cfa/mois" : "Devis" }</span>
      </Card.Meta>
      <Card.Description>
          <List>
            
            <List.Item>
              <List.Icon name="universal access" />
              <List.Content>Nombre d'accés : {item.access ? item.access : "A déterminer"} </List.Content>
            </List.Item>                
            <List.Item>
              <List.Icon name={item.ds_scan ? 'check' : "close"} />
              <List.Content>Dok Scan</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_bank ? 'check' : "close"} />
              <List.Content>Dok Banque</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_archi ? 'check' : "close"} />
              <List.Content>Dok Archi</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_ai ? 'check' : "close"} />
              <List.Content>Dok Intelligence</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_fact ? 'check' : "cog"} />
              <List.Content>Dok Fact {!item.ds_fact.default ? item.ds_fact.price : ""} </List.Content>
            </List.Item>

          </List>
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
    {item.name != "Entreprise" &&
      <Button style={{backgroundColor:'#ab3134', color:"#ffffff", width: "82%"}} onClick={(e) => this.handleUpgradeClick(e,item.index,item.priceId)}>
          ACHETER
      </Button>
      }
      {item.name == "Entreprise" &&
      <Button style={{backgroundColor:'#ffffff', color:"#ab3134", border:'solid 1px #ab3134', width: "82%"}} onClick={() => { window.open("https://www.dokcompta.com/contact-1/", "_blank") }}>
        NOUS CONTACTER
      </Button>
      
      }
    </Card.Content>
  </Card>
  );
  
  return (
    
  <div style={{display:'flex', flexDirection:'row', alignItems:'baseline'}}>
     
     {packsList}

  </div>
 
  );
  
 }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(UpgradePartial);
