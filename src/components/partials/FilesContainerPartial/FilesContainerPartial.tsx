import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../../global.module.css';

import { motion } from "framer-motion";

import AppIcons from '../../../icons';

// Partials
import FileBaseItem from "../../partials/FileBaseItem/FileBaseItem";
import Emitter from '../../../services/emitter';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import InfiniteScroll from 'react-infinite-scroller';


class FilesContainerPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      preview_right_pos : -110,
      files : [],
      fromMonthChanged : false,
      active_month:this.props.mainReduxState.active_month,
      selected_file : {uri : AppIcons.ds_facture_model},
      selected_uri : "",
      mediahost: "https://dscompta-api.herokuapp.com/files/",

      //nl
      items : []
    };

    this.onFileBaseClicked = this.onFileBaseClicked.bind(this);

  }
    
  componentDidMount(){

    // this.setState({files : this.props.files});

    Emitter.on(Emitter.types.FROM_MONTH_CHANGED, (status) => this.onFromMonthChanged(status));
    // Emitter.on(Emitter.types.FILES_TYPE_CHANGED, (newType) => this.onTypeChanged(newType));
    Emitter.on(Emitter.types.FILE_TOGGLED, (file) => this.onFileToggled(file));

    // setTimeout(() => {
    //   this.onMonthChanged(this.state.active_month)
    // }, 150);

    // setTimeout(() => {
    //   this.onTypeChanged("customer")
    // }, 350);
    
 
  }
  //nl
  _loadMore() {
   
    if(!this.state.fromMonthChanged)
      this.props._loadMore(false, true);
  }
  

  _getItems() {
    let items = []
    for(var i = 0; i < 10; i++) {
      //@ts-ignore
      items.push({name: 'An item_'+i })
    }
    return items
  }

  onFromMonthChanged(status:boolean)
  {
    this.setState({fromMonthChanged : status});
  }
   
  // onTypeChanged(type:string)
  // {
  //   var files = this.props.files;
  //   // var filterFiles = files.filter((e)=>e.type == type && new Date(e.date).getFullYear() == new Date().getFullYear());
  //   var filteredFiles = files.filter((e)=>e.type == type && this.state.active_month == new Date(e.date).getMonth()&& new Date(e.date).getFullYear() == new Date().getFullYear());
     
  //   this.setState({files : filteredFiles});
    
  //   setTimeout(() => {
  //     Emitter.emit(Emitter.types.FILES_COUNT,this.state.files.length);
  //   }, 150);

  // }
  
  onFileToggled(file:any)
  {
    this.setState({selected_uri : file.uri});
  }

  onFileBaseClicked(value:any) {
    
    localStorage.setItem('selected_file', JSON.stringify(this.props.files[value]));
    
    this.props.onOneFileClicked(value);
    this.togglePreview();
    
  }
  togglePreview()
  {
    var _file = localStorage.getItem('selected_file') || "{}";
    var file = JSON.parse(_file);
    // console.log(file)
    this.setState({selected_file : file})

    var preview_pos = this.state.preview_right_pos == -110 ? 0 : -110;
    this.setState({preview_right_pos : preview_pos/* , details_visibility : details_visibility */});
    
    var display = this.state.preview_right_pos == -110 ? true : false;
    
    setTimeout(() => {
      if(!display || preview_pos == -110 )
      Emitter.emit(Emitter.types.FILE_TOGGLED, {});
    }, 50);


   
  }


 render() {

  // ANIMATIONS PROPS
  
  const filesItems = this.props.files.map((item :any, index : number) =>
  <FileBaseItem edit={this.props.edit} remove={this.props.remove} item={item} title={item.name} status={item.status} index={index} onFileBaseClicked={this.onFileBaseClicked} key={index} />
);


  return (

    <div className={styles.appInnerMiddleContainer}>
      <div className={styles.appInnerMiddleFilesContainer}>
          {/* {filesItems} */}
            <InfiniteScroll
                style={{width:'100%'}}
                pageStart={0}
                loadMore={()=>this._loadMore()}
                hasMore={this.props.has_more}
                loader={<img src={AppIcons.ds_spinner_icon} style={{width:46, height:"auto", marginTop:20}} alt="" />}
                useWindow={false}
                >
                {filesItems}
                {/* {renderedItems} */}
            </InfiniteScroll>
          {/* LOADING .... */}
          {!this.props.has_more && this.props.files.length == 0 &&
            <span style={{marginTop:'20%'}}>Aucune facture à afficher</span>
          }
      </div>

          <motion.div animate={{right : this.state.preview_right_pos+'%'}} 
                      transition={{ ease: "easeOut"}}
              className={styles.appInnerMiddlePreviewContainer}>
              <div style={{position:"absolute", left:-50, top:0}}>
                <img src={AppIcons.ds_preview_folder_banner} style={{width:56, height:"auto"}} alt="" />
                <img  src={AppIcons.ds_preview_folder_icon} 
                style={{  position: "absolute",
                          width: 34,
                          left: "20%",
                          top: "18%",
                          height: "auto", cursor:"pointer"}}
                          onClick={()=>{this.togglePreview()}}
                          alt="" />
              </div>
              <div id="preview_file_container" className={styles.previewFileContainer}>
                
                  <img src={this.state.mediahost+this.state.selected_uri} style={{width:"100%", height:"auto"}} alt=""/>
                
              </div>
          </motion.div>




         
      </div>
  
 
  );
  
 }
}
 

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilesContainerPartial);
