import React, { Component } from 'react';
import styles from './style.module.css';
import { Icon, Intent } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";
import { Position, Toaster } from "@blueprintjs/core";

import AppIcons from '../../../icons';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import TesseractServices from '../../../services/TesseractServices';
import Emitter from '../../../services/emitter';




class FileBaseItem extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success"
    };
    this.onFileBaseClicked = this.onFileBaseClicked.bind(this);
  }

  
  onFileBaseClicked() {
    this.props.onFileBaseClicked(this.props.index)
    Emitter.emit(Emitter.types.FILE_TOGGLED, this.props.item);
  }

  formatDate(item:any)
  {
    var real_date = new Date(item.date);
    var real_date_string = real_date.toLocaleDateString();
    if(item.status == "success")
      return real_date_string.replaceAll('/', '-');
    else
      return "-";
    // return real_date_string.replaceAll('/', '-');

  }

  onFilesUpdated = ()=>
  {
    Emitter.emit(Emitter.types.FILES_UPDATED, "removed");
  }

  onRemoveFile = (event : Event ,id : string, type : string)=>{

    event.preventDefault();
    event.stopPropagation();

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    var data = {id : id, type : type};
    this.props.mainReduxActions.remove_bill(data);
    this.onFilesUpdated();
    
    TesseractServices._removeBill(id).then((data)=>{
       
      AppToaster.show({ intent:Intent.SUCCESS, message: "Facture supprimée avec succés !" });

    }).catch((err)=>{
      AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de la suppression !" });
    });
    
  }

 

 render() {

// ANIMATIONS PROPS


  return (
    
      <div className={styles.filebaseContainer} onClick={this.onFileBaseClicked} >
          <div className={styles.iconContainer}>
            <img src={AppIcons.ds_fileitem_icon} alt="" />
          </div>
          <div className={styles.detailsContainer}>
            <span style={{marginTop:-3}} >{this.props.title}</span>
            <span style={{marginTop:-6}} >Date : {this.formatDate(this.props.item)}</span>
          </div>
          <div className={styles.statsContainer}>
              <span>#{this.props.item.id}</span>
              {this.props.status == "success" && 
                <img src={AppIcons.ds_stat_success_icon} alt="" />
              }
              {this.props.status == "progress" && 
                <img src={AppIcons.ds_stat_pending_icon} alt="" />
              }
              {this.props.status == "error" && 
                <img src={AppIcons.ds_stat_failed_icon} alt="" />
              }
          </div>
          {this.props.remove && 
            <div style={{marginRight:5, marginTop:-4}}>
              <Icon icon="trash" intent={Intent.DANGER}  onClick={(e:any) => this.onRemoveFile(e,this.props.item.id ,this.props.item.type)} />
            </div>
          }
          {this.props.edit && 
            <div style={{marginRight:5, marginTop:-4}}>
              <Icon icon="edit" intent={Intent.SUCCESS} />
            </div>
          }
      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FileBaseItem);
