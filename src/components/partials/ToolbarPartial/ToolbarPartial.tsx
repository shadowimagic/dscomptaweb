import React, { Component } from 'react';
import styles from './style.module.css';

import AppIcons from '../../../icons';
import Emitter from '../../../services/emitter';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


class ToolbarPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      base_menu : [
        {
          title : "Traiter",
          route : "route"
        },
        {
          title : "Visualiser",
          route : "route"
        },
        {
          title : "Editer",
          route : "route"
        },
        {
          title : "Supprimer",
          route : "route"
        },
        {
          title : "Générer",
          route : "route"
        }
      ],
      files_count : 0,
      menu : [],
      clicked_toolbar_item : "none",
      clicked_toolbar_item_option : "none",
      item_focused_color : "#AB3134",
      item_unfocused_color: "#14142B",

    };
    this.onFileBaseClicked = this.onFileBaseClicked.bind(this);
    this.onToolbarMenuItemClicked = this.onToolbarMenuItemClicked.bind(this);
  }

  
  componentDidMount()
  {

    var context_menu : any[] = [];
    this.props.menu.forEach((el : any) => {
      
      var title = el.title;
      var item = this.state.base_menu.find((e : any) => e.title == title);
      if(item != null)
       context_menu.push(el);

    });

    this.setState({menu : context_menu});
    // console.log(context_menu);

    Emitter.on(Emitter.types.CLOSE_POPOVER, (src) => this.onClose());
    Emitter.on(Emitter.types.FILES_COUNT, (count) => this.onCount(count));

  }
  
 
  onClose()
  {
    var visibility = "none";
    this.setState({clicked_toolbar_item :visibility, clicked_toolbar_item_option : visibility});
  } 

  onCount(count :number)
  {
    this.setState({files_count : count});
  } 

  onFileBaseClicked() {
    this.props.onFileBaseClicked(this.props.index)
  }

  onToolbarMenuItemClicked(e : Event, name : string) {

    e.stopPropagation();

    var cti_value = name == this.state.clicked_toolbar_item ? "none" : name;

    var ctiopt_value = this.state.clicked_toolbar_item_option == "Générer" ? "none" : name
    
    this.setState({clicked_toolbar_item : cti_value, clicked_toolbar_item_option : ctiopt_value})
    this.props.onToolbarMenuItemClicked(name, "none")

  }

  onOptClicked(e : Event,type : string)
  {
    e.stopPropagation();
    this.props.onToolbarMenuItemClicked("Générer", type);
    this.setState({clicked_toolbar_item_option : "none"})
  } 

  getStatusCount(status : string)
  {
     return this.props.files.filter(e=>e.status==status).length;
  } 


  render() {

  // ANIMATIONS PROPS
  const maintoolbarItems = this.state.menu.map((item :any, index : number) =>
    <div key={index} style={{position:"relative"}}>
      <button className={(this.state.clicked_toolbar_item == item.title ? styles.toolbarItemBtnFocused : styles.toolbarItemBtn)} onClick={(e:any) => this.onToolbarMenuItemClicked(e,item.title)} >{item.title}</button>
      {item.title == "Générer" && this.state.clicked_toolbar_item_option == "Générer" &&

      <div className={styles.contextMenuBox}>
        <button onClick={(e:any)=>this.onOptClicked(e,"XLS")}    >vers Excel</button>
        <button onClick={(e:any)=>this.onOptClicked(e,"SAGE")}   >vers SAGE</button>
        <button onClick={(e:any)=>this.onOptClicked(e,"OTHERS")} >vers Autres</button>
      </div>
      }
    </div>
  );

  return (
    
  <div className={styles.appContentContainerCenterToolbar}>
                
    {/* ---------------------------- */}
      <div className={styles.appInnerToolbarNavigation}>
          <span>{this.props.title}</span>
          {this.props.context_title != "" &&
          <span> &#62; </span>
          }
          <span> {this.props.context_title} </span>
      </div>
    {/* ---------------------------- */}
    {this.props.display_options &&
      <div className={styles.appInnerToolbarTools}>
        {maintoolbarItems}
      </div>
    }

    {/* ---------------------------- */}

    
    <div className={styles.appInnerToolbarStats}>
      
     {this.props.display_stats &&
      <div className={styles.appInnerToolbarStatsBadges}>
        
        {/* <div className={styles.appProcessStatBadgeItem} style={{backgroundColor : '#C3E6CD'}}>
          <img src={AppIcons.ds_stat_success_icon} alt="" />
          <span>Succés : <span>{this.getStatusCount("success")}</span></span>
        </div>

        <div className={styles.appProcessStatBadgeItem} style={{backgroundColor : '#F5D9A8'}}>
          <img src={AppIcons.ds_stat_pending_icon} alt="" />
          <span>En cours : <span>{this.getStatusCount("progress")}</span></span>
        </div>

        <div className={styles.appProcessStatBadgeItem} style={{backgroundColor : '#FDD4CD'}}>
          <img src={AppIcons.ds_stat_failed_icon} alt="" />
          <span>Echec : <span>{this.getStatusCount("error")}</span></span>
        </div> */}

      </div>
      }

      
      {this.props.display_count &&
        <div className={styles.appInnerToolbarStatsFilescount}>
        {this.props.files.length} Fichiers
      </div>
      }

      </div>
   

  </div>
 
  );
  
 }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToolbarPartial);
