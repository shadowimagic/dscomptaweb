import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../../global.module.css';

import { motion } from "framer-motion";
import { Icon, Intent, Label, Position, Toaster } from "@blueprintjs/core";
import AppIcons from '../../../icons';
import { Modal, Form, Input, Button,Select, Loader, Dimmer, Dropdown, Divider } from 'semantic-ui-react';

// Partials
import Emitter from '../../../services/emitter';
import {reactLocalStorage} from 'reactjs-localstorage';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import UserServices from '../../../services/UserServices';
import countries from '../../SignupScreen/countries';

 
const modes = [
  { key: 'ESP : ESPECE' , text : 'ESP : ESPECE', value : 'ESP : ESPECE' },
  { key: 'CB	Carte bancaire' , text : 'CB	Carte bancaire', value : 'CB	Carte bancaire' },
  { key: 'CHEQ : Par Chèque' , text : 'CHEQ : Par Chèque', value : 'CHEQ : Par Chèque' },
  { key: 'LCR : Lettre de change' , text : 'LCR : Lettre de change', value : 'LCR : Lettre de change' },
  { key: 'LCRA : Lettre de change acc', text : 'LCRA : Lettre de change acc' , value : 'LCRA : Lettre de change acc' },
  { key: 'PAY : Paypal' , text : 'PAY : Paypal' , value : 'PAY : Paypal' },
  { key: 'PRE : Prélèvement' , text : 'PRE : Prélèvement', value : 'PRE : Prélèvement' },
  { key: 'VIR : Par Virement', text : 'VIR : Par Virement' , value : 'VIR : Par Virement' },
]

const delais = [
  { key:"15J : 15 jours date de facture"       , text:"15J : 15 jours date de facture"       , value:"15J : 15 jours date de facture"       },
  { key:"15JFDM : 15 jours fin de mois"        , text:"15JFDM : 15 jours fin de mois"        , value:"15JFDM : 15 jours fin de mois"        },
  { key:"30J : 30 jours date de facture"       , text:"30J : 30 jours date de facture"       , value:"30J : 30 jours date de facture"       },
  { key:"30JFDM : 30 jours fin de mois"        , text:"30JFDM : 30 jours fin de mois"        , value:"30JFDM : 30 jours fin de mois"        },
  { key:"45J : 45 jours date de facture"       , text:"45J : 45 jours date de facture"       , value:"45J : 45 jours date de facture"       },
  { key:"45JFDM : 45 jours fin de mois"        , text:"45JFDM : 45 jours fin de mois"        , value:"45JFDM : 45 jours fin de mois"        },
  { key:"60J : 60 jours date de facture"       , text:"60J : 60 jours date de facture"       , value:"60J : 60 jours date de facture"       },
  { key:"60JFDM : 60 jours fin de mois"        , text:"60JFDM : 60 jours fin de mois"        , value:"60JFDM : 60 jours fin de mois"        },
  { key:"90J : 90 jours date de facture"       , text:"90J : 90 jours date de facture"       , value:"90J : 90 jours date de facture"       },
  { key:"90JFDM : 90 jours fin de mois"        , text:"90JFDM : 90 jours fin de mois"        , value:"90JFDM : 90 jours fin de mois"        },
  { key:"Comptant à la commande"               , text:"Comptant à la commande"               , value:"Comptant à la commande"}, 
  { key:"A réception de facture"               , text:"A réception de facture"               , value:"A réception de facture"}
]

var signaturePad;

class BillsCreatorPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      creating_bill:false,
      preview_right_pos : -110,
      showCustomerCreationModal: false,
      showCustomerEditionModal: false,
      files : [{},{},{}],
      active_month:this.props.mainReduxState.active_month,
      selected_file : {uri : AppIcons.ds_facture_model},
      selected_uri : "",
      mediahost: "https://dscompta-api.herokuapp.com/files/",

      bill_from:"",
      bill_from_address:"",
      bill_from_tel:null,
      bill_from_mail:"",
      bill_from_city:"",
      bill_from_zipcode:null,
      company_desc : '',

      bill_to:"",
      bill_to_address:"",
      bill_to_tel:null,
      bill_to_mail:"",
      bill_to_city:"",
      bill_to_zipcode:null,

      bill_address : "",
      bill_no : 1,
      bill_date : new Date((this.props.mainReduxState.active_month+1)+"/01/"+this.props.mainReduxState.exercise_year).toISOString().split('T')[0],
      bill_command_no : null,
      bill_deadline : null,
      bill_conditions : "Paiement à 15 jours dès réception de la facture.",
      bill_legal : "",

      bill_items : [{qty:1, name:"", unit_price:0, total_ht:0, taxe:0}],
      bill_logo_ref : AppIcons.ds_bill_logo_selector,
      bill_logo : '',
      bill_signing : AppIcons.ds_signing_logo,
      bill_total_ht : 0,
      bill_total : 0,


      create_data : {},

      company_customers : [],
      company_customers_list : [],
      selected_company_customer : {id:"", name:"", data:{cc_bill_mode:"", cc_bill_delay:""}, companyId:""},



      // CUSTOMER CARD STATE : -----------------

      cc_customer_error_designation : false,
      cc_customer_error_address : false,
      cc_customer_error_tel : false,
      cc_customer_error_mail : false,
      cc_customer_error_city : false,
      cc_customer_error_postal_code : false,

      cc_customer_designation_loading : false,
      cc_customer_designation_icon : '',

      customer_card_creating : false,
      customers_list_loading : false,


      cc_customer_designation : "",
      cc_customer_address : "",
      cc_customer_tel : "",
      cc_customer_mail : "",
      cc_customer_country : "GB",
      cc_customer_city : "",
      cc_customer_postal_code : null,
      
      cc_contact_name : "",
      cc_contact_surname : "",
      cc_contact_address : "",
      cc_contact_tel : "",
      cc_contact_mail : "",

      cc_bill_address : "",
      cc_bill_mode : "",
      cc_bill_delay : "",

      // CUSTOMER CARD EDIT STATE : -----------------

      cc_edit_customer_designation : "",
      cc_edit_customer_address : "",
      cc_edit_customer_tel : "",
      cc_edit_customer_mail : "",
      cc_edit_customer_country : "",
      cc_edit_customer_city : "",
      cc_edit_customer_postal_code : null,
      
      cc_edit_contact_name : "",
      cc_edit_contact_surname : "",
      cc_edit_contact_address : "",
      cc_edit_contact_tel : "",
      cc_edit_contact_mail : "",

      cc_edit_bill_address : "",
      cc_edit_bill_mode : "",
      cc_edit_bill_delay : "",

      // ---------------------------------------

      showSignatureModal : false,

    };

  }
    
  componentDidMount(){
 
    // Emitter.once(Emitter.types.MONTH_CHANGED, (newMonth) => this.onMonthChanged(newMonth));
    // Emitter.once(Emitter.types.FILES_TYPE_CHANGED, (newType) => this.onTypeChanged(newType));
    Emitter.once(Emitter.types.CREATE_BILL, () => this.onCreateBill());

    var company = reactLocalStorage.getObject('ls_loggedCompany');
    this.setState({
      bill_from : company.name,
    
      bill_signing : company.config.signature || AppIcons.ds_signing_logo,
      cc_bill_mode : modes[0].key,
      cc_bill_delay : delais[0].key
    });

    // Get Company customers
    this.getCompanyCcs(company.id);
    
    // Update current company config

    if(!company.config)
          company.config = {
            address:'',
            tel:'',
            mail:'',
            city:'',
            postalcode:1000,
            desc:'',
            bill_conditions:'',
            bill_legislation:'',
            companyDesc:'',
          }
 
        this.setState({

          bill_from:company.name,
          bill_from_address:company.config.address,
          bill_from_tel:company.config.tel,
          bill_from_mail:company.config.mail,
          bill_from_city:company.config.city,
          bill_from_zipcode:company.config.postalcode,
          bill_conditions : company.config.bill_conditions,
          bill_legal : company.config.bill_legislation,
          company_desc : company.config.desc

        })
         

  }
  
  onMonthChanged(month:number)
  {
    var files = this.props.files;
    // console.log(month)
    var filterFiles = files.filter((e)=>month == new Date(e.date).getMonth() && new Date(e.date).getFullYear() == new Date().getFullYear());
    this.setState({files : filterFiles, active_month : month});
    
    setTimeout(() => {
      Emitter.emit(Emitter.types.FILES_COUNT,this.state.files.length);
    }, 150);
  }
   
  onTypeChanged(type:string)
  {
    var files = this.props.files;
    // var filterFiles = files.filter((e)=>e.type == type && new Date(e.date).getFullYear() == new Date().getFullYear());
    var filteredFiles = files.filter((e)=>e.type == type && this.state.active_month == new Date(e.date).getMonth()&& new Date(e.date).getFullYear() == new Date().getFullYear());
     
    this.setState({files : filteredFiles});
    
    setTimeout(() => {
      Emitter.emit(Emitter.types.FILES_COUNT,this.state.files.length);
    }, 150);

  }
   

  toggleCustomerCreationModal = (status:boolean) => {

    this.setState({ showCustomerCreationModal : status});
  }

  toggleCustomerEditionModal = (status:boolean) => {

    this.setState({ showCustomerEditionModal : status});
  }


  toggleSignatureModal = (status:boolean) => {
    this.setState({ showSignatureModal : status});

    if(status)
    {
      setTimeout(() => {
    
        var canvas = document.getElementById("signature-pad");
        // console.log(canvas);
        //@ts-ignore
        signaturePad = new SignaturePad(canvas);

        signaturePad.on();
      
      }, 100);

    }
  }
  

  updateCompanySignature(){

    if(!signaturePad.isEmpty())
    {
      const uri = signaturePad.toDataURL();
     
      const AppToaster = Toaster.create({
        className: "recipe-toaster",
        position: Position.TOP,
      });
  
      var company = reactLocalStorage.getObject('ls_loggedCompany');
      var companyId = company.id;

      var config = company.config;
  
      config.signature =    uri;

      var data = {
        config: config
      }
  

      this.setState({creating_bill : true});
  
      UserServices._updateCompanyConfig(companyId, data).then((res)=>{
        
          this.setState({creating_bill : false, showSignatureModal : false});
          AppToaster.show({ intent:Intent.SUCCESS, message: "Signature enregistrée !" });
  
          var newCompany = company;
          newCompany.config = data.config;

          // Update current redux state
          this.props.mainReduxActions.update_company(newCompany);


        }).catch((err)=>{

          this.setState({creating_bill : false, showSignatureModal : false});
          AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'enregistrement !" });

        console.log(err);
      })
      
    }

  }

  resetSignature(){
      // Clears the canvas
      signaturePad.clear();
  }


  handleBillFrom(value : string){
    this.setState({bill_from : value})
  }  
  handleBillTo(value : string){
    this.setState({bill_to : value})
  }  
  handleBillAddress(value : string){
    this.setState({bill_address : value})
  }  
  handleBillNo(value : number){
    this.setState({bill_no : value})
  }  
  handleBillDate(value : Date){
    this.setState({bill_date : value})
  }  
  handleBillCommandNo(value : number){
    this.setState({bill_command_no : value})
  }  
  handleBillDeadline(value : Date){
    this.setState({bill_deadline : value})
  }
  handleBillConditions(value : string){
    this.setState({bill_conditions : value})
  }    

  handleBillFromAddress(value : string){
    this.setState({bill_from_address : value})
  }   
  handleBillFromTel(value : number){
    this.setState({bill_from_tel : value})
  }   
  handleBillFromMail(value : string){
    this.setState({bill_from_mail : value})
  }
  handleBillFromCity(value : string){
    this.setState({bill_from_city : value})
  }   
  handleBillFromZipcode(value : number){
    this.setState({bill_from_zipcode : value})
  }    
  
  handleBillToAddress(value : string){
    this.setState({bill_to_address : value})
  }   
  handleBillToTel(value : number){
    this.setState({bill_to_tel : value})
  }   
  handleBillToMail(value : string){
    this.setState({bill_to_mail : value})
  }  
  handleBillToCity(value : string){
    this.setState({bill_to_city : value})
  }   
  handleBillToZipcode(value : number){
    this.setState({bill_to_zipcode : value})
  }  
  handleBillItemQty(index : number, value : number){

    var bill_items_copy = [...this.state.bill_items]; // make a separate copy of the bill_items_copy
    bill_items_copy[index].qty = value;

    this.setState({bill_items : bill_items_copy});

    // Total price calculation
    var totalPrice = value * bill_items_copy[index].unit_price;
    this.handleBillItemTotalPrice(index, totalPrice);

  }  
 
  handleBillItemLabel(index : number, value : string){

    var bill_items_copy = [...this.state.bill_items]; // make a separate copy of the bill_items_copy
    bill_items_copy[index].name = value;

    this.setState({bill_items : bill_items_copy});
  }  

  handleBillItemUnitPrice(index : number, value : number){

    var bill_items_copy = [...this.state.bill_items]; // make a separate copy of the bill_items_copy
    bill_items_copy[index].unit_price = value;

    this.setState({bill_items : bill_items_copy});

    // Total price calculation
    var totalPrice = value * bill_items_copy[index].qty;
    this.handleBillItemTotalPrice(index, totalPrice);

  }  

  handleBillItemTotalPrice(index : number, value : number){

    var bill_items_copy = [...this.state.bill_items]; // make a separate copy of the bill_items_copy
    bill_items_copy[index].total_ht = value;

    this.setState({bill_items : bill_items_copy});

    // TOTAL HT UPDATE
    var total_ht = 0;
    for (let i = 0; i < this.state.bill_items.length; i++) {
      const element =  this.state.bill_items[i];
      total_ht += element.total_ht;
    }
    this.setState({bill_total_ht : total_ht});

    // TOTAL UPDATE
    var total = 0;
    for (let i = 0; i < this.state.bill_items.length; i++) {
      const element =  this.state.bill_items[i];
      var taxValue = element.total_ht * (element.taxe/100); 
      total += (element.total_ht + taxValue);
    }
    this.setState({bill_total : total});


  }  

  handleBillItemTax(index : number, value : number){

    var bill_items_copy = [...this.state.bill_items]; // make a separate copy of the bill_items_copy
    bill_items_copy[index].taxe = value;

    this.setState({bill_items : bill_items_copy});


    // TOTAL UPDATE
    var total = 0;
    for (let i = 0; i < this.state.bill_items.length; i++) {
      const element =  this.state.bill_items[i];
      var taxValue = element.total_ht * (element.taxe/100); 
      total += (element.total_ht + taxValue);
    }
    this.setState({bill_total : total});
    
  }  


  handleSelectedCustomer = (e, {value})=>{

    // this.setState({cc_customer_country : e.target.textContent})
    this.getSelectedCustomer(value);
  }  

  // --------- CUSTOMER CARD NEW ----------
 
  handleCcCustomerDesignation(value : string){
    this.setState({cc_customer_designation : value})
    
    if(value.trim().length < 2)
    {
      this.setState({cc_customer_error_designation : { content: '2 caractéres minimum', pointing: 'below' }, cc_customer_designation_loading : false, cc_customer_designation_icon : ''});
    }
    else
    {
      // Check existing :::
      this.setState({cc_customer_error_designation : false, cc_customer_designation_loading : true, cc_customer_designation_icon : ''});

      UserServices._checkCustomerCardByName(value).then(resp =>{

        if(resp.data.length > 0)
        {
          this.setState({cc_customer_error_designation : { content: "Nom déja existant", pointing: 'below' }, cc_customer_designation_loading : false, cc_customer_designation_icon : ''});
        }
        else
        {
          this.setState({cc_customer_error_designation : false, cc_customer_designation_loading : false, cc_customer_designation_icon : 'check'});
        }

      }).catch((err)=>{

        this.setState({cc_customer_error_designation : { content: "Erreur lors de la vérification", pointing: 'below' }, cc_customer_designation_loading : false, cc_customer_designation_icon : ''});

      })
      // this.setState({company_name_error : false});
    }
  }

  handleCcCustomerAddress(value : string){
    this.setState({cc_customer_address : value})
  }  

  handleCcCustomerTel(value : string){

    var newValue = value.replace(/\D/g, "");
    this.setState({cc_customer_tel : newValue})

    if(newValue.trim().length < 6)
    {
      this.setState({cc_customer_error_tel : { content: 'Entrez un numéro valide', pointing: 'below' }});
    }
    else
    {
      this.setState({cc_customer_error_tel : false});
    }
  } 

  handleCcCustomerMail(value : string){
    this.setState({cc_customer_mail : value})

    if(this.validateEmail(value))
    {
      this.setState({cc_customer_mail : value, cc_customer_error_mail : false})
    }
    else
    {
      this.setState({cc_customer_error_mail : true})
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  handleCcCustomerCountry = (e, {value})=>{

    this.setState({cc_customer_country : e.target.textContent})
  }  

  handleCcCustomerCity(value : string){
    this.setState({cc_customer_city : value})
  }  

  handleCcCustomerPostalCode(value : number){
    this.setState({cc_customer_postal_code : value})
  } 
  
  handleCcContactName(value : string){
    this.setState({cc_contact_name : value})
  } 

  handleCcContactSurname(value : string){
    this.setState({cc_contact_surname : value})
  } 

  handleCcContactAddress(value : string){
    this.setState({cc_contact_address : value})
  } 
  handleCcContactTel(value : string){
    this.setState({cc_contact_tel : value})
  } 
  handleCcContactMaill(value : string){
    this.setState({cc_contact_mail : value})
  } 

  handleCcBillAddress(value : string){
    this.setState({cc_bill_address : value})
  } 
  
  handleCcBillMode(value : string){
    this.setState({cc_bill_mode : value})
  } 
  
  handleCcBillDelay(value : string){
    console.log(value)
    this.setState({cc_bill_delay : value})
  } 

  // --------------------------------------

  // --------- CUSTOMER CARD EDIT ----------
 
  handleCcEditCustomerDesignation(value : string){
    this.setState({cc_edit_customer_designation : value})
  }  

  handleCcEditCustomerAddress(value : string){
    this.setState({cc_edit_customer_address : value})
  }  

  handleCcEditCustomerTel(value : string){
    this.setState({cc_edit_customer_tel : value})
  }  

  handleCcEditCustomerMail(value : string){
    this.setState({cc_edit_customer_mail : value})
  }  

  handleCcEditCustomerCountry = (e, {value})=>{

    this.setState({cc_edit_customer_country : e.target.textContent})
  }  

  handleCcEditCustomerCity(value : string){
    this.setState({cc_edit_customer_city : value})
  }  

  handleCcEditCustomerPostalCode(value : number){
    this.setState({cc_edit_customer_postal_code : value})
  } 
  
  handleCcEditContactName(value : string){
    this.setState({cc_edit_contact_name : value})
  } 

  handleCcEditContactSurname(value : string){
    this.setState({cc_edit_contact_surname : value})
  } 

  handleCcEditContactAddress(value : string){
    this.setState({cc_edit_contact_address : value})
  } 
  handleCcEditContactTel(value : string){
    this.setState({cc_edit_contact_tel : value})
  } 
  handleCcEditContactMaill(value : string){
    this.setState({cc_edit_contact_mail : value})
  } 

  handleCcEditBillAddress(value : string){
    this.setState({cc_edit_bill_address : value})
  } 
  
  handleCcEditBillMode(value : string){
    this.setState({cc_edit_bill_mode : value})
  } 
  
  handleCcEditBillDelay(value : string){
    this.setState({cc_edit_bill_delay : value})
  } 
  
  // --------------------------------------


  addNewBillItem = () =>
  {
      var newItem = {qty:1, name:"", unit_price:0, total_ht:0, taxe:0}

      this.setState({
        bill_items: [...this.state.bill_items, newItem]
      })
  }

  removeBillItem(index : number)
  {
    var bill_items_copy = [...this.state.bill_items]; // make a separate copy of the bill_items_copy
    if (index !== -1) {
      bill_items_copy.splice(index, 1);
      this.setState({bill_items: bill_items_copy});
    }
  }

 
  getCompanyCcs = (companyId : string)=>{

      this.setState({customers_list_loading : true});

      UserServices._getCompanyCustomerCards(companyId).then(resp =>{
        
        var d = resp.data;
        var customers = [];

        // console.log(d);

        for (let i = 0; i < d.length; i++) {
          const el = d[i];
          //@ts-ignore
          customers.push({key:el.data.cc_customer_designation, text:el.data.cc_customer_designation, value:el.data.cc_customer_designation})
        }

        this.setState({customers_list_loading : false,company_customers : d ,company_customers_list : customers });

      }).catch((err)=>{
        
        this.setState({customers_list_loading : false});
      
    });
  }

  getCountryKey = (text) =>
  {
    var key = countries.find((e)=>e.text == text)?.key;
     
    return key;
  }

  getSelectedCustomer = (name : string)=>{

    var selectedCc = this.state.company_customers.find(e=>e.data.cc_customer_designation == name);
    this.setState({selected_company_customer : selectedCc})

    this.setState({
      
      cc_edit_customer_designation : selectedCc.data.cc_customer_designation,
      cc_edit_customer_address : selectedCc.data.cc_customer_address,
      cc_edit_customer_tel : selectedCc.data.cc_customer_tel,
      cc_edit_customer_mail : selectedCc.data.cc_customer_mail,
      cc_edit_customer_country : selectedCc.data.cc_customer_country,
      cc_edit_customer_city : selectedCc.data.cc_customer_city,
      cc_edit_customer_postal_code : selectedCc.data.cc_customer_postal_code,
      
      cc_edit_contact_name : selectedCc.data.cc_contact_name,
      cc_edit_contact_surname : selectedCc.data.cc_contact_surname,
      cc_edit_contact_address : selectedCc.data.cc_contact_address,
      cc_edit_contact_tel : selectedCc.data.cc_contact_tel,
      cc_edit_contact_mail : selectedCc.data.cc_contact_mail,

      cc_edit_bill_address : selectedCc.data.cc_bill_address,
      cc_edit_bill_mode : selectedCc.data.cc_bill_mode,
      cc_edit_bill_delay : selectedCc.data.cc_bill_delay
    
    })

    // console.log(selectedCc);

  }

  createNewCustomerCard = () =>{

    var company = reactLocalStorage.getObject('ls_loggedCompany');

    var data = 
      {
        name : this.state.cc_customer_designation.toLowerCase(),
        data : {
                  cc_customer_designation : this.state.cc_customer_designation,
                  cc_customer_address : this.state.cc_customer_address,
                  cc_customer_tel : this.state.cc_customer_tel,
                  cc_customer_mail : this.state.cc_customer_mail,
                  cc_customer_country : this.state.cc_customer_country,
                  cc_customer_city : this.state.cc_customer_city,
                  cc_customer_postal_code  : this.state.cc_customer_postal_code,
                  cc_contact_name : this.state.cc_contact_name,
                  cc_contact_surname : this.state.cc_contact_surname,
                  cc_contact_address : this.state.cc_contact_address,
                  cc_contact_tel : this.state.cc_contact_tel,
                  cc_contact_mail : this.state.cc_contact_mail,
                  cc_bill_address : this.state.cc_bill_address,
                  cc_bill_mode  : this.state.cc_bill_mode,
                  cc_bill_delay : this.state.cc_bill_delay
              },
              
            companyId : company.id

        }; 
  
  
  if(this.state.cc_customer_error_designation || this.state.cc_customer_error_address || this.state.cc_customer_error_city || this.state.cc_customer_error_mail || this.state.cc_customer_error_tel)
    return;

  
    this.setState({customer_card_creating : true, showCustomerCreationModal : false});

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });


    UserServices._createCustomerCard(data).then(resp =>{
       

        AppToaster.show({ intent:Intent.SUCCESS, message: "Fiche créée avec succés !" });
        this.setState({customer_card_creating : false,
        
          cc_customer_designation : "" ,
          cc_customer_address : "" ,
          cc_customer_tel : "" ,
          cc_customer_mail : "" ,
          cc_customer_country : "" ,
          cc_customer_city : "" ,
          cc_customer_postal_code  : null ,
          cc_contact_name : "" ,
          cc_contact_surname : "" ,
          cc_contact_address : "" ,
          cc_contact_tel : "" ,
          cc_contact_mail : "" ,
          cc_bill_address : "" 
              
        
        });

        // Get new customers
        var company = reactLocalStorage.getObject('ls_loggedCompany');
        this.getCompanyCcs(company.id);
 

      }).catch((err)=>{
        // console.log(err);
        this.setState({customer_card_creating : false});

      AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la création de la fiche. Veuillez réessayer !" });

    });
     

  // console.log(data);

  }



updateCustomerCard = () =>{

    // var company = reactLocalStorage.getObject('ls_loggedCompany');

    var data = 
      {
        name : this.state.cc_edit_customer_designation.toLowerCase(),
        data : {
                  cc_customer_designation : this.state.cc_edit_customer_designation,
                  cc_customer_address : this.state.cc_edit_customer_address,
                  cc_customer_tel : this.state.cc_edit_customer_tel,
                  cc_customer_mail : this.state.cc_edit_customer_mail,
                  cc_customer_country : this.state.cc_edit_customer_country,
                  cc_customer_city : this.state.cc_edit_customer_city,
                  cc_customer_postal_code  : this.state.cc_edit_customer_postal_code,
                  cc_contact_name : this.state.cc_edit_contact_name,
                  cc_contact_surname : this.state.cc_edit_contact_surname,
                  cc_contact_address : this.state.cc_edit_contact_address,
                  cc_contact_tel : this.state.cc_edit_contact_tel,
                  cc_contact_mail : this.state.cc_edit_contact_mail,
                  cc_bill_address : this.state.cc_edit_bill_address,
                  cc_bill_mode  : this.state.cc_edit_bill_mode,
                  cc_bill_delay : this.state.cc_edit_bill_delay
              }
              
            // ,companyId : company.id

        }; 
  
  
  if(this.state.cc_edit_customer_error_designation || this.state.cc_edit_customer_error_address || this.state.cc_edit_customer_error_city || this.state.cc_edit_customer_error_mail || this.state.cc_edit_customer_error_tel)
    return;

  
    this.setState({customer_card_creating : true, showCustomerEditionModal : false});

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });


    UserServices._updateCustomerCard(this.state.selected_company_customer.id, data).then(resp =>{

        AppToaster.show({ intent:Intent.SUCCESS, message: "Fiche éditée avec succés !" });
        this.setState({customer_card_creating : false,
        
          cc_customer_designation : "" ,
          cc_customer_address : "" ,
          cc_customer_tel : "" ,
          cc_customer_mail : "" ,
          cc_customer_country : "" ,
          cc_customer_city : "" ,
          cc_customer_postal_code  : null ,
          cc_contact_name : "" ,
          cc_contact_surname : "" ,
          cc_contact_address : "" ,
          cc_contact_tel : "" ,
          cc_contact_mail : "" ,
          cc_bill_address : "" 
              
        
        });

        // Get new customers
        var company = reactLocalStorage.getObject('ls_loggedCompany');
        this.getCompanyCcs(company.id);
 

      }).catch((err)=>{
        // console.log(err);
        this.setState({customer_card_creating : false});

      AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la mis à jour de la fiche. Veuillez réessayer !" });

    });
     

  // console.log(data);

  }
  


  _onUploadClicked = (event :Event)=>{
    event.stopPropagation();
  }
  
  _uploadFile = (event : any) =>{
  
      this.setState({context_menu_index : -1})
      
      event.preventDefault();
      event.stopPropagation();
      // console.log(event.target);
  
      var tgt = event.target,
      files = tgt.files,
      file = files[0];
        
      if (FileReader && files && files.length) {
        var fr = new FileReader();
        fr.onload = () => {
            
            var base64 : any;
            base64 = fr.result;
            this.setState({bill_logo : base64, bill_logo_ref : base64})
        }

        fr.readAsDataURL(file);

      }
  
  }

  _uploadSigning = (event : any) =>{
  
    this.setState({context_menu_index : -1})
    
    event.preventDefault();
    event.stopPropagation();
    // console.log(event.target);

    var tgt = event.target,
    files = tgt.files,
    file = files[0];
      
    if (FileReader && files && files.length) {
      var fr = new FileReader();
      fr.onload = () => {
          
          var base64 : any;
          base64 = fr.result;
          this.setState({bill_signing : base64})
      }

      fr.readAsDataURL(file);

    }
}

onCreateBill = () =>{

  this.setState({creating_bill : true});

  // console.log("Creating...")
  const AppToaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP,
  });



  var currentUser = reactLocalStorage.getObject('ls_loggedUser');
  var userCompanyId = currentUser.companyId;
  
  var no_order = this.state.bill_command_no || -1;
  var deadline = this.state.bill_deadline || new Date();

  let data = {
    name              : this.state.bill_no + "_" + new Date().valueOf() + "_"+ this.state.selected_company_customer.name,
    no_ref            : parseInt(this.state.bill_no),
    from_name         : this.state.bill_from,
    bill_from_address         : this.state.bill_from_address,
    bill_from_tel         : parseInt(this.state.bill_from_tel),
    bill_from_mail        : this.state.bill_from_mail,
    bill_from_city         : this.state.bill_from_city,
    bill_from_zipcode        : parseInt(this.state.bill_from_zipcode),    
    to_name           : this.state.selected_company_customer.name,
    bill_to_address         : this.state.selected_company_customer.data.cc_customer_address,
    bill_to_tel         : parseInt(this.state.selected_company_customer.data.cc_customer_tel),
    bill_to_mail        : this.state.selected_company_customer.data.cc_customer_mail,
    bill_to_city         : this.state.selected_company_customer.data.cc_customer_city,
    bill_to_zipcode        : parseInt(this.state.selected_company_customer.data.cc_customer_postal_code), 

    to_id             : this.state.selected_company_customer.name,
    date              : new Date(this.state.bill_date),
    to_address        : this.state.bill_address,
    no_order          : no_order,
    deadline          : deadline,
    total             : this.state.bill_total,
    items             : this.state.bill_items,
    logo              : this.state.bill_logo,
    signing           : this.state.bill_signing,
    type              : this.props.tab == "bills" ? "bill" : "quote",
    conditions        : this.state.selected_company_customer.data.cc_bill_mode + " - " + this.state.selected_company_customer.data.cc_bill_delay,
    legal             : this.state.bill_legal,
    companyDesc       : this.state.company_desc,
    companyId         : userCompanyId
  }

  this.setState({create_data : data});

  setTimeout(() => {
    
      console.log(this.state.create_data);

      UserServices._createComputedBill(this.state.create_data).then((res)=>{

          this.setState({creating_bill : false});
          Emitter.off(Emitter.types.CREATE_BILL, () => this.setState({create_data : {}}) );
          AppToaster.show({ intent:Intent.SUCCESS, message: this.props.tab == "bills" ? "Facture créée avec succés !": "Devis créé avec succés !" });
          Emitter.emit(Emitter.types.CHANGE_BILL_VIEW, "display");
          Emitter.emit(Emitter.types.UPDATE_COMPUTED_BILLS_TAB, "all");
        
      }).catch((err)=>{
        console.log(err);
      })

}, 200);
 

}




 render() {

  // ANIMATIONS PROPS

  const bill_items = this.state.bill_items.map((item :any, index:number) =>

    <div style={{display:'flex', flex:1, width: '100%', justifyContent:'space-between', fontWeight:'bolder'}}>
        <div className={styles.appContainerRightField} style={{flex:0.5, marginRight:15}}>
          <span>Qté :</span>
          <input className={globalStyles.appInputBase} placeholder="Qte" value={item.qty} onChange={(e:any)=>this.handleBillItemQty(index, e.target.value)} type="number" min="1" />
        </div>
        <div className={styles.appContainerRightField} style={{flex:3.5}}>
          <span>Désignation :</span>
          <input className={globalStyles.appInputBase} placeholder="Désignation..." value={item.name} onChange={(e:any)=>this.handleBillItemLabel(index, e.target.value)} type="text" />
        </div>
        <div className={styles.appContainerRightField} style={{flex:2}}>
          <span>Prix Unit. HT :</span>
          <input className={globalStyles.appInputBase} placeholder="0" value={item.unit_price} onChange={(e:any)=>this.handleBillItemUnitPrice(index, e.target.value)}  type="number"  min="0"  />
        </div>
        <div className={styles.appContainerRightField} style={{flex:2}}>
          <span>Montant HT :</span>
          <input className={globalStyles.appInputBase} placeholder="0" value={item.total_ht}  type="number" disabled  />
        </div>
        <div className={styles.appContainerRightField} style={{flex:0.5}}>
          <span>Taxe(%) :</span>
          <input className={globalStyles.appInputBase} placeholder="0" value={item.taxe} onChange={(e:any)=>this.handleBillItemTax(index, e.target.value)}  type="number"  min="0" />
        </div>
        <div className={styles.appContainerRightField} onClick={()=>this.removeBillItem(index)} style={{alignItems:'center', justifyContent:'center', flex:0.5}}>
          <img src={AppIcons.ds_cross_icon} style={{width:18,height:'auto', cursor:'pointer', marginTop:15}}  alt="" />
        </div>

    </div>
);


  return (

    <div className={styles.appInnerMiddleContainer}>
              
      <Dimmer active={this.state.customer_card_creating}>
        <Loader content='Traitement...' />
      </Dimmer>        

      <div className={styles.appInnerContainer} style={{width:'90%', margin:'auto', overflow:'hidden'}}>
          <div style={{display:'flex', flex:2, width: '100%',flexDirection:'column', alignItems:'flex-start', textAlign:'left'}}>
          
          {/* Field */}
              <div className={styles.appContainerRightField}>
                <span>De :</span>
                <input className={globalStyles.appInputBase} placeholder="Saisir un nom" value={this.state.bill_from} onChange={(e:any)=>this.handleBillFrom(e.target.value)} type="text" />
              </div>
              <div style={{position:'relative', display:'flex', width:'77%', marginLeft:'3%', borderLeft:'dashed 1px gray', padding:8, flexDirection:'column', alignItems:'center', backgroundColor:'#b7b7b7', borderRadius:4}}>
                <div style={{position:'absolute', left:'-3%', height:'90%', width:1, borderLeft:'dashed 1px gray'}}></div>
                <div style={{display:'flex', width:'100%', alignItems:'center'}}>
                    <div className={styles.appContainerRightField} style={{width:'95%'}} >
                      <span>Adresse :</span>
                      <input className={globalStyles.appInputBase} style={{width:'100%'}} placeholder="Adresse" value={this.state.bill_from_address} onChange={(e:any)=>this.handleBillFromAddress(e.target.value)} type="text" />
                    </div>
                </div>
                <div style={{display:'flex', width:'100%', alignItems:'center'}}>
                    <div className={styles.appContainerRightField} >
                      <span>Tel :</span>
                      <input className={globalStyles.appInputBase} placeholder="Tel" value={this.state.bill_from_tel} onChange={(e:any)=>this.handleBillFromTel(e.target.value)} type="number" />
                    </div>
                    <div className={styles.appContainerRightField} >
                      <span>E-mail :</span>
                      <input className={globalStyles.appInputBase} placeholder="E-mail" value={this.state.bill_from_mail} onChange={(e:any)=>this.handleBillFromMail(e.target.value)} type="text" />
                    </div>
                </div>
                <div style={{display:'flex', width:'100%', alignItems:'center'}}>
                    <div className={styles.appContainerRightField} >
                      <span>Ville :</span>
                      <input className={globalStyles.appInputBase} placeholder="Ville" value={this.state.bill_from_city} onChange={(e:any)=>this.handleBillFromCity(e.target.value)} type="text" />
                    </div>
                    <div className={styles.appContainerRightField} >
                      <span>Code Postal :</span>
                      <input className={globalStyles.appInputBase} placeholder="Code Postal" value={this.state.bill_from_zipcode} onChange={(e:any)=>this.handleBillFromZipcode(e.target.value)} type="number" />
                    </div>
                </div>
              </div>
          {/* Field */}
          <div className={styles.appContainerRightField} style={{marginTop:10}}>
            <span>Facturé A :</span>
            
            <div style={{display:'flex', width:'100%', marginBottom:5}}>
               
              <Dropdown value={this.state.input_user_country} error={this.state.user_country_error} style={{padding:0, lineHeight:3, textIndent:5, width:'62%', height:38, border:'solid 1px', marginBottom:8}}
                placeholder='Client'
                fluid
                search
                selection
                loading={this.state.customers_list_loading}
                options={this.state.company_customers_list}
                //@ts-ignore
                onChange={this.handleSelectedCustomer}
              />

              <div style={{marginLeft:15, marginRight:10, marginTop:8, cursor:'pointer', zIndex:30, opacity : this.state.selected_company_customer.id == "" ? 0.3 : 1 , pointerEvents : this.state.selected_company_customer.id == "" ? 'none' : 'painted' }} onClick={(e)=>this.toggleCustomerEditionModal(true)}>
                <Icon icon="edit" intent={Intent.PRIMARY} />
              </div>              

              <button className={styles.newCustomerBtn} style={{height:40, borderRadius:4, cursor:'pointer'}} onClick={()=>this.toggleCustomerCreationModal(true)} >NOUVEAU +</button>
            </div>

            <input className={globalStyles.appInputBase} placeholder="Nom du client" value={this.state.selected_company_customer.data.cc_customer_designation} /* onChange={(e:any)=>this.handleBillTo(e.target.value)} */ type="text" />
          </div>
          <div style={{position:'relative', display:'flex', width:'77%', marginLeft:'3%', borderLeft:'dashed 1px gray', padding:8, flexDirection:'column', alignItems:'center', backgroundColor:'#b7b7b7', borderRadius:4, marginBottom:10}}>
              <div style={{position:'absolute', left:'-3%', height:'90%', width:1, borderLeft:'dashed 1px gray'}}></div>
                <div style={{display:'flex', width:'100%', alignItems:'center'}}>
                  <div className={styles.appContainerRightField}  style={{width:'95%'}} >
                    <span>Adresse :</span>
                    <input className={globalStyles.appInputBase}  style={{width:'100%'}} placeholder="Adresse du client" value={this.state.selected_company_customer.data.cc_customer_address} /* onChange={(e:any)=>this.handleBillToAddress(e.target.value)} */ type="text" />
                  </div>
                </div>
                <div style={{display:'flex', width:'100%', alignItems:'center'}}>
                  <div className={styles.appContainerRightField} >
                    <span>Tel :</span>
                    <input className={globalStyles.appInputBase} placeholder="Tel. du client " value={this.state.selected_company_customer.data.cc_customer_tel}  /* onChange={(e:any)=>this.handleBillToTel(e.target.value)} */ type="number" />
                  </div>
                  <div className={styles.appContainerRightField} >
                    <span>E-mail :</span>
                    <input className={globalStyles.appInputBase} placeholder="E-mail du client" value={this.state.selected_company_customer.data.cc_customer_mail}  /* onChange={(e:any)=>this.handleBillToMail(e.target.value)} */ type="text" />
                  </div>
                </div>
                <div style={{display:'flex', width:'100%', alignItems:'center'}}>
                    <div className={styles.appContainerRightField} >
                      <span>Ville :</span>
                      <input className={globalStyles.appInputBase} placeholder="Ville" value={this.state.selected_company_customer.data.cc_customer_city}  /* onChange={(e:any)=>this.handleBillToCity(e.target.value)} */ type="text" />
                    </div>
                    <div className={styles.appContainerRightField} >
                      <span>Code Postal :</span>
                      <input className={globalStyles.appInputBase} placeholder="Code Postal" value={this.state.selected_company_customer.data.cc_customer_postal_code} /* onChange={(e:any)=>this.handleBillToZipcode(e.target.value)} */ type="number" />
                    </div>
                </div>
            </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            <span>Envoyé A :</span>
            <input className={globalStyles.appInputBase} placeholder="Adresse de livraison (facultatif)" value={this.state.selected_company_customer.data.cc_bill_address} onChange={(e:any)=>this.handleBillAddress(e.target.value)} type="text" />
          </div>



          </div>
          {/* Right SECTION */}
          <div style={{display:'flex', flex:1, width: '100%',flexDirection:'column', alignItems:'flex-end'}}>
            
            {/* Field */}
            <div className={styles.appContainerRightField}>
              <span>Mon Logo : </span>
              <input className={styles.logoSelector} type="file" name={"files"} id={"files"} multiple accept=".png, .jpeg" onClick={(e:any)=>this._onUploadClicked(e)} onChange={this._uploadFile} />
              <label style={{display:'flex', position:'relative' ,border: 'solid 9px #cd858d7d', borderRadius: 7,alignItems: 'center',justifyContent: 'center',padding: 10}} htmlFor={"files"}  onClick={(e:any)=>this._onUploadClicked(e)}  >
                <img src={this.state.bill_logo_ref} style={{height:140}} alt=""/>
              </label>
            </div>

            {/* Field */}
            <div className={styles.appContainerRightField}>
              <span>{this.props.tab == "bills" ? "Facture N° :" :  "Devis N° :" }</span>
              <input className={globalStyles.appInputBase} placeholder="001" value={this.state.bill_no} onChange={(e:any)=>this.handleBillNo(e.target.value)} type="number" min="1" />
            </div>

            {/* Field */}
            <div className={styles.appContainerRightField}>
              <span>Date :</span>
              <input className={globalStyles.appInputBase} value={this.state.bill_date} onChange={(e:any)=>this.handleBillDate(e.target.value)} type="date" />
            </div>

            {/* Field */}
            <div className={styles.appContainerRightField}>
              <span>Commande N° :</span>
              <input className={globalStyles.appInputBase} placeholder="001" value={this.state.bill_command_no} onChange={(e:any)=>this.handleBillCommandNo(e.target.value)} type="number" min="0" />
            </div>

            {/* Field */}
            <div className={styles.appContainerRightField}>
              <span>Echéance :</span>
              <input className={globalStyles.appInputBase} value={this.state.bill_deadline} onChange={(e:any)=>this.handleBillDeadline(e.target.value)} type="date" />
            </div>


          </div>

      </div>

      <div className={styles.appInnerContainer} style={{width:'90%', margin:'auto', flexDirection:'column', overflow:'hidden'}}>

        {/* Items List  */}
        <div style={{display:'flex', flex:1, width: '100%', justifyContent:'space-between', fontWeight:'bolder', borderBottom:'solid 1px gray', marginBottom:15, marginTop:15}}>
          <span>{this.props.tab == "bills" ? "Eléments de la facture" : "Eléments du devis"}</span>
        </div>
        {/* Items */}
        {bill_items}
      </div>


      <div className={styles.appInnerContainer} style={{width:'90%', margin:'auto', flexDirection:'column', alignItems:'flex-end', overflow:'hidden'}}>
         
          <div className={styles.appContainerBtn}>
            <button className={styles.signinEnabled} onClick={this.addNewBillItem} > + AJOUTER UN NOUVEL ARTICLE
            {/* <img src={AppIcons.ds_plus_gray_icon} style={{width:14, height:'auto', marginLeft:25, marginBottom:1}} alt="" /> */}
            </button>
          </div>
      </div>

      <div className={styles.appInnerContainer} style={{width:'90%', margin:'auto', flexDirection:'column', alignItems:'center', marginBottom:20, overflow:'hidden'}}>
         
          <span style={{fontWeight:'bolder', fontSize:16, textAlign:'left'}}>Total HT : {this.state.bill_total_ht}</span>
          <span style={{fontWeight:'bolder', fontSize:20, textAlign:'left'}}>TOTAL :  {this.state.bill_total}</span>

      </div>

      <div className={styles.appInnerContainer} style={{width:'90%', margin:'auto', alignItems:'center', justifyContent:'space-evenly', overflow:'hidden'}}>

            {/* Field */}
            <div className={styles.appContainerRightField}>
              <span>Conditions et modalités de paiement : </span>
              <input className={globalStyles.appInputBase} placeholder="Optionel" value={this.state.selected_company_customer.data.cc_bill_mode + " - " + this.state.selected_company_customer.data.cc_bill_delay}/*  onChange={(e:any)=>this.handleBillConditions(e.target.value)} */ type="text" />
            </div>

            {/* Field */}
            <div className={styles.appContainerRightField} style={{alignItems:'flex-end'}}>
              <span style={{alignSelf:'unset'}}>Signature : </span>
              <label style={{display:'flex', position:'relative' ,border: 'solid 9px #cd858d7d', borderRadius: 4,alignItems: 'center',justifyContent: 'center',padding: 10}}  onClick={(e)=>this.toggleSignatureModal(true)}  >
                <img src={this.state.bill_signing} style={{height: 70, backgroundColor:'#ffffff', cursor : 'pointer'}} alt=""/>
              </label>
             
            </div>
           
            
        </div>

    {/* -------------  MODAL CREATE CUSTOMER CARD ---------------- */}

    <Modal
      onClose={() => this.toggleCustomerCreationModal(false)}
      onOpen={() => this.toggleCustomerCreationModal(true)}
      open={this.state.showCustomerCreationModal}>

      <Modal.Header>NOUVELLE FICHE CLIENT</Modal.Header>
      <Modal.Content style={{width:'95%', paddingBottom: 20}}>
          
        <Form style={{width:'90%'}}>
            
            <p style={{textAlign:'center', color:'#AB3134', fontSize:14, fontWeight:'bolder', marginBottom:2}}>Informations Générales</p>
            <Divider section style={{margin:0}} />
 
            <Label style={{fontWeight:'bolder', marginBottom:2, marginTop:12}}>Désignation du Client</Label>
            <Form.Field
                control={Input} type="text" value={this.state.cc_customer_designation} error={this.state.cc_customer_error_designation} loading={this.state.cc_customer_designation_loading} icon={this.state.cc_customer_designation_icon}  onChange={(e) => this.handleCcCustomerDesignation(e.target.value)} style={{width:'100%', marginBottom:10}} placeholder='Raison Sociale ou Nom complet du client' />

            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_customer_address} onChange={(e) => this.handleCcCustomerAddress(e.target.value)}
                label="Adresse"
                placeholder="Adresse"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_customer_tel} onChange={(e) => this.handleCcCustomerTel(e.target.value)}
                label="Tel"
                placeholder="Telephone"
              />

              <Form.Field
                control={Input}
                type="text"
                error={this.state.cc_customer_error_mail}
                value={this.state.cc_customer_mail} onChange={(e) => this.handleCcCustomerMail(e.target.value)}
                label="Email"
                placeholder="Email"
              />

            </Form.Group>

            <Form.Group widths='equal'>

              <div style={{width:'100%', marginLeft:'1%'}}>
                <Label style={{fontWeight:'bolder', marginBottom:2}}>Pays</Label>
                <Dropdown style={{padding:0, lineHeight:3, textIndent:5, width:'99%', height:38, border:'solid 1px', marginBottom:8}}
                  placeholder='Pays'
                  fluid
                  search
                  selection
                  options={countries}
                  defaultValue="AF"

                  //@ts-ignore
                  onChange={this.handleCcCustomerCountry}
                  // onChange={(e) => this.handleUserCountry(e.target.value)}
                />
              </div>  

              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_customer_city} onChange={(e) => this.handleCcCustomerCity(e.target.value)}
                label="Ville"
                placeholder="Ville"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_customer_postal_code} onChange={(e) => this.handleCcCustomerPostalCode(e.target.value)}
                label="Code Postal"
                placeholder="Code Postal"
              />

            </Form.Group>



            {/*  -----------------------------CONTACT---------------------------  */}
            <p style={{textAlign:'center', color:'#AB3134', fontSize:14, fontWeight:'bolder', marginBottom:2}}>Informations Contact</p>
            <Divider section style={{marginTop:0}} />

            <Form.Group widths='equal'>            
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_contact_name} onChange={(e) => this.handleCcContactName(e.target.value)}
                label="Prénom"
                placeholder="Saisir le prénom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_contact_surname} onChange={(e) => this.handleCcContactSurname(e.target.value)}
                label="Nom"
                placeholder="Saisir le nom"
              />
              
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_contact_address} onChange={(e) => this.handleCcContactAddress(e.target.value)}
                label="Adresse"
                placeholder="Adresse"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_contact_tel} onChange={(e) => this.handleCcContactTel(e.target.value)}
                label="Tel"
                placeholder="Telephone"
              />
              
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_contact_mail} onChange={(e) => this.handleCcContactMaill(e.target.value)}
                label="Email"
                placeholder="Email"
              />              


            </Form.Group>

            {/*  ---------------------------- BILL --------------------------  */}
            <p style={{textAlign:'center', color:'#AB3134', fontSize:14, fontWeight:'bolder', marginBottom:2}}>Détails de facturation</p>
            <Divider section style={{marginTop:0}} />
          
              <Form.Group widths='equal'>
                <Form.Field
                  control={Input}
                  value={this.state.cc_bill_address} onChange={(e) => this.handleCcBillAddress(e.target.value)}
                  label='Adresse de livraison'
                  placeholder='Adresse de livraison'
                />
                <Form.Field style={{padding: 0, lineHeight: 3,height: 10, textIndent:10, borderColor:'gray'}}
                  control={Select}
                  value={this.state.cc_bill_mode} onChange={(e, { value }) => this.handleCcBillMode(value)}
                  label='Mode de paiement'
                  options={modes}
                  placeholder='Mode de paiement'
                />

                <Form.Field style={{padding: 0, lineHeight: 3,height: 10, textIndent:10, borderColor:'gray'}}
                  control={Select}
                  value={this.state.cc_bill_delay} onChange={(e, { value }) => this.handleCcBillDelay(value)}
                  label='Délai de paiement'
                  options={delais}
                  placeholder='Délai de paiement'
                />
              </Form.Group>
             


          </Form>


      </Modal.Content>
      <Modal.Actions style={{paddingBottom:100}}>
        <Button color='black' onClick={() => this.toggleCustomerCreationModal(false)}>
          Annuler
        </Button>
        <Button
          content="CREER"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.createNewCustomerCard()}
          positive
        />
      </Modal.Actions>
    </Modal>




    {/* -------------  MODAL UPDATE CUSTOMER CARD ---------------- */}

    <Modal
      onClose={() => this.toggleCustomerEditionModal(false)}
      onOpen={() => this.toggleCustomerEditionModal(true)}
      open={this.state.showCustomerEditionModal}>

      <Modal.Header>EDITER LA FICHE CLIENT</Modal.Header>
      <Modal.Content style={{width:'95%', paddingBottom: 20}}>
          
        <Form style={{width:'90%'}}>
            
            <p style={{textAlign:'center', color:'#AB3134', fontSize:14, fontWeight:'bolder', marginBottom:2}}>Informations Générales</p>
            <Divider section style={{margin:0}} />
 
            <Label style={{fontWeight:'bolder', marginBottom:2, marginTop:12}}>Désignation du Client</Label>
            <Form.Field
                control={Input} type="text" value={this.state.cc_edit_customer_designation} error={this.state.cc_customer_error_designation} loading={this.state.cc_edit_customer_designation_loading} icon={this.state.cc_edit_customer_designation_icon}  onChange={(e) => this.handleCcEditCustomerDesignation(e.target.value)} style={{width:'100%', marginBottom:10}} placeholder='Raison Sociale ou Nom complet du client' />
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_customer_address} onChange={(e) => this.handleCcEditCustomerAddress(e.target.value)}
                label="Adresse"
                placeholder="Adresse"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_customer_tel} onChange={(e) => this.handleCcEditCustomerTel(e.target.value)}
                label="Tel"
                placeholder="Telephone"
              />

              <Form.Field
                control={Input}
                type="text"
                error={this.state.cc_edit_customer_error_mail}
                value={this.state.cc_edit_customer_mail} onChange={(e) => this.handleCcEditCustomerMail(e.target.value)}
                label="Email"
                placeholder="Email"
              />

            </Form.Group>

            <Form.Group widths='equal'>

              <div style={{width:'100%', marginLeft:'1%'}}>
                <Label style={{fontWeight:'bolder', marginBottom:2}}>Pays</Label>
                <Dropdown style={{padding:0, lineHeight:3, textIndent:5, width:'99%', height:38, border:'solid 1px', marginBottom:8}}
                  placeholder='Pays'
                  fluid
                  search
                  selection
                  options={countries}
                  defaultValue={this.getCountryKey(this.state.cc_edit_customer_country)}

                  //@ts-ignore
                  onChange={this.handleCcEditCustomerCountry}
                  // onChange={(e) => this.handleUserCountry(e.target.value)}
                />
              </div>  

              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_customer_city} onChange={(e) => this.handleCcEditCustomerCity(e.target.value)}
                label="Ville"
                placeholder="Ville"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_customer_postal_code} onChange={(e) => this.handleCcEditCustomerPostalCode(e.target.value)}
                label="Code Postal"
                placeholder="Code Postal"
              />

            </Form.Group>
 
            {/*  -----------------------------CONTACT---------------------------  */}
            <p style={{textAlign:'center', color:'#AB3134', fontSize:14, fontWeight:'bolder', marginBottom:2}}>Informations Contact</p>
            <Divider section style={{marginTop:0}} />

            <Form.Group widths='equal'>            
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_contact_name} onChange={(e) => this.handleCcEditContactName(e.target.value)}
                label="Prénom"
                placeholder="Saisir le prénom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_contact_surname} onChange={(e) => this.handleCcEditContactSurname(e.target.value)}
                label="Nom"
                placeholder="Saisir le nom"
              />
              
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_contact_address} onChange={(e) => this.handleCcEditContactAddress(e.target.value)}
                label="Adresse"
                placeholder="Adresse"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_contact_tel} onChange={(e) => this.handleCcEditContactTel(e.target.value)}
                label="Tel"
                placeholder="Telephone"
              />
              
              <Form.Field
                control={Input}
                type="text"
                value={this.state.cc_edit_contact_mail} onChange={(e) => this.handleCcEditContactMaill(e.target.value)}
                label="Email"
                placeholder="Email"
              />              


            </Form.Group>

            {/*  ---------------------------- BILL --------------------------  */}
            <p style={{textAlign:'center', color:'#AB3134', fontSize:14, fontWeight:'bolder', marginBottom:2}}>Détails de facturation</p>
            <Divider section style={{marginTop:0}} />
          
              <Form.Group widths='equal'>
                <Form.Field
                  control={Input}
                  value={this.state.cc_edit_bill_address} onChange={(e) => this.handleCcEditBillAddress(e.target.value)}
                  label='Adresse de livraison'
                  placeholder='Adresse de livraison'
                />
                <Form.Field style={{padding: 0, lineHeight: 3,height: 10, textIndent:10, borderColor:'gray'}}
                  control={Select}
                  value={this.state.cc_edit_bill_mode} onChange={(e, { value }) => this.handleCcEditBillMode(value)}
                  label='Mode de paiement'
                  options={modes}
                  placeholder='Mode de paiement'
                />

                <Form.Field style={{padding: 0, lineHeight: 3,height: 10, textIndent:10, borderColor:'gray'}}
                  control={Select}
                  value={this.state.cc_edit_bill_delay} onChange={(e, { value }) => this.handleCcEditBillDelay(value)}
                  label='Délai de paiement'
                  options={delais}
                  placeholder='Délai de paiement'
                />
              </Form.Group>
            
          </Form>
 
      </Modal.Content>
      <Modal.Actions style={{paddingBottom:100}}>
        <Button color='black' onClick={() => this.toggleCustomerEditionModal(false)}>
          Annuler
        </Button>
        <Button
          content="CREER"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.updateCustomerCard()}
          positive
        />
      </Modal.Actions>
    </Modal>



    {/* ----------------------------------------------------- */}



    {/* -------------  MODAL SIGNATURE ---------------------- */}

    <Modal
      onClose={() => this.toggleSignatureModal(false)}
      onOpen={() => this.toggleSignatureModal(true)}
      open={this.state.showSignatureModal}
    >
      <Modal.Header>ENREGISTRER VOTRE SIGNATURE</Modal.Header>
      <Modal.Content style={{width:'95%', display:'flex', justifyContent:'center'}}>
         
        <canvas id="signature-pad" className={styles.signature} width="400" height="200"></canvas>

      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleSignatureModal(false)}>
          Annuler
        </Button>
        <Button
          icon='redo'
          onClick={() => this.resetSignature()}
          negative
        />
        <Button
          content="Enregistrer"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.updateCompanySignature()}
          positive
        />
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}



    {/* --------------------------------------------------- */}


        <Modal
          open={this.state.creating_bill}>
            <Dimmer active>
              <Loader />
              <span >Création...</span>
            </Dimmer>
        </Modal>

    </div>
  );
  
 }
}
 

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BillsCreatorPartial);
