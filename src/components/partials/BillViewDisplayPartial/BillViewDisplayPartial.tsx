import React, { Component } from 'react';
import styles from './style.module.css';

import { motion } from "framer-motion";
// import PDFViewer from 'pdf-viewer-reactjs';

import AppIcons from '../../../icons';

// Partials
import BillsContainerPartial from "../BillsContainerPartial/BillsContainerPartial";
import { Tab } from 'semantic-ui-react'
import { TabId, Button } from "@blueprintjs/core";
import Emitter from '../../../services/emitter';

const height = window.innerHeight;

class BillViewDisplayPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      preview_right_pos : -110,
      selected_bill : {items:[]},
      navbarTabId : "all",
      from_preview : false,
    };

    
  }
    

componentDidMount(){

  // Initial 
  Emitter.emit(Emitter.types.UPDATE_COMPUTED_BILLS_TAB, 'all');

}

togglePreview()
{

  var preview_pos = this.state.preview_right_pos == -110 ? 0 : -110;
  var details_visibility = this.state.preview_right_pos == -110 ? true : false;
  this.setState({preview_right_pos : preview_pos, details_visibility : details_visibility, from_preview : true});
  
}

gotoCreate = () =>{
      this.props.onChangeView('create');
  }

onBillClicked = (value : any) =>{

    // console.log("Bill clicked")
    this.setState({selected_bill : value});
    this.togglePreview();
}

formatDate(item:any)
{
  var real_date = new Date(item.date);
  var real_date_string = real_date.toLocaleDateString();
  return real_date_string.replaceAll('/', '-');
}

getTotalHt(item:any)
{
  var items = item.items;
  var ht =  0;
  for (let i = 0; i < items.length; i++) {
    
    var element = items[i];
    var this_ht = parseInt(element.total_ht);
    ht += this_ht;
  }

  return ht;
}

getTax(item:any)
{
  var items = item.items;
  var tax =  0;
  var counter =  0;
  for (let i = 0; i < items.length; i++) {
    
    var element = items[i];
    var this_tax = element.taxe;
    if(this_tax > 0)
    {
      tax += parseInt(this_tax);
      counter++;
    }
  }

  if(tax == 0) 
    return "";
  return tax/counter;

}

getTotalTax(item:any)
{
  var total_ht = this.getTotalHt(item);
  return (Math.fround(this.state.selected_bill.total - total_ht)).toFixed(1);

}

downloadPdf = () => 
{
  var bloc = document.getElementById("pdf_preview");
  var filename = this.state.selected_bill.name;
  var opt = {
    margin:       1,
    filename:     filename+'.pdf',
    image:        { type: 'jpeg', quality: 1 },
    html2canvas:  { scale: 2 },
    jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
  };
  //@ts-ignore
  html2pdf().set(opt).from(bloc).save()
}

handleChange = (e, data) => {
  
  var keysTitles = ['all', 'payed', 'suffering', 'unpayed', 'sent'];
  var key = keysTitles[data.activeIndex];

  Emitter.emit(Emitter.types.UPDATE_COMPUTED_BILLS_TAB, key);

}



 render() {

  // ANIMATIONS PROPS

    const panes = 
      [
        {
          menuItem: this.props.tab == 'quotes' ? 'Tous' : 'Toutes',
          render: () => <Tab.Pane attached={false} style={{height:'100%'}}> <BillsContainerPartial onBillClicked={this.onBillClicked} tab={this.props.tab} type="all"  /> </Tab.Pane>,
        },
        {
          menuItem: this.props.tab == 'quotes' ? 'Payés' :  'Payées',
          render: () => <Tab.Pane attached={false} style={{height:'100%'}}> <BillsContainerPartial onBillClicked={this.onBillClicked} tab={this.props.tab} type="payed"  /> </Tab.Pane>,
        },
        {
          menuItem: 'En souffrance',
          render: () => <Tab.Pane attached={false} style={{height:'100%'}}> <BillsContainerPartial onBillClicked={this.onBillClicked} tab={this.props.tab} type="suffering" /> </Tab.Pane>,
        },
        {
          menuItem: this.props.tab == 'quotes' ? 'Impayés' :  'Impayées',
          render: () => <Tab.Pane attached={false} style={{height:'100%'}}> <BillsContainerPartial onBillClicked={this.onBillClicked} tab={this.props.tab} type="unpayed" /> </Tab.Pane>,
        },
        // {
        //   menuItem: this.props.tab == 'quotes' ? 'Envoyés' :  'Envoyées',
        //   render: () => <Tab.Pane attached={false} style={{height:'100%'}}> <BillsContainerPartial onBillClicked={this.onBillClicked} tab={this.props.tab} type="sent" /> </Tab.Pane>,
        // },
      ]
  

  
  const billItemsList = this.state.selected_bill.items.map((item :any, index : number) =>

    <div style={{display : 'flex', alignItems:'flex-end', justifyContent:'space-between', fontSize:17}}>
      <span style={{fontFamily:"Helvetica", fontWeight:'normal', marginTop:5}}> {item.name} </span>
      <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5}}> {item.total_ht} </span>
    </div>);


  return (

    <div className={styles.appInnerMiddleContainer}  style={{overflow:'hidden' }}>
        
      <div style={{display:'flex' ,alignItems:'center', alignSelf:'center', justifyContent:'center', margin:'5px 0px', cursor:'pointer'}}  >
          <Button rightIcon="add" intent="success" text={this.props.tab == 'quotes' ? "NOUVEAU DEVIS" : "NOUVELLE FACTURE"} style={{marginRight:8, backgroundColor : '#ab3134'}} onClick={this.gotoCreate} />
      </div>

      <div className={styles.appInnerMiddleFilesContainer} style={{overflow:'hidden'}}>
        <Tab menu={{ secondary: true, pointing: true }} panes={panes} style={{height:'100%'}} onTabChange={this.handleChange} />
      </div>



      {/* PREVIEW */}
      <motion.div animate={{right : this.state.preview_right_pos+'%'}} 
              transition={{ ease: "easeOut"}}
              className={styles.appInnerMiddlePreviewContainer}>
              <div style={{position:"absolute", left:-50, top:0}}>
                <img src={AppIcons.ds_preview_folder_banner} style={{width:56, height:"auto"}} alt="" />
                <img  src={AppIcons.ds_preview_folder_icon} 
                style={{  position: "absolute",
                          width: 34,
                          left: "20%",
                          top: "18%",
                          height: "auto", cursor:"pointer"}}
                          onClick={()=>{this.togglePreview()}}
                          alt="" />

                
              </div>

              <div className={styles.previewFileContainer} style={{position:'relative', paddingBottom:30}}>
                
                <Button rightIcon="download" intent="success" color="#ffffff" text="TELECHARGER" style={{position:'absolute', top:5, right:5, backgroundColor : '#ab3134'}} onClick={this.downloadPdf} />

                 <div id="pdf_preview" style={{display:'flex', flexDirection:'column', width:'90%', margin:'auto', marginTop:15}}>
                    
                    <div style={{display:'flex', marginTop:5, marginBottom:20, justifyContent:'space-between', alignItems:'center'}}>
                      <div style={{display:'flex'}}>
                        <img src={this.state.selected_bill.logo} style={{width:160, height:"auto"}} alt="" />
                      </div>
                      <div style={{display:'flex', justifyContent:'flex-end', flexDirection:'column', textAlign:'right'}}>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bold', fontSize:20, color:'#AB3134'}}>{ this.props.tab == 'quotes' ? 'DEVIS' : 'FACTURE'}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bold', fontSize:17, color:'#1a1a1a'}}>N° : {this.state.selected_bill.no_ref}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bold', fontSize:17, color:'#1a1a1a'}}>Date : {this.formatDate(this.state.selected_bill)}</span>
                      </div>
                    </div>
                
                    <div style={{display:'flex', marginTop:25, marginBottom:20, justifyContent:'space-between', alignItems:'center'}}>
                      <div style={{display:'flex', backgroundColor:'gainsboro', border:'solid 1px gainsboro', padding:15, justifyContent:'flex-start', flexDirection:'column', textAlign:'left', marginRight:40}}>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bold', fontSize:17, color:'#1a1a1a'}}>{this.state.selected_bill.from_name}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a', marginBottom:10}}>{this.state.selected_bill.bill_from_address}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a'}}>Tel : {this.state.selected_bill.bill_from_tel}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a'}}>E-mail : {this.state.selected_bill.bill_from_mail}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a'}}>{this.state.selected_bill.bill_from_city} , CP : {this.state.selected_bill.bill_from_zipcode}</span>
                        
                      </div>
                      <div style={{display:'flex', backgroundColor:'transparent', border:'solid 2px gray', padding:15, justifyContent:'flex-start', flexDirection:'column', textAlign:'left'}}>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bold', fontSize:17, color:'#1a1a1a'}}>Client : {this.state.selected_bill.to_name}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a', marginBottom:10}}>{this.state.selected_bill.bill_to_address}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a'}}>Tel : {this.state.selected_bill.bill_to_tel}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a'}}>E-mail : {this.state.selected_bill.bill_to_mail}</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', fontSize:14, color:'#1a1a1a'}}>{this.state.selected_bill.bill_to_city} , CP : {this.state.selected_bill.bill_to_zipcode}</span>

                      </div>
                    </div>


                   {/* ITEMS HEADER */}
                    <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', textAlign:"left", padding: '8px 0px', marginTop:10, borderTop:'solid 1px gray' , borderBottom:'solid 1px gray'}}>
                      <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5, fontSize:17, color:'#AB3134', textTransform:'uppercase'}}>Designation</span>
                      <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5, fontSize:17, color:'#AB3134', textTransform:'uppercase'}}>MONTANT HT</span>
                    </div>

                    <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', textAlign:"left", flexDirection:'column', marginTop:20, marginBottom:40}}>
                      {billItemsList}
                    </div>


                    {/* RECAP */}
                    <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', textAlign:"left", marginTop:20}}>
                      <div style={{display:'flex', flex:1, alignContent:'center', flexDirection:'column'}}></div>
                      <div style={{display:'flex', flex:1, alignContent:'center', flexDirection:'column' , backgroundColor:'#f2f2f2', padding:15, borderTop:'solid 1px gray'}}>
                        <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', alignItems:'flex-end', fontSize:18}}>
                          <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5}}> Total HT </span>
                          <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5}}> {this.getTotalHt(this.state.selected_bill)} </span>
                        </div>
                        <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', alignItems:'flex-end', fontSize:18}}>
                          <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5}}> Taxe {this.getTax(this.state.selected_bill)}% </span>
                          <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5}}> {this.getTotalTax(this.state.selected_bill)} </span>
                        </div>

                        <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', alignItems:'flex-end'}}>
                          <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5, fontSize:20, color:'#AB3134', textTransform:'uppercase'}}>TOTAL</span>
                          <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5, fontSize:20, color:'#AB3134'}}> {this.state.selected_bill.total} </span>
                        </div>

                      </div>
                    </div>


                    {/* CONDTS + SIGNING */}

                    <div style={{display:'flex', alignContent:'center', justifyContent:'space-between', textAlign:"left", marginTop:50}}>
                      <div style={{display:'flex', flex:1, alignContent:'center', flexDirection:'column'}}>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5, fontSize:12, color:'#AB3134', textTransform:'uppercase'}}>conditions et modalités de paiement</span>
                        <span style={{fontFamily:"Helvetica", fontWeight:'bolder', marginTop:5, fontSize:12}}> {this.state.selected_bill.conditions} </span>
                      </div>
                      <div style={{display:'flex', flex:1, alignItems:'flex-end', flexDirection:'column'}}>
                        <img src={this.state.selected_bill.signing} style={{width:120, height:"auto"}} alt="" />
                      </div>
                    </div>

                    {/* LEGAL */}

                    <div style={{display:'flex', alignContent:'center', flexDirection:'column', justifyContent:'space-between', textAlign:"left", marginTop:20, borderTop:'solid 1px gray'}}>
                      <div style={{display:'flex', flex:1, justifyContent:'center', flexDirection:'column'}}>
                        <span style={{fontFamily:"Helvetica", fontWeight:'normal', marginTop:5, fontSize:10}}> {this.state.selected_bill.legal}  </span>
                      </div>
                      <div style={{display:'flex', flex:1, justifyContent:'center', flexDirection:'column', marginTop:20, borderTop:'solid 1px gray'}}>
                        <span style={{fontFamily:"Helvetica", textAlign: 'center', fontWeight:'bold', marginTop:5, fontSize:12}}>
                          {this.state.selected_bill.companyDesc} 
                        </span>
                      </div>
                    </div>


                 </div>
              </div>
          </motion.div>




    </div>
  
 
  );
  
 }
}

export default BillViewDisplayPartial;
