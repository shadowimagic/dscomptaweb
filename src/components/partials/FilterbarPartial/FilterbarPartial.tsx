import React, { Component } from 'react';
import styles from './style.module.css';

import { motion } from "framer-motion";

import AppIcons from '../../../icons';
import Emitter from '../../../services/emitter';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


class FilterbarPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      current_month_index : 11,
      current_month_name : "Decembre",
      months_list_visibility : "none",
      months : ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],

    };
    this.toggleMonthsList = this.toggleMonthsList.bind(this);
  }

  componentDidMount()
  {
    var currentMonth = this.props.mainReduxState.active_month;
    this.setState({current_month_index : currentMonth, current_month_name : this.state.months[currentMonth]});

    Emitter.on(Emitter.types.CLOSE_POPOVER, (src) => this.onClose());

  }
  
  
  onClose()
  {
    var visibility = "none";
    this.setState({months_list_visibility : visibility});
  }  
  toggleMonthsList(e:Event,month : number, change_month : boolean = true)
  {
    e.stopPropagation();
    var visibility = this.state.months_list_visibility == "block" ? "none" : "block";
    this.setState({current_month_index : month, current_month_name : this.state.months[month],  months_list_visibility : visibility});

    if(change_month)
    { 
      // Emit month change : Redux & Emitter
      this.props.mainReduxActions.change_month(month);
      Emitter.emit(Emitter.types.MONTH_CHANGED, month);
    }

  }

 render() {
// ANIMATIONS PROPS
  const monthsList = this.state.months.map((item :any, index : number) =>
  <div onClick={(e:any)=> {this.toggleMonthsList(e,index)}} key={index} className={styles.monthItem}>
    <span>{item}</span> 
  </div>
  );


  return (
    
  <div className={styles.appInnerFilterContainer}>
    
    {(this.props.hide_filter == null || this.props.hide_filter == false) &&
      <div className={styles.appInnerDisplayFilterContainer}>
        <img src={AppIcons.ds_grid_display_icon}  style={{height:"50%", width:"auto", cursor:"pointer"}} alt="" />
      </div>
    }

    <div className={styles.appInnerFilterBox}>
      <span style={{fontFamily : 'Poppins', fontSize:12, marginRight:15,fontWeight:"bolder" }}>Periode </span>
      <div className={styles.appInnerMonthsFilterContainer}>
        <span style={{flex : 3,fontFamily:'Poppins', color:"#14142B", fontSize:14, fontWeight : "bold", paddingLeft:5, paddingRight:5}}>{this.state.current_month_name}</span>
        <img  onClick={(e:any)=> {this.toggleMonthsList(e,this.state.current_month_index, false)}} src={AppIcons.ds_filter_dropper_icon} style={{height:"80%", width:"auto", cursor:"pointer"}} alt="" />
        <div style={{width:1, height:"100%", backgroundColor:"black", marginLeft:5, marginRight:5}}></div>
        <span style={{flex : 2,fontFamily:'Poppins',color:"#14142B", fontSize:14, fontWeight : "bolder"}}>{this.props.mainReduxState.selected_company.exercise_year}</span>
        <motion.div animate={{display : this.state.months_list_visibility}} className={styles.appInnerMonthsListContainer}>
          {monthsList}
        </motion.div>
      </div>
    </div>
  </div>
  );
  
 }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(FilterbarPartial);
