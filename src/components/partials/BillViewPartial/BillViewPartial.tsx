import React, { Component } from 'react';
import styles from './style.module.css';

import { motion } from "framer-motion";
// import PDFViewer from 'pdf-viewer-reactjs';

import AppIcons from '../../../icons';
import BillViewDisplayPartial from '../BillViewDisplayPartial/BillViewDisplayPartial';
import BillViewCreatePartial from '../BillViewCreatePartial/BillViewCreatePartial';
import Emitter from '../../../services/emitter';

// Partials



class BillViewPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      preview_right_pos : -110,
      selected_file : {uri : AppIcons.ds_facture_model},
      activePartial : "display"
    };

   
  }

componentDidMount(){

  Emitter.on(Emitter.types.CHANGE_BILL_VIEW, (view)=>this.onChangeView(view))

}
    

 onChangeView = (view) =>{

  this.setState({activePartial : view});
}


render() {

  // ANIMATIONS PROPS
 

  return (

    

    <div className={styles.appInnerMiddleContainer} >
        
      {this.state.activePartial == "display" &&
         
        <BillViewDisplayPartial tab={this.props.tab} files={this.props.files} onChangeView={this.onChangeView} />
      }

      {this.state.activePartial == "create" &&
        <BillViewCreatePartial  tab={this.props.tab} onChangeView={this.onChangeView}  />
      }
         
    </div>
  
 
  );
  
 }
}

export default BillViewPartial;
