import React, { Component } from 'react';
import styles from './style.module.css';

import { motion } from "framer-motion";
// import PDFViewer from 'pdf-viewer-reactjs';

import AppIcons from '../../../icons';

// Partials
import BillSettingsScreen from "../../Dashboard/BillSettingsScreen/BillSettingsScreen"; 

import { Tab, Tabs, TabId } from "@blueprintjs/core";
import {Button as CBTN} from "@blueprintjs/core";
import BillsCreatorPartial from '../BillsCreatorPartial/BillsCreatorPartial';
import Emitter from '../../../services/emitter';
import { Modal, Button } from 'semantic-ui-react';
 
class BillViewCreatePartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      preview_right_pos : -110,
      selected_file : {uri : AppIcons.ds_facture_model},
      navbarTabId : "simple",

      showSettingsModal : false
       
    };

  }
    
  private handleNavbarTabChange = (navbarTabId: TabId) => this.setState({ navbarTabId });
  

  gotoDisplay = () =>{
      this.props.onChangeView('display');
  }

  createBill = () =>{
    Emitter.emit(Emitter.types.CREATE_BILL, "create");
  }

  toggleSettingsModal = (status:boolean) => {

    this.setState({ showSettingsModal : status});
  }

  updateBillSettings = ()=>{


  }

 
 render() {

  // ANIMATIONS PROPS
 
const SimpleBillComponent: React.FunctionComponent = () => (
  <div>
     <BillsCreatorPartial tab={this.props.tab} />
  </div>
);

  

  return (

    

    <div className={styles.appInnerMiddleContainer}>
        
        <div style={{display:'flex', alignItems:'center', justifyContent:'flex-end', marginTop:4, marginRight:4}}>
          
          {/* <div style={{alignItems:'center', justifySelf:'center', justifyContent:'center', margin:'5px 0px', cursor:'pointer'}} onClick={this.gotoDisplay} >
              <span style={{fontWeight:'bolder', textDecoration:'underline'}}>Annuler</span>
              <img src={AppIcons.ds_plus_green_icon} style={{width:15, height:'auto', marginLeft:5, marginTop:3}} alt=""/>
          </div> */}
          
          <CBTN icon="cog" intent="none" text=""  style={{marginRight:8, backgroundColor:'transparent'}} onClick={()=>this.toggleSettingsModal(true)} />
          <CBTN rightIcon="confirm" intent="success" text={this.props.tab == "bills" ? "SAUVEGARDER LA FACTURE" : "SAUVEGARDER LE DEVIS"} style={{marginRight:8, backgroundColor : '#ab3134'}} onClick={this.createBill} />
          <CBTN rightIcon="cross" intent="danger" text="Annuler" style={{backgroundColor : '#ab3134'}} onClick={this.gotoDisplay} />

        </div>
       

      <div className={styles.appInnerMiddleFilesContainer}>

        <Tabs id="TabsCreateBills" selectedTabId={this.state.navbarTabId} onChange={this.handleNavbarTabChange}>
            <Tab id="simple" title={this.props.tab == "bills" ? "Facture" : "Devis" } panel={<SimpleBillComponent />} />
            {/* <Tab id="advanced" title="Facture Compléte" panel={<AdvancedBillComponent />} /> */}
            {/* <Tabs.Expander /> */}
        </Tabs>

         
      </div>
 

    {/* -------------  MODAL SETTINGS ---------------------- */}


    <Modal
      onClose={() => this.toggleSettingsModal(false)}
      onOpen={() => this.toggleSettingsModal(true)}
      open={this.state.showSettingsModal}
    >
      <Modal.Header>PARAMETRES DE FACTURATION</Modal.Header>
      <Modal.Content style={{width:'95%', paddingBottom: 20}}>

        <BillSettingsScreen/>

      </Modal.Content>
      <Modal.Actions style={{paddingBottom:100}}>
        <Button color='black' onClick={() => this.toggleSettingsModal(false)}>
          Fermer
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}



         
  </div>
  
 
  );
  
 }
}

export default BillViewCreatePartial;
