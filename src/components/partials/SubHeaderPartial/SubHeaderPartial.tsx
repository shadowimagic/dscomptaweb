import React, { Component } from 'react';
import styles from './style.module.css';

import { motion } from "framer-motion";

import AppIcons from '../../../icons';
import Emitter from '../../../services/emitter';


class SubHeaderPartial extends Component<any, any> {
   
  constructor(props:any) {
    super(props);
    this.state = {
      status : "success",
      active_hmenu_index:0,

    };


    this.onHmenuClicked = this.onHmenuClicked.bind(this);

  }
  
  onHmenuClicked(e:Event ,index : number, route:string = "")
  { 
      e.stopPropagation();

      this.setState({active_hmenu_index : index});
      this.props.onSubmenuClicked(index);
      
      Emitter.emit(Emitter.types.FILES_TYPE_CHANGED, route);

  }

  


 render() {

// ANIMATIONS PROPS
    
  return (
    
     <div className={styles.appHeader}>
          
          <div className={styles.appHeaderTitlebox}>
            <span>{this.props.title}</span>
          </div>


         {this.props.display_hmenu &&
            <div className={styles.appHeaderHmenuBox} style={{marginRight : "auto"}}>
              <button onClick={(e:any)=>{this.onHmenuClicked(e,0,"customer")}} className={(this.state.active_hmenu_index == 0 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Factures Clients
              </button>
              <button onClick={(e:any)=>{this.onHmenuClicked(e,1,"provider")}} className={(this.state.active_hmenu_index == 1 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Factures Fournisseurs
              </button>
            </div>
          }

          {this.props.display_bill_hmenu &&
            <div className={styles.appHeaderHmenuBox} style={{marginRight : "auto"}}>
              <button onClick={(e:any)=>{this.onHmenuClicked(e,0)}} className={(this.state.active_hmenu_index == 0 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Mes Factures
              </button>
              <button onClick={(e:any)=>{this.onHmenuClicked(e,1)}} className={(this.state.active_hmenu_index == 1 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Mes Devis
              </button>
              {/* <button onClick={(e:any)=>{this.onHmenuClicked(e,2)}} className={(this.state.active_hmenu_index == 2 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Mes Clients
              </button> */}
            </div>
          }

          {this.props.display_bank_hmenu &&
            <div className={styles.appHeaderHmenuBox} style={{marginRight : "auto"}}>
              <button onClick={(e:any)=>{this.onHmenuClicked(e,0)}} className={(this.state.active_hmenu_index == 0 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Banque
              </button>
              <button onClick={(e:any)=>{this.onHmenuClicked(e,1)}} className={(this.state.active_hmenu_index == 1 ? styles.appHeaderHmenuItemActive : styles.appHeaderHmenuItem )} >
                Paramétrage
              </button>
               
            </div>
          }


      {this.props.display_searchbar && 
        <div className={styles.appHeaderSearchbox}>
          <input placeholder="Rechercher un document..." type="search" name="" id=""/>
          <button>
            <img src={AppIcons.ds_search_icon} alt=""/>
          </button>
        </div>
      }

    </div>
 
  );
  
 }
}

export default SubHeaderPartial;
