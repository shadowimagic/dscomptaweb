import React, { Component } from 'react'
import { Grid, Form, Modal, Dimmer, Loader, Header, Button, Icon, Input, List, Image,TextArea } from 'semantic-ui-react'
import AppIcons from '../../../icons';
import UserServices from '../../../services/UserServices';
import {Intent, Label, Position, Toaster } from "@blueprintjs/core";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';

import styles from './style.module.css';
import {reactLocalStorage} from 'reactjs-localstorage';

class AccountsScreen extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 
      activeItem: 'general',
      isLoading : false,
      isCreating : false,
      isUpdating : false,
      showEditionModal : false,
      showUserDeleteModal : false,
      selectedUserIndex : 0,
      selectedUser : {name:'', surname:'', email:'', password:''},
      companyDomainName : '@faseya.com',

      newUserName : '',
      newUserSurname : '',
      newUserUsername : '',
      newUserPassword : '',

      
      selectedUserName : '',
      selectedUserSurname : '',
      selectedUserUsername : '',
      selectedUserPassword : '',

      passwordVisibility : 'password',


      users : []
    
    }
  }

 
  componentDidMount(){

    this.getUsers();  
  
  }
 

  
  getUsers = () => {
    
    var currentCompany = reactLocalStorage.getObject('ls_loggedCompany');
    var companyId = currentCompany.id;

    var domainName = currentCompany.domain_name;
    var companyName = currentCompany.name;
    if(currentCompany.domain_name == null || currentCompany.domain_name == undefined || currentCompany.domain_name == '')
    {
      domainName = companyName.toLowerCase();
      domainName.replace(/ /g, '-');
      domainName = "@"+domainName+".com";
    }

    this.setState({isLoading : true, companyDomainName : domainName});

    UserServices._getCompanyUsers(companyId).then((res)=>{

        var users = res.data;
        this.setState({
          users : users,
          isLoading : false
        });

        // Update current redux state
        // this.props.mainReduxActions.update_company(company);

      }).catch((err)=>{
      console.log(err);
    })

  }

 
  handleNewUserName = (e, newUserName) => {
    this.setState({ newUserName });
  }

  handleNewUserSurname = (e, newUserSurname) => {
    this.setState({ newUserSurname });
  }
 
  handleNewUserUsername = (e, newUserUsername) => {
    this.setState({ newUserUsername });
  }

  handleNewUserPassword = (e, newUserPassword) => {
    this.setState({ newUserPassword });
  }

   
  handleSelectedUserName = (e, selectedUserName) => {
    this.setState({ selectedUserName });
  }

  handleSelectedUserSurname = (e, selectedUserSurname) => {
    this.setState({ selectedUserSurname });
  }
 
  handleSelectedUserUsername = (e, selectedUserUsername) => {
    this.setState({ selectedUserUsername });
  }

  handleSelectedUserPassword = (e, selectedUserPassword) => {
    this.setState({ selectedUserPassword });
  }

  togglePasswordVisibility = () => {
    
    var visibility = this.state.passwordVisibility == "password" ? "text" : "password" ;
    this.setState({ passwordVisibility:visibility });

  }

  toggleEditionModal = (status, index=0) => {
    
    if(status)
    //@ts-ignore
      this.setState({selectedUser : this.state.users[index], selectedUserName : this.state.users[index].name, selectedUserSurname : this.state.users[index].surname,selectedUserUsername : this.getUsername(this.state.users[index].email),selectedUserPassword : this.state.users[index].password
      });

    this.setState({ showEditionModal : status });
  }

  toggleUserDeleteModal = (status:boolean, itemIndex:number=-1) => {
    this.setState({ showUserDeleteModal : status, selectedUserIndex:itemIndex });
  }


  // CREATE USER
  createUser = () => {

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    // API CALLBACK
    //...
    var currentCompany = reactLocalStorage.getObject('ls_loggedCompany');

    this.setState({isCreating : true});

    var data = {
      name : this.state.newUserName,
      surname : this.state.newUserSurname,
      email : this.state.newUserUsername+this.state.companyDomainName,
      password : this.state.newUserPassword,
      companyId: currentCompany.id,
      type : "accountant"
    }
    //@ts-ignore
    UserServices._createUser(data).then((res)=>{
      
        this.setState({isCreating : false});
 
        AppToaster.show({ intent:Intent.SUCCESS, message: "Compte créé avec succés !" });
 
        // Load new data
        this.getUsers();

      }).catch((err)=>{
      console.log(err);
    })
   
 }

  // UPDATE USER
  updateUser = () => {

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    // API CALLBACK
    //...
    this.setState({isUpdating : true});

    var data = {
      name : this.state.selectedUserName,
      surname : this.state.selectedUserSurname,
      email : this.state.selectedUserUsername+this.state.companyDomainName,
      password : this.state.selectedUserPassword,
    }
    //@ts-ignore
    UserServices._updateUser(this.state.selectedUser.id, data).then((res)=>{
      
        this.setState({isUpdating : false});
 
        AppToaster.show({ intent:Intent.SUCCESS, message: "Mis à jour réussi !" });

        // Update redux state
        //Update Local only if current user
        var loggedUser = reactLocalStorage.getObject('ls_loggedUser');
        if(loggedUser.id == this.state.selectedUser.id)
        {
          var user = this.state.selectedUser;
          var newUser = {...user , ...data };
          // console.log(newUser);
          this.props.mainReduxActions.update_user(newUser);
        }
        // Load new data
        this.getUsers();

      }).catch((err)=>{
      console.log(err);
    })
  
    this.toggleEditionModal(false);
 }



  deleteUser = () => {

    // API CALLBACK
    //...
    console.log(this.state.selectedUserIndex)

    this.toggleUserDeleteModal(false);


 }
 
 setRole(type) {
 
  var role = "Administratuer";
  switch (type) {
    case "owner":
     role = "Administrateur"
      break;
    case "accountant":
     role = "Comptable"
      break;
 
  }
  return role;
}

  getUsername(email) {
    
    return email.split('@')[0];
  }

  render() {

    const users = this.state.users.map((item :any, index : number) =>

      <List.Item style={{paddingTop:10, paddingBottom:10}}>
        <Image avatar src={AppIcons.ds_signin_select_icon} />
        <List.Content>
          <List.Header>{item.name} {item.surname}</List.Header>
          <span>{this.setRole(item.type)}</span>
        </List.Content>
        <List.Content floated='right'>
          <Button.Group basic size='small' style={{marginRight:5}}>
            <Button icon='edit' onClick={() => this.toggleEditionModal(true, index)} />
            <Button color="red" icon='trash' onClick={() => this.toggleUserDeleteModal(true,index)} /> 
          </Button.Group>
        </List.Content>
      </List.Item>
    );

    return (
    <div className={styles.baseContainer}>

      <Dimmer active={this.state.isLoading}>
        <Loader content='Chargement...' />
      </Dimmer>        
      
      <Dimmer active={this.state.isCreating}>
        <Loader content='Création...' />
      </Dimmer> 

      <Dimmer active={this.state.isUpdating}>
        <Loader content='Mis à jour...' />
      </Dimmer>        

      <div className={styles.secondContainer}>

        <div className={styles.innerContainer} style={{borderRight:"solid 2px gainsboro"}}>
          <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Profils comptables</span>
          
          <List divided verticalAlign='middle'>
            
            {users}
           
          </List>

        </div>

        {/* -------------  RIGHT ---------------- */}

        <div className={styles.innerContainer} style={{alignItems:'flex-end'}}>
          <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Créer un nouveau compte</span>
          
          <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newUserName}
                onChange={(e) => this.handleNewUserName(e,e.target.value)}
                label="Prénom"
                placeholder="Saisir le prénom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newUserSurname}
                onChange={(e) => this.handleNewUserSurname(e,e.target.value)}
                label="Nom"
                placeholder="Saisir le nom"
              />
              
            </Form.Group>

            <Form.Group widths='equal'  style={{display:'flex', width:'67%' ,flexDirection:'column' , marginLeft:0}}>

              <span style={{fontWeight:'bold'}}>Nom d'utilisateur</span>
              <Input 
                label={this.state.companyDomainName} 
                labelPosition='right' 
                placeholder='utilisateur' 
                value={this.state.newUserUsername}
              
                onChange={(e) => this.handleNewUserUsername(e,e.target.value)}
                
                />
              
            </Form.Group>
             
            <Form.Group widths='equal'>
             
              <Form.Field
                control={Input}
                type={this.state.passwordVisibility}
                icon={<Icon name='eye' color='black' style={{top:0, right:0}}  link onClick={this.togglePasswordVisibility} />}
                value={this.state.newUserPassword}
                onChange={(e) => this.handleNewUserPassword(e,e.target.value)}
                label="Mot de passe"
                placeholder="Saisir un mot de passe"
              />
              
            </Form.Group>

          </Form>

          <div style={{marginTop:40}}>
            <Button  style={{backgroundColor : '#ab3134'}} positive onClick={() => this.createUser() }>CREER</Button>
          </div>

        </div>
      </div>
      


    {/* -------------  MODAL EDITION ---------------------- */}


    <Modal
      onClose={() => this.toggleEditionModal(false)}
      onOpen={() => this.toggleEditionModal(true)}
      open={this.state.showEditionModal}
    >
      <Modal.Header>MODIFIER LE PROFIL</Modal.Header>
      <Modal.Content style={{width:'95%'}}>
         

        <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedUserName}
                onChange={(e) => this.handleSelectedUserName(e,e.target.value)}
                label="Prénom"
                placeholder="Saisir le prénom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedUserSurname}
                onChange={(e) => this.handleSelectedUserSurname(e,e.target.value)}
                label="Nom"
                placeholder="Saisir le nom"
              />
              
            </Form.Group>

            <Form.Group widths='equal'  style={{display:'flex', width:'67%' ,flexDirection:'column' , marginLeft:0}}>

              <span style={{fontWeight:'bold'}}>Nom d'utilisateur</span>
              <Input 
                label={this.state.companyDomainName} 
                labelPosition='right' 
                placeholder='utilisateur' 
                value={this.getUsername(this.state.selectedUserUsername)}
              
                onChange={(e) => this.handleSelectedUserUsername(e,e.target.value)}
                
                />
              
            </Form.Group>
             
            <Form.Group widths='equal'>
             
              <Form.Field
                control={Input}
                type={this.state.passwordVisibility}
                icon={<Icon name='eye' color='black' style={{top:0, right:0}}  link onClick={this.togglePasswordVisibility} />}
                value={this.state.selectedUserPassword}
                onChange={(e) => this.handleSelectedUserPassword(e,e.target.value)}
                label="Mot de passe"
                placeholder="Saisir un mot de passe"
              />
              
            </Form.Group>

          </Form>


      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleEditionModal(false)}>
          Annuler
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}}
          content="Mettre à jour"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.updateUser()}
          positive
        />
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}

    {/* ------------------DELETE USER MODAL------------- */}

    <Modal
      basic
      onClose={() => this.toggleUserDeleteModal(false)}
      onOpen={() => this.toggleUserDeleteModal(true)}
      open={this.state.showUserDeleteModal}
      size='small' 
    >
      <Header icon>
        <Icon name='trash' />
        Suppression compte utilisateur
      </Header>
      <Modal.Content>
        <p>
          Souhaitez-vous vraiment supprimer ce compte utilisateur ?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.toggleUserDeleteModal(false)}>
          <Icon name='remove' /> Non
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.deleteUser()}>
          <Icon name='checkmark' /> Oui
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}


    </div>
   

    
    )
  }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}
function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(AccountsScreen);
