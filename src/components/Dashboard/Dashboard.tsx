import React, { Component } from 'react'
import { Menu, Segment, Header, Button, Icon } from 'semantic-ui-react'
import CustomersScreen from './CustomersScreen/CustomersScreen';
import AccountsScreen from './AccountsScreen/AccountsScreen';
import GeneralScreen from './GeneralScreen/GeneralScreen';
import StatisticsScreen from './StatisticsScreen/StatisticsScreen';
import ExercisesSettings from './ExercisesSettings/ExercisesSettings';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';
import {reactLocalStorage} from 'reactjs-localstorage';

import styles from './style.module.css';


class Dashboard extends Component<any, any> {
  constructor(props:any){
    super(props)
    
    this.state = { 
      activeItem: 'general',
      loggedCompany : {}, // The account owner's company
    
    }

  }

  handleItemClick = (e,  name) => {
    
    this.setState({ activeItem: name });
  
  };

  componentDidMount(){
    var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
    this.setState({loggedCompany : loggedCompany});
  }
 
  render() {
    const { activeItem } = this.state

    return (
      <div style={{width:'100%' , textAlign:'left', height:'100%'}}>
        {this.state.loggedCompany.type == "accounting" && 
          <Menu pointing secondary className={styles.menuItem}>
            <Menu.Item 
              name='general'
              active={activeItem === 'general'}
              onClick={(e) =>this.handleItemClick(e,"general")}
            />
            <Menu.Item
              name='comptes'
              active={activeItem === 'comptes'}
              onClick={(e) =>this.handleItemClick(e,"comptes")}
            />
            <Menu.Item
              name='entreprises clientes'
              active={activeItem === 'entreprises clientes'}
              onClick={(e) =>this.handleItemClick(e,"entreprises clientes")}
            />
            <Menu.Menu >
              <Menu.Item
                name='statistiques'
                active={activeItem === 'statistiques'}
                onClick={(e) =>this.handleItemClick(e,"statistiques")}
              />
            </Menu.Menu>
            
            <Menu.Menu >
              <Menu.Item
                name='exercice comptable'
                active={activeItem === 'exercice comptable'}
                onClick={(e) =>this.handleItemClick(e,"exercice comptable")}
              />
            </Menu.Menu>

          </Menu>
        }

        {this.state.loggedCompany.type == "customer" && 
          <Menu pointing secondary className={styles.menuItem}>
            <Menu.Item 
              name='general'
              active={activeItem === 'general'}
              onClick={(e) =>this.handleItemClick(e,"general")}
            />
          </Menu>
        }


        <Segment >
          {this.state.activeItem === "general" &&
            <GeneralScreen />
          }

          {this.state.activeItem === "comptes" &&
            <AccountsScreen />
          }

          {this.state.activeItem === "entreprises clientes" &&
            <CustomersScreen />
          }

          {this.state.activeItem === "statistiques" &&
            <StatisticsScreen />
          }

          {this.state.activeItem === "exercice comptable" &&
            <ExercisesSettings />
          }

        </Segment>
      </div>
    )
  }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
