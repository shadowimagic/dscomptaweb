import React, { Component } from 'react'
import { Card, Popup, Dropdown, Segment, Form, Modal, Header, Dimmer, Loader, Button,Icon, Input, List, Image,TextArea } from 'semantic-ui-react'
import AppIcons from '../../../icons';
import UserServices from '../../../services/UserServices';
import {reactLocalStorage} from 'reactjs-localstorage';
import {Button as CBTN,Intent, Label, Position, Toaster } from "@blueprintjs/core";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import {
  FileData 
} from 'chonky';

import styles from './style.module.css'; 
import TesseractServices from '../../../services/TesseractServices';

interface CustomFileData extends FileData {
  parentId?: string;
  childrenIds?: string[];
}
interface CustomFileMap {
  [fileId: string]: CustomFileData;
}


class ExercisesSettings extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 

    activeItem: 'general',
    isLoading :   false,
    isCreating :  false,
    isUpdating :  false,
    showNewBankModal:false,

    newBankName : '',
    newBankUrl : '',
    newBankUsername : '',
    newBankPassword : '',

    newBankName_error : false,
    newBankUrl_error :  false,
    newBankUsername_error : false,
    newBankPassword_error : false,

    bankFaviconBase : 'https://s2.googleusercontent.com/s2/favicons?domain=',
    exercises : [{year:'2020', closed : true},{year:'2021', closed : false}],


    fileMap:[null],
    fileMapId: "",
    currentFolderId:"",

    baseFileMap:[null],
    rootFolderId:"",

    currentFolderIdRef:null,

    exercise_processing_step : 'Archivage...',
    exercise_processing_state : 'Cette operation peut prendre un peu de temps !',
   
  }
}


componentDidMount(){

 
}
 
toggleNewExerciseModal = (status) => {
  this.setState({ showNewExerciseModal : status });
}

toggleNewExerciseModalCheck = (status) => {
  if(!this.props.mainReduxState.selected_company.id)
  {

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    AppToaster.show({ intent:Intent.WARNING, message: "Veuillez selectionner d'abord un dossier !" });
  
  }
  else{
    this.setState({ showNewExerciseModal : status });
  }
}


_createExercise = () =>
{

  const AppToaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP,
  });

  var selectedCompany = this.props.mainReduxState.selected_company;
  var companyId = selectedCompany.id;
  var currentExercise = selectedCompany.exercise_year;
  var nextExercise = currentExercise + 1;



  var exercise_data = {
    exercise_year  : nextExercise
  }

  this.setState({showNewExerciseModal : false, isLoading : true});

  // GET GED
  UserServices._getCompanyGed(companyId).then((res)=>{
     
    var fsMap = res.data[0].files; //Where Request type
    var fsMapId = res.data[0].id;

    const baseFileMap = (fsMap.fileMap as unknown) as CustomFileMap;
    const rootFolderId = fsMap.rootFolderId;
    
    this.setState({ fileMapId : fsMapId, baseFileMap : baseFileMap, fileMap : baseFileMap, rootFolderId : rootFolderId, currentFolderIdRef:rootFolderId });


  
    var categories = ["Factures Clients","Factures Fournisseurs","Relevés Bancaires"];
    var months = ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"];
    
    // Create for 3 Main categories YEAR folder
    for (let i = 0; i < categories.length; i++) 
    {
      const el = categories[i];
      this.createFolder(currentExercise.toString(), el);

      // Create for each YEAR folder months folders
      var monthParentId = el+ '-' +currentExercise.toString();
      for (let j = 0; j < months.length; j++) {
        const el_m = months[j];
        this.createFolder(el_m, monthParentId);
      }
    }

    // GET BILLS (CUSTOMERS + PROVIDERS)
    TesseractServices._getBills(companyId).then((res)=>{

        this.setState({exercise_processing_step : "Archivage des factures..." , exercise_processing_state : "Patientez encore un peu s'il vous plait !"});

        var bills = res.data;

        bills.forEach(bill => {
          // Create file
          if(bill.type == "customer")
          {
            var real_date = new Date(bill.date);
            var month_name = months[real_date.getMonth()];
            
            var parentId = "Factures Clients-"+currentExercise.toString()+"-"+month_name;
            var fileName = bill.uri;

            this.createFile(fileName, parentId);

          }
          else
          {
            var real_date = new Date(bill.date);
            var month_name = months[real_date.getMonth()];
            
            var parentId = "Factures Fournisseurs-"+currentExercise.toString()+"-"+month_name;
            var fileName = bill.uri;

            this.createFile(fileName, parentId);

          }

        });


        // Get COMPANY BANK STATEMENTS
        UserServices._getAllBankStatements(companyId, currentExercise.toString() ).then((res)=>{
          
          this.setState({exercise_processing_step : "Archivage des relevés bancaires..." , exercise_processing_state : "Plus que quelques instants !"});

          var statements = res.data;
  
          statements.forEach(statement => {
            // Create file
           
              var month_string = statement.month; // month index is stored as string
              var month_name = months[parseInt(month_string)];
              
              var parentId = "Relevés Bancaires-"+currentExercise.toString()+"-"+month_name;
              var fileName = statement.filename;

              var extra = statement.data;

              this.createFile(fileName, parentId, true, extra  );
  
          });


          // UPDATE GED ...
          setTimeout(() => {
                
              var fsMap = {"rootFolderId":this.state.rootFolderId ,"fileMap": this.state.fileMap};

              UserServices._updateGed(this.state.fileMapId, fsMap).then((res)=>{
                  
                  this.setState({exercise_processing_step : "Ouverture de l'exercice..."});

                  
                    UserServices._updateCompany(companyId, exercise_data).then((res)=>{
                       
                        setTimeout(() => {
                          
                          this.setState({exercise_processing_step : "" , exercise_processing_state : "Terminé !"});
                          
                          setTimeout(() => {
                              this.setState({isLoading : false});
                              AppToaster.show({ intent:Intent.SUCCESS, message: "Nouvel exercice ouvert avec succés" });
                              // Wait a bit to redirect to home
                              setTimeout(() => {
                                  window.location.reload();
                              }, 200);

                          }, 300);

                        }, 300);
                       

                      }).catch((err)=>{
                        this.setState({isLoading : false});
                        AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'ouverture de l'exercice !" });
                      })

                  
                 
              })
              .catch((err)=>{
      
                this.setState({isLoading : false});
                AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'ouverture de l'exercice !" });
            
              });

          }, 150);


   
        }).catch((err)=>{
          this.setState({isLoading : false});
          AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'ouverture de l'exercice !" });      
        }) // Company Bank statements





      }).catch((err)=>{
        this.setState({isLoading : false});
        AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'ouverture de l'exercice !" });    
      }) // Company Bills

    }).catch((err)=>{
    // console.log(err);
    this.setState({isLoading : false});
    AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de l'ouverture de l'exercice !" });

  })



}


createFolder = (folderName: string, parentId : string) => {

  const newFileMap = { ...this.state.fileMap };
 
  // Create the new folder
  // Get Here current folder childcount
  var thisId = newFileMap[parentId].id;
  var childCount = newFileMap[parentId].childrenCount;
  var newItemId = "";

  var newChildCount = childCount + 1;

  newItemId = `${thisId}-${folderName}`;
 
  newFileMap[newItemId] = {
      id: newItemId,
      name: folderName,
      color: "#c29399",
      isDir: true,
      modDate: new Date(),
      parentId: parentId,
      childrenIds: [],
      childrenCount: 0,
  };


  // Update parent folder to reference the new folder.
  var parent = newFileMap[parentId];
  newFileMap[parentId] = {
      ...parent,
      childrenIds: [...parent.childrenIds, newItemId],
      childrenCount : newChildCount
  };

  this.setState({fileMap : newFileMap}) // 

  setTimeout(() => {
      // console.log(this.state.fileMap);
  }, 250);
  

}
 


createFile = (fileName: string, parentId : string, has_extra= false, extra = null) => {

  const newFileMap = { ...this.state.fileMap };
 
  // Create the new file
  // Get Here current file childcount
  var thisId = newFileMap[parentId].id;
  var childCount = newFileMap[parentId].childrenCount;
  var newItemId = "";
  var newChildCount = childCount + 1;
  if(childCount == 0)
  {
      newItemId = `${thisId}-new-file-${childCount}`;
  }
  else
  {
      var lastChildIndex = newFileMap[parentId].childrenIds.length - 1;
      var lastChildId = newFileMap[parentId].childrenIds[lastChildIndex];
      var lastCountString = lastChildId.split("-").pop();
      var lastCountInt = parseInt(lastCountString);

      lastCountInt = lastCountInt + 1;
      newItemId = `${thisId}-new-file-${lastCountInt}`;
  }

  if(has_extra)
  {
    newFileMap[newItemId] = {
      id: newItemId,
      name: fileName,
      isDir: false,
      modDate: new Date(),
      parentId: parentId,
      childrenIds: [],
      childrenCount: 0,
      has_extra : true,
      csv_data : extra
    };
  }
  else
  { 
    newFileMap[newItemId] = {
        id: newItemId,
        name: fileName,
        isDir: false,
        modDate: new Date(),
        parentId: parentId,
        childrenIds: [],
        childrenCount: 0,
    };
  }


  // Update parent file to reference the new file.
  var parent = newFileMap[parentId];
  newFileMap[parentId] = {
      ...parent,
      childrenIds: [...parent.childrenIds, newItemId],
      childrenCount : newChildCount
  };

  this.setState({fileMap : newFileMap}) // 

  setTimeout(() => {
      // console.log(this.state.fileMap);
  }, 250);
  

}


render() {
 
    
    return (
    <div className={styles.baseContainer}>
        
      <Dimmer active={this.state.isLoading}>
        <span>{this.state.exercise_processing_state}</span>
        <Loader content={this.state.exercise_processing_step} />
      </Dimmer> 
  
      <Segment placeholder>
        <Header icon>
          <Icon name='calendar alternate outline' />
          Exercice courant : <strong style={{color:'#ab3134'}}> {this.props.mainReduxState.exercise_year}</strong> 
        </Header>
        <Segment.Inline>
          <Button  className={styles.customBtn} primary onClick={()=>this.toggleNewExerciseModalCheck(true)} >Clôturer et Ouvrir un nouvel exercice</Button>
        </Segment.Inline>
      </Segment>
    

    {/* -------------  MODAL CREATE EXERCISE  ---------------------- */}
     
      <Modal
          basic
          onClose={() => this.toggleNewExerciseModal(false)}
          onOpen={() => this.toggleNewExerciseModal(true)}
          open={this.state.showNewExerciseModal}
          size='small' 
        >
          <Header icon>
            <Icon name='calendar outline' />
            Ouverture d'exercice
          </Header>
          <Modal.Content>
            <p>
              Souhaitez-vous débuter l'exercice ? <span style={{fontWeight:'bolder'}}>{this.props.mainReduxState.selected_company.exercise_year + 1}</span> 
            </p>
          </Modal.Content>
          <Modal.Actions>
            <Button basic  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.toggleNewExerciseModal(false)}>
              <Icon name='remove' /> Non
            </Button>
            <Button  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this._createExercise()}>
              <Icon name='checkmark' /> Oui
            </Button>
          </Modal.Actions>
        </Modal>

      {/* --------------------------------------------------- */}



    </div>
   

    
    )
  }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ExercisesSettings);
