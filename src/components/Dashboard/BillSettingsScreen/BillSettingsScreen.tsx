import React, { Component } from 'react'
import { Grid, Form, Button, Divider, Segment, Dimmer, Loader,Input, Checkbox,Radio, Select,TextArea } from 'semantic-ui-react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


import styles from './style.module.css';
import {reactLocalStorage} from 'reactjs-localstorage';
import { Icon, Intent, Label, Position, Toaster } from "@blueprintjs/core";
import UserServices from '../../../services/UserServices';


class BillSettingsScreen extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 
      activeItem: 'general',

      isLoading : false,

      companyName : 'Faseya',
      companyRCS : 'GB123456',
      companyAddress : '34 rue des prunais',
      companyTel : '6304678973',
      companyMail : 'contact@faseya.com',
      companyCity : 'Paris',
      companyPostalCode : '7825',
      companyDesc : 'XYZ : SASU au capital de 1.000.000 F - Immatriculation au RCS de Meaux : 842 264 483 Code APE 6202A - TVA intracommunautaire FR12842264483 www.faseya.com',
      
      companyBillConditions : 'Paiement à 15 jours dès réception de la facture.',
      companyBillLegislation : "Conformément à la loi 92-1442 du 31 décembre 1992, il est précisé qu'il n'est pas accordé d'escompte pour paiement anticipé et qu'au delà de 8 jours après la date de règlement prévue, un intérêt de retard égal à trois fois et demi le taux d'intérêt légal sera facturé. L'indemnité forfaitaire pour frais de recouvrement due en cas de retard de paiement sera de 40 EUR.",

    }
  }

 
  componentDidMount(){

    this.setState({isLoading : true});

    var loggedUser = reactLocalStorage.getObject('ls_loggedUser');
    var companyId = loggedUser.companyId;

    UserServices._getCompanyData(companyId).then((res)=>{

        var company = res.data;

        if(!company.config)
          company.config = {
            address:'',
            tel:'',
            mail:'',
            city:'',
            postalcode:1000,
            desc:'',
            bill_conditions:'',
            bill_legislation:''
          }

        this.setState({
          companyName : company.name,
          companyRCS : company.rcs == "undefined" ? "" : company.rcs,
          companyAddress : company.config.address,
          companyTel : company.config.tel,
          companyMail : company.config.mail,
          companyCity : company.config.city,
          companyPostalCode : company.config.postalcode,
          companyDesc : company.config.desc,
          companyBillConditions : company.config.bill_conditions,
          companyBillLegislation : company.config.bill_legislation,

          isLoading : false
        });

        // Update current redux state
        this.props.mainReduxActions.update_company(company);


         
      }).catch((err)=>{
      console.log(err);
    })

  }
 

  handleCompanyName = (e, companyName) => {
    this.setState({ companyName });
  }

  handleCompanyRCS = (e, companyRCS) => {
    this.setState({ companyRCS });
  }

  handleCompanyAddress = (e, companyAddress) => {
    this.setState({ companyAddress });
  }

  handleCompanyTel = (e, companyTel) => {

    var newValue = companyTel.replace(new RegExp(/[^\d]/,'ig'), "");
    this.setState({ companyTel : newValue });
  }

  handleCompanyMail = (e, companyMail) => {
    this.setState({ companyMail });
  }

  handleCompanyPostalCode = (e, companyPostalCode) => {

    var newValue = companyPostalCode.replace(new RegExp(/[^\d]/,'ig'), "");
    this.setState({ companyPostalCode : newValue });
  }

  handleCompanyCity = (e, companyCity) => {
    this.setState({ companyCity });
  }

  handleCompanyDesc = (e, companyDesc) => {
    this.setState({ companyDesc });
  }

  handleCompanyBillConditions = (e, companyBillConditions) => {
    this.setState({ companyBillConditions });
  }

  handleCompanyBillLegislation = (e, companyBillLegislation) => {
    this.setState({ companyBillLegislation });
  }

  updateCompany = () =>{

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    var company = reactLocalStorage.getObject('ls_loggedCompany');
    var companyId = company.id;

    var config = company.config;
 
    config.desc =    this.state.companyDesc;
    config.bill_legislation = this.state.companyBillLegislation;

    var data = {
      config: config
    }

 
    this.setState({isLoading : true});

    UserServices._updateCompanyConfig(companyId, data).then((res)=>{
      
        this.setState({isLoading : false});
 
        AppToaster.show({ intent:Intent.SUCCESS, message: "Mis à jour réussie !" });

      }).catch((err)=>{
      console.log(err);
    })
  
  }



  render() {

    return (
    <div className={styles.baseContainer}>
        
      <Dimmer active={this.state.isLoading}>
        <Loader content='Chargement...' />
      </Dimmer>

      <div className={styles.secondContainer}>

        {/* <div className={styles.innerContainer} style={{borderRight:"solid 2px gainsboro"}}>
          <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Informations Générales</span>
          <Form style={{width:'90%'}}>
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                value={this.state.companyName}
                onChange={(e) => this.handleCompanyName(e,e.target.value)}
                label="Nom de l'entreprise"
                placeholder="Nom de l'entreprise"
              />
              <Form.Field
                control={Input}
                value={this.state.companyRCS}
                onChange={(e) => this.handleCompanyRCS(e,e.target.value)}
                label="RCS"
                placeholder="RCS"
              />
              
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.companyTel}
                onChange={(e) => this.handleCompanyTel(e,e.target.value)}
                label="Telephone"
                placeholder="+123 45 67 08"
              />
              <Form.Field
                control={Input}
                type="mail"
                value={this.state.companyMail}
                onChange={(e) => this.handleCompanyMail(e,e.target.value)}
                label="E-mail"
                placeholder="contact@entreprise.com"
              />
              
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.companyPostalCode}
                onChange={(e) => this.handleCompanyPostalCode(e,e.target.value)}
                label="Code Postal"
                placeholder="7515"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.companyCity}
                onChange={(e) => this.handleCompanyCity(e,e.target.value)}
                label="Ville"
                placeholder="Ville"
              />
              
            </Form.Group>
           
            <Form.Field
              control={TextArea}
              value={this.state.companyAddress}
              onChange={(e) => this.handleCompanyAddress(e,e.target.value)}
              label="Adresse"
              placeholder="Adresse"
            />
 

          </Form>
        </div> */}

        {/* -------------  RIGHT ---------------- */}

        <div className={styles.innerContainer} style={{alignItems:'flex-end'}}>
          <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Configuration</span>
          <Form style={{width:'90%'}}>
             
            <Form.Field
              style={{width:"94%"}}
              control={TextArea}
              value={this.state.companyDesc}
              onChange={(e) => this.handleCompanyDesc(e,e.target.value)}
              label="Description de l'entreprise"
              placeholder="XYZ : SAS au capital de 1.000.000 F - Immatriculation au RCS de Ville : 842 264 483 Code APE 6202A - TVA intracommunautaire AB12842264483 www.xyz.com"
            />

            {/* <Form.Field
              style={{width:"94%"}}
              control={TextArea}
              value={this.state.companyBillConditions}
              onChange={(e) => this.handleCompanyBillConditions(e,e.target.value)}
              label="Facturation : Conditions et Modalités"
              placeholder="Conditions et Modalités "
            />
              */}
            <Form.Field
              style={{width:"94%", minHeight:90}}
              
              control={TextArea}
              value={this.state.companyBillLegislation}
              onChange={(e) => this.handleCompanyBillLegislation(e,e.target.value)}
              label="Facturation : Legislation"
              placeholder="Lois régissant la facture"
            />
             
          </Form>


        </div>
      </div>
      
      <div className={styles.secondContainer} style={{justifyContent:'flex-end'}}>
        
        <div style={{marginTop:40}}>
          <Button  style={{backgroundColor : '#ab3134'}} positive onClick={this.updateCompany} >SAUVEGARDER</Button>
        </div>

      </div>

    </div>
   

    
    )
  }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}
function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BillSettingsScreen);
