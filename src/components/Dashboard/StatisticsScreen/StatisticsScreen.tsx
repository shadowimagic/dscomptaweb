import React, { Component } from 'react'
import { Statistic,Icon, Image, Segment, Progress } from 'semantic-ui-react'
import AppIcons from '../../../icons';

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import {reactLocalStorage} from 'reactjs-localstorage';


import {
  BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import styles from './style.module.css';
import UserServices from '../../../services/UserServices';
import TesseractServices from '../../../services/TesseractServices';

const data = [
  {
    name: 'Janv', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Fev', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Mars', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Avr', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Mai', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Juin', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Juil', ajoutés: 0, traités: 0, amt: 0,
  },

  {
    name: 'Aout', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Sept', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Oct', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Nov', ajoutés: 0, traités: 0, amt: 0,
  },
  {
    name: 'Dec', ajoutés: 0, traités: 0, amt: 0,
  },
];
 

const width = window.innerWidth * 0.9;

class StatisticsScreen extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 
    activeItem: 'statistiques',
    customers : 0,
    collabs: 0,
    files : data,
    filesCount :0

    }
  }

 
  componentDidMount(){

    this._getCustomersCount();
    this._getFilesCount();
    this._getCollabs();


  }

  
 
  _getCustomersCount = () =>
  {
      this.setState({customers : this.props.mainReduxState.customers.length});
  }
 
  _getFilesCount = () =>
  {
      this.setState({isLoading : true});
  
      var selectedCompany = this.props.mainReduxState.selected_company;
      var selectedCompanyId = selectedCompany.id;
   
      var currentYear = this.props.mainReduxState.exercise_year;
      var dateStringMin = "01/01/"+currentYear.toString(); 
      var dateStringMax = "12/31/"+currentYear.toString();
      var minDate = new Date(dateStringMin).toISOString();
      var maxDate = new Date(dateStringMax).toISOString();

      var monthsNames = ["Janv", "Fev", "Mars", "Avr", "Mai", "Juin", "Juil", "Aout", "Sept", "Oct", "Nov", "Dec"];

      var filesByMonth : any[] = [];
      for (let i = 0; i < 12; i++) {
        
        var monthFiles = {monthIndex : i, name : monthsNames[i], ajoutés: 0, traités: 0, amt: 0};
        filesByMonth.push(monthFiles);
        
      }
  
      TesseractServices._getCompanyExerciseBills(selectedCompanyId,minDate,maxDate).then(resp =>{

        var files = resp.data;


        for (let j = 0; j < files.length; j++) {
          const file = files[j];
          var fileDate = file.date;
          var fileDateMonthIndex = new Date(fileDate).getMonth();

          filesByMonth[fileDateMonthIndex].ajoutés += 1;

          if(file.status == "success" || file.status == "error")
            filesByMonth[fileDateMonthIndex].traités += 1;
          
        }


        this.setState({files : filesByMonth, filesCount : files.length});


      }).catch((err)=>{
      
      });
  
  }

  _getCollabs = () =>
  {
    var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
    var loggedCompanyId = loggedCompany.id;

    UserServices._getCompanyUsers(loggedCompanyId).then(resp =>{

      var collabs = resp.data.length;
      collabs = collabs - 1;

      this.setState({collabs : collabs});

    }).catch((err)=>{

    });
  
  }


  render() {

    
    return (
    <div className={styles.baseContainer}>


      <div style={{display:'flex', width:'100%', marginTop:40, flexDirection:'column'}}>
     
        <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Vue d'ensemble</span>

        <Statistic.Group widths='three' style={{backgroundColor:'#f3f3f3', padding:25, borderRadius:4, border:'solid 1px gainsboro'}}>
          <Statistic>
            <Statistic.Value>{this.state.customers}</Statistic.Value>
            <Statistic.Label>Entreprises Clientes</Statistic.Label>
          </Statistic>

          <Statistic>
            <Statistic.Value>
              <Icon name='file' size='small' />+{this.state.filesCount}
            </Statistic.Value>
            <Statistic.Label>Fichiers</Statistic.Label>
          </Statistic>

          <Statistic>
            <Statistic.Value>
              <Image src='https://react.semantic-ui.com/images/avatar/small/joe.jpg' className='circular inline' />
              {this.state.collabs}
            </Statistic.Value>
            <Statistic.Label>Collaborateurs</Statistic.Label>
          </Statistic>
        </Statistic.Group>

      </div>

      <div style={{display:'flex', width:'100%', marginTop:40, flexDirection:'column'}}>
     
        <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Données des fichiers</span>

        <BarChart
          width={width}
          height={300}
          data={this.state.files}
          margin={{
            top: 5, right: 30, left: 0, bottom: 5,
          }}
        >
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Legend />
          <Bar dataKey="ajoutés" fill="#82ca9d" />
          <Bar dataKey="traités" fill="#8884d8" />
        </BarChart>
  
      </div>

      <div style={{display:'flex', width:'100%', marginTop:40, flexDirection:'column'}}>
     
         <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}></span>
         {/* <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Utilisation Stockage</span> */}
{/*
        <Segment inverted style={{color:'#1a1a1a'}}> 
          <span style={{color:'white', marginBottom:10}}>Votre utilisation : 8Go/50Go </span>
          <Progress percent={13} inverted color='red' progress />
        </Segment>
  */}
      </div>

    </div>
    )
  }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}
function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(StatisticsScreen);
