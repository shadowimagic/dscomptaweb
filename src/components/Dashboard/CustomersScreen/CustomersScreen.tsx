import React, { Component } from 'react'
import { Dropdown, Form, Modal, Header, Dimmer, Loader, Button,Icon, Input, List, Image,TextArea } from 'semantic-ui-react'
import AppIcons from '../../../icons';
import UserServices from '../../../services/UserServices';
import {reactLocalStorage} from 'reactjs-localstorage';
import {Intent, Label, Position, Toaster } from "@blueprintjs/core";


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


import styles from './style.module.css';
import countries from '../../SignupScreen/countries';
import packs from '../../../constants/packs';
const countryOptions = countries;

class CustomersScreen extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 

    activeItem: 'general',
    loggedCompany : {pack : packs[0]},
    isLoading : false,
    isCreating : false,
    isUpdating : false,
    showUserEditionModal : false,
    showCompanyEditionModal : false,
    showCompanyNewModal : false,
    showCompanyDeleteModal : false,
    showUserDeleteModal :false,

    selectedCompanyIndex:0,
    selectedCompany : {},
    selectedUserIndex:0,
    selectedUser : {name:'', surname:'', email:'', password:''},
    companyName : 'faseya',

    newUserName : '',
    newUserSurname : '',
    newUserUsername : '',
    newUserPassword : '',

    selectedUserName : '',
    selectedUserSurname : '',
    selectedUserUsername : '',
    selectedUserPassword : '',
    selectedUserDomainName : '',

    newCompanyName : '',
    newCompanyDomainName : '@mon-entreprise.com',
    newCompanyRCS : '',
    newCompanyAddress : '',
    newCompanyCity : '',
    newCompanyCountry : 'Afghanistan',
    newCompanyCountryDialCode : '+000',
    newCompanyPostalCode : '',
    newCompanyTel :  '',
    newCompanyMail : '',    

    selectedCompanyName : '',
    selectedCompanyRCS : '',
    selectedCompanyAddress : '',
    selectedCompanyCity : '',
    selectedCompanyCountry : 'Afghanistan',
    selectedCompanyCountryDialCode : '+000',
    selectedCompanyPostalCode : '',
    selectedCompanyTel :  '',
    selectedCompanyMail : '',


    passwordVisibility : 'password',


    customers : [],
    users : []
   
  }
}


componentDidMount(){

  // Set from previous local redux state
  this.setState({customers : this.props.mainReduxState.customers});

  // LoggedCompany
  var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');

  if(!loggedCompany.pack)
    loggedCompany.pack = packs[0];

  // console.log(loggedCompany)
  this.setState({loggedCompany : loggedCompany});
  
  // Get remote customers
  this.getCustomers();   
 

  // Get Dial Countries
  this.getSelectedCountryDial(this.state.selectedCompanyCountry);
  this.getNewCountryDial(this.state.newCompanyCountry);

}



getCustomers = () => {
  
  var currentCompany = reactLocalStorage.getObject('ls_loggedCompany');
  var companyId = currentCompany.id;

  this.setState({isLoading : true});

  UserServices._getCompanyAffiliatedCompanies(companyId).then((res)=>{

      var customers = res.data;
      this.setState({
        customers : customers,
      });

      // Update local customers
      this.props.mainReduxActions.update_customers_list(customers);

      // Get First Company Users
      if(customers.length > 0 )
      {
        // this.getCompanyUsers(customers[0].id);
        this.selectCompany(0);
      }
      this.setState({isLoading : false});

      // Update current redux state
      // this.props.mainReduxActions.update_company(company);

    }).catch((err)=>{
      // console.log(err);
      this.setState({
        isLoading : false
      });
  })

}

getCompanyUsers = (companyId) => {
   
  this.setState({isLoading :true});
  UserServices._getCompanyUsers(companyId).then((res)=>{

      var users = res.data;
      console.log(res);

      this.setState({
        users : users,
        isLoading : false
      });
 
    }).catch((err)=>{
      this.setState({
        isLoading : false
      });
      // console.log(err);
  })

}



  //  NEW USER CTRLRS  
  handleNewUserName = (e, newUserName) => {
    this.setState({ newUserName });
  }

  handleNewUserSurname = (e, newUserSurname) => {
    this.setState({ newUserSurname });
  }
 
  handleNewUserUsername = (e, newUserUsername) => {
    this.setState({ newUserUsername });
  }

  handleNewUserPassword = (e, newUserPassword) => {
    this.setState({ newUserPassword });
  }

  //  SELECTED USER CTRLRS  
  handleSelectedUserName = (e, selectedUserName) => {
    this.setState({ selectedUserName });
  }

  handleSelectedUserSurname = (e, selectedUserSurname) => {
    this.setState({ selectedUserSurname });
  }
  
  handleSelectedUserUsername = (e, selectedUserUsername) => {
    this.setState({ selectedUserUsername });
  }

  handleSelectedUserPassword = (e, selectedUserPassword) => {
    this.setState({ selectedUserPassword });
  }

// NEW COMPANY CTRLRS

  getDomainName(companyName:string)
  {
    var domainName = companyName.toLowerCase();
    domainName = domainName.replace(/ /g, '-');
    return domainName;
  }

  handleNewCompanyName = (e, newCompanyName) => {

    var domainName = this.getDomainName(newCompanyName);
    // new RegExp("^[a-zA-Z0-9\s]+$")
    domainName = '@'+domainName+'.com';

    this.setState({ newCompanyName : newCompanyName, newCompanyDomainName : domainName});
  }

  handleNewCompanyRCS = (e, newCompanyRCS) => {
    this.setState({ newCompanyRCS });
  }

  handleNewCompanyAddress = (e, newCompanyAddress) => {
    this.setState({ newCompanyAddress });
  }

  handleNewCompanyTel = (e, newCompanyTel) => {

    var newValue = newCompanyTel.replace(new RegExp(/[^\d]/,'ig'), "");
    this.setState({ newCompanyTel : newValue });
  }

  handleNewCompanyMail = (e, newCompanyMail) => {
    this.setState({ newCompanyMail });
  }

  handleNewCompanyPostalCode = (e, newCompanyPostalCode) => {

    var newValue = newCompanyPostalCode.replace(new RegExp(/[^\d]/,'ig'), "");
    this.setState({ newCompanyPostalCode : newValue });
  }

  handleNewCompanyCity = (e, newCompanyCity) => {
    this.setState({ newCompanyCity });
  }


// EDITION COMPANY CTRLRS

  handleSelectedCompanyName = (e, selectedCompanyName) => {
    this.setState({ selectedCompanyName });
  }

  handleSelectedCompanyRCS = (e, selectedCompanyRCS) => {
    this.setState({ selectedCompanyRCS });
  }

  handleSelectedCompanyAddress = (e, selectedCompanyAddress) => {
    this.setState({ selectedCompanyAddress });
  }

  handleSelectedCompanyTel = (e, selectedCompanyTel) => {

    var newValue = selectedCompanyTel.replace(new RegExp(/[^\d]/,'ig'), "");
    this.setState({ selectedCompanyTel : newValue });
  }

  handleSelectedCompanyMail = (e, selectedCompanyMail) => {
    this.setState({ selectedCompanyMail });
  }

  handleSelectedCompanyPostalCode = (e, selectedCompanyPostalCode) => {

    var newValue = selectedCompanyPostalCode.replace(new RegExp(/[^\d]/,'ig'), "");
    this.setState({ selectedCompanyPostalCode : newValue });
  }

  handleSelectedCompanyCity = (e, selectedCompanyCity) => {
    this.setState({ selectedCompanyCity });
  }  



  togglePasswordVisibility = () => {
    
    var visibility = this.state.passwordVisibility == "password" ? "text" : "password" ;
    this.setState({ passwordVisibility:visibility });

  }

  toggleUserEditionModal = (status:boolean, index:number=-1 ) => {

    if(status)
    //@ts-ignore
      this.setState({selectedUser : this.state.users[index], selectedUserName : this.state.users[index].name, selectedUserSurname : this.state.users[index].surname,selectedUserUsername : this.getUsername(this.state.users[index].email),selectedUserDomainName : this.getUserDomain(this.state.users[index].email),selectedUserPassword : this.state.users[index].password
      });
      
    this.setState({ showUserEditionModal : status, selectedUserIndex:index });
  }

  getUsername(email) {
    
    return email.split('@')[0];
  }

  getUserDomain(email) {
    
    return '@'+email.split('@')[1];
  }

  toggleCompanyEditionModal = (status:boolean, itemIndex:number=-1) => {

    if(status)
    //@ts-ignore
      this.setState({selectedCompany : this.state.customers[itemIndex], 
        selectedCompanyName : this.state.customers[itemIndex].name, 
        selectedCompanyRCS : this.state.customers[itemIndex].rcs,
        selectedCompanyAddress : this.state.customers[itemIndex].config.address,
        selectedCompanyTel : this.state.customers[itemIndex].config.tel,
        selectedCompanyMail : this.state.customers[itemIndex].config.mail,
        selectedCompanyCity : this.state.customers[itemIndex].config.city,
        selectedCompanyCountry : this.state.customers[itemIndex].config.country,
        selectedCompanyCountryDialCode : this.getSelectedCountryDial(this.state.customers[itemIndex].config.country),
        selectedCompanyPostalCode : this.state.customers[itemIndex].config.postalcode
      });

    this.setState({ showCompanyEditionModal : status, selectedCompanyIndex:itemIndex });
  }

  toggleCompanyNewModal = (status) => {
    this.setState({ showCompanyNewModal : status });

    if(status)
      this.getNewCountryDial(this.state.newCompanyCountry);
  }

  toggleCompanyDeleteModal = (status:boolean, itemIndex:number=-1) => {
    this.setState({ showCompanyDeleteModal : status, selectedCompanyIndex:itemIndex});
  }
  
  toggleUserDeleteModal = (status:boolean, itemIndex:number=-1) => {
    this.setState({ showUserDeleteModal : status, selectedUserIndex:itemIndex });
  }

  deleteCompany = () => {

     // API CALLBACK
     //...
      var customer = this.state.customers[this.state.selectedCompanyIndex];
      
      const AppToaster = Toaster.create({
        className: "recipe-toaster",
        position: Position.TOP,
      });

      this.setState({isUpdating : false});

      UserServices._deleteCompany(customer.id).then((res)=>{  
        
        AppToaster.show({ intent:Intent.SUCCESS, message: "Entreprise supprimée avec succés !" });
  
        this.setState({isUpdating : false});
        // Load new data
        this.getCustomers();

      }).catch((err)=>{
        AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la suppression !" });
        this.setState({isUpdating : false});
        console.log(err);
    })

    this.toggleCompanyDeleteModal(false);

  }
  
  deleteUser = () => {

    // API CALLBACK
    //... 

    var user = this.state.users[this.state.selectedUserIndex];
      
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    this.setState({isUpdating : false});

    UserServices._deleteUser(user.id).then((res)=>{  
      
      AppToaster.show({ intent:Intent.SUCCESS, message: "Utilisateur supprimée avec succés !" });

      this.setState({isUpdating : false});
      
      // Load new data
      this.getCompanyUsers(this.state.selectedCompany.id);

    }).catch((err)=>{
      AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la suppression !" });
      this.setState({isUpdating : false});
      console.log(err);
  })

   
  this.toggleUserDeleteModal(false);


 }

 handleSelectedCompanyCountry = (e, {value})=>{

    // console.log(e.target.textContent, value)
    this.setState({selectedCompanyCountry :e.target.textContent})
    // Set dial code
    this.getSelectedCountryDial(e.target.textContent);

  }

  handleNewCompanyCountry = (e, {value})=>{

    // console.log(e.target.textContent, value)
    this.setState({newCompanyCountry :e.target.textContent})
    // Set dial code
    this.getNewCountryDial(e.target.textContent);

  }

 getSelectedCountryDial = (text) =>
  {
    var code = countries.find((e)=>e.text == text)?.dial_code;
    this.setState({selectedCompanyCountryDialCode : code});

    return code;
  } 
  
  getNewCountryDial = (text) =>
  {
    var code = countries.find((e)=>e.text == text)?.dial_code;
    this.setState({newCompanyCountryDialCode : code});

    return code;
  } 

  getCountryKey = (text) =>
  {
    var key = countries.find((e)=>e.text == text)?.key;
     
    return key;
  } 

  selectCompany = (index) => {
    this.setState({ selectedCompanyIndex : index, selectedCompany: this.state.customers[index] });

    // Load new data
    this.getCompanyUsers(this.state.customers[index].id);
  }


  createCompany = () => {


    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    // API CALLBACK
    //...
    var currentCompany = reactLocalStorage.getObject('ls_loggedCompany');

    this.setState({isCreating : true});
    var config = {

      address : this.state.newCompanyAddress,
      tel     :this.state.newCompanyTel,
      mail    :this.state.newCompanyMail,
      country    :this.state.newCompanyCountry,
      city    :this.state.newCompanyCity,
      postalcode    :this.state.newCompanyPostalCode,
      desc    :'',
      bill_conditions   :'',
      bill_legislation  :''
    };

    var data = {
      
      rcs : this.state.newCompanyRCS,
      name : this.state.newCompanyName,
      domain_name : this.state.newCompanyDomainName,
      address : this.state.newCompanyAddress,
      license : currentCompany.id,
      has_owner : true,
      config : config,
      affiliatedId: currentCompany.id,
      type : "customer",
      exercise_year : new Date().getFullYear()

    }
    

    //@ts-ignore
    UserServices._createCompany(data).then((res)=>{  
         // INIT GED
        var company = res.data;

        var archiRootId = `archi-${company.id}`;
        var gedBase = {
          "files": {
            "rootFolderId": archiRootId,
            "fileMap": {
              [archiRootId]: { // Variable Key usage
                "id": archiRootId,
                "name": "Archivage",
                "isDir": true,
                "childrenIds": [
                  "Factures Clients",
                  "Factures Fournisseurs",
                  "Relevés Bancaires",
                  "Autres"
                ],
                "childrenCount": 4
              },
              "Factures Clients": {
                "id": "Factures Clients",
                "name": "Factures Clients",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              },
              "Factures Fournisseurs": {
                "id": "Factures Fournisseurs",
                "name": "Factures Fournisseurs",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              },
              "Relevés Bancaires": {
                "id": "Relevés Bancaires",
                "name": "Relevés Bancaires",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              },
              "Autres": {
                "id": "Autres",
                "name": "Autres",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              }
            }
          },
          "companyId": company.id
        };

      
        // console.log(gedBase);

        UserServices._initGed(gedBase).then(resp =>{
        
          this.setState({isCreating : false});
          
          AppToaster.show({ intent:Intent.SUCCESS, message: "Entreprise cliente ajoutée !" });
  
          // Load new data
          this.getCustomers();

        }).catch((err)=>{
          this.setState({isCreating : false});
          console.log(err);
        })


      }).catch((err)=>{
        this.setState({isCreating : false});
        console.log(err);
    })

    this.toggleCompanyNewModal(false);

  }



  updateCompany = () => {

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    // API CALLBACK
    //... 
    this.setState({isUpdating : true});
    var config = {
      address       :this.state.selectedCompanyAddress,
      tel           :this.state.selectedCompanyTel,
      mail          :this.state.selectedCompanyMail,
      country       :this.state.selectedCompanyCountry,
      city          :this.state.selectedCompanyCity,
      postalcode    :this.state.selectedCompanyPostalCode,
    };

    var data = {
      rcs : this.state.selectedCompanyRCS,
      name : this.state.selectedCompanyName,
      address : this.state.selectedCompanyAddress,
      config : config,
    }

    
    //@ts-ignore
  UserServices._updateCompany(this.state.selectedCompany.id , data).then((res)=>{
      
        this.setState({isUpdating : false});
 
        AppToaster.show({ intent:Intent.SUCCESS, message: "Mis à jour réussie !"});
 
        // Load new data
        this.getCustomers();

      }).catch((err)=>{
        this.setState({isUpdating : false});
        console.log(err);
    })

    this.toggleCompanyEditionModal(false);

  }


  // CREATE USER
  createUser = () => {

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    // API CALLBACK
    //...
    var selectedCompany = this.state.selectedCompany;

    this.setState({isCreating : true});

    var data = {
      name : this.state.newUserName,
      surname : this.state.newUserSurname,
      email : this.state.newUserUsername+this.state.selectedCompany.domain_name,
      password : this.state.newUserPassword,
      companyId: selectedCompany.id,
      type : "customer"
    }

    // console.log(selectedCompany);

    //@ts-ignore
    UserServices._createUser(data).then((res)=>{
      
        // console.log(res);
        this.setState({
          isCreating : false,
          newUserName : '',
          newUserSurname : '',
          newUserUsername : '',
          newUserPassword : ''
        });
            
      
        AppToaster.show({ intent:Intent.SUCCESS, message: "Compte créé avec succés !" });
 
        // Load new data
        this.getCompanyUsers(selectedCompany.id);


      }).catch((err)=>{
        this.setState({
          isCreating : false,
          newUserName : '',
          newUserSurname : '',
          newUserUsername : '',
          newUserPassword : ''
        });
      console.log(err);
    })
   
 }

  // UPDATE USER
  updateUser = () => {

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    // API CALLBACK
    //...
    var selectedCompany = this.state.selectedCompany;

    this.setState({isUpdating : true});

    var data = {
      name : this.state.newUserName,
      surname : this.state.newUserSurname,
      email : this.state.newUserUsername+this.state.selectedCompany.domain_name,
      password : this.state.newUserPassword,
    }

    // console.log(selectedCompany);

    //@ts-ignore
    UserServices._updateUser(data).then((res)=>{
      
        // console.log(res);

        this.setState({
          isUpdating : false,
          newUserName : '',
          newUserSurname : '',
          newUserUsername : '',
          newUserPassword : ''
        });
      
        AppToaster.show({ intent:Intent.SUCCESS, message: "Mis à jour réussie !" });
 
        // Load new data
        this.getCompanyUsers(selectedCompany.id);


      }).catch((err)=>{
        this.setState({
          isUpdating : false,
          newUserName : '',
          newUserSurname : '',
          newUserUsername : '',
          newUserPassword : ''
        });
      console.log(err);
    })

    this.toggleUserEditionModal(false);
   
 }






  render() {

    
    const customers = this.state.customers.map((item :any, index : number) =>

      <List.Item onClick={() => this.selectCompany(index)} style={{paddingTop:10, paddingBottom:10, borderRadius:4, marginBottom:5}} className={ (this.state.selectedCompanyIndex == index ? [styles.companyItem, styles.companyItemActive].join(' ') : styles.companyItem )  } >
        <Image avatar src={AppIcons.ds_signup_select_icon} style={{marginLeft:5}} />
        <List.Content>
          <List.Header>{item.name}</List.Header>
          <span>Entreprise cliente</span>
        </List.Content>
        <List.Content floated='right'>
          <Button.Group basic size='small' style={{marginRight:5}}>
            <Button icon='edit' onClick={() => this.toggleCompanyEditionModal(true, index)} />
            <Button color="red" icon='trash' onClick={() => this.toggleCompanyDeleteModal(true,index)} /> 
          </Button.Group>
        </List.Content>
      </List.Item>
    );

    const users = this.state.users.map((item :any, index : number) =>
      <List.Item style={{paddingTop:10, paddingBottom:10}}>
        <Image avatar src={AppIcons.ds_signin_select_icon} />
        <List.Content>
          <List.Header>{item.name} {item.surname}</List.Header>
          <span>Utilisateur</span>
        </List.Content>
        <List.Content floated='right'>
          <Button.Group basic size='small' style={{marginRight:5}}>
            <Button icon='edit' onClick={() => this.toggleUserEditionModal(true, index)} />
            <Button color="red" icon='trash' onClick={() => this.toggleUserDeleteModal(true,index)} /> 
          </Button.Group>
        </List.Content>
      </List.Item>
    );

    return (
    <div className={styles.baseContainer}>
        
      <Dimmer active={this.state.isLoading}>
        <Loader content='Chargement...' />
      </Dimmer>        
      
      <Dimmer active={this.state.isCreating}>
        <Loader content='Création...' />
      </Dimmer> 

      <Dimmer active={this.state.isUpdating}>
        <Loader content='Mis à jour...' />
      </Dimmer>        

      <div className={styles.secondContainer}>

        <div className={styles.innerContainer} style={{borderRight:"solid 2px gainsboro"}}>
          <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134" }}>Entreprises</span>
          
          <List divided verticalAlign='middle' style={{ backgroundColor:'#f2f2f285', borderRadius : 5, padding:5}}>
            
            {this.state.customers.length == 0 &&
              <span  style={{fontStyle:'italic'}}>Vous avez 0 entreprise cliente</span>
            }
            {this.state.customers.length > 0 &&
              customers
            }
            
           
          </List>
         
          <div style={{marginTop:40, textAlign:'center'}}>
            {/* {this.state.loggedCompany.pack } */}
            {this.state.customers.length < this.state.loggedCompany.pack.access && 
              <Button positive onClick={()=>this.toggleCompanyNewModal(true)} >+ NOUVELLE ENTREPRISE</Button>
            }
            {this.state.customers.length >= this.state.loggedCompany.pack.access && 
              <span style={{color:"#ab3134", textAlign:'center'}}>Vous avez atteint le maximum d'entreprises clientes <br/> dans votre forfait. </span>
            }
          </div>

        </div>

        {/* -------------  RIGHT ---------------- */}

        <div className={styles.innerContainer} style={{alignItems:'flex-start'}}>
          
          {/* ------- PROFILS LIST ------- */}

        <span style={{textDecoration: 'underline', marginBottom:40, fontWeight:'bolder', color:"#AB3134", alignSelf:'flex-start', marginLeft:'4%' }}>Profils de l'entreprise</span>
          <div style={{width:'96%', marginLeft:'4%', marginBottom:15 }}>
            <List divided verticalAlign='middle' style={{ backgroundColor:'#f2f2f285', borderRadius : 5, padding:5}}>
              
              {this.state.users.length == 0 &&
                <span style={{fontStyle:'italic'}}>Aucun compte associé</span>
              }
              {this.state.users.length > 0 &&
                users
              }
            
            </List>
          </div>
          {/* ---------------------------- */}


          
          <span style={{textDecoration: 'underline', marginBottom:20, fontWeight:'bolder', color:"#AB3134", alignSelf:'flex-start', marginLeft:'4%' }}>Ajouter un compte</span>
          
          <Form style={{width:'96%', marginLeft:'4%' }}>
            
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newUserName}
                onChange={(e) => this.handleNewUserName(e,e.target.value)}
                label="Prénom"
                placeholder="Saisir le prénom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newUserSurname}
                onChange={(e) => this.handleNewUserSurname(e,e.target.value)}
                label="Nom"
                placeholder="Saisir le nom"
              />
              
            </Form.Group>

            <Form.Group widths='equal'  style={{display:'flex', width:'67%' ,flexDirection:'column' , marginLeft:0}}>

              <span style={{fontWeight:'bold'}}>Nom d'utilisateur</span>
              <Input 
                label={this.state.selectedCompany.domain_name} 
                labelPosition='right' 
                placeholder='utilisateur' 
                value={this.state.newUserUsername}
              
                onChange={(e) => this.handleNewUserUsername(e,e.target.value)}
                
                />
              
            </Form.Group>
             
            <Form.Group widths='equal'>
             
              <Form.Field
                control={Input}
                type={this.state.passwordVisibility}
                icon={<Icon name='eye' color='black' style={{top:0, right:0}}  link onClick={this.togglePasswordVisibility} />}
                value={this.state.newUserPassword}
                onChange={(e) => this.handleNewUserPassword(e,e.target.value)}
                label="Mot de passe"
                placeholder="Saisir un mot de passe"
              />
              
            </Form.Group>

          </Form>
          
          {this.state.selectedCompany.id &&

            <div style={{marginTop:10, alignSelf:'flex-end'}}>
              <Button  style={{backgroundColor : '#ab3134'}} positive onClick={() => this.createUser()} >CREER LE COMPTE</Button>
            </div>
          }
          {!this.state.selectedCompany.id &&

            <div style={{marginTop:10, alignSelf:'flex-end'}}>
              <Button  style={{backgroundColor : '#ab3134'}} positive disabled >CREER LE COMPTE</Button>
            </div>
          }


       

        </div>
      </div>
      

    {/* -------------  MODAL EDITION COMPANY ---------------------- */}

    <Modal
      onClose={() => this.toggleCompanyEditionModal(false)}
      onOpen={() => this.toggleCompanyEditionModal(true)}
      open={this.state.showCompanyEditionModal}
    >
      <Modal.Header>METTRE A JOUR L'ENTREPRISE</Modal.Header>
      <Modal.Content style={{width:'95%'}}>
         

        <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyName}
                onChange={(e) => this.handleSelectedCompanyName(e,e.target.value)}
                label="Nom de l'entreprise"
                placeholder="Saisir le nom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyRCS}
                onChange={(e) => this.handleSelectedCompanyRCS(e,e.target.value)}
                label="RCS"
                placeholder="Saisir le RCS"
              />
              
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyAddress}
                onChange={(e) => this.handleSelectedCompanyAddress(e,e.target.value)}
                label="Adresse"
                placeholder="Adresse de l'entreprise"
              />

              <div style={{display:'flex', width:'90%' ,flexDirection:'column', marginLeft:7}}>

                <label style={{fontWeight:'bold', marginBottom:2}}>Pays</label>
                <Dropdown style={{padding:0, lineHeight:3, textIndent:5, width:'99%', height:37, border:'solid 1px', marginBottom:8}}
                  placeholder='Selectionner un pays'
                  fluid
                  search
                  selection
                  options={countryOptions} 
                  defaultValue={this.getCountryKey(this.state.selectedCompanyCountry)}
                  //@ts-ignore
                  onChange={this.handleSelectedCompanyCountry}
                />
              </div>

              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyCity}
                onChange={(e) => this.handleSelectedCompanyCity(e,e.target.value)}
                label="Ville"
                placeholder="Ville"
              />
              
            </Form.Group>
            <Form.Group widths='equal'>
              {/* <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyTel}
                onChange={(e) => this.handleSelectedCompanyTel(e,e.target.value)}
                label="Telephone"
                placeholder="123 45 67 80"
              /> */}
              <div style={{display:'flex', width:'100%' ,flexDirection:'column', marginLeft:7}}>
                  <label style={{fontWeight:'bold', marginBottom:3}}>Telephone</label>
                  <Input style={{width:'58%', height:38}} label={this.state.selectedCompanyCountryDialCode} placeholder='Telephone' value={this.state.selectedCompanyTel} onChange={(e) => this.handleSelectedCompanyTel(e,e.target.value)}  />
              </div>

              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyMail}
                onChange={(e) => this.handleSelectedCompanyMail(e,e.target.value)}
                label="E-mail"
                placeholder="contact@exemple.com"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedCompanyPostalCode}
                onChange={(e) => this.handleSelectedCompanyPostalCode(e,e.target.value)}
                label="Code Postal"
                placeholder="Code Postal"
              />


            </Form.Group>


          </Form>


      </Modal.Content>
      <Modal.Actions>
        <Button  color='black' onClick={() => this.toggleCompanyEditionModal(false)}>
          Annuler
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}}
          content="Mettre à jour"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.updateCompany()}
          positive
        />
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}


    {/* -------------  MODAL CREATION COMPANY ---------------------- */}
    
    <Modal
      onClose={() => this.toggleCompanyNewModal(false)}
      onOpen={() => this.toggleCompanyNewModal(true)}
      open={this.state.showCompanyNewModal}
    >
      <Modal.Header>AJOUTER UNE NOUVELLE ENTREPRISE</Modal.Header>
      <Modal.Content style={{width:'95%'}}>
         

        <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyName}
                onChange={(e) => this.handleNewCompanyName(e,e.target.value)}
                label="Nom de l'entreprise"
                placeholder="Saisir le nom"
              />
               {/* <Form.Field
                control={Input}
                className={styles.domainName}
                type="text"
                value={this.state.newCompanyDomainName}
                label="Nom de domaine"
                placeholder="@mon-entreprise.com"
              /> */}

              <div style={{display:'flex', width:'70%', height:40, marginTop:'3%', alignItems:'center', fontWeight:'bolder', color:'#AB3134'}}>
                <span style={{marginRight:5}}> &#10151; </span>
                <span>{this.state.newCompanyDomainName}</span>
              </div>
               
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyRCS}
                onChange={(e) => this.handleNewCompanyRCS(e,e.target.value)}
                label="RCS"
                placeholder="Saisir le RCS"
              />
              
            </Form.Group>
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyAddress}
                onChange={(e) => this.handleNewCompanyAddress(e,e.target.value)}
                label="Adresse"
                placeholder="Adresse de l'entreprise"
              />

              <div style={{display:'flex', width:'90%' ,flexDirection:'column', marginLeft:7}}>

                <label style={{fontWeight:'bold', marginBottom:2}}>Pays</label>
                <Dropdown style={{padding:0, lineHeight:3, textIndent:5, width:'99%', height:37, border:'solid 1px', marginBottom:8}}
                  placeholder='Selectionner un pays'
                  fluid
                  search
                  selection
                  options={countryOptions} 
                  defaultValue='AF'
                  //@ts-ignore
                  onChange={this.handleNewCompanyCountry}
                />
              </div>

              <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyCity}
                onChange={(e) => this.handleNewCompanyCity(e,e.target.value)}
                label="Ville"
                placeholder="Ville"
              />
              
            </Form.Group>
            <Form.Group widths='equal'>
              {/* <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyTel}
                onChange={(e) => this.handleNewCompanyTel(e,e.target.value)}
                label="Telephone"
                placeholder="123 45 67 80"
              /> */}

              <div style={{display:'flex', width:'100%' ,flexDirection:'column', marginLeft:7}}>
                  <label style={{fontWeight:'bold', marginBottom:3}}>Telephone</label>
                  <Input style={{width:'58%', height:38}} label={this.state.newCompanyCountryDialCode} placeholder='Telephone' value={this.state.newCompanyTel} onChange={(e) => this.handleNewCompanyTel(e,e.target.value)}  />
              </div>

              <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyMail}
                onChange={(e) => this.handleNewCompanyMail(e,e.target.value)}
                label="E-mail"
                placeholder="contact@exemple.com"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.newCompanyPostalCode}
                onChange={(e) => this.handleNewCompanyPostalCode(e,e.target.value)}
                label="Code Postal"
                placeholder="Code Postal"
              />


            </Form.Group>


          </Form>


      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleCompanyNewModal(false)}>
          Annuler
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}}
          content="Ajouter l'entreprise"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.createCompany()}
          positive
        />
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}


    {/* -------------  MODAL EDITION USER ---------------------- */}


    <Modal
      onClose={() => this.toggleUserEditionModal(false)}
      onOpen={() => this.toggleUserEditionModal(true)}
      open={this.state.showUserEditionModal}
    >
      <Modal.Header>MODIFIER LE PROFIL</Modal.Header>
      <Modal.Content style={{width:'95%'}}>
         

        <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedUserName}
                onChange={(e) => this.handleSelectedUserName(e,e.target.value)}
                label="Prénom"
                placeholder="Saisir le prénom"
              />
              <Form.Field
                control={Input}
                type="text"
                value={this.state.selectedUserSurname}
                onChange={(e) => this.handleSelectedUserName(e,e.target.value)}
                label="Nom"
                placeholder="Saisir le nom"
              />
              
            </Form.Group>

            <Form.Group widths='equal'  style={{display:'flex', width:'67%' ,flexDirection:'column' , marginLeft:0}}>

              <span style={{fontWeight:'bold'}}>Nom d'utilisateur</span>
              <Input 
                label={this.state.selectedUserDomainName} 
                labelPosition='right' 
                placeholder='utilisateur' 
                value={this.state.selectedUserUsername}
              
                onChange={(e) => this.handleSelectedUserUsername(e,e.target.value)}
                
                />
              
            </Form.Group>
             
            <Form.Group widths='equal'>
             
              <Form.Field
                control={Input}
                type={this.state.passwordVisibility}
                icon={<Icon name='eye' color='black' style={{top:0, right:0}}  link onClick={this.togglePasswordVisibility} />}
                value={this.state.selectedUserPassword}
                onChange={(e) => this.handleSelectedUserPassword(e,e.target.value)}
                label="Mot de passe"
                placeholder="Saisir un mot de passe"
              />
              
            </Form.Group>

          </Form>


      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleUserEditionModal(false)}>
          Annuler
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}}
          content="Mettre à jour"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this.updateUser()}
          positive
        />
      </Modal.Actions>
    </Modal>


    {/* --------------------------------------------------- */}

    {/* ------------------DELETE COMPANY MODAL------------- */}

    <Modal
      basic
      onClose={() => this.toggleCompanyDeleteModal(false)}
      onOpen={() => this.toggleCompanyDeleteModal(true)}
      open={this.state.showCompanyDeleteModal}
      size='small' 
    >
      <Header icon>
        <Icon name='trash' />
        Suppression entreprise cliente
      </Header>
      <Modal.Content>
        <p>
          Souhaitez-vous vraiment supprimer le compte de cette entreprise cliente ainsi que tous ses comptes utilisateurs associés ?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.toggleCompanyDeleteModal(false)}>
          <Icon name='remove' /> Non
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.deleteCompany()}>
          <Icon name='checkmark' /> Oui
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}

    {/* ------------------DELETE USER MODAL------------- */}

    <Modal
      basic
      onClose={() => this.toggleUserDeleteModal(false)}
      onOpen={() => this.toggleUserDeleteModal(true)}
      open={this.state.showUserDeleteModal}
      size='small' 
    >
      <Header icon>
        <Icon name='trash' />
        Suppression compte utilisateur
      </Header>
      <Modal.Content>
        <p>
          Souhaitez-vous vraiment supprimer ce compte utilisateur ?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.toggleUserDeleteModal(false)}>
          <Icon name='remove' /> Non
        </Button>
        <Button  style={{backgroundColor : '#ab3134'}} inverted onClick={() => this.deleteUser()}>
          <Icon name='checkmark' /> Oui
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}



    </div>
   

    
    )
  }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CustomersScreen);
