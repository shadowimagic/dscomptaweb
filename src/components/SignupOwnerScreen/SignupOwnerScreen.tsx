import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../global.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../icons';

import routes from '../../constants/routes.json';
// import { history } from '../../store';

import UserServices from '../../services/UserServices';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';
import {history} from '../../redux/store';

const height = window.innerHeight;
//const history = useHistory();


 
class SignupOwnerScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {
      screen: "splash",
      splash_fade : 1,
      input_user_type : 1,

      input_username : "",
      input_usersurname : "",
      input_usermail : "",
      input_password : "",
      display_errors : false,
      disable_signin_btn : false,
      signup_status : "default",
 
      
    }

  }
   
 
  componentDidMount()
  {
  
  }

  handleUsername(value : string){

    this.setState({input_username : value})
  }  

  handleUsersurname(value : string){

    this.setState({input_usersurname : value})
  }  

  handleUsermail(value : string){

    this.setState({input_usermail : value})
  }
  handlePassword(value : string){

    this.setState({input_password : value})
  }

  goBack()
  {
    history.replace(routes.SIGNUP);
  }
  
  
  signup = ()=>
  {
      var companyId = this.props.mainReduxState.created_company.id;
      var data = {
        name: this.state.input_username,
        surname: this.state.input_usersurname,
        email: this.state.input_usermail,
        password: this.state.input_password,
        type : "owner",
        companyId : companyId
      }

    if(data.email != "" && data.password != "" && data.name != "" && data.surname != "")
    {
       this.setState({
         display_errors : false,
         disable_signin_btn :true,
         signup_status : "creating"
      });

      //  console.log(data);
       UserServices._signup(data).then((resp:any) =>{

          this.setState({
            disable_signin_btn :false,
            signup_status : "success"
          });

          // Setup logged user
          let createdUser = resp.data;
          this.props.mainReduxActions.update_user(createdUser);
          
          // Setup tab link
          // this.props.mainReduxActions.update_tab("settings");

          UserServices._setCompanyOwnership(companyId).then((resp:any) =>{});

          setTimeout(()=>{
              history.replace(routes.HOME)
          },1100)
        

        }).catch((err)=>{
          
          this.setState({
            disable_signin_btn :false,
            signup_status : "error"
          });
          console.log(err);

        });
    }
    else
    {
      this.setState({display_errors : true});
    }

  }


 render() {

// ANIMATIONS PROPS


  return (
    <div className={styles.container} data-tid="container" style={{height : (height ) , backgroundImage: `url(${AppIcons.ds_backg_2})`, backgroundSize: "cover", backgroundRepeat:"no-repeat" }}>
       
       <div className={globalStyles.appBar} style={{display:"flex", width:"100%",backgroundColor : "#cd858d", maxHeight:40}}>
        {/* <img src={Img} className={globalStyles.appBarLogo} alt=""/> */}
        <img src={AppIcons.ds_logo_min} className={globalStyles.appBarLogo}  alt="" />
        {/* <span style={{fontFamily : 'Poppins', color:"#111111", fontSize:12, fontWeight:"bold"}}> jjj</span> */}

 
          <div style={{display :"flex", alignItems:"center", color:"#ffffff", marginRight : "auto", marginLeft : 30, cursor :"pointer"}} onClick={()=>{ this.goBack()}}>
            <span style={{marginRight : 10, fontWeight : "bolder"}}>&#8672;</span>
            <span style={{fontSize : 14, fontWeight : "bolder"}}>Retour</span>
          </div>

        <div className={globalStyles.appBarOptsBox}>

          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#4caf50"}}></div>
          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#ffc107"}}></div>
          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#f44336"}}></div>

        </div>

      </div>

      <div style={{display :"flex" , marginTop : "10%"}}>

       
       <div className={styles.appContentContainerRight} style={{backgroundColor : "#ffffffe0"}} >
         <span style={{fontSize : 12, color : "#AB3134",fontWeight:"bolder" }}>CREEZ VOTRE COMPTE</span>
         <div style={{width : 30, height:4, backgroundColor : "#8E295F", marginBottom : 20}}></div>

        <div className={styles.appContainerRightFields} >
           
          {/* Field */}
          <div className={styles.appContainerRightField}>
            {/* <span>Date</span> */}
            <input className={globalStyles.appInputBase} placeholder="Votre prenom" value={this.state.input_username} onChange={(e:any)=>this.handleUsername(e.target.value)} type="text" style={{backgroundColor : "#ffffff85"}}
  />
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            {/* <span>Date</span> */}
            <input className={globalStyles.appInputBase} placeholder="Votre nom" value={this.state.input_usersurname} onChange={(e:any)=>this.handleUsersurname(e.target.value)} type="text" style={{backgroundColor : "#ffffff85"}}
  />
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            {/* <span>Date</span> */}
            <input className={globalStyles.appInputBase} placeholder="Votre identifiant" value={this.state.input_usermail} onChange={(e:any)=>this.handleUsermail(e.target.value)} type="text" style={{backgroundColor : "#ffffff85"}}
  />
          </div>

          {/* Field */}
          <div className={styles.appContainerRightField}>
            {/* <span>N° de Piéce</span> */}
            <input className={globalStyles.appInputBase} placeholder="Mot de passe" value={this.state.input_password} onChange={(e:any)=>this.handlePassword(e.target.value)} type="text" style={{backgroundColor : "#ffffff85"}}
  />
          </div>

          
          <div className={styles.appContainerBtn}>
            <button className={(this.state.disable_signin_btn ? styles.signinDisabled : styles.signinEnabled)} onClick={this.signup} >TERMINER</button>
          </div>

          {/* {this.state.signup_status == "default" &&
            <span style={{color : "#111111",fontFamily:"Poppins", fontSize:14, fontWeight : "bolder"}}>Pas encore de compte ? <span onClick={()=>{history.replace(routes.SIGNUP) }} style={{color:"#AB3134", cursor:"pointer"}}> Vous inscrire</span> </span> 
          } */}
          
          <div style={{display : "flex", flexDirection : "column", alignContent:"center", alignItems:"center" }}>
            
            {this.state.signup_status == "creating" &&
              <img src={AppIcons.ds_signup_loading} style={{width : 100, opacity : 0.6}}  alt="" />
            }
            {this.state.signup_status == "creating" &&
              <span style={{color : "#111111",fontFamily:"Poppins", fontSize:12, fontWeight : "bolder"}}>Création en cours... </span> 
            }
            {this.state.signup_status == "error" &&
              <span style={{color : "#D63737",fontFamily:"Poppins", fontSize:12, fontWeight : "bolder"}}>Une erreur est survenue. Réessayez </span> 
            }
            {this.state.signup_status == "success" &&
              <span style={{fontFamily:"Poppins", fontSize:12, fontWeight : "bolder", color : "#43C057"}}>Compte créé avec succés ! </span> 
            }
            </div>

        </div>

       </div>
      
      </div>

    </div>
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupOwnerScreen);
