import AppIcons from '../../icons';
const bankFaviconBase = 'https://s2.googleusercontent.com/s2/favicons?domain=';

const banksList = [

        {
            text: "BICIG (Gabon)",
            value: "BICIG (Gabon)",
            key: "BICIG (Gabon)",
            link: "https://www.bicignet.net/part/fr/dciweb.htm?p0=idesai.tht&t=p",
            image: { avatar: true, src: bankFaviconBase+"https://www.bicignet.net/part/fr/dciweb.htm?p0=idesai.tht&t=p" },
        },
        {
            text: "ORABANK",
            value: "ORABANK",
            key: "ORABANK",
            link: "https://www.orabank.net/fr/compte-en-ligne",
            image: { avatar: true, src: bankFaviconBase+"https://www.orabank.net/fr/compte-en-ligne" },
        },
        {
            text: "SGBS (Senegal)",
            value: "SGBS (Senegal)",
            key: "SGBS (Senegal)",
            link: "https://connect.societegenerale.sn/webplus/#/",
            image: { avatar: true, src: bankFaviconBase+"https://connect.societegenerale.sn/webplus/#/" },
        },
        {
            text: "SGBCI (Côte d'Ivoire)",
            value: "SGBCI (Côte d'Ivoire)",
            key: "SGBCI (Côte d'Ivoire)",
            link: "https://connect.societegenerale.ci/webplus/#/",
            image: { avatar: true, src: bankFaviconBase+"https://connect.societegenerale.ci/webplus/#/" },
        },
         
        {
            text: "ECOBANK",
            value: "ECOBANK",
            key: "ECOBANK",
            link: "https://www.gtpsecurecard.com/CASHXPRESS/",
            image: { avatar: true, src: bankFaviconBase+"https://www.gtpsecurecard.com/CASHXPRESS/" },
        },
        {
            text: "BOA",
            value: "BOA",
            key: "BOA",
            link: "https://boaweb.of.africa/users/authentication",
            image: { avatar: true, src: bankFaviconBase+"https://boaweb.of.africa/users/authentication" },
        },
        
        

]


export default banksList;