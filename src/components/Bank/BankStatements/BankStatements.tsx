import React, { Component } from 'react'
import { Dropdown, Popup,Table, Icon,Select, Form, Button, Divider, Segment, Menu, Dimmer, Loader,Input } from 'semantic-ui-react';
import { Button as CBTN } from "@blueprintjs/core";

import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import { motion } from "framer-motion";

import FileViewer from 'react-file-viewer';

import styles from './style.module.css';
import { Intent, Label, Position, Toaster } from "@blueprintjs/core";
import UserServices from '../../../services/UserServices';
import BankStatementsContainer from './BankStatementsContainer';
import Emitter from '../../../services/emitter';
import { emit } from 'process';
import AppIcons from '../../../icons';
import TesseractServices from '../../../services/TesseractServices';

class BankStatements extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 
      activeItem: 'Janvier',

      isLoading : false,
      isImporting : false,
      preview_right_pos : -110,
      display : false,
      selectedStatement : {},
      selectedStatementData :[],
      companyName : '',
      companyRCS : '',
      companyAddress : '',
      companyTel : '',
      companyMail : '',
      companyCity : '',
      companyPostalCode : '',
      mediahost: "https://dscompta-api.herokuapp.com/files/",
      
      current_month_index : 0,
      current_month_name : "Janvier",
      months_list_visibility : "none",

      months : ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
     
      exercises_loading : false,
      current_exercise : "2021",
      exercises : [{ key: '2020', value: '2020', text: '2020', closed:true }, { key: '2021', value: '2021', text: '2021', closed:false }],
      selectedExercise :{}
    }
  }

 
  componentDidMount(){


    // Set current Month 
    var currentMonthIndex = new Date().getMonth();

    this.setState({isLoading : true, selectedExercise : this.state.exercises[1],
                  
                  current_month_index : currentMonthIndex,
                  current_month_name : this.state.months[currentMonthIndex],
                  activeItem : this.state.months[currentMonthIndex]
    });


  }
 

_parseCSV(str) {
    var rows = str.split('\n');
  
    return rows.reduce(function(table, row) {
      var cols = row.split(',');
  
      table.push(cols.map(function(c) {
        return c.trim();
      }));
  
      return table;
    }, []);
  }

_uploadStatement = () =>{

  var self = this;
  var input = document.createElement('input');
  input.type = 'file';
  input.name = 'file';
  input.accept = '.pdf';
                 
  input.onchange = (e) => { 
    //@ts-ignore
    var file = e.target.files[0]; 
    var fr=new FileReader(); 
    fr.onload=function(){ 

      // console.log(fr.result); 
      const AppToaster = Toaster.create({
          className: "recipe-toaster",
          position: Position.TOP,
          });

      var base64 : any;
      base64 = fr.result;

      var ext = file.name.split(/\.(?=[^\.]+$)/)[1];

      if(ext == "pdf")
      {

        self.setState({isImporting : true});

          // var csv_json = {"rows" : csv_array};
          var data = {
            filename : file.name.split(/\.(?=[^\.]+$)/)[0],
            month : self.state.current_month_index.toString(),
            year : self.props.mainReduxState.exercise_year.toString(),
            companyId : self.props.mainReduxState.selected_company.id
          }

          // console.log(data);
          self._render(data, base64);
      
      }
      else
      {
        AppToaster.show({ intent:Intent.WARNING, message: "Veuillez importer un fichier pdf svp !" });
      }



    } 
      
    fr.readAsDataURL(file); 

    // var formData = new FormData();
    // formData.append('file',file);

   

  }

  input.click();
  
}



_dataURLtoFile = (dataurl, filename)  => {
 
  var arr = dataurl.split(','),
      mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), 
      n = bstr.length, 
      u8arr = new Uint8Array(n);
      
  while(n--){
      u8arr[n] = bstr.charCodeAt(n);
  }
  
  return new File([u8arr], filename, {type:mime});
}

_render = (_data, base64) =>
{
    let self = this;
    var url = base64;
    var PDFJS = window['pdfjs-dist/build/pdf'];

    PDFJS.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';

    var loadingTask = PDFJS.getDocument(url);
    
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
      });

    loadingTask.promise.then(function(pdf) {
    
    var canvasdiv = document.getElementById("canvas_renderer");
    var totalPages = pdf.numPages

    // for (let pageNumber = 1; pageNumber <= totalPages; pageNumber++) {
    let pageNumber = 1;    
    pdf.getPage(pageNumber).then((page) => {

        var scale = 3;
        var viewport = page.getViewport({ scale: scale });

        var canvas = document.createElement('canvas');
        canvas.classList.add("renderer")
        canvasdiv?.appendChild(canvas);

        // Prepare canvas using PDF page dimensions
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = { canvasContext: context, viewport: viewport };

        var renderTask = page.render(renderContext);
        renderTask.promise.then(() =>{
            // console.log(canvas.toDataURL('image/png'));
            var _uri = canvas.toDataURL('image/png');

            // var file = self._dataURLtoFile(_uri, _data.filename+'.png');
            var file = self._dataURLtoFile(_uri, _data.filename+'.png');
            var formData = new FormData();
            formData.append('files',file);
            

            TesseractServices._uploadFile(formData).then(resp =>{

              var savedFile = resp.data.files[0];
              _data.filename = savedFile.filename;

              console.log(resp);

              //////////////// CREATE BANK STATEMENT /////////////////////

              UserServices._createBankStatement(_data).then(resp =>{
            
                self.setState({isImporting : false});
    
                  AppToaster.show({ intent:Intent.SUCCESS, message: "Relevé ajouté avec succés !" });
                  
                  // Get New Data
                  setTimeout(() => {
            
                    Emitter.emit(Emitter.types.CHANGE_EXERCISE_YEAR, self.props.mainReduxState.exercise_year);
                    
                  }, 150);
      
                }).catch((err)=>{
                
                  console.log(err);
                  self.setState({isImporting : false});
                  AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de l'import. Réessayez svp !" });
              });



              ////////////////////////////////////////////////////////////



            }).catch((err)=>{
            
              self.setState({isImporting : false});
              AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de l'import. Réessayez svp !" });

            });


        });
      });
    // }

    }, function(reason) {
      // PDF loading error
      console.error(reason);
    });
}


  handleSelectedExercise = (e, {value})=>{

    var exoIndex = this.state.exercises.findIndex(e => e.value == value);

    this.setState({selectedExercise : this.state.exercises[exoIndex]});
    // this.getSelectedCustomer(value);
    setTimeout(() => {
    
      Emitter.emit(Emitter.types.CHANGE_EXERCISE_YEAR, value);
      
    }, 150);
  }  


  handleItemClick = (e, name) => {

    this.setState({ activeItem: name })
    Emitter.emit(Emitter.types.CHANGE_EXERCISE_YEAR, this.props.mainReduxState.exercise_year)
  }


  onStatementClicked = (statement : any) => {

    this.setState({ selectedStatement: statement })
    
    setTimeout(() => {
      
      // console.log(statement);
      this.setState({ display: true, preview_right_pos : 0 })
      
    }, 150);
  }

  getFileType(filename:string)
  {
    var real_filename = filename.split(/_(.+)/)[1];
   
    //@ts-ignore
    var real_filename_ext = real_filename.split(/\.(?=[^\.]+$)/)[1];

    return real_filename_ext;
    
  }

  hidePreview()
  {
   
    var preview_pos = -110;
    this.setState({
      preview_right_pos : preview_pos,
      display : false
    });
    
  }

  toggleMonthsList(e:Event,month : number, change_month : boolean = true)
  {
    e.stopPropagation();
    e.preventDefault();
    var visibility = this.state.months_list_visibility == "block" ? "none" : "block";
    this.setState({ activeItem: this.state.months[month], current_month_index : month, current_month_name : this.state.months[month],  months_list_visibility : visibility});
 
  
    // if(change_month)
    // { 
    //     Emitter.emit(Emitter.types.CHANGE_EXERCISE_YEAR, this.props.mainReduxState.exercise_year);
    // } 

  }

  downloadExcel = () => 
  {
    let t2e = new Table2Excel('#statement_table', 'exported_to_excel');
    t2e.export();
  }

  render() {

    const monthsList = this.state.months.map((item :any, index : number) =>
    <div onClick={(e:any)=> {this.toggleMonthsList(e,index)}} key={index} className={styles.monthItem}>
      <span>{item}</span> 
    </div>
    );
     
    const pageItems = this.state.months.map((item :any, index:number) =>
      <div style={{display : this.state.activeItem === item ? 'block' : 'none'}}>
          <BankStatementsContainer month={index} exercise_year={this.state.selectedExercise.value} exercise_closed={this.state.selectedExercise.closed} onStatementClicked={this.onStatementClicked} />
      </div>
    );

    
    const statementHeader = this.state.selectedStatementData.map((item :any, index : number) =>{
      if(index == 0) // Only the header
        return(
        <Table.Row >
          <Table.HeaderCell>N°</Table.HeaderCell>
            {
              item.map((item2 :any, index2 : number) => {
                return (
                  <Table.HeaderCell>{item2}</Table.HeaderCell>
                );
            })}

        </Table.Row>
        
        )
       }
    );
    
    const statementOps = this.state.selectedStatementData.map((item :any, index : number) =>{
      if(index != 0) // All except the header
        return(
        <Table.Row >
          <Table.Cell > {index} </Table.Cell>
            {
              item.map((item2 :any, index2 : number) => {
                return (
                  <Table.Cell>{item2}</Table.Cell>
                );
            })}

        </Table.Row>
        
        )
       }
    );


    return (
    <div className={styles.baseContainer}>
        
      <Dimmer active={this.state.isImporting}>
        <Loader content='Importation...' />
      </Dimmer>
 
      <div>
       
        <div className={styles.appInnerFilterContainer}>

          <div className={styles.appInnerFilterBox}>
            <div className={styles.appInnerMonthsFilterContainer}>
              <span style={{flex : 3,fontFamily:'Poppins', color:"#14142B", fontSize:14, fontWeight : "bold", paddingLeft:5, paddingRight:5}}>{this.state.current_month_name}</span>
              <img  onClick={(e:any)=> {this.toggleMonthsList(e,this.state.current_month_index, false)}} src={AppIcons.ds_filter_dropper_icon} style={{height:"80%", width:"auto", cursor:"pointer"}} alt="" />
              <div style={{width:1, height:"100%", backgroundColor:"black", marginLeft:5, marginRight:5}}></div>
              <span style={{flex : 2,fontFamily:'Poppins',color:"#14142B", fontSize:14, fontWeight : "bolder"}}>{new Date().getFullYear()}</span>
              <motion.div animate={{display : this.state.months_list_visibility}} className={styles.appInnerMonthsListContainer}>
                {monthsList}
              </motion.div>
            </div>
          </div>

          <div className={styles.appContainerBtn}>
            <button className={styles.signinEnabled}  onClick={()=>this._uploadStatement()}  >IMPORTER UN RELEVE
              <Icon style={{marginLeft:10}} name="download" />
            </button>
            
          </div>

        </div>

        <Segment style={{height:'100%', border:'none', boxShadow:'none',padding:0, marginTop:5}}>
          {pageItems}
        </Segment>

      </div>


        {/* RIGHT BLOC */}

        <motion.div animate={{right : this.state.preview_right_pos+'%'}} 
                      transition={{ ease: "easeOut"}}
              className={styles.appInnerMiddlePreviewContainer}>
              <div style={{position:"absolute", left:-50, top:0}}>
                <img src={AppIcons.ds_preview_folder_banner} style={{width:56, height:"auto"}} alt="" />
                <img src={AppIcons.ds_preview_folder_icon} 
                style={{  position: "absolute",
                          width: 34,
                          left: "20%",
                          top: "18%",
                          height: "auto", cursor:"pointer"}}
                          onClick={()=>{this.hidePreview()}}
                          alt="" />
              </div>
              <div id="preview_file_container" className={styles.previewFileContainer}>
                
                  {/* <CBTN rightIcon="download" intent="success" color="#ffffff" text="VERS EXCEL" style={{position:'absolute', top:5, right:5, backgroundColor : '#ab3134'}} onClick={this.downloadExcel} /> */}
                  
                  <div id="pdf_preview" style={{display:'flex', flexDirection:'column', width:'100%', margin:'auto', marginTop:25}}>

                    {this.state.display &&

                      <img src={this.state.mediahost+this.state.selectedStatement.filename} style={{width:"100%", height:"auto"}} alt=""/>
                        
                    }
                  </div>
              </div>
          </motion.div>


    </div>
   

    
    )
  }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}
function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BankStatements);
