import React, { Component } from 'react'
import { Card, Icon, Table,Popup, Image, Tab, Grid, Form, Button, Divider, Segment, Menu, Dimmer, Loader,Input } from 'semantic-ui-react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


import styles from './style.module.css';
import { Button as CBTN, Intent, Label, Position, Toaster } from "@blueprintjs/core";
import UserServices from '../../../services/UserServices';
import AppIcons from '../../../icons';
import TesseractServices from '../../../services/TesseractServices';
import Emitter from '../../../services/emitter';
import InfiniteScroll from 'react-infinite-scroller';


const height = window.innerHeight;

class BankStatementsContainer extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 
      activeItem: 'Janvier',

      isLoading : false,
      isImporting : false,
      has_more : false,

      companyName : '',
      companyRCS : '',
      companyAddress : '',
      companyTel : '',
      companyMail : '',
      companyCity : '',
      companyPostalCode : '',
      
      months : ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"],
      statements : [],
    }
  }

 
  componentDidMount(){

    this._loadBankStatements();

    Emitter.on(Emitter.types.CHANGE_EXERCISE_YEAR, (newYear)=>this._loadBankStatements())
  }
 
 
_loadBankStatements = () =>
{
    this.setState({isLoading : true});

    var selectedCompany = this.props.mainReduxState.selected_company;
    var selectedCompanyId = selectedCompany.id;

    var currentMonth = this.props.month.toString();
    var currentYear = this.props.mainReduxState.exercise_year;



    UserServices._getBankStatements(selectedCompanyId, currentMonth, currentYear).then(resp =>{
      
      var statements = resp.data;
      // console.log(statements);

      this.setState({isLoading : false, statements : statements})

    }).catch(err=>{
      const AppToaster = Toaster.create({
        className: "recipe-toaster",
        position: Position.TOP,
      });
  
      this.setState({isLoading : false});

      AppToaster.show({ intent:Intent.WARNING, message: "Erreur de chargement. Réessayez svp !" });

    })
 
}
  
  onStatementClicked = (statement : any) => {

    this.props.onStatementClicked(statement);
  }

  formatDate(item:any)
  {
    var real_date = new Date(item.date);
    var real_date_string = real_date.toLocaleDateString();
    return real_date_string.replaceAll('/', '-');
    
  }
  
  formatName(filename:string)
  {
    var real_filename = filename 
    return real_filename;
    
  }
  

  render() {
     
  
    const filesItems = this.state.statements.map((item :any, index : number) =>

        <Table.Row style={{cursor:'pointer'}} onClick={()=>this.onStatementClicked(item)} >
          <Table.Cell > <img src={AppIcons.ds_statements_icon} style={{width:25, height:'auto'}} alt=""/> </Table.Cell>
          <Table.Cell>{this.formatName(item.filename)}</Table.Cell>
          <Table.Cell>{this.formatDate(item)}</Table.Cell>
        
          <Table.Cell collapsing textAlign='right'> 
             <Popup trigger={<Button  onClick={(e)=>e.stopPropagation()} style={{backgroundColor : '#ffffff',border : 'solid 1px #AB3134', color : '#AB3134'}} icon='ellipsis horizontal' />} on='click'>
                <Button.Group basic size='small'>
                  {/* TRASH */}
                  <Popup content='Supprimer' inverted trigger={<Button color='red' icon='trash' onClick={(e) => {} } />} />
                </Button.Group>
               
             </Popup>
          </Table.Cell>
        </Table.Row>
    );


    return (
    <div className={styles.baseContainer} style={{height:'100%'}}>
        
      <Dimmer active={this.state.isLoading}>
        <Loader content='Chargement...' />
      </Dimmer>        
      
      <Dimmer active={this.state.isImporting}>
        <Loader content='Importation...' />
      </Dimmer>

        <div style={{display:'flex', width: '100%',flexDirection:'column', alignItems:'center', height:'80%', overflowY:'scroll', paddingBottom:0}}>
            <InfiniteScroll
                style={{width:'100%', height: (height * 0.62) }}
                pageStart={0}
                loadMore={()=>{}}
                hasMore={this.state.has_more}
                loader={<img src={AppIcons.ds_spinner_icon} style={{width:46, height:"auto", marginTop:20}} alt="" />}
                useWindow={false}
                >
                  <Table singleLine>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Aperçu</Table.HeaderCell>
                        <Table.HeaderCell>Titre</Table.HeaderCell>
                        <Table.HeaderCell>Date d'ajout</Table.HeaderCell>
                         
                        <Table.HeaderCell collapsing textAlign='right'> <Icon name='th list' /> </Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body style={{height:100}}>
                      {filesItems}
                    </Table.Body>
                  </Table>
           
                </InfiniteScroll>
                {!this.state.has_more && this.state.statements.length == 0 &&
                  <span style={{marginTop:'16%', marginBottom:'16%'}}>Aucun relevé à afficher</span>
                }
              </div>

    </div>
   

    
    )
  }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}
function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(BankStatementsContainer);
