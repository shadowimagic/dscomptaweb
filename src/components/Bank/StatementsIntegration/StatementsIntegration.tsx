import React, { Component } from 'react'
import { Card, Popup, Dropdown, Item, Form, Modal, Header, Dimmer, Loader, Button,Icon, Input, List, Image,TextArea } from 'semantic-ui-react'
import AppIcons from '../../../icons';
import UserServices from '../../../services/UserServices';
import {Button as CBTN,Intent, Label, Position, Toaster } from "@blueprintjs/core";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import {reactLocalStorage} from 'reactjs-localstorage';


import styles from './style.module.css';
import banksList from '../banksList'; 
import countries from '../../SignupScreen/countries';
 
class StatementsIntegration extends Component<any, any> {
  constructor(props:any){
    super(props)

    this.state = { 

    activeItem: 'general',
    loggedCompany : {},
    isLoading :   false,
    isCreating :  false,
    isUpdating :  false,
    showNewBankModal:false,

    newBankName : '',
    newBankUrl : '', 

    newBankName_error : false,
    newBankUrl_error :  false, 
 
    showSelectedBankModal:false,
    showDeleteBankModal:false,
    bankToDeleteId : '',
 
    selectedBankId : '',
    selectedBankName : '',
    selectedBankUrl : '', 

    selectedBankName_error : false,
    selectedBankUrl_error :  false,
    selectedBankUsername_error : false,
    selectedBankPassword_error : false,

    bankFaviconBase : 'https://s2.googleusercontent.com/s2/favicons?domain=',
    addableBank : {},
    selected_addable_bank : 'BOA',
    availableBanks : [],
    banks : [],
    bank_key : 'ECOBANK',
   
  }
}


componentDidMount(){
  
  var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
  this.setState({loggedCompany : loggedCompany});
  
  this._loadBanks();

}

_loadBanks = () =>
{
    this.setState({isLoading : true});

    var selectedCompany = this.props.mainReduxState.selected_company;
    var selectedCompanyId = selectedCompany.id;

    UserServices._getBankCards(selectedCompanyId).then(resp =>{
      
      var banks = resp.data;

      var availableBanks : any[] = [];

      for (let i = 0; i < banksList.length; i++) 
      {
        var b = banksList[i];
        if(!banks.find((e)=>e.name == b.key))
        {
          availableBanks.push(b);        
        }
      }
      // console.log(availableBanks);

      this.setState({isLoading : false, banks : banks,
      // Later set a filter from received data 
      availableBanks : availableBanks
      })

    }).catch(err=>{
      const AppToaster = Toaster.create({
        className: "recipe-toaster",
        position: Position.TOP,
      });
  
      AppToaster.show({ intent:Intent.WARNING, message: "Erreur de chargement. Réessayez svp !" });

    })
 
}
 
_gotoIntegration = (url : string) =>
{
  window.open(url);
}




_createBank = () =>
{
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    if(this.state.newBankName_error || this.state.newBankUrl_error || this.state.newBankUsername_error || this.state.newBankPassword_error ||  this.state.newBankName.trim() == "" || this.state.newBankUrl.trim() == "" || this.state.newBankUsername.trim() == "" || this.state.newBankPassword.trim() == "")
    {
      AppToaster.show({ intent:Intent.WARNING, message: "Veuillez remplir tous les champs !" });
    }
    else
    {

      // var currentCompany = reactLocalStorage.getObject('ls_loggedCompany');
      // var companyId = currentCompany.id;

      var selectedCompany = this.props.mainReduxState.selected_company;
      var selectedCompanyId = selectedCompany.id;
    
      var data = {
        name : this.state.newBankName,
        url : this.state.newBankUrl,
        companyId : selectedCompanyId
      }

      // console.log(data);

      UserServices._createBankCard(data).then(resp =>{

        this.setState({showNewBankModal : false,
          newBankName : '',
          newBankUrl  : '',
        });

        AppToaster.show({ intent:Intent.SUCCESS, message: "Banque ajoutée avec succés !" });

        // Load next
        this._loadBanks();

      }).catch(err=>{

        this.setState({showNewBankModal : false});

        AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de la création. Réessayez svp !" });

      })

    }


}



_addBank = () =>
{
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    if(!this.state.addableBank.key)
    {
      AppToaster.show({ intent:Intent.WARNING, message: "Veuillez selectionner une banque !" });
    }
    else
    {
 
      var selectedCompany = this.props.mainReduxState.selected_company;
      var selectedCompanyId = selectedCompany.id;
    
      var data = {
        name : this.state.addableBank.key,
        url : this.state.addableBank.link,
        companyId : selectedCompanyId
      }

      // console.log(data);

      UserServices._createBankCard(data).then(resp =>{

        this.setState({showNewBankModal : false,
          addableBank : {}
        });

        AppToaster.show({ intent:Intent.SUCCESS, message: "Banque ajoutée avec succés !" });

        // Load next
        this._loadBanks();

      }).catch(err=>{

        this.setState({showNewBankModal : false});

        AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de la création. Réessayez svp !" });

      })

    }


}

_editBank = (item: any) =>
{
  this.setState({
      selectedBankId : item.id,
      selectedBankName : item.name,
      selectedBankUrl : item.url
  })

  this.toggleSelectedBankModal(true);

}


_updateBank = () =>
{
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    if(this.state.newBankName_error || this.state.newBankUrl_error || this.state.newBankUsername_error || this.state.newBankPassword_error)
    {
      AppToaster.show({ intent:Intent.WARNING, message: "Veuillez remplir tous les champs !" });
    }
    else
    {

       
      var data = {
        name : this.state.selectedBankName,
        url : this.state.selectedBankUrl
      }

      // console.log(data);

      UserServices._updateBankCard(this.state.selectedBankId, data).then(resp =>{

        this.setState({showSelectedBankModal : false});

        AppToaster.show({ intent:Intent.SUCCESS, message: "Banque mise à jour !" });

        // Load next
        this._loadBanks();

      }).catch(err=>{

        this.setState({showSelectedBankModal : false});

        AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de l'édition. Réessayez svp !" });

      })

    }


}
 
_triggerDeleteBank = (item: any) =>
{
  this.setState({bankToDeleteId : item.id});
  this.toggleDeleteBankModal(true);
}

_deleteBank = () =>
{
  
  const AppToaster = Toaster.create({
    className: "recipe-toaster",
    position: Position.TOP,
  });
 
  this.setState({isLoading : true, showDeleteBankModal : false});

  UserServices._deleteBankCard(this.state.bankToDeleteId).then(resp =>{

    this.setState({isLoading : false});

    AppToaster.show({ intent:Intent.SUCCESS, message: "Banque supprimée !" });

    // Load next
    this._loadBanks();

  }).catch(err=>{

    this.setState({isLoading : false});

    AppToaster.show({ intent:Intent.WARNING, message: "Erreur lors de la suppression. Réessayez svp !" });

  })

}

toggleNewBankModal = (status) => {
  this.setState({ showNewBankModal : status });
}

toggleSelectedBankModal = (status) => {
  this.setState({ showSelectedBankModal : status });
}

toggleDeleteBankModal = (status) => {
  this.setState({ showDeleteBankModal : status });
}

handleNewBankName = (e, newBankName) => {
 
  this.setState({ newBankName : newBankName.toUpperCase()});

  if(newBankName.trim().length == 0)
  {
    this.setState({newBankName_error : { content: 'Champ vide', pointing: 'below' }});
  }
  else
  {
    this.setState({newBankName_error : false});
  }
}

handleNewBankUrl = (e, newBankUrl) => {
  this.setState({ newBankUrl });

  if(newBankUrl.trim().length == 0)
  {
    this.setState({newBankUrl_error : { content: 'Champ vide', pointing: 'below' }});
  }
  else
  {
    this.setState({newBankUrl_error : false});
  }
}


handleSelectedBankName = (e, selectedBankName) => {
 
  this.setState({ selectedBankName : selectedBankName.toUpperCase()});

  if(selectedBankName.trim().length == 0)
  {
    this.setState({selectedBankName_error : { content: 'Champ vide', pointing: 'below' }});
  }
  else
  {
    this.setState({selectedBankName_error : false});
  }
}

handleSelectedBankUrl = (e, selectedBankUrl) => {
  this.setState({ selectedBankUrl });

  if(selectedBankUrl.trim().length == 0)
  {
    this.setState({selectedBankUrl_error : { content: 'Champ vide', pointing: 'below' }});
  }
  else
  {
    this.setState({selectedBankUrl_error : false});
  }
}

handleSelectedBank = (e:any)=>{

  // e.stopPropagtion();

  var text = e.target.textContent;

  var addableBank = this.state.availableBanks.find(e=>e.value == text);
  this.setState({addableBank : addableBank})
 
}  
 
 
 
render() {
 
     
    const userBanks = this.state.banks.map((item :any, index:number) =>

        <Item key={index} style={{border:'solid 1px gainsboro', padding:5, marginBottom:5}}>
          <Item.Image size='tiny' src={this.state.bankFaviconBase+item.url} />
                                  
          <Item.Content verticalAlign='middle'>
            <Item.Header>
              {item.name}
            </Item.Header>
            <div style={{float:'right'}}>
              <Button animated='vertical'>
                <Button.Content hidden onClick={()=>this._triggerDeleteBank(item)}>Supprimer</Button.Content>
                <Button.Content visible>
                  <Icon name='trash' />
                </Button.Content>
              </Button>
              <Button icon="download" content="Intégrer relevé" labelPosition="right" style={{backgroundColor : '#ab3134', color:'#ffffff', marginTop:1}}  onClick={()=>this._gotoIntegration(item.url)}  />
            </div>
          </Item.Content>
        </Item>
      );


    return (
    <div className={styles.baseContainer}>
        
      <Dimmer active={this.state.isLoading}>
        <Loader content='Chargement...' />
      </Dimmer>        

      <div style={{display:'flex', width:'100%', flexDirection:'column', height:'100%'}}>
        
      {/* {this.state.loggedCompany.type == "customer" && */}
        <div style={{display:'flex', flex:1, marginBottom:5}}>
          <Form style={{width:'30%', marginTop:0}}>
            

            <Form.Select style={{padding: 1, display: 'flex',alignItems: 'center'}}
              fluid 
              options={this.state.availableBanks}
              // value={this.state.bank_key}
              placeholder="Selectionner une banque pour l'ajouter"
              onChange={(e)=>this.handleSelectedBank(e)}
            />


          </Form>
 

          <div className={styles.appContainerBtn} style={{position:'relative', zIndex:3, marginLeft:-6}}>
            <button className={styles.signinEnabled} style={{backgroundColor:'#ab3134', color:'#ffffff', borderTopLeftRadius:0, borderBottomLeftRadius:0}}  onClick={()=>this._addBank() }  >AJOUTER 
              <Icon style={{marginLeft:10}} name="add" />
            </button>
            
          </div>

          <div className={styles.appContainerBtn} style={{marginLeft:15}}>
            <button className={styles.signinEnabled}  onClick={()=>this.toggleNewBankModal(true)}  >CREER UNE BANQUE 
              <Icon style={{marginLeft:5}} name="arrow right" />
            </button>
            
          </div>

        </div>

      {/* } */}

        <div style={{display:'flex', flex:1, marginBottom:5, marginTop:20}}>

        {this.state.banks.length == 0 &&
          <div style={{display:'flex', width:'100%', alignItems:'center' , flexDirection:'column', padding:'100px 0px'}}>
            <Image src={AppIcons.ds_bank_icon} style={{width:40}} />
            <span style={{marginTop:15}}>Aucune banque configurée. </span>
          </div>
        }
        {this.state.banks.length > 0 &&

          <div style={{display:'flex', width:'100%', flexDirection:'column'}}>
            <div style={{display:'flex', width:'100%', flexDirection:'column', textTransform:'uppercase', marginBottom:15}}>
              <h4>Banques ajoutées</h4>
            </div>

            <div style={{display:'flex', width:'100%', flexDirection:'column'}}>
              <Item.Group>
                {userBanks}
              </Item.Group>
            </div>
          </div>
        }
      </div>
    
    </div>

    {/* -------------  MODAL CREATE BANK ---------------------- */}
    
    <Modal
      onClose={() => this.toggleNewBankModal(false)}
      onOpen={() => this.toggleNewBankModal(true)}
      open={this.state.showNewBankModal}
    >
      <Modal.Header>CREER UNE NOUVELLE BANQUE</Modal.Header>
      <Modal.Content style={{width:'95%'}}>
         
        <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                error={this.state.newBankName_error}
                control={Input}
                type="text"
                value={this.state.newBankName}
                onChange={(e) => this.handleNewBankName(e,e.target.value)}
                label="Nom de la banque"
                placeholder="Saisir le nom"
              />
 
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Field
                error={this.state.newBankUrl_error}
                control={Input}
                type="text"
                value={this.state.newBankUrl}
                onChange={(e) => this.handleNewBankUrl(e,e.target.value)}
                label="Lien de la banque"
                placeholder="https://www.ma-banque.com/connexion"
              />

              <div style={{display:'flex', width:'50%', alignItems:'flex-end'}}>
                <img style={{visibility: this.state.newBankUrl == '' ? 'hidden' : 'visible', width:35,height:35, border:'solid 3px gainsboro'}} src={this.state.bankFaviconBase+this.state.newBankUrl} alt=""/>
              </div>
              
            </Form.Group>

          </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleNewBankModal(false)}>
          Annuler
        </Button>
        <Button style={{backgroundColor : '#ab3134'}}
          content="Ajouter la banque"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this._createBank()}
          positive
        />
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}


    {/* -------------  MODAL UPDATE BANK ---------------------- */}
    
    <Modal
      onClose={() => this.toggleSelectedBankModal(false)}
      onOpen={() => this.toggleSelectedBankModal(true)}
      open={this.state.showSelectedBankModal}
    >
      <Modal.Header>METTRE A JOUR LA BANQUE</Modal.Header>
      <Modal.Content style={{width:'95%'}}>
         
        <Form style={{width:'90%'}}>
            
            <Form.Group widths='equal'>
              <Form.Field
                error={this.state.selectedBankName_error}
                control={Input}
                type="text"
                value={this.state.selectedBankName}
                onChange={(e) => this.handleSelectedBankName(e,e.target.value)}
                label="Nom de la banque"
                placeholder="Saisir le nom"
              />
 
            </Form.Group>

            <Form.Group widths='equal'>
              <Form.Field
                error={this.state.selectedBankUrl_error}
                control={Input}
                type="text"
                value={this.state.selectedBankUrl}
                onChange={(e) => this.handleSelectedBankUrl(e,e.target.value)}
                label="Lien de la banque"
                placeholder="https://www.ma-banque.com/connexion"
              />

              <div style={{display:'flex', width:'50%', alignItems:'flex-end'}}>
                <img style={{visibility: this.state.selectedBankUrl == '' ? 'hidden' : 'visible', width:35,height:35, border:'solid 3px gainsboro'}} src={this.state.bankFaviconBase+this.state.selectedBankUrl} alt=""/>
              </div>
              
            </Form.Group>

          </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleSelectedBankModal(false)}>
          Annuler
        </Button>
        <Button
          content="Mettre à jour"
          labelPosition='right'
          icon='checkmark'
          onClick={() => this._updateBank()}
          positive
        />
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}




    {/* ------------------DELETE BANK MODAL------------- */}

    <Modal
      basic
      onClose={() => this.toggleDeleteBankModal(false)}
      onOpen={() => this.toggleDeleteBankModal(true)}
      open={this.state.showDeleteBankModal}
      size='small' 
    >
      <Header icon>
        <Icon name='trash' />
        Suppression de la banque
      </Header>
      <Modal.Content>
        <p>
          Souhaitez-vous vraiment supprimer cette banque ?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button basic color='red' inverted onClick={() => this.toggleDeleteBankModal(false)}>
          <Icon name='remove' /> Non
        </Button>
        <Button color='green' inverted onClick={() => this._deleteBank()}>
          <Icon name='checkmark' /> Oui
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}





    </div>
   

    
    )
  }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatementsIntegration);
