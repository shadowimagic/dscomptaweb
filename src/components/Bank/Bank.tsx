import React, { Component } from 'react'
import { Menu, Segment } from 'semantic-ui-react'
import BankStatementsIntegration from './BankStatementsIntegration/BankStatementsIntegration';
import BankStatements from './BankStatements/BankStatements';
import SettingsScreen from './SettingsScreen/SettingsScreen';

import styles from './style.module.css';


export default class Bank extends Component {
  state = { activeItem: 'relevés bancaires' }

  handleItemClick = (e,  name) => {
    
    this.setState({ activeItem: name });
  
  };

  componentDidMount(){

  }
 
  render() {
    const { activeItem } = this.state

    return (
      <div style={{width:'100%' , textAlign:'left', height:'100%'}}>
        <Menu pointing secondary className={styles.menuItem}>
          <Menu.Item 
            name='relevés bancaires'
            active={activeItem === 'relevés bancaires'}
            onClick={(e) =>this.handleItemClick(e,"relevés bancaires")}
          />
          <Menu.Item
            name='intégration des relevés'
            active={activeItem === 'intégration des relevés'}
            onClick={(e) =>this.handleItemClick(e,"intégration des relevés")}
          />

          <Menu.Item
            name='paramétrage'
            active={activeItem === 'paramétrage'}
            onClick={(e) =>this.handleItemClick(e,"paramétrage")}
          />
          
          {/* <Menu.Menu >
            <Menu.Item
              name='paramétres'
              active={activeItem === 'paramétres'}
              onClick={(e) =>this.handleItemClick(e,"paramétres")}
            />
          </Menu.Menu> */}
        </Menu>

        <Segment >
          {this.state.activeItem === "relevés bancaires" &&
            <BankStatements />
          }

          {this.state.activeItem === "intégration des relevés" &&
            <BankStatementsIntegration />
          }

          {this.state.activeItem === "paramétrage" &&
            <SettingsScreen />
          }

          {/* {this.state.activeItem === "paramétres" &&
            <SettingsScreen />
          } */}

        </Segment>
      </div>
    )
  }
}