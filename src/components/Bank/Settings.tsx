import React, { Component } from 'react'
import { Menu, Segment, Container } from 'semantic-ui-react'
import StatementsIntegration from './StatementsIntegration/StatementsIntegration';
import BankStatements from './BankStatements/BankStatements';
import SettingsScreen from './SettingsScreen/SettingsScreen';

import styles from './style.module.css';
import ExercisesSettings from '../Dashboard/ExercisesSettings/ExercisesSettings';


export default class Settings extends Component {
  state = { activeItem: 'nouveaux dossiers' }

  handleItemClick = (e,  name) => {
    
    this.setState({ activeItem: name });
  
  };

  componentDidMount(){

  }
 
  render() {
    const { activeItem } = this.state
    return (
      <div style={{display:'flex', width:'100%' , textAlign:'left', height:'100%'}}>
        <div style={{display:'flex'}}>
          <Menu  pointing secondary vertical className={styles.menuItem}>
            <Menu.Item 
              color='brown' 
              name='nouveaux dossiers'
              active={activeItem === 'nouveaux dossiers'}
              onClick={(e) =>this.handleItemClick(e,"nouveaux dossiers")}
            />
            <Menu.Item 
              color='brown'
              name="paramétres d' accés"
              active={activeItem === "paramétres d'accés"}
              onClick={(e) =>this.handleItemClick(e,"paramétres d'accés")}
            />
             
            <Menu.Item 
              color='brown'
              name='exercice comptable'
              active={activeItem === 'exercice comptable'}
              onClick={(e) =>this.handleItemClick(e,"exercice comptable")}
            /> 
            
            <Menu.Item 
              color='brown'
              name='paramétres généraux'
              active={activeItem === 'paramétres généraux'}
              onClick={(e) =>this.handleItemClick(e,"paramétres généraux")}
            /> 
          </Menu>
        </div>
        <div style={{display:'flex'}}>
          <Segment style={{border:'none', boxShadow:'none'}} >
            {this.state.activeItem === "nouveaux dossiers" &&
              <Container>
                <p>
                   NOUVEAUX DOSSIERS
                </p>
              </Container>
            }

            {this.state.activeItem === "paramétres d'accés" &&
              <Container>
                <p>
                  PARAMETRES D'ACCES
                </p>
              </Container>
            }

            {this.state.activeItem === "exercice comptable" &&
              <ExercisesSettings />
            }

            {this.state.activeItem === "paramétres généraux" &&
              <Container>
                <p>
                  PARAMETRES GENERAUX
                </p>
              </Container>
            }

          </Segment>
        
        </div>

      </div>
    )
  }
}