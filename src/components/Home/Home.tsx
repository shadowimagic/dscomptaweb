import React, { Component } from 'react';
// import 'bootstrap/dist/css/bootstrap.css';
// import 'semantic-ui-css/semantic.css'
import globalStyles from '../global.module.css';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
import {history} from '../../redux/store';
import routes from '../../constants/routes.json';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';

import { Position, Tooltip } from "@blueprintjs/core";
import { Form, Segment, Modal,Button, Card, Label,List, Image, Item, Icon } from 'semantic-ui-react'


// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../icons';
import {reactLocalStorage} from 'reactjs-localstorage';

// Screens
import MainScreen from "../screens/MainScreen/MainScreen";
import BoxScreen from "../screens/BoxScreen/BoxScreen";
import ProcessingScreen from "../screens/ProcessingScreen/ProcessingScreen";
import GedScreen from "../screens/GedScreen/GedScreen";
import BankScreen from "../screens/BankScreen/BankScreen";
import BillScreen from "../screens/BillScreen/BillScreen";
import SettingsScreen from "../screens/SettingsScreen/SettingsScreen";
import StatsScreen from '../screens/StatsScreen/StatsScreen';
import Emitter from '../../services/emitter';
import packs from '../../constants/packs';

import { IdleSessionTimeout } from "idle-session-timeout";
import UpgradePartial from '../partials/UpgradePartial/UpgradePartial';

const packsInitial = packs; 

const height = window.innerHeight;
//const history = useHistory();
const MAX_MINS_INACTIVITY = 25; // mins
const TRIAL_DAYS = 14;

 
class Home extends Component<any, any> {
  processMenuRef: HTMLDivElement | null = null;
  constructor(props:any)
  {
    super(props)
    
    this.state = {
      screen: "main",
      username : null,
      isLoading : false,
      menubar_focuser_top :5,
      menubar_index : 0,
      display_results : false,
      selected_file : {},
      loggedCompany : {},
      trial_expired : false,
      screens : [
        { name : "main" },
        { name : "box" },
        { name : "processing" },
        { name : "ged" },
        { name : "bank" },
        { name : "devis" },
        { name : "stats" },
        { name : "settings" }
      ],
      submenu : [
        {
          title : "Factures Clients",
          route : "route"
        },
        {
          title : "Factures Fournisseurs",
          route : "route"
        },
        {
          title : "Documents Compta",
          route : "route"
        },
        {
          title : "Dossier XY",
          route : "route"
        }
      ],

      showChangePackModal : false,
      showValidatePackModal : false,
      packs : [],
      license_checking : false,
      input_license_key : '',
      input_license_key_error : false,

    }

    this.goBackToProcessScreen = this.goBackToProcessScreen.bind(this);
    this.onDisplayResultsEmitted = this.onDisplayResultsEmitted.bind(this);

  }
   
  

  componentDidMount()
  {

    var _screen = this.props.mainReduxState.active_tab;
    var index = this.state.screens.indexOf(this.state.screens.find(e=>e.name == _screen));
    // console.log(index);
    window.addEventListener('contextmenu', function (e) { 
      // do something here... 
      e.preventDefault(); 
    }, false);

    // Packs
    var _packs : any[] = [];
    for (let i = 1; i < packsInitial.length; i++) {
      const el = packsInitial[i];
      _packs.push(el);
    }

    this.setState({packs : _packs});



    this.setState({
      // menubar_focuser_top : pos,
      menubar_index : index,
      screen: _screen
    });



    // GET DATA FROM LOCALSTORAGE
    // Check if user loggedIn
    if(reactLocalStorage.getObject('ls_loggedUser').id != null && reactLocalStorage.getObject('ls_loggedUser').id != undefined)
    {
       var loggedUser = reactLocalStorage.getObject('ls_loggedUser');
       var currentCompany = reactLocalStorage.getObject('ls_loggedCompany');
       this.props.mainReduxActions.update_user(loggedUser);
       this.props.mainReduxActions.update_company(currentCompany);
      

      // Logged company type presets
      if(currentCompany.type == "customer")
      {
        //Update local redux state
        this.props.mainReduxActions.update_selected_company(currentCompany);
        this.props.mainReduxActions.update_exercise_year(currentCompany.exercise_year);
      }

      this.setState({ loggedCompany: currentCompany });



      // Trial version check
      if(currentCompany.pack.name == "Trial")
        this.getTrialLeftDays(currentCompany);

    }
    else
    {
      history.replace(routes.SIGNIN)
    }


    // ----------------Session ----------------------
 
    //time out in X mins of inactivity
    let session = new IdleSessionTimeout(MAX_MINS_INACTIVITY * 60 * 1000);
    
    session.onTimeOut = () => {
    // here you can call your server to log out the user
        this.unlog();
    };

    session.start();

    // -------------------------------------------------

  }

  

  getTrialLeftDays = (company)  => 
  {
    var startDate = new Date(company.created_at);
    var maxDate = startDate;
    maxDate.setDate(maxDate.getDate() + TRIAL_DAYS);
  
    var currentDate = new Date();
    const diffDays=(a,b)=>{
      return (a - b)/864e5|0
    };
  
    var leftDays = diffDays(maxDate, currentDate) + 1;
    var expired = false;
    if(company.pack.name == "Trial") // Only if trial
    {
      if(leftDays < 0)
      {
        expired = true; 
      }
    }
    
    this.setState({trial_expired : expired})
  }  

  onMenubar = (event : Event, index : number) => {

    event.stopPropagation();
    var item = event.target;
    // @ts-ignore
    var pos = item.offsetTop;
    this.setState({
        menubar_focuser_top : pos,
        menubar_index : index,
        screen: this.state.screens[index].name
      });
    
  }

  onGotoScreen = (index : number) =>{
    
    let menuFocuser = document.getElementById('menubar_focuser');
    let menuFocuserHeight = menuFocuser?.clientHeight || 38;
    let margin = 15;
    if( index == 0 ) margin = 5;
    let pos = (margin) + (menuFocuserHeight * index);

    this.setState({
      menubar_focuser_top : pos,
      menubar_index : index,
      screen: this.state.screens[index].name
    });
  }

  onDisplayResultsEmitted = (status : boolean) => {
     
     this.setState({display_results : status});
  }
  

  goBackToProcessScreen()
  {
      this.setState({display_results : false});
  }

  unlog()
  {
    // Reset localstorage data
    reactLocalStorage.clear();
    history.replace(routes.SIGNIN)
  }

  gotoMain = () =>{
    history.replace(routes.HOME);
    this.onGotoScreen(0);
  }

  onDismissPopover = (e:Event)=>{
    e.preventDefault();
    e.stopPropagation();
    Emitter.emit(Emitter.types.CLOSE_POPOVER, "");
  }


  toggleChangePackModal = (status:boolean) => {

    this.setState({ showChangePackModal : status});
  }

  toggleValidatePackModal = (status:boolean) => {

    this.setState({ showValidatePackModal : status, showChangePackModal : false});
  }
  

  handleLicenseActivationKey = (value : string) => {
 
    this.setState({input_license_key : value})
    if(value.trim().length == 24)
    {
      this.setState({input_license_key_error : false })
    }
    else
    {
      this.setState({input_license_key_error : { content: 'Licence invalide', pointing: 'below' }  })
    }
  
  }

  activateLicense = () => {
 
  }
  
  

 render() {

// ANIMATIONS PROPS


  const packsList = this.state.packs.map((item :any, index : number) =>

  <Card style={{flex:1}}>
    {item.name == "Standard" &&
      <Image src={AppIcons.ds_pack_1_icon} wrapped ui={false} />
    }
    {item.name == "Premium" &&
      <Image src={AppIcons.ds_pack_2_icon} wrapped ui={false} />
    }
    {item.name == "Entreprise" &&
      <Image src={AppIcons.ds_pack_3_icon} wrapped ui={false} />
    }
    <Card.Content>
      <Card.Header>{item.name}</Card.Header>
      <Card.Meta>
        <span style={{fontWeight:'bolder', fontSize:18, color:"#ab3134"}}>{item.price ? item.price + "/mois" : "Devis" }</span>
      </Card.Meta>
      <Card.Description>
          <List>
            
            <List.Item>
              <List.Icon name="universal access" />
              <List.Content>Nombre d'accés : {item.access ? item.access : "A déterminer"} </List.Content>
            </List.Item>                
            <List.Item>
              <List.Icon name={item.ds_scan ? 'check' : "close"} />
              <List.Content>Dok Scan</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_bank ? 'check' : "close"} />
              <List.Content>Dok Banque</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_archi ? 'check' : "close"} />
              <List.Content>Dok Archi</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_ai ? 'check' : "close"} />
              <List.Content>Dok Intelligence</List.Content>
            </List.Item>

            <List.Item>
              <List.Icon name={item.ds_fact ? 'check' : "cog"} />
              <List.Content>Dok Fact {!item.ds_fact.default ? item.ds_fact.price : ""} </List.Content>
            </List.Item>

          </List>
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
    {item.name != "Entreprise" &&
      <Button style={{backgroundColor:'#ab3134', color:"#ffffff", width: "82%"}} onClick={() => {}}>
          ACHETER
      </Button>
      }
      {item.name == "Entreprise" &&
      <Button style={{backgroundColor:'#ffffff', color:"#ab3134", border:'solid 1px #ab3134', width: "82%"}} onClick={() => { window.open("https://www.dokcompta.com/contact-1/", "_blank") }}>
        NOUS CONTACTER
      </Button>
      
      }
    </Card.Content>
  </Card>
  );

  return (
    <div className={globalStyles.container} data-tid="container" style={{height : (height ) }} onClick={(e:any)=>this.onDismissPopover(e)}>
      
      <div className={globalStyles.appBar}>
        
        <img src={AppIcons.ds_logo_min} className={globalStyles.appBarLogo}  alt="" />
      
        {this.state.display_results && 
          <div style={{display :"flex", alignItems:"center", color:"#000000", marginRight : "auto", marginLeft : 30, cursor :"pointer"}} onClick={()=>{ this.goBackToProcessScreen()}}>
            <span style={{marginRight : 10, fontWeight : "bolder"}}>&#8672;</span>
            <span style={{fontSize : 14, fontWeight : "bolder"}}>Retour</span>
          </div>
        }

        {this.props.mainReduxState.selected_company.id && this.state.loggedCompany.type == "accounting" && 
          <div className={globalStyles.customerHeader} onClick={this.gotoMain}>
            <div style={{display:'flex', fontSize: 14, fontFamily:'Poppins', cursor:'pointer'}} >
                        
                <div style={{display:'flex', height:'100%', alignItems:'center', justifyContent:'center'}}>
                  <img src={AppIcons.ds_signup_select_icon} style={{width:25, height:25, aspectRatio:"auto"}} alt="" />
                </div>
                <div style={{display:'flex', marginLeft:8, flexDirection:'column', textAlign:'left'}}> 
                  <span style={{fontSize:10 ,lineHeight: 0.5,marginTop: 6}}>Dossier actif :</span>
                  <span style={{fontSize:12, fontWeight:'bold', color:'#EB5757'}}>{this.props.mainReduxState.selected_company.name}</span>
                </div> 
              </div>
          </div>
        }

        <span style={{fontFamily:"Poppins", fontSize:12, fontWeight:"bold", color: "black", marginLeft:"auto", marginRight:50}}> {this.props.mainReduxState.user.name} : <span style={{cursor:"pointer", color:"#DC4246"}} onClick={this.unlog}>Deconnexion</span></span>

        <div className={globalStyles.appBarOptsBox}>

          {/* <div className={globalStyles.appBarOpt} style={{backgroundColor : "#4caf50"}}></div>
          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#ffc107"}}></div>
          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#f44336"}}></div> */}

        </div>

      </div>



   {!this.state.trial_expired &&
    <div className={globalStyles.appBody} style={{height : (height * 0.98 ) }}>

      {this.state.screen != "process_result" && 

          <div className={globalStyles.appMenuBar}>
           
            <motion.img id="menubar_focuser" animate={{ top: this.state.menubar_focuser_top }} src={AppIcons.ds_menubar_focuser_icon} style={{position:'absolute', width : 48, top:0}} alt=""/>
            
            <Tooltip content="Accueil" position={Position.RIGHT}>
              <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,0)}}>
                <img className={(this.state.menubar_index == 0 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_home_icon} alt=""/>
              </div>
            </Tooltip>
            
            <Tooltip content="Box" position={Position.RIGHT} className={(this.props.mainReduxState.selected_company.id ? styles.menuItemActive : styles.menuItemInactive )}>
              <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,1)}}>
                <img className={(this.state.menubar_index == 1 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_box_icon} alt=""/>
              </div>
            </Tooltip>
            
            
            {this.state.loggedCompany.type == "accounting" &&
              <Tooltip content="Traitement" position={Position.RIGHT} className={(this.props.mainReduxState.selected_company.id ? styles.menuItemActive : styles.menuItemInactive )}>
              <div ref={ref => this.processMenuRef = ref}  className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,2)}}>
                <img className={(this.state.menubar_index == 2 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_processing_icon} alt=""/>
              </div>
            </Tooltip>
            }

            <Tooltip content="Ged" position={Position.RIGHT} className={(this.props.mainReduxState.selected_company.id ? styles.menuItemActive : styles.menuItemInactive )}>
              <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,3)}}>
                <img className={(this.state.menubar_index == 3 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_ged_icon} alt=""/>
              </div>
            </Tooltip>
            
            <Tooltip content="Banque" position={Position.RIGHT} className={(this.props.mainReduxState.selected_company.id ? styles.menuItemActive : styles.menuItemInactive )} >
              <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,4)}}>
                <img className={(this.state.menubar_index == 4 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_bank_icon} alt=""/>
              </div>
            </Tooltip>
            
            <Tooltip content="Facturation" position={Position.RIGHT} className={(this.props.mainReduxState.selected_company.id ? styles.menuItemActive : styles.menuItemInactive )}>
              <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,5)}}>
                <img className={(this.state.menubar_index == 5 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_devis_icon} alt=""/>
              </div>
            </Tooltip>

            {/* <Tooltip content="Statistiques" position={Position.RIGHT}>
              <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,6)}}>
                <img className={(this.state.menubar_index == 6 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_stats_icon} alt=""/>
              </div>
            </Tooltip> */}
            
            
              {/* Settings */}
              {/* <div className={ globalStyles.appMenuBarItemLast}>
                <Tooltip content="Paramétres" position={Position.RIGHT} >
                  <div className={[globalStyles.appMenuBarItem].join(' ')} onClick={(e:any)=>{this.onMenubar(e,7)}}>
                      <img className={(this.state.menubar_index == 7 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } style={{pointerEvents:'none'}} src={AppIcons.ds_gear_icon} alt=""/>
                  </div>
                </Tooltip>
              </div> */}

              <div >
                <Tooltip content="Paramétres" position={Position.RIGHT}>
                  <div className={[globalStyles.appMenuBarItem, "menuItem"].join(' ')} onClick={(e:any)=>{this.onMenubar(e,7)}}>
                    <img className={(this.state.menubar_index == 7 ? globalStyles.appMenuBarItemIconOn : globalStyles.appMenuBarItemIcon ) } src={AppIcons.ds_gear_icon} alt=""/>
                  </div>
                </Tooltip>
              </div>
          </div>

          }
        
          {this.state.screen == "main" && 
          <MainScreen onSelectCompany={this.onGotoScreen} />
          }

          {this.state.screen == "box" && 
          <BoxScreen />
          }

          {this.state.screen == "processing" && 
          <ProcessingScreen display_results={this.state.display_results} onDisplayResultsEmitted={this.onDisplayResultsEmitted} />
          } 
          
          {this.state.screen == "ged" && 
          <GedScreen />
          }

          {this.state.screen == "bank" && 
          <BankScreen />
          }

          {this.state.screen == "devis" && 
          <BillScreen />
          }

          {this.state.screen == "stats" && 
          <StatsScreen />
          }

          {this.state.screen == "settings" && 
          <SettingsScreen />
          }



          

      </div>

      }

      {this.state.trial_expired &&
        <div className={globalStyles.appBody} style={{height : (height * 0.98 ) }}>
          
          <div style={{display:'flex', width:'100%', alignItems:'center', justifyContent:'center', flexDirection:'column'}}>
             <img src={AppIcons.ds_license_expired} style={{width:'30%'}} alt=""/>
             <p>Votre période d'éssaie gratuit est expirée !
             <br/>Veuillez souscrire à l'un de nos forfaits pour continuer d'utiliser Dok Compta.</p>

             <Button primary style={{backgroundColor:'#AB3134'}} floated='right' onClick={()=> this.toggleChangePackModal(true) }>
                CHANGER DE FORFAIT
                <Icon name="chevron right" />
              </Button>
          </div>
   
        </div>
      }

      <div className={globalStyles.appFooter}>
      </div>
 



    {/* -------------  MODAL CHANGE PACKS ---------------------- */}
    
    <Modal
      onClose={() => this.toggleChangePackModal(false)}
      onOpen={() => this.toggleChangePackModal(true)}
      open={this.state.showChangePackModal}
    >
      <Modal.Header>CHANGER DE FORFAIT
        {/* <Button floated='right' style={{backgroundColor:'#ab3134', color:"#ffffff", border:'solid 1px #ab3134'}} onClick={() => this.toggleValidatePackModal(true)}>
          ACTIVER MA LICENCE <Icon style={{marginLeft:10}} name="key" />
        </Button> */}
      </Modal.Header>
       
      <Modal.Content style={{width:'95%'}}>
       
        <Card.Group style={{justifyContent:'center'}} >

          {/* {packsList} */}

          <UpgradePartial />
          

        </Card.Group>


      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleChangePackModal(false)}>
          Fermer
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}



    {/* -------------  MODAL VALIDATE LICENSE ---------------------- */}

    <Modal
      onClose={() => this.toggleValidatePackModal(false)}
      onOpen={() => this.toggleValidatePackModal(true)}
      open={this.state.showValidatePackModal}
    >
      <Modal.Header>ACTIVATION DE VOTRE LICENCE</Modal.Header>
      <Modal.Content style={{width:'95%', paddingBottom: 20}}>

        <Form loading={this.state.license_checking}>
          <Form.Input style={{textAlign:'center' }} error={this.state.input_license_key_error} label="Clé d'activation" placeholder='604C-068B-29FA-1700-5188-E449' onChange={(e) => this.handleLicenseActivationKey(e.target.value)} />
          <div style={{display:'flex', justifyContent:'center'}}>
            <Button style={{backgroundColor:'#ab3134', color:"#ffffff", border:'solid 1px #ab3134'}} onClick={() => this.activateLicense()} >ACTIVER</Button>
          </div>
        </Form>

      </Modal.Content>
    </Modal>

    {/* --------------------------------------------------- */}



    </div>
  );
  
 }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}
function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);
