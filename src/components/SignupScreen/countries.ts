const countries = [

        {
            text: "Afghanistan",
            dial_code: "+93",
            value: "AF",
            key: "AF"
        },
        {
            text: "Aland Islands",
            dial_code: "+358",
            value: "AX",
            key: "AX"
        },
        {
            text: "Albania",
            dial_code: "+355",
            value: "AL",
            key: "AL"
        },
        {
            text: "Algeria",
            dial_code: "+213",
            value: "DZ",
            key: "DZ"
        },
        {
            text: "AmericanSamoa",
            dial_code: "+1 684",
            value: "AS",
            key: "AS"
        },
        {
            text: "Andorra",
            dial_code: "+376",
            value: "AD",
            key: "AD"
        },
        {
            text: "Angola",
            dial_code: "+244",
            value: "AO",
            key: "AO"
        },
        {
            text: "Anguilla",
            dial_code: "+1 264",
            value: "AI",
            key: "AI"
        },
        {
            text: "Antarctica",
            dial_code: "+672",
            value: "AQ",
            key: "AQ"
        },
        {
            text: "Antigua and Barbuda",
            dial_code: "+1268",
            value: "AG",
            key: "AG"
        },
        {
            text: "Argentina",
            dial_code: "+54",
            value: "AR",
            key: "AR"
        },
        {
            text: "Armenia",
            dial_code: "+374",
            value: "AM",
            key: "AM"
        },
        {
            text: "Aruba",
            dial_code: "+297",
            value: "AW",
            key: "AW"
        },
        {
            text: "Australia",
            dial_code: "+61",
            value: "AU",
            key: "AU"
        },
        {
            text: "Austria",
            dial_code: "+43",
            value: "AT",
            key: "AT"
        },
        {
            text: "Azerbaijan",
            dial_code: "+994",
            value: "AZ",
            key: "AZ"
        },
        {
            text: "Bahamas",
            dial_code: "+1 242",
            value: "BS",
            key: "BS"
        },
        {
            text: "Bahrain",
            dial_code: "+973",
            value: "BH",
            key: "BH"
        },
        {
            text: "Bangladesh",
            dial_code: "+880",
            value: "BD",
            key: "BD"
        },
        {
            text: "Barbados",
            dial_code: "+1 246",
            value: "BB",
            key: "BB"
        },
        {
            text: "Belarus",
            dial_code: "+375",
            value: "BY",
            key: "BY"
        },
        {
            text: "Belgium",
            dial_code: "+32",
            value: "BE",
            key: "BE"
        },
        {
            text: "Belize",
            dial_code: "+501",
            value: "BZ",
            key: "BZ"
        },
        {
            text: "Benin",
            dial_code: "+229",
            value: "BJ",
            key: "BJ"
        },
        {
            text: "Bermuda",
            dial_code: "+1 441",
            value: "BM",
            key: "BM"
        },
        {
            text: "Bhutan",
            dial_code: "+975",
            value: "BT",
            key: "BT"
        },
        {
            text: "Bolivia, Plurinational State of",
            dial_code: "+591",
            value: "BO",
            key: "BO"
        },
        {
            text: "Bosnia and Herzegovina",
            dial_code: "+387",
            value: "BA",
            key: "BA"
        },
        {
            text: "Botswana",
            dial_code: "+267",
            value: "BW",
            key: "BW"
        },
        {
            text: "Brazil",
            dial_code: "+55",
            value: "BR",
            key: "BR"
        },
        {
            text: "British Indian Ocean Territory",
            dial_code: "+246",
            value: "IO",
            key: "IO"
        },
        {
            text: "Brunei Darussalam",
            dial_code: "+673",
            value: "BN",
            key: "BN"
        },
        {
            text: "Bulgaria",
            dial_code: "+359",
            value: "BG",
            key: "BG"
        },
        {
            text: "Burkina Faso",
            dial_code: "+226",
            value: "BF",
            key: "BF"
        },
        {
            text: "Burundi",
            dial_code: "+257",
            value: "BI",
            key: "BI"
        },
        {
            text: "Cambodia",
            dial_code: "+855",
            value: "KH",
            key: "KH"
        },
        {
            text: "Cameroon",
            dial_code: "+237",
            value: "CM",
            key: "CM"
        },
        {
            text: "Canada",
            dial_code: "+1",
            value: "CA",
            key: "CA"
        },
        {
            text: "Cape Verde",
            dial_code: "+238",
            value: "CV",
            key: "CV"
        },
        {
            text: "Cayman Islands",
            dial_code: "+ 345",
            value: "KY",
            key: "KY"
        },
        {
            text: "Central African Republic",
            dial_code: "+236",
            value: "CF",
            key: "CF"
        },
        {
            text: "Chad",
            dial_code: "+235",
            value: "TD",
            key: "TD"
        },
        {
            text: "Chile",
            dial_code: "+56",
            value: "CL",
            key: "CL"
        },
        {
            text: "China",
            dial_code: "+86",
            value: "CN",
            key: "CN"
        },
        {
            text: "Christmas Island",
            dial_code: "+61",
            value: "CX",
            key: "CX"
        },
        {
            text: "Cocos (Keeling) Islands",
            dial_code: "+61",
            value: "CC",
            key: "CC"
        },
        {
            text: "Colombia",
            dial_code: "+57",
            value: "CO",
            key: "CO"
        },
        {
            text: "Comoros",
            dial_code: "+269",
            value: "KM",
            key: "KM"
        },
        {
            text: "Congo",
            dial_code: "+242",
            value: "CG",
            key: "CG"
        },
        {
            text: "Congo, The Democratic Republic of the Congo",
            dial_code: "+243",
            value: "CD",
            key: "CD"
        },
        {
            text: "Cook Islands",
            dial_code: "+682",
            value: "CK",
            key: "CK"
        },
        {
            text: "Costa Rica",
            dial_code: "+506",
            value: "CR",
            key: "CR"
        },
        {
            text: "Cote d'Ivoire",
            dial_code: "+225",
            value: "CI",
            key: "CI"
        },
        {
            text: "Croatia",
            dial_code: "+385",
            value: "HR",
            key: "HR"
        },
        {
            text: "Cuba",
            dial_code: "+53",
            value: "CU",
            key: "CU"
        },
        {
            text: "Cyprus",
            dial_code: "+357",
            value: "CY",
            key: "CY"
        },
        {
            text: "Czech Republic",
            dial_code: "+420",
            value: "CZ",
            key: "CZ"
        },
        {
            text: "Denmark",
            dial_code: "+45",
            value: "DK",
            key: "DK"
        },
        {
            text: "Djibouti",
            dial_code: "+253",
            value: "DJ",
            key: "DJ"
        },
        {
            text: "Dominica",
            dial_code: "+1 767",
            value: "DM",
            key: "DM"
        },
        {
            text: "Dominican Republic",
            dial_code: "+1 849",
            value: "DO",
            key: "DO"
        },
        {
            text: "Ecuador",
            dial_code: "+593",
            value: "EC",
            key: "EC"
        },
        {
            text: "Egypt",
            dial_code: "+20",
            value: "EG",
            key: "EG"
        },
        {
            text: "El Salvador",
            dial_code: "+503",
            value: "SV",
            key: "SV"
        },
        {
            text: "Equatorial Guinea",
            dial_code: "+240",
            value: "GQ",
            key: "GQ"
        },
        {
            text: "Eritrea",
            dial_code: "+291",
            value: "ER",
            key: "ER"
        },
        {
            text: "Estonia",
            dial_code: "+372",
            value: "EE",
            key: "EE"
        },
        {
            text: "Ethiopia",
            dial_code: "+251",
            value: "ET",
            key: "ET"
        },
        {
            text: "Falkland Islands (Malvinas)",
            dial_code: "+500",
            value: "FK",
            key: "FK"
        },
        {
            text: "Faroe Islands",
            dial_code: "+298",
            value: "FO",
            key: "FO"
        },
        {
            text: "Fiji",
            dial_code: "+679",
            value: "FJ",
            key: "FJ"
        },
        {
            text: "Finland",
            dial_code: "+358",
            value: "FI",
            key: "FI"
        },
        {
            text: "France",
            dial_code: "+33",
            value: "FR",
            key: "FR"
        },
        {
            text: "French Guiana",
            dial_code: "+594",
            value: "GF",
            key: "GF"
        },
        {
            text: "French Polynesia",
            dial_code: "+689",
            value: "PF",
            key: "PF"
        },
        {
            text: "Gabon",
            dial_code: "+241",
            value: "GA",
            key: "GA"
        },
        {
            text: "Gambia",
            dial_code: "+220",
            value: "GM",
            key: "GM"
        },
        {
            text: "Georgia",
            dial_code: "+995",
            value: "GE",
            key: "GE"
        },
        {
            text: "Germany",
            dial_code: "+49",
            value: "DE",
            key: "DE"
        },
        {
            text: "Ghana",
            dial_code: "+233",
            value: "GH",
            key: "GH"
        },
        {
            text: "Gibraltar",
            dial_code: "+350",
            value: "GI",
            key: "GI"
        },
        {
            text: "Greece",
            dial_code: "+30",
            value: "GR",
            key: "GR"
        },
        {
            text: "Greenland",
            dial_code: "+299",
            value: "GL",
            key: "GL"
        },
        {
            text: "Grenada",
            dial_code: "+1 473",
            value: "GD",
            key: "GD"
        },
        {
            text: "Guadeloupe",
            dial_code: "+590",
            value: "GP",
            key: "GP"
        },
        {
            text: "Guam",
            dial_code: "+1 671",
            value: "GU",
            key: "GU"
        },
        {
            text: "Guatemala",
            dial_code: "+502",
            value: "GT",
            key: "GT"
        },
        {
            text: "Guernsey",
            dial_code: "+44",
            value: "GG",
            key: "GG"
        },
        {
            text: "Guinea",
            dial_code: "+224",
            value: "GN",
            key: "GN"
        },
        {
            text: "Guinea-Bissau",
            dial_code: "+245",
            value: "GW",
            key: "GW"
        },
        {
            text: "Guyana",
            dial_code: "+595",
            value: "GY",
            key: "GY"
        },
        {
            text: "Haiti",
            dial_code: "+509",
            value: "HT",
            key: "HT"
        },
        {
            text: "Holy See (Vatican City State)",
            dial_code: "+379",
            value: "VA",
            key: "VA"
        },
        {
            text: "Honduras",
            dial_code: "+504",
            value: "HN",
            key: "HN"
        },
        {
            text: "Hong Kong",
            dial_code: "+852",
            value: "HK",
            key: "HK"
        },
        {
            text: "Hungary",
            dial_code: "+36",
            value: "HU",
            key: "HU"
        },
        {
            text: "Iceland",
            dial_code: "+354",
            value: "IS",
            key: "IS"
        },
        {
            text: "India",
            dial_code: "+91",
            value: "IN",
            key: "IN"
        },
        {
            text: "Indonesia",
            dial_code: "+62",
            value: "ID",
            key: "ID"
        },
        {
            text: "Iran, Islamic Republic of Persian Gulf",
            dial_code: "+98",
            value: "IR",
            key: "IR"
        },
        {
            text: "Iraq",
            dial_code: "+964",
            value: "IQ",
            key: "IQ"
        },
        {
            text: "Ireland",
            dial_code: "+353",
            value: "IE",
            key: "IE"
        },
        {
            text: "Isle of Man",
            dial_code: "+44",
            value: "IM",
            key: "IM"
        },
        {
            text: "Israel",
            dial_code: "+972",
            value: "IL",
            key: "IL"
        },
        {
            text: "Italy",
            dial_code: "+39",
            value: "IT",
            key: "IT"
        },
        {
            text: "Jamaica",
            dial_code: "+1 876",
            value: "JM",
            key: "JM"
        },
        {
            text: "Japan",
            dial_code: "+81",
            value: "JP",
            key: "JP"
        },
        {
            text: "Jersey",
            dial_code: "+44",
            value: "JE",
            key: "JE"
        },
        {
            text: "Jordan",
            dial_code: "+962",
            value: "JO",
            key: "JO"
        },
        {
            text: "Kazakhstan",
            dial_code: "+7 7",
            value: "KZ",
            key: "KZ"
        },
        {
            text: "Kenya",
            dial_code: "+254",
            value: "KE",
            key: "KE"
        },
        {
            text: "Kiribati",
            dial_code: "+686",
            value: "KI",
            key: "KI"
        },
        {
            text: "Korea, Democratic People's Republic of Korea",
            dial_code: "+850",
            value: "KP",
            key: "KP"
        },
        {
            text: "Korea, Republic of South Korea",
            dial_code: "+82",
            value: "KR",
            key: "KR"
        },
        {
            text: "Kuwait",
            dial_code: "+965",
            value: "KW",
            key: "KW"
        },
        {
            text: "Kyrgyzstan",
            dial_code: "+996",
            value: "KG",
            key: "KG"
        },
        {
            text: "Laos",
            dial_code: "+856",
            value: "LA",
            key: "LA"
        },
        {
            text: "Latvia",
            dial_code: "+371",
            value: "LV",
            key: "LV"
        },
        {
            text: "Lebanon",
            dial_code: "+961",
            value: "LB",
            key: "LB"
        },
        {
            text: "Lesotho",
            dial_code: "+266",
            value: "LS",
            key: "LS"
        },
        {
            text: "Liberia",
            dial_code: "+231",
            value: "LR",
            key: "LR"
        },
        {
            text: "Libyan Arab Jamahiriya",
            dial_code: "+218",
            value: "LY",
            key: "LY"
        },
        {
            text: "Liechtenstein",
            dial_code: "+423",
            value: "LI",
            key: "LI"
        },
        {
            text: "Lithuania",
            dial_code: "+370",
            value: "LT",
            key: "LT"
        },
        {
            text: "Luxembourg",
            dial_code: "+352",
            value: "LU",
            key: "LU"
        },
        {
            text: "Macao",
            dial_code: "+853",
            value: "MO",
            key: "MO"
        },
        {
            text: "Macedonia",
            dial_code: "+389",
            value: "MK",
            key: "MK"
        },
        {
            text: "Madagascar",
            dial_code: "+261",
            value: "MG",
            key: "MG"
        },
        {
            text: "Malawi",
            dial_code: "+265",
            value: "MW",
            key: "MW"
        },
        {
            text: "Malaysia",
            dial_code: "+60",
            value: "MY",
            key: "MY"
        },
        {
            text: "Maldives",
            dial_code: "+960",
            value: "MV",
            key: "MV"
        },
        {
            text: "Mali",
            dial_code: "+223",
            value: "ML",
            key: "ML"
        },
        {
            text: "Malta",
            dial_code: "+356",
            value: "MT",
            key: "MT"
        },
        {
            text: "Marshall Islands",
            dial_code: "+692",
            value: "MH",
            key: "MH"
        },
        {
            text: "Martinique",
            dial_code: "+596",
            value: "MQ",
            key: "MQ"
        },
        {
            text: "Mauritania",
            dial_code: "+222",
            value: "MR",
            key: "MR"
        },
        {
            text: "Mauritius",
            dial_code: "+230",
            value: "MU",
            key: "MU"
        },
        {
            text: "Mayotte",
            dial_code: "+262",
            value: "YT",
            key: "YT"
        },
        {
            text: "Mexico",
            dial_code: "+52",
            value: "MX",
            key: "MX"
        },
        {
            text: "Micronesia, Federated States of Micronesia",
            dial_code: "+691",
            value: "FM",
            key: "FM"
        },
        {
            text: "Moldova",
            dial_code: "+373",
            value: "MD",
            key: "MD"
        },
        {
            text: "Monaco",
            dial_code: "+377",
            value: "MC",
            key: "MC"
        },
        {
            text: "Mongolia",
            dial_code: "+976",
            value: "MN",
            key: "MN"
        },
        {
            text: "Montenegro",
            dial_code: "+382",
            value: "ME",
            key: "ME"
        },
        {
            text: "Montserrat",
            dial_code: "+1664",
            value: "MS",
            key: "MS"
        },
        {
            text: "Morocco",
            dial_code: "+212",
            value: "MA",
            key: "MA"
        },
        {
            text: "Mozambique",
            dial_code: "+258",
            value: "MZ",
            key: "MZ"
        },
        {
            text: "Myanmar",
            dial_code: "+95",
            value: "MM",
            key: "MM"
        },
        {
            text: "Namibia",
            dial_code: "+264",
            value: "NA",
            key: "NA"
        },
        {
            text: "Nauru",
            dial_code: "+674",
            value: "NR",
            key: "NR"
        },
        {
            text: "Nepal",
            dial_code: "+977",
            value: "NP",
            key: "NP"
        },
        {
            text: "Netherlands",
            dial_code: "+31",
            value: "NL",
            key: "NL"
        },
        {
            text: "Netherlands Antilles",
            dial_code: "+599",
            value: "AN",
            key: "AN"
        },
        {
            text: "New Caledonia",
            dial_code: "+687",
            value: "NC",
            key: "NC"
        },
        {
            text: "New Zealand",
            dial_code: "+64",
            value: "NZ",
            key: "NZ"
        },
        {
            text: "Nicaragua",
            dial_code: "+505",
            value: "NI",
            key: "NI"
        },
        {
            text: "Niger",
            dial_code: "+227",
            value: "NE",
            key: "NE"
        },
        {
            text: "Nigeria",
            dial_code: "+234",
            value: "NG",
            key: "NG"
        },
        {
            text: "Niue",
            dial_code: "+683",
            value: "NU",
            key: "NU"
        },
        {
            text: "Norfolk Island",
            dial_code: "+672",
            value: "NF",
            key: "NF"
        },
        {
            text: "Northern Mariana Islands",
            dial_code: "+1 670",
            value: "MP",
            key: "MP"
        },
        {
            text: "Norway",
            dial_code: "+47",
            value: "NO",
            key: "NO"
        },
        {
            text: "Oman",
            dial_code: "+968",
            value: "OM",
            key: "OM"
        },
        {
            text: "Pakistan",
            dial_code: "+92",
            value: "PK",
            key: "PK"
        },
        {
            text: "Palau",
            dial_code: "+680",
            value: "PW",
            key: "PW"
        },
        {
            text: "Palestinian Territory, Occupied",
            dial_code: "+970",
            value: "PS",
            key: "PS"
        },
        {
            text: "Panama",
            dial_code: "+507",
            value: "PA",
            key: "PA"
        },
        {
            text: "Papua New Guinea",
            dial_code: "+675",
            value: "PG",
            key: "PG"
        },
        {
            text: "Paraguay",
            dial_code: "+595",
            value: "PY",
            key: "PY"
        },
        {
            text: "Peru",
            dial_code: "+51",
            value: "PE",
            key: "PE"
        },
        {
            text: "Philippines",
            dial_code: "+63",
            value: "PH",
            key: "PH"
        },
        {
            text: "Pitcairn",
            dial_code: "+872",
            value: "PN",
            key: "PN"
        },
        {
            text: "Poland",
            dial_code: "+48",
            value: "PL",
            key: "PL"
        },
        {
            text: "Portugal",
            dial_code: "+351",
            value: "PT",
            key: "PT"
        },
        {
            text: "Puerto Rico",
            dial_code: "+1 939",
            value: "PR",
            key: "PR"
        },
        {
            text: "Qatar",
            dial_code: "+974",
            value: "QA",
            key: "QA"
        },
        {
            text: "Romania",
            dial_code: "+40",
            value: "RO",
            key: "RO"
        },
        {
            text: "Russia",
            dial_code: "+7",
            value: "RU",
            key: "RU"
        },
        {
            text: "Rwanda",
            dial_code: "+250",
            value: "RW",
            key: "RW"
        },
        {
            text: "Reunion",
            dial_code: "+262",
            value: "RE",
            key: "RE"
        },
        {
            text: "Saint Barthelemy",
            dial_code: "+590",
            value: "BL",
            key: "BL"
        },
        {
            text: "Saint Helena, Ascension and Tristan Da Cunha",
            dial_code: "+290",
            value: "SH",
            key: "SH"
        },
        {
            text: "Saint Kitts and Nevis",
            dial_code: "+1 869",
            value: "KN",
            key: "KN"
        },
        {
            text: "Saint Lucia",
            dial_code: "+1 758",
            value: "LC",
            key: "LC"
        },
        {
            text: "Saint Martin",
            dial_code: "+590",
            value: "MF",
            key: "MF"
        },
        {
            text: "Saint Pierre and Miquelon",
            dial_code: "+508",
            value: "PM",
            key: "PM"
        },
        {
            text: "Saint Vincent and the Grenadines",
            dial_code: "+1 784",
            value: "VC",
            key: "VC"
        },
        {
            text: "Samoa",
            dial_code: "+685",
            value: "WS",
            key: "WS"
        },
        {
            text: "San Marino",
            dial_code: "+378",
            value: "SM",
            key: "SM"
        },
        {
            text: "Sao Tome and Principe",
            dial_code: "+239",
            value: "ST",
            key: "ST"
        },
        {
            text: "Saudi Arabia",
            dial_code: "+966",
            value: "SA",
            key: "SA"
        },
        {
            text: "Senegal",
            dial_code: "+221",
            value: "SN",
            key: "SN"
        },
        {
            text: "Serbia",
            dial_code: "+381",
            value: "RS",
            key: "RS"
        },
        {
            text: "Seychelles",
            dial_code: "+248",
            value: "SC",
            key: "SC"
        },
        {
            text: "Sierra Leone",
            dial_code: "+232",
            value: "SL",
            key: "SL"
        },
        {
            text: "Singapore",
            dial_code: "+65",
            value: "SG",
            key: "SG"
        },
        {
            text: "Slovakia",
            dial_code: "+421",
            value: "SK",
            key: "SK"
        },
        {
            text: "Slovenia",
            dial_code: "+386",
            value: "SI",
            key: "SI"
        },
        {
            text: "Solomon Islands",
            dial_code: "+677",
            value: "SB",
            key: "SB"
        },
        {
            text: "Somalia",
            dial_code: "+252",
            value: "SO",
            key: "SO"
        },
        {
            text: "South Africa",
            dial_code: "+27",
            value: "ZA",
            key: "ZA"
        },
        {
            text: "South Georgia and the South Sandwich Islands",
            dial_code: "+500",
            value: "GS",
            key: "GS"
        },
        {
            text: "Spain",
            dial_code: "+34",
            value: "ES",
            key: "ES"
        },
        {
            text: "Sri Lanka",
            dial_code: "+94",
            value: "LK",
            key: "LK"
        },
        {
            text: "Sudan",
            dial_code: "+249",
            value: "SD",
            key: "SD"
        },
        {
            text: "Suriname",
            dial_code: "+597",
            value: "SR",
            key: "SR"
        },
        {
            text: "Svalbard and Jan Mayen",
            dial_code: "+47",
            value: "SJ",
            key: "SJ"
        },
        {
            text: "Swaziland",
            dial_code: "+268",
            value: "SZ",
            key: "SZ"
        },
        {
            text: "Sweden",
            dial_code: "+46",
            value: "SE",
            key: "SE"
        },
        {
            text: "Switzerland",
            dial_code: "+41",
            value: "CH",
            key: "CH"
        },
        {
            text: "Syrian Arab Republic",
            dial_code: "+963",
            value: "SY",
            key: "SY"
        },
        {
            text: "Taiwan",
            dial_code: "+886",
            value: "TW",
            key: "TW"
        },
        {
            text: "Tajikistan",
            dial_code: "+992",
            value: "TJ",
            key: "TJ"
        },
        {
            text: "Tanzania, United Republic of Tanzania",
            dial_code: "+255",
            value: "TZ",
            key: "TZ"
        },
        {
            text: "Thailand",
            dial_code: "+66",
            value: "TH",
            key: "TH"
        },
        {
            text: "Timor-Leste",
            dial_code: "+670",
            value: "TL",
            key: "TL"
        },
        {
            text: "Togo",
            dial_code: "+228",
            value: "TG",
            key: "TG"
        },
        {
            text: "Tokelau",
            dial_code: "+690",
            value: "TK",
            key: "TK"
        },
        {
            text: "Tonga",
            dial_code: "+676",
            value: "TO",
            key: "TO"
        },
        {
            text: "Trinidad and Tobago",
            dial_code: "+1 868",
            value: "TT",
            key: "TT"
        },
        {
            text: "Tunisia",
            dial_code: "+216",
            value: "TN",
            key: "TN"
        },
        {
            text: "Turkey",
            dial_code: "+90",
            value: "TR",
            key: "TR"
        },
        {
            text: "Turkmenistan",
            dial_code: "+993",
            value: "TM",
            key: "TM"
        },
        {
            text: "Turks and Caicos Islands",
            dial_code: "+1 649",
            value: "TC",
            key: "TC"
        },
        {
            text: "Tuvalu",
            dial_code: "+688",
            value: "TV",
            key: "TV"
        },
        {
            text: "Uganda",
            dial_code: "+256",
            value: "UG",
            key: "UG"
        },
        {
            text: "Ukraine",
            dial_code: "+380",
            value: "UA",
            key: "UA"
        },
        {
            text: "United Arab Emirates",
            dial_code: "+971",
            value: "AE",
            key: "AE"
        },
        {
            text: "United Kingdom",
            dial_code: "+44",
            value: "GB",
            key: "GB"
        },
        {
            text: "United States",
            dial_code: "+1",
            value: "US",
            key: "US"
        },
        {
            text: "Uruguay",
            dial_code: "+598",
            value: "UY",
            key: "UY"
        },
        {
            text: "Uzbekistan",
            dial_code: "+998",
            value: "UZ",
            key: "UZ"
        },
        {
            text: "Vanuatu",
            dial_code: "+678",
            value: "VU",
            key: "VU"
        },
        {
            text: "Venezuela, Bolivarian Republic of Venezuela",
            dial_code: "+58",
            value: "VE",
            key: "VE"
        },
        {
            text: "Vietnam",
            dial_code: "+84",
            value: "VN",
            key: "VN"
        },
        {
            text: "Virgin Islands, British",
            dial_code: "+1 284",
            value: "VG",
            key: "VG"
        },
        {
            text: "Virgin Islands, U.S.",
            dial_code: "+1 340",
            value: "VI",
            key: "VI"
        },
        {
            text: "Wallis and Futuna",
            dial_code: "+681",
            value: "WF",
            key: "WF"
        },
        {
            text: "Yemen",
            dial_code: "+967",
            value: "YE",
            key: "YE"
        },
        {
            text: "Zambia",
            dial_code: "+260",
            value: "ZM",
            key: "ZM"
        },
        {
            text: "Zimbabwe",
            dial_code: "+263",
            value: "ZW",
            key: "ZW"
        }



]


export default countries;