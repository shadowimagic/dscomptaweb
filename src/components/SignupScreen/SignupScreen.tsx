import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../global.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import UserServices from '../../services/UserServices';
import AppIcons from '../../icons';

import routes from '../../constants/routes.json';
import {history} from '../../redux/store';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';
import { Grid, Form,Dropdown,Icon, Header, Button, Modal, Dimmer, Loader,Input, Checkbox,Radio, Select,TextArea } from 'semantic-ui-react';
import {Intent, Label, Position, Toaster } from "@blueprintjs/core";
import countries from './countries';
import packs from '../../constants/packs';

const height = window.innerHeight;
//const history = useHistory();
const countryOptions = countries;

 
class SignupScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {
      screen: "splash",
      splash_fade : 1,

      input_user_name : "",
      input_user_surname : "",
      input_user_company_name : "",
      input_user_function : "",
      input_user_country : "Gabon",
      input_user_telephone : "",
      input_user_mail : "",
      input_user_password : "",

      country_dial_code : "+000",

      
      passwordVisibility : "password",
 
      isSigning : false,

      company_name_error :false,
      user_name_error : false,
      user_surname_error : false,
      user_password_error : false,
      user_function_error : false,
      user_mail_error : false,
      user_tel_error : false,
      user_country_error : false,

      company_name_icon : '',
      company_name_loading : false,

      disable_signup_btn :false,
      display_creation :false,
      display_errors :false,
      license_correct :"none",

      account_created : false,

      showCreatedModal : false
      
    }

  }
   
 
  componentDidMount()
  {
 
      // setTimeout(() => {
      // }, 2000);

      // Set Dial code
      this.getCountryDial(this.state.input_user_country)
  }
 
  toggleCreatedModal = (status : boolean) => {
    this.setState({showCreatedModal : status});
  }
  
  handleUserName(value : string){

    this.setState({input_user_name : value})
    if(value.trim().length < 2)
    {
      this.setState({user_name_error : { content: '2 caractéres minimum', pointing: 'below' }});
    }
    else
    {
      this.setState({user_name_error : false});
    }
  }

  handleUserSurname(value : string){

    this.setState({input_user_surname : value})
    if(value.trim().length < 2)
    {
      this.setState({user_surname_error : { content: '2 caractéres minimum', pointing: 'below' }});
    }
    else
    {
      this.setState({user_surname_error : false});
    }
  }

  handleUserCompanyName(value : string){

    this.setState({input_user_company_name : value})

    if(value.trim().length < 2)
    {
      this.setState({company_name_error : { content: '2 caractéres minimum', pointing: 'below' }, company_name_loading : false, company_name_icon : ''});
    }
    else
    {
      // Check existing :::
      this.setState({company_name_error : false, company_name_loading : true, company_name_icon : ''});

      UserServices._checkCompanyByName(value).then(resp =>{

        if(resp.data.length > 0)
        {
          this.setState({company_name_error : { content: "Nom déja existant", pointing: 'below' }, company_name_loading : false, company_name_icon : ''});
        }
        else
        {
          this.setState({company_name_error : false, company_name_loading : false, company_name_icon : 'check'});
        }

      }).catch((err)=>{

        this.setState({company_name_error : { content: "Erreur lors de la vérification", pointing: 'below' }, company_name_loading : false, company_name_icon : ''});

      })
      // this.setState({company_name_error : false});
    }
  }

  handleUserFunction(value : string){

    this.setState({input_user_function : value})

    if(value.trim().length == 0)
    {
      this.setState({user_function_error : { content: 'Renseignez une fonction', pointing: 'below' }});
    }
    else
    {
      this.setState({user_function_error : false});
    }
  }

  handleUserCountry = (e, {value})=>{

    // console.log(e.target.textContent, value)
    this.setState({input_user_country :e.target.textContent})
    // Set dial code
    this.getCountryDial(e.target.textContent);

  }

  handleUserTel(value : string){

    var newValue = value.replace(/\D/g, "");
    this.setState({input_user_telephone : newValue})

    if(newValue.trim().length < 6)
    {
      this.setState({user_tel_error : { content: 'Entrez un numéro valide', pointing: 'below' }});
    }
    else
    {
      this.setState({user_tel_error : false});
    }
  }

  handleUserMail(value : string){

    this.setState({input_user_mail : value})


    if(this.validateEmail(value))
    {
      this.setState({input_user_mail : value, user_mail_error : false})
    }
    else
    {
      this.setState({user_mail_error : true})
    }
  }

  validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  handleUserPassword = (e, userPassword) => {
    
    this.setState({ input_user_password : userPassword });

    if(userPassword.trim().length < 6)
    {
      this.setState({user_password_error : { content: '6 caractéres minimum', pointing: 'below' }});
    }
    else
    {
      this.setState({user_password_error : false});
    }
  }

  togglePasswordVisibility = () => {
    
    var visibility = this.state.passwordVisibility == "password" ? "text" : "password" ;
    this.setState({ passwordVisibility:visibility });

  }  
  
  goBack()
  {
    history.replace(routes.WELCOME);
  }
  
  buildEmail(user_name, company_name)
  {
    var domainName = this.buildDomainName(company_name);
    var userName = user_name.split(" ")[0].toLowerCase();

    return userName + domainName;
  }

  buildDomainName(company_name)
  {
    var domainName = company_name.toLowerCase();
    domainName.replace(/ /g, '-');
    domainName = "@"+domainName+".com";
    return domainName;
  }

  getCountryDial = (text) =>
  {
    var code = countries.find((e)=>e.text == text)?.dial_code;
    this.setState({country_dial_code : code});

    return code;
  }  

  createCompany = ()=>
  {

    this.setState({isSigning : true});

    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });

    
     
    var company_data = {
      
      rcs: "undefined",
      type: "accounting",
      name: this.state.input_user_company_name,
      address: "Indéfinie",
      domain_name : this.buildDomainName(this.state.input_user_company_name),
      owner_name: this.state.input_user_name,
      owner_surname: this.state.input_user_surname,
      license : "trial",
      config :{
        address:'',
        tel:this.state.input_user_telephone,
        mail: this.state.input_user_mail,
        country:this.state.input_user_country,
        city:'',
        postalcode:1000,
        desc:'',
        bill_conditions:'',
        bill_legislation:''
      },
      pack : packs[0],
      exercise_year : new Date().getFullYear()

    }

    var user_email = this.buildEmail(this.state.input_user_name, this.state.input_user_company_name);
    
    var user_data = {
      name: this.state.input_user_name,
      surname: this.state.input_user_surname,
      email: user_email,
      customer_email: this.state.input_user_mail,
      password: this.state.input_user_password,
      type : "owner",
      function : this.state.input_user_function,
      companyId : ""
    }   

    if(user_data.name == "" || user_data.surname == "" || company_data.name == "" || user_data.function == "" || company_data.config.mail == "" || company_data.config.tel == ""  || user_data.password == "" )
    {
      AppToaster.show({ intent:Intent.WARNING, message: "Veuillez remplir tous les champs !" });
      this.setState({isSigning : false});

      return;
    }

    // console.log(company_data);

    UserServices._createCompany(company_data).then(resp =>{


        // Mail Html 
        var mailHtmlBody = `<h2>Bienvenue sur Dok Compta</h2><br/><br/>

        <span>Bonjour ${user_data.name} </span>,
        <br/>
        <br/>

        <span>Nous vous remercions pour avoir choisi de tester notre application. <br/> Nous vous offrons une période de test de 14 jours. <br/>
        Intégrer votre comptabilité dans Dok Compta et digitalisez votre comptabilité.</span><br/><br/>
    
        <span style='color:#7ed07e'><strong>Votre compte est actif.</strong></span><br/><br/>
        <span style='color:#1a1a1a'> Nom d'utilisateur : <strong>${user_data.email} </strong> </span><br/>
        <span style='color:#1a1a1a'> Mot de passe : <strong>${user_data.password} </strong> </span><br/><br/>
    
        <a href='http://app.dokcompta.com/#/signin' style='padding:10px 12px; background-color: #ab3134; color:#ffffff;' >Connectez-vous à votre espace</a> `;
        
        // Update created company
        var company = resp.data;
        this.props.mainReduxActions.update_company(company);

        // INIT GED
        var archiRootId = `archi-${company.id}`;
        var gedBase = {
          "files": {
            "rootFolderId": archiRootId,
            "fileMap": {
              [archiRootId]: { // Variable Key usage
                "id": archiRootId,
                "name": "Archivage",
                "isDir": true,
                "childrenIds": [
                  "Factures Clients",
                  "Factures Fournisseurs",
                  "Relevés Bancaires",
                  "Autres"
                ],
                "childrenCount": 4
              },
              "Factures Clients": {
                "id": "Factures Clients",
                "name": "Factures Clients",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              },
              "Factures Fournisseurs": {
                "id": "Factures Fournisseurs",
                "name": "Factures Fournisseurs",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              },
              "Relevés Bancaires": {
                "id": "Relevés Bancaires",
                "name": "Relevés Bancaires",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              },
              "Autres": {
                "id": "Autres",
                "name": "Autres",
                "color": "#c29399",
                "isDir": true,
                "modDate": new Date().toISOString(),
                "childrenIds": [],
                "childrenCount": 0,
                "parentId": archiRootId
              }
            }
          },
          "companyId": company.id
        };

      
        // console.log(gedBase);

        UserServices._initGed(gedBase).then(resp =>{
      
            user_data.companyId = company.id;
            UserServices._createUser(user_data).then(resp =>{
              
              this.setState({
                isSigning : false
              });

              // SEND MAIL
              var mail_data = {name:user_data.name, customer_mail:user_data.customer_email, subject: "Dok Compta : Données de connexion", html: mailHtmlBody, username:user_data.email, password:user_data.password };

              UserServices._sendMail(mail_data).then(resp =>{
               
              }).catch((err)=>{
               
              });

              AppToaster.show({ intent:Intent.SUCCESS, message: "Compte créé avec succés !" });
              this.setState({showCreatedModal : true,
                // Reset
                input_user_name : '',
                input_user_surname : '',
                input_user_password : '',
                input_user_function : '',
                input_user_country : 'Afghanistan',
                country_dial_code : this.getCountryDial('Afghanistan'),
                input_user_company_name : '',
                input_user_mail : '',
                input_user_telephone : '',
                company_name_icon : ''
              });

            }).catch((err)=>{
              // console.log(err);
              this.setState({
                isSigning : false
              });
              
              AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la création du compte. Veuillez réessayer !" });

            });

        }).catch((err)=>{
          // console.log(err);
          this.setState({
            isSigning : false
          });

          AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la création du compte. Veuillez réessayer !" });

        });

        // setTimeout(()=>{
        //     history.replace(routes.SIGNUPOWNER)
        // },1100)


        }).catch((err)=>{
          
          this.setState({
            isSigning : false
          });

          AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la création du compte. Veuillez réessayer !" });


      });
 

  }

 render() {

  // ANIMATIONS PROPS

 
  return (
    <div className={styles.container} data-tid="container" style={{height : (height ), flexDirection : "column", backgroundColor:'revert' }}>
       
               
        <Dimmer active={this.state.isSigning}>
          <Loader content='Inscription...' />
        </Dimmer>        
      

       <div className={globalStyles.appBar} style={{display:"flex", width:"100%",backgroundColor : "#cd858d", maxHeight:40}}>
        {/* <img src={Img} className={globalStyles.appBarLogo} alt=""/> */}
        <img src={AppIcons.ds_logo_min} className={globalStyles.appBarLogo}  alt="" />
        {/* <span style={{fontFamily : 'Poppins', color:"#111111", fontSize:12, fontWeight:"bold"}}> jjj</span> */}


          <div style={{display :"flex", alignItems:"center", color:"#ffffff", marginRight : "auto", marginLeft : 30, cursor :"pointer"}} onClick={()=>{ this.goBack()}}>
            <span style={{marginRight : 10, fontWeight : "bolder"}}>&#8672;</span>
            <span style={{fontSize : 14, fontWeight : "bolder"}}>Retour</span>
          </div>

        {/* <div className={globalStyles.appBarOptsBox}>

          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#4caf50"}}></div>
          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#ffc107"}}></div>
          <div className={globalStyles.appBarOpt} style={{backgroundColor : "#f44336"}}></div>

        </div> */}

      </div>

      <div style={{display :"flex" , marginTop : "1%"}}>

        <div style={{display:'flex', flex:2}}>
          <img src={AppIcons.ds_signup_banner} style={{width:'90%', marginLeft:'3%'}} alt=""/>
        </div>

        <div style={{display:'flex', flex:1, flexDirection:'column', alignItems:'center', marginRight:40, paddingLeft:15}}>
          
          <div style={{display:'flex', width:'90%', textAlign:'center', justifyContent:'center', marginBottom:10, flexDirection:'column'}}>
            <span style={{fontSize:42, fontWeight:'bolder', color:'#AB3134', margin:5, textAlign:'center'}}>Dok Compta</span>
            <span style={{fontSize:16, fontWeight:'bolder', color:'#AB3134', margin:5, textAlign:'center'}}>INSCRIPTION</span>
          </div>
          
          <div style={{display:'flex', width:'90%', height:'80%', marginLeft:'5%'/*,  border: 'solid 0.5px #AB313485',backgroundColor: '#ffffff',boxShadow: '0px 6px 0px gainsboro' */, padding:5, borderRadius:5, marginTop:10, textAlign:'left'/* , overflowY:'scroll', overflowX:'hidden' */}}>
            <Form style={{width:'100%', marginTop:15}}>
              
              <Form.Group unstackable widths={2}>
                <Form.Field control={Input} value={this.state.input_user_name} onChange={(e) => this.handleUserName(e.target.value)} error={this.state.user_name_error} label='Prénom' placeholder='Votre prénom' />
                <Form.Field control={Input} value={this.state.input_user_surname} onChange={(e) => this.handleUserSurname(e.target.value)}  error={this.state.user_surname_error} label='Nom' placeholder='Votre nom' />
              </Form.Group>
 
              <Form.Group unstackable widths={2}>
                <Form.Field control={Input} value={this.state.input_user_company_name} onChange={(e) => this.handleUserCompanyName(e.target.value)}  error={this.state.company_name_error} loading={this.state.company_name_loading} icon={this.state.company_name_icon} label='Société' placeholder='Votre société' />
                <Form.Field control={Input} value={this.state.input_user_function} onChange={(e) => this.handleUserFunction(e.target.value)}  error={this.state.user_function_error} label='Fonction' placeholder='Votre fonction' />
              </Form.Group>

              <label style={{fontWeight:'bold'}}>Pays</label>
              <Dropdown error={this.state.user_country_error} style={{padding:0, lineHeight:3, textIndent:5, width:'99%', height:38, border:'solid 1px', marginBottom:8}}
                placeholder='Votre pays'
                fluid
                search
                selection
                options={countryOptions} 
                defaultValue='AF'
                // defaultValue={this.state.input_user_country}
                //@ts-ignore
                onChange={this.handleUserCountry}
              />

              <Form.Group widths={2}>

                <div style={{display:'flex', width:'65%' ,flexDirection:'column', marginLeft:7}}>
                  <label style={{fontWeight:'bold', marginBottom:4}}>Telephone</label>
                  <Input style={{width:'100%', height:38}} label={this.state.country_dial_code} placeholder='Telephone' value={this.state.input_user_telephone} onChange={(e) => this.handleUserTel(e.target.value)}  error={this.state.user_tel_error} />
                </div>
               
                {/* <Form.Field control={Input} value={this.state.input_user_telephone} onChange={(e) => this.handleUserTel(e.target.value)}  error={this.state.user_tel_error} label='Telephone' placeholder='Telephone' > */}
                <Form.Field value={this.state.input_user_mail} onChange={(e) => this.handleUserMail(e.target.value)}  error={this.state.user_mail_error}
                  id='form-input-control-error-email'
                  control={Input}
                  label='Email'
                  placeholder='nom@societe.com'
                  
                />
              </Form.Group>

              <Form.Field
                control={Input} 
                error={this.state.user_password_error}
                type={this.state.passwordVisibility}
                icon={<Icon name='eye' color='black' style={{top:0, right:0}}  link onClick={this.togglePasswordVisibility} />}
                value={this.state.input_user_password}
                onChange={(e) => this.handleUserPassword(e,e.target.value)}
                label="Mot de passe"
                placeholder="Saisir un mot de passe"
              />
              
              {/* <Form.Checkbox label='I agree to the Terms and Conditions' /> */}

              <Button primary style={{backgroundColor:'#AB3134'}} onClick={()=> this.createCompany()} >S'INSCRIRE</Button>
            </Form>


          </div>
          <span style={{color : "#111111", paddingBottom:25, fontFamily:"Poppins", fontSize:14, fontWeight : "bolder"}}>Déja un compte ? <span onClick={()=>{history.replace(routes.SIGNIN) }} style={{color:"#AB3134", cursor:"pointer"}}> Vous connecter</span> </span> 

        </div>

      </div>


      <Modal
        basic
        onClose={() => this.toggleCreatedModal(false)}
        onOpen={() => this.toggleCreatedModal(true)}
        open={this.state.showCreatedModal}
        size='small' 
      >
        <Header icon>
          <Icon name='check' />
          Compte créé avec succés !
        </Header>
        <Modal.Content>
          <p style={{textAlign:'center', fontSize:20 , pointerEvents:'none'}}>

          Merci pour votre demande d’inscription.
          Vous recevrez un mail de confirmation de la validation de votre compte.
          Vérifier votre mail et profitez de votre période de test dès maintenant.
          
          <br/>
          <br/>
          
          <span style={{fontSize:14}}>L’équipe Dok Compta</span>

          </p>
        </Modal.Content>
         
      </Modal>


    </div>
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignupScreen);
