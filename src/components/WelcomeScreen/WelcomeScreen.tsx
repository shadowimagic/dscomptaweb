import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../global.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../icons';

import routes from '../../constants/routes.json';
// import { history } from '../../store';

import UserServices from '../../services/UserServices';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';

// import { fromBase64, fromBuffer } from "pdf2pic";
import {history} from '../../redux/store';

import { loadStripe } from '@stripe/stripe-js';
import TesseractServices from '../../services/TesseractServices';
import { Session } from 'inspector';
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe('pk_test_51Iw7QaHjs6zu68wd4D6DfA1VLzjSwqV6SYPiecrqszgxtAEG9DorNldPe8F6VaSOqXon0KTlQPcjhrFC0jtgZbY6001hzIudEK');

const height = window.innerHeight;
//const history = useHistory();


 
class WelcomeScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {
      screen: "splash",
      splash_fade : 1,
      input_user_type : 1,
      input_usermail : "",
      input_password : "",
      display_errors : false,
      disable_signin_btn : false,
      connexion_status : "default",

      // Test redux 
      username : "admin@ds.com",
      password : "admin"
      
    }

  }
   
 
  componentDidMount()
  {
  }
 
  gotoSignup = ()=>
  {
    history.replace(routes.SIGNUP)
  }
   
  gotoSignin = ()=>
  {
    history.replace(routes.SIGNIN)
  }




 render() {

// ANIMATIONS PROPS


  return (
    <div className={styles.container} data-tid="container" style={{height : (height ) , backgroundImage: `url(${AppIcons.ds_backg_2})`, backgroundSize: "cover", backgroundRepeat:"no-repeat" }}>
       
       <div style={{display : "flex", flexDirection:"column", alignItems:"center", width:"100%", marginTop:"0%"}}>
          <img src={AppIcons.ds_logo_big} style={{width : 80, height : "auto"}}  alt="" />
          <span style={{fontSize : 22, color : "#ffffff",fontWeight:"bolder" }}>Dok Compta</span>
       </div>

       <div style={{display : "flex",width: "100%",
          flex: 2,
          justifyContent: "center",
          alignSelf: "center",
          alignItems: "flex-start",
          justifySelf: "flex-end",
          marginTop: 100}}>
          <div className={styles.appContentContainerRight} style={{backgroundColor : "#ffffffe0"}} >
              <div style={{width : 30, height:4, backgroundColor : "#8E295F", marginBottom : 20}}></div>

              <img src={AppIcons.ds_signup_select_icon} style={{width : 80, height : "auto"}}  alt="" />

              <button className={styles.callToAction} onClick={this.gotoSignup} >OUVRIR UN COMPTE</button>

          </div>

          <div className={styles.appContentContainerRight} style={{backgroundColor : "#ffffffe0"}} >
            <div style={{width : 30, height:4, backgroundColor : "#8E295F", marginBottom : 20}}></div>

            <img src={AppIcons.ds_signin_select_icon} style={{width : 80, height : "auto"}}  alt="" />
          
            <button className={styles.callToAction} onClick={this.gotoSignin} >SE CONNECTER</button>
            
          </div>

 
          
       </div>
      

    </div>
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(WelcomeScreen);
