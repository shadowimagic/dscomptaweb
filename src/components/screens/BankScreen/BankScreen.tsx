import React, { Component } from 'react';
import styles from '../../global.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';

//const history = useHistory();


// import { Link } from 'react-router-dom';
// import routes from '../../../constants/routes.json';
// import { history, configuredStore } from '../../../store';

// Partials
import SubHeaderPartial from '../../partials/SubHeaderPartial/SubHeaderPartial';
import Bank from "../../Bank/Bank";
import Settings from '../../Bank/Settings';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';

// Data
import TesseractServices from '../../../services/TesseractServices';

class BankScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {

      isLoading : false,
      subheader_title : "Banque",
       
      current_screen : "bank",
      toolbar_context_title : "Factures Clients",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      details_visibility : false,
      current_data_type : "customer",
      data_loading  : false,
      


    };


  }
  

  componentDidMount()
  {
   
 
  }

 
  onSubmenuClicked = (index : number)=>{

    var screen_name = this.state.tabs[index];

    this.setState({current_screen : screen_name})

  }

  

 render() {

  // ANIMATIONS PROPS

  return (
    
      <div className={styles.appContentContainer} style={{overflowY:'scroll'}}>

        <SubHeaderPartial title={this.state.subheader_title}  display_searchbar={false} 
        onSubmenuClicked={this.onSubmenuClicked} />

        <div className={styles.appInnerBody}>
        {/* LEFT BLOC */}
        
        {/*MIDDLE BLOC */}

          <div className={styles.appContentContainerCenter}>
            {/* --------- INNER MIDDLE ---------- */}
 
              <Bank />
            
          </div>
          
        {/* RIGHT BLOC */}

        
        </div>

      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BankScreen);
