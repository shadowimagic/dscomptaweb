import React, { Component, useRef } from 'react';
import globalStyles from '../../global.module.css';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';

//const history = useHistory();

import { Icon, Intent, Label, Position, Toaster } from "@blueprintjs/core";

// Partials
import ToolbarPartial from '../../partials/ToolbarPartial/ToolbarPartial';
import FilterbarPartial from '../../partials/FilterbarPartial/FilterbarPartial';
import SubHeaderPartial from '../../partials/SubHeaderPartial/SubHeaderPartial';
import SubmenuPartial from '../../partials/SubmenuPartial/SubmenuPartial';
import ActivitiesPartial from '../../partials/ActivitiesPartial/ActivitiesPartial';
import DetailsPanelPartial from "../../partials/DetailsPanelPartial/DetailsPanelPartial";
import FilesContainerPartial from "../../partials/FilesContainerPartial/FilesContainerPartial";
import Files_mocked from '../../../services/mocks/Files_mocked';
import ProcessResultScreen from '../ProcessResultScreen/ProcessResultScreen';


// Data
import TesseractServices from '../../../services/TesseractServices';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import { clearInterval } from 'timers'; 
import Emitter from '../../../services/emitter';
const height = window.innerHeight;
const MAX_FILES = 15;

var refreshInterval: NodeJS.Timeout;
// const _tableNode = document.getElementById("table");
class ProcessingScreen extends Component<any, any> {
  
  _isMounted = false;

  constructor(props:any)
  {
    super(props)
    this._isMounted = false;
    this.state = {

      isLoading : false,
      subheader_title : "TRAITEMENT",
      toolbar_context_title : "Factures Clients",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      leftmenu_visibility : false,
      details_visibility : false,
      display_process_notif : false,
      display_results : false,
      current_data_type : "customer",
      has_more : true,
      active_month:this.props.mainReduxState.active_month,
      data_loading : false,
      submenu : [
        {
          title : "Factures Clients",
          route : "customer"
        },
        {
          title : "Factures Fournisseurs",
          route : "provider"
        } 
      ],
      toolbar_menu : [
        {
          title : "Traiter"
        },
        {
          title : "Visualiser"
        },
        // {
        //   title : "Editer"
        // },
        // {
        //   title : "Supprimer"
        // },
        {
          title : "Générer"
        }
      ],
  
      global_files : [],
      files : [],
      processing : false, // Will help for Interval api call to checkout if we have all our files processed "success" or "failed"
      exportVisibility : "visible"

    };


    this.onOneFileClicked = this.onOneFileClicked.bind(this);
    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
    this.onToolbarMenuItemClicked = this.onToolbarMenuItemClicked.bind(this);
 
  }
  


  componentDidMount()
  {
  
    // Reset local data
    this._resetLocalData();

    this._isMounted = true;
    //nl
    this._loadMore();

    //nl
    Emitter.on(Emitter.types.FILES_TYPE_CHANGED, (type) => this._onBillsTypeChanged(type));
    Emitter.on(Emitter.types.MONTH_CHANGED, (newMonth) => this.onMonthChanged(newMonth));

  }

  componentWillUnmount(){
    this._isMounted = false;
  }

  //nl
  onMonthChanged(month:number)
  {
    Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, true);
    // Reset local data
    this._resetLocalData();

    if(this._isMounted)
        this.setState({active_month : month});
    setTimeout(() => {
      
      this._loadMore();
      
    }, 500);
  }

 //nl
 _onBillsTypeChanged(type :string)
 {
    Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, true);

    // Reset local data
    this._resetLocalData();
   // LoadMore
    if(this._isMounted)
      this.setState({current_data_type : type});
   setTimeout(() => {
     this._loadMore();
   }, 500);
 }

// Reset current data
_resetLocalData()
{
  var resetedBills = {files : [], page : 1 };
       
  switch (this.state.current_data_type) {
    case "customer":
      this.props.mainReduxActions.update_customers_processable_bills(resetedBills);
      break;

    case "provider":
      this.props.mainReduxActions.update_providers_processable_bills(resetedBills);
      break;      
  }

  
}


// nl
_loadMore = (fromUpdate : boolean = false)=>{

    this._loadLocal();

    switch (this.state.current_data_type) {
      case "customer":
        this._getCustomersBills(fromUpdate);
        break;

      case "provider":
        this._getProvidersBills(fromUpdate);
        break;      
    }

}


// nl
_loadLocal = ()=>{

  switch (this.state.current_data_type) {
    case "customer":
    if(this._isMounted)
          this.setState({files : this.props.mainReduxState.customers_processable_bills.files});
      break;

    case "provider":
    if(this._isMounted)
          this.setState({files : this.props.mainReduxState.providers_processable_bills.files});
      break;      
  }

}

//nl
_getCustomersBills = (fromUpdate : boolean)=>{

    if(this._isMounted)
        this.setState({has_more : true});

    // Get current customers bills settings
    var customersBills = this.props.mainReduxState.customers_processable_bills;
 
    var currentPage = customersBills.page;
    // Later set companyId to the selected one : TO IMPROVE
    var selectedCompany = this.props.mainReduxState.selected_company;

    // Get lowest date of the current year : 01/01/currentYear
    // var currentYear = new Date().getFullYear();
    var currentYear = selectedCompany.exercise_year == 0 || !selectedCompany.exercise_year ? new Date().getFullYear() : selectedCompany.exercise_year;
    var realMonth = this.state.active_month + 1;
    var realMonthString = realMonth.toString();
     
    if(realMonthString.length == 1) realMonthString = "0"+realMonthString;
    var dateStringMin = realMonthString+"/01/"+currentYear.toString();
    var febDays = new Date(currentYear, 2, 0).getDate();
    var monthsEnd = [31,febDays,31,30,31,30,31,31,30,31,30,31];
    var dateStringMax = realMonthString+"/"+monthsEnd[this.state.active_month]+"/"+currentYear.toString(); // 
    var minDate = new Date(dateStringMin).toISOString();
    var maxDate = new Date(dateStringMax).toISOString();
     
    TesseractServices._getCompanyBills(selectedCompany.id, this.state.current_data_type, minDate, maxDate ,currentPage).then(resp =>{

      var newCustomersBills = {...customersBills};
      var newFiles = resp.data;
      var newPage = newCustomersBills.page;
      if(resp.data.length > 0) newPage = newPage + 1;

      // console.log(newFiles);
      // Set loader status
      if(newFiles.length < MAX_FILES)
      {
        if(this._isMounted)
          this.setState({has_more : false});
      }

      if(fromUpdate)
        newCustomersBills.files = [];

      newCustomersBills = {...newCustomersBills, files : [...newCustomersBills.files, ...newFiles], page : newPage };
      

      this.props.mainReduxActions.update_customers_processable_bills(newCustomersBills);
      
      if(this._isMounted)
          this.setState({files : newCustomersBills.files});

      // Reset from month changed
      setTimeout(() => {
        Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, false);
      }, 300);
          
      }).catch((err)=>{
      
        this._loadingError();
        if(this._isMounted)
            this.setState({has_more : false});      
        console.log(err);
      });

}
 
//nl
_getProvidersBills = (fromUpdate : boolean)=>{

    if(this._isMounted)
        this.setState({has_more : true});
    // Get current customers bills settings
    var providersBills = this.props.mainReduxState.providers_processable_bills;
    var currentPage = providersBills.page;
    // Later set companyId to the selected one : TO IMPROVE
    var selectedCompany = this.props.mainReduxState.selected_company;

    // Get lowest date of the current year : 01/01/currentYear
    // var currentYear = new Date().getFullYear();
    var currentYear = selectedCompany.exercise_year == 0 || !selectedCompany.exercise_year ? new Date().getFullYear() : selectedCompany.exercise_year;
    var realMonth = this.state.active_month + 1;
    var realMonthString = realMonth.toString();
     
    if(realMonthString.length == 1) realMonthString = "0"+realMonthString;
    var dateStringMin = realMonthString+"/01/"+currentYear.toString();
    var febDays = new Date(currentYear, 2, 0).getDate();
    var monthsEnd = [31,febDays,31,30,31,30,31,31,30,31,30,31];
    var dateStringMax = realMonthString+"/"+monthsEnd[this.state.active_month]+"/"+currentYear.toString(); // 
    var minDate = new Date(dateStringMin).toISOString();
    var maxDate = new Date(dateStringMax).toISOString();

    TesseractServices._getCompanyBills(selectedCompany.id, this.state.current_data_type, minDate, maxDate ,currentPage).then(resp =>{

      var newProvidersBills = {...providersBills};
      var newFiles = resp.data;
      var newPage = newProvidersBills.page;
      if(resp.data.length > 0) newPage = newPage + 1;

      // Set loader status
      if(newFiles.length < MAX_FILES)
      {
        if(this._isMounted)
            this.setState({has_more : false});
      }

      if(fromUpdate)
        newProvidersBills.files = [];

      newProvidersBills = {...newProvidersBills, files : [...newProvidersBills.files, ...newFiles], page : newPage };
      
      //    this.setState({data_loading : false});
      //    this.setState({global_files : resp.data, data_loading : false});
      this.props.mainReduxActions.update_providers_processable_bills(newProvidersBills);
      
      if(this._isMounted)
        this.setState({files : newProvidersBills.files});
  
      // Reset from month changed
      setTimeout(() => {
        Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, false);
      }, 300);


      }).catch((err)=>{
      
        this._loadingError();
        if(this._isMounted)
            this.setState({has_more : false});      
        console.log(err);
      });

}

//nl
_loadingError()
  {
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    AppToaster.show({ intent:Intent.WARNING, message: "Erreur de chargement" });
  }


  onFilesUpdated() {
    
    setTimeout(() =>{
      this.setDataType(this.state.current_data_type);
    },250);
    // console.log("Files Updated --- ");

  }
 
  _getProcessableBills()
  {
    
    var selectedCompany = this.props.mainReduxState.selected_company;

    // Get lowest date of the current year : 01/01/currentYear
    // var currentYear = new Date().getFullYear();
    if (this.props.mainReduxState.files.length > 0 ) 
    {
      this.setDataType(this.state.current_data_type);
    }
    
    if(this._isMounted)
        this.setState({data_loading : true});

    TesseractServices._getProcessableBills(selectedCompany.id).then(resp =>{

    if(this._isMounted)
          this.setState({data_loading : false});
       this.props.mainReduxActions.add_files(resp.data);
       this.setDataType(this.state.current_data_type);
    
       }).catch((err)=>{
       
         console.log(err);
    if(this._isMounted)
            this.setState({data_loading : false});

       });
  }

  setDataType(type : string)
  {
    var files = this.props.mainReduxState.files.filter((e)=>e.type == type  && ( e.is_exported == false || e.is_exported == undefined));
    // console.log(files)
    if(this._isMounted)
        this.setState({files : files, current_data_type : type});
    // console.log(this.state.files);
  }

  onSwitchToTypeData(type : string)
  {
    this.setDataType(type);
  }

  onOneFileClicked(fileIndex : number) {
    //  Get File Data to display (Preview) + (Details Preview) 
    // localStorage.setItem('selected_file', JSON.stringify(this.state.files[fileIndex]));
  
    // console.log("File clicked : " + fileIndex)
  }
 
  onSubmenuClicked(index : number) {

    var current_data_type = this.state.submenu[index].route;
    if(this._isMounted)
        this.setState({
        toolbar_context_title : this.state.submenu[index].title,
        current_data_type : current_data_type
    });
    //Switch content
    // setTimeout(() => {
    //   this.onSwitchToTypeData(current_data_type);
    // }, 15);
  }
 
  onToolbarMenuItemClicked(option_name : string, type :string)
  {
    // console.log("Clicked To " + option_name);
    // Che ckout which method to call
    switch (option_name) {
      case "Traiter": // Traiter
        this.onProcessingClicked();
        break;
      case "Visualiser" : // Visualiser
        this.onPreviewClicked();
      break;

      case "Générer": // Generer
        // this.onExport()
        if(type == "XLS")
        {
          this.exportToXLS();
        }
      break;
     
    }
  }

  exportToXLS()
  {
      this.props.onDisplayResultsEmitted(true);
    if(this._isMounted)
          this.setState({exportVisibility : "hidden"});
      
      setTimeout(()=>{
       
          let t2e = new Table2Excel('#gentable');
          t2e.export();
          
      },500);

  }

  
  onProcessingClicked()
  {
    if(this._isMounted)
        this.setState({display_process_notif : true})

    var processableFiles = this.state.files.filter((e:any)=>e.status == "none");
    this.props.mainReduxActions.init_process_files(this.state.current_data_type);

    setTimeout(() => {
    if(this._isMounted)
          this.setState({display_process_notif : false}) 
      this._loadLocal();
    }, 1200)
    if(this._isMounted)
        this.setState({processing : true});

    // Init process in server side
    TesseractServices._initBillsProcessing(processableFiles).then(resp =>{

      if(this.state.processing)
      {
        setInterval(() => {
          
            TesseractServices._getProcessingBills(processableFiles).then(resp =>{

              //@ts-ignore
              var files = resp.data;

              // Update Local state data
              this.props.mainReduxActions.update_process_files(files);
              this._loadLocal();
              // Ending condition
              if(this._checkProcessingEnd(files))
            if(this._isMounted)
                      this.setState({processing : false});
        
            });// End _getProcessingFiles

        },5200);
      }

    });// End _initBillsProcessing
  
  }

  //nl
  _checkProcessingEnd(files: any[])
  {
    var filesCount = files.length;
    var counter = 0;

    for (let i = 0; i < files.length; i++) {

      const el = files[i];
      if(el.status == "success" || el.status == "error")
      {
        counter++;
      }
    }

    if(counter == filesCount)
      return true;

    return false;

  }

  onProcessingClickedOld()
  {
    if(this._isMounted)
        this.setState({display_process_notif : true})

    var processableFiles = this.state.files.filter((e:any)=>e.status == "none");
    // ----------- UPDATE LOCALLY FILES STATUS ------------- 
    //.... : Only "none" setted files to "progress"
    this.props.mainReduxActions.init_process_files(this.state.current_data_type);

    setTimeout(() => {
    if(this._isMounted)
          this.setState({display_process_notif : false}) 
    }, 1200)
  
    if(this._isMounted)
        this.setState({processing : true});
  
    // Then Load more
    // setTimeout(() => {
    //   this._loadMore(true); //fromUpdate
    // }, 500);

    //--------------- UPDATE IN API --------------------
    // console.log(processableFiles);
    TesseractServices._initBillsProcessing(processableFiles).then(resp =>{

      var refreshInterval = setInterval(() => {
        
        if(this.state.processing)
        {  
          // Let's update current type bills page number (decrement)
          switch (this.state.current_data_type) {
            case "customer":

              // BOX
              var customersUpdatedBills = {...this.props.mainReduxState.customers_bills};
                customersUpdatedBills.page = 1;

              this.props.mainReduxActions.update_customers_bills(customersUpdatedBills);

              // PROCESSING
              var customersUpdatedProcessableBills = {...this.props.mainReduxState.customers_processable_bills};
                customersUpdatedProcessableBills.page = 1;

              this.props.mainReduxActions.update_customers_processable_bills(customersUpdatedProcessableBills);

              break;
        
            case "provider":

              // BOX
              var providersUpdatedBills = {...this.props.mainReduxState.providers_bills};
                providersUpdatedBills.page = 1;

              this.props.mainReduxActions.update_providers_bills(providersUpdatedBills);

              // PROCESSING
              var providersUpdatedProcessableBills = {...this.props.mainReduxState.providers_processable_bills};
                providersUpdatedProcessableBills.page = 1;
 
              this.props.mainReduxActions.update_providers_processable_bills(providersUpdatedProcessableBills);

              break;      
          }          

            // Then Load more
            setTimeout(() => {

              this._loadMore(true); //fromUpdate
            }, 500);
 
        }
  
      }, 5200)
    
      }).catch((err)=>{
      
        console.log(err);
    if(this._isMounted)
            this.setState({display_process_notif : false,processing:false});
    });
  
  }


  onPreviewClicked()
  {
    if(this._isMounted)
    //    this.setState({display_process_notif : true})
    this.props.onDisplayResultsEmitted(true);
    if(this._isMounted)
        this.setState({exportVisibility : "visible"});
  }


 render() {

  // ANIMATIONS PROPS


  return (
    
      <div className={globalStyles.appContentContainer}  style={{overflowY:'scroll'}} >

        <SubHeaderPartial title={this.state.subheader_title} display_hmenu={true} display_searchbar={this.state.display_searchbar}
        onSubmenuClicked={this.onSubmenuClicked} />

        <div className={globalStyles.appInnerBody}>
        {/* LEFT BLOC */}
        
          {this.state.leftmenu_visibility &&
            <div className={globalStyles.appContentContainerLeft}>

            <SubmenuPartial submenu={this.state.submenu}/>

            <ActivitiesPartial activities={this.state.activities} />

          </div>
          }
        
        {/*MIDDLE BLOC */}

          <div className={globalStyles.appContentContainerCenter}>
              
              <ToolbarPartial title={this.state.subheader_title} context_title={this.state.toolbar_context_title} menu={this.state.toolbar_menu} files={this.state.files}  display_stats={true}  display_options={this.state.files.length > 0} display_count={true} 
                onToolbarMenuItemClicked={this.onToolbarMenuItemClicked}
              />

              <FilterbarPartial/>

              {/* --------- INNER MIDDLE ---------- */}

              <FilesContainerPartial  key="files_container_partial" edit={this.props.mainReduxState.edit_box_items}  data_loading={this.state.data_loading} files={this.state.files} has_more={this.state.has_more} onOneFileClicked={this.onOneFileClicked}  _loadMore={this._loadMore} />


          </div>
          
        {/* RIGHT BLOC */}

        <DetailsPanelPartial display={this.state.details_visibility} />

        </div>
        
        {this.state.display_process_notif &&
          <div className={styles.processingNotifPopup}>
            <div className={styles.processingNotifBox}>
              <img src={AppIcons.ds_sync_icon} alt=""/>
              <span>Traitement lancé</span>
            </div>
          </div>
        }

        {this.props.display_results &&
          <ProcessResultScreen files={this.state.files} exportVisibility={this.state.exportVisibility} onMonthChanged={this.onMonthChanged} />  
        }

      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProcessingScreen);
