import React, { Component } from 'react';
import globalStyles from '../../global.module.css';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';
import routes from '../../../constants/routes.json';
//const history = useHistory();
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
import UserServices from '../../../services/UserServices';
import BillSettingsScreen from "../../Dashboard/BillSettingsScreen/BillSettingsScreen"; 


import {reactLocalStorage} from 'reactjs-localstorage'; 
import { Form, Modal,Button, Card, Label,List, Image, Item, Icon } from 'semantic-ui-react'
import packs from '../../../constants/packs';
import UpgradePartial from '../../partials/UpgradePartial/UpgradePartial';
const packsInitial = packs; 

const TRIAL_DAYS = 14;


 
class MainScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {

      isLoading : false,
      license_checking : false,
      input_license_key : '',
      input_license_key_error : false,
      menubar_focuser_top :5,
      menubar_index : 0,
      selectedCompanyIndex : null,
      loggedCompany : {}, // The account owner's company
      selectedCompany : {}, // The selected customer
      customers_loading:false,
      customers_error:false,
      submenu : [
        {
          title : "Accueil",
          route : "route"
        }
      ],
      customers : [],
      packs : [],

      trial_left_days : 0,

      showSettingsModal : false,
      showChangePackModal :false,
      showValidatePackModal :false,

    }
  }


  componentDidMount(){

    var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
    this.setState({loggedCompany : loggedCompany});

    // Packs
    var _packs : any[] = [];
    for (let i = 1; i < packsInitial.length; i++) {
      const el = packsInitial[i];
      _packs.push(el);
    }

    this.setState({packs : _packs});




    // Logged company type presets
    if(loggedCompany.type == "customer")
    {
      this.setState({ selectedCompany: loggedCompany });
      //Update local redux state
      this.props.mainReduxActions.update_selected_company(loggedCompany);
      this.props.mainReduxActions.update_exercise_year(loggedCompany.exercise_year);
    }
    else
    { 
      // Get customers 
      this.setState({customers : this.props.mainReduxState.customers});
      this.getCustomers();
    }
    
  }
    
//  onSelectCompany = () =>
//  {
//     this.props.onSelectCompany(1);
//  }

toggleSettingsModal = (status:boolean) => {

  this.setState({ showSettingsModal : status});
}

toggleChangePackModal = (status:boolean) => {

  this.setState({ showChangePackModal : status});
}

toggleValidatePackModal = (status:boolean) => {

  this.setState({ showValidatePackModal : status, showChangePackModal : false});
}

 getCustomers = () => {
  
  var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
  this.setState({loggedCompany : loggedCompany});
  // console.log(loggedCompany);

  var companyId = loggedCompany.id;

  this.setState({customers_loading : true, customers_error : false});
  
  UserServices._getCompanyAffiliatedCompanies(companyId).then((res)=>{

      var customers = res.data;
      this.setState({
        customers : customers,
        customers_loading : false, 
        customers_error : false
      });

      // Update local redux state
      this.props.mainReduxActions.update_customers_list(customers);

      // Get First Company Users
      if(customers.length > 0 )
      {
        // this.getCompanyUsers(customers[0].id);
        // this.selectCompany(0);
      }
      // Update current redux state
      // this.props.mainReduxActions.update_company(company);

    }).catch((err)=>{
      // console.log(err);
      this.setState({
        customers_loading : false, customers_error : true
      });
  })


}

selectCompany = (index) => {
  
  this.setState({ selectedCompanyIndex : index, selectedCompany: this.state.customers[index] });
  //Update local redux state
  this.props.mainReduxActions.update_selected_company(this.state.customers[index]);
  this.props.mainReduxActions.update_exercise_year(this.state.customers[index].exercise_year);
  // Set company selection nav : go to box + activate all menu 
  this.props.onSelectCompany(1); // "1" for BOX menu index . see gotoScreen()
}

gotoSettings = () =>
{
    this.props.mainReduxActions.update_tab("settings");
    setTimeout(()=>{
        history.replace(routes.HOME)
    },10)
}

getTrialLeftDays(company)
{
  var startDate = new Date(company.created_at);
  var maxDate = startDate;
  maxDate.setDate(maxDate.getDate() + TRIAL_DAYS);

  var currentDate = new Date();
  const diffDays=(a,b)=>{
    return (a - b)/864e5|0
  };

  var leftDays = diffDays(maxDate, currentDate) + 1;
  var trial_left_days = leftDays.toString();
  if(leftDays < 0)
  {
    trial_left_days = "0 (Période d'essaie terminée)"; 
  }
 
  return trial_left_days;
}


handleLicenseActivationKey = (value : string) => {
 
  this.setState({input_license_key : value})
  if(value.trim().length == 24)
  {
    this.setState({input_license_key_error : false })
  }
  else
  {
    this.setState({input_license_key_error : { content: 'Licence invalide', pointing: 'below' }  })
  }

}

activateLicense = () => {
 
}



 render() {
 

  const customers = this.state.customers.map((item :any, index : number) =>
    <div className={(this.props.mainReduxState.selected_company.id == item.id ? styles.customerItemActive : styles.customerItem)} style={{display:'flex', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', cursor:'pointer'}} onClick={() =>this.selectCompany(index)} >
      <div style={{display:'flex', alignItems:'center', justifyContent:'center'}}>
        <img src={AppIcons.ds_signup_select_icon} style={{width:30, height:30, aspectRatio:"auto"}} alt="" />
      </div>
      <div style={{display:'flex', marginLeft:8, flexDirection:'column', textAlign:'left'}}> 
        <span>{item.name}</span>
        <span style={{fontSize:10}}>Entreprise Cliente</span>
      </div> 
    </div>);

  const packsList = this.state.packs.map((item :any, index : number) =>

      <Card style={{flex:1}}>
        {item.name == "Standard" &&
          <Image src={AppIcons.ds_pack_1_icon} wrapped ui={false} />
        }
        {item.name == "Premium" &&
          <Image src={AppIcons.ds_pack_2_icon} wrapped ui={false} />
        }
        {item.name == "Entreprise" &&
          <Image src={AppIcons.ds_pack_3_icon} wrapped ui={false} />
        }
        <Card.Content>
          <Card.Header>{item.name}</Card.Header>
          <Card.Meta>
            <span style={{fontWeight:'bolder', fontSize:18, color:"#ab3134"}}>{item.price ? item.price + "/mois" : "Devis" }</span>
          </Card.Meta>
          <Card.Description>
              <List>
                
                <List.Item>
                  <List.Icon name="universal access" />
                  <List.Content>Nombre d'accés : {item.access ? item.access : "A déterminer"} </List.Content>
                </List.Item>                
                <List.Item>
                  <List.Icon name={item.ds_scan ? 'check' : "close"} />
                  <List.Content>Dok Scan</List.Content>
                </List.Item>

                <List.Item>
                  <List.Icon name={item.ds_bank ? 'check' : "close"} />
                  <List.Content>Dok Banque</List.Content>
                </List.Item>

                <List.Item>
                  <List.Icon name={item.ds_archi ? 'check' : "close"} />
                  <List.Content>Dok Archi</List.Content>
                </List.Item>

                <List.Item>
                  <List.Icon name={item.ds_ai ? 'check' : "close"} />
                  <List.Content>Dok Intelligence</List.Content>
                </List.Item>

                <List.Item>
                  <List.Icon name={item.ds_fact ? 'check' : "cog"} />
                  <List.Content>Dok Fact {!item.ds_fact.default ? item.ds_fact.price : ""} </List.Content>
                </List.Item>

              </List>
          </Card.Description>
        </Card.Content>
        <Card.Content extra>
         {item.name != "Entreprise" &&
          <Button style={{backgroundColor:'#ab3134', color:"#ffffff", width: "82%"}} onClick={() => {}}>
              ACHETER
          </Button>
          }
          {item.name == "Entreprise" &&
          <Button style={{backgroundColor:'#ffffff', color:"#ab3134", border:'solid 1px #ab3134', width: "82%"}} onClick={() => { window.open("https://www.dokcompta.com/contact-1/", "_blank") }}>
              NOUS CONTACTER
          </Button>
          }
        </Card.Content>
      </Card>
  );

// ANIMATIONS PROPS


  return (
    
      <div className={globalStyles.appContentContainer} style={{overflowY:'scroll'}} >

        <div className={globalStyles.appInnerBody}>
        {/* LEFT BLOC */}
         
        
        {/*MIDDLE BLOC */}

          <div className={globalStyles.appContentContainerCenter} style={{backgroundColor:'transparent'}}>

              <div className={globalStyles.appInnerMiddleContainer} style={{overflowY: 'scroll', width:'100%', height:'100%'}}>
                 
                
                <div style={{display:'flex', width:'100%', height:'95%', justifyContent:'space-between'}}>
                  
                {this.state.loggedCompany.type == "accounting" &&

                  <div style={{display:'flex', color:'#000000', width:'25%', padding:'0px 5px', border:'solid 1px gainsboro', height:'100%', flexDirection:'column', backgroundColor:'#FDFAFA'}}>
                    
                    <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, padding:'10px 0px', textIndent:10, color: '#ffffff', backgroundColor:'#C09196'}}>Dossiers</div>
                    <div style={{display:'flex', fontFamily:'Poppins', marginTop:5}}>
                      {/* <input type="search" name="" id="" placeholder="Rechercher un dossier..." style={{padding:'8px 2px'}} /> */}
                    </div>
                    <hr/>

                    <div style={{display:'flex', width:'100%', flexDirection:'column', height:'90%', overflowY:'scroll', paddingBottom:10}}>
                      
                      {customers}

                      {this.state.customers_loading &&
                        <img src={AppIcons.ds_spinner_icon} style={{width:46, height:"auto", alignSelf:'center', marginTop:20}} alt="" />
                      }
                      {!this.state.customers_loading && !this.state.customers_error && this.state.customers.length == 0 &&

                        <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                          <span style={{alignSelf:'center', marginTop:'20%', maxWidth:'80%'}} > Vous n'avez pas encore d'entreprises clientes <br/><br/> 
                          <span style={{color:'gray'}}>En créer dans vos paramétres</span> <br/> <img style={{width:20}} src={AppIcons.ds_gear_icon} alt=""/>
                          </span>
                        </div>
                      }

                      {this.state.customers_error && this.state.customers.length == 0 &&

                      <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
                        <span style={{alignSelf:'center', marginTop:'20%', maxWidth:'80%'}} > Erreur lors du chargement des entreprises clientes </span><br/><br/> 
                        <Button content="Actualiser" labelPosition="right" icon='redo'  primary style={{backgroundColor: 'transparent', color:' #AB3134', border:'solid 1px #AB3134', maxWidth:100, marginTop:10}}   onClick={()=>this.getCustomers()} />
                          
                      </div>
                      }

                    </div>
                    
                  </div>

                  }

                  {/* ACTUALITES */}
                  <div style={{display:'flex', width: this.state.loggedCompany.type == "accounting" ? '45%' : "100%", border:'solid 1px gainsboro', height:'100%', flexDirection:'column'}}>

                  {this.state.loggedCompany.type == "accounting" &&
                    <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, padding:'10px 0px', textIndent:10, color: '#ffffff', backgroundColor:'#C09196', textTransform:'uppercase'}}>Tableau de board de <strong>{this.state.loggedCompany.name}</strong></div>
                  }
                  {this.state.loggedCompany.type == "accounting" &&
                    <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, textAlign:'left', width:'95%', marginLeft:'2.5%'}}>
                    
                      <Item.Group divided>
                        <Item>
                          <Item.Image style={{width:150}} src={AppIcons.ds_company_icon} />

                          <Item.Content>
                            <Item.Header>Votre Abonnement</Item.Header>
                            <Item.Meta>
                            <Label.Group color='brown'>
                                <Label color="black" style={{textTransform:'uppercase'}}>
                                  {this.state.loggedCompany.pack.name}
                                  <Icon style={{marginLeft:5}} name='briefcase' />
                                </Label> 
                              </Label.Group>
                            </Item.Meta>
                            <Item.Description> Vous etes abonné au forfait <strong> <span style={{textTransform:'uppercase'}}>
                                  {this.state.loggedCompany.pack.name}</span></strong> 
                              <br/>
                              
                              {this.state.loggedCompany.pack.name == "Trial" &&
                                <span>
                                  Il vous reste <strong style={{fontSize:21}}>{this.getTrialLeftDays(this.state.loggedCompany)}</strong> jours.
                                </span>
                              }

                             </Item.Description>
                            <Item.Extra>
                              <Button primary style={{backgroundColor:'#AB3134'}} floated='right' onClick={()=> this.toggleChangePackModal(true) }>
                                CHANGER DE FORFAIT
                                <Icon name="chevron right" />
                              </Button>
                              {/* <Label color="green" floated='left'>TRIAL</Label> */}
                            </Item.Extra>
                          </Item.Content>
                        </Item>
                      </Item.Group>

                    </div>
                    }


                    {/* ACTUALITES */}

                  {this.state.loggedCompany.type == "customer" &&
                    <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, padding:'10px 0px', textIndent:10, color: '#ffffff', backgroundColor:'#C09196'}}>Actualités</div>
                  }
                  {this.state.loggedCompany.type == "customer" &&

                    <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, flexDirection:'column', alignItems:'center'}}>
                        <div style={{display:'flex', color:'#000000',fontWeight:'bold', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', backgroundColor:'#fff', width:'94%'}}>
                          
                          <div style={{display:'flex', height:'100%', alignItems:'center', justifyContent:'center'}}>
                            <img src={AppIcons.ds_actu_1} style={{width:130, height:'auto', aspectRatio:"auto"}} alt="" />
                          </div>
                          <div style={{display:'flex', marginLeft:8, flexDirection:'column', textAlign:'left'}}> 
                            <span>L'ére digitale de la comptabilité</span>
                            <span style={{fontSize:10}}>En savoir plus</span>
                          </div> 
                        </div>

                         <div style={{display:'flex', color:'#000000',fontWeight:'bold', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', backgroundColor:'#fff', width:'94%'}}>
                          
                          <div style={{display:'flex', height:'100%', alignItems:'center', justifyContent:'center'}}>
                            <img src={AppIcons.ds_actu_2} style={{width:130, height:'auto', aspectRatio:"auto"}} alt="" />
                          </div>
                          <div style={{display:'flex', marginLeft:8, flexDirection:'column', textAlign:'left'}}> 
                            <span>Plans comptables</span>
                            <span style={{fontSize:10}}>En savoir plus</span>
                          </div> 
                        </div>

                         <div style={{display:'flex', color:'#000000',fontWeight:'bold', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', backgroundColor:'#fff', width:'94%'}}>
                          
                          <div style={{display:'flex', height:'100%', alignItems:'center', justifyContent:'center'}}>
                            <img src={AppIcons.ds_actu_3} style={{width:130, height:'auto', aspectRatio:"auto"}} alt="" />
                          </div>
                          <div style={{display:'flex', marginLeft:8, flexDirection:'column', textAlign:'left'}}> 
                            <span>TOP 5 de l'actualité fiscale</span>
                            <span style={{fontSize:10}}>En savoir plus</span>
                          </div> 
                        </div>


                    </div>
                    }

 
                  </div>
                  
                  <div style={{display:'flex', width:'25%', border:'solid 1px gainsboro', height:'100%', flexDirection:'column'}}>

                      <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, padding:'10px 0px', textIndent:10, color: '#ffffff', backgroundColor:'#C09196'}}>Vos Services</div>
                      <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, flexDirection:'column', textAlign:'left', width:'95%', marginLeft:'2.5%'}}>

                        <List divided>

                          <List.Item style={{paddingTop:10, paddingBottom:10}}>
                            <Image avatar src={AppIcons.ds_service_icon} />
                            <List.Content>
                              <List.Header>BOX</List.Header>
                              <span style={{fontSize:10}}>Configuré par défaut</span>
                            </List.Content>
                            <List.Content floated='right'>
                                {/* <Button style={{fontSize:12, backgroundColor:'#ab3134', color:'#ffffff'}}>configurer</Button> */}
                            </List.Content>
                          </List.Item>

                          <List.Item style={{paddingTop:10, paddingBottom:10}}>
                            <Image avatar src={AppIcons.ds_service_icon} />
                            <List.Content>
                              <List.Header>BANQUE</List.Header>
                              <span style={{fontSize:10}}>Configuré par défaut</span>
                            </List.Content>
                            <List.Content floated='right'>
                                {/* <Button style={{fontSize:12, backgroundColor:'#ab3134', color:'#ffffff'}}>configurer</Button> */}
                            </List.Content>
                          </List.Item>
                        
                          <List.Item style={{paddingTop:10, paddingBottom:10}}>
                            <Image avatar src={AppIcons.ds_service_icon} />
                            <List.Content>
                              <List.Header>FACTURATION</List.Header>
                              <span style={{fontSize:10}}>A configurer</span>
                            </List.Content>
                            <List.Content floated='right'>
                                <Button style={{fontSize:12, backgroundColor:'#ab3134', color:'#ffffff'}} onClick={()=>this.toggleSettingsModal(true)}>configurer</Button>
                            </List.Content>
                          </List.Item>

                        </List>
                          
                    </div>  

                      {/* VIDEOS */}

                      <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, padding:'10px 0px', textIndent:10, color: '#ffffff', backgroundColor:'#C09196'}}>Tutos Vidéos</div>
                      <div style={{display:'flex', fontFamily:'Poppins', marginTop:5, flexDirection:'column', alignItems:'center'}}>
                          {/* Video */}
                          <div style={{display:'flex', color:'#000000',fontWeight:'bold', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', backgroundColor:'#fff', width:'80%', flexDirection:'column'}}>
                            
                            <div style={{display:'flex', height:'100%', alignItems:'center'}}>
                              {/* <img src={AppIcons.ds_actu_2} style={{width:130, height:'auto', aspectRatio:"auto"}} alt="" /> */}
                              <iframe
                                  width="853"
                                  height="auto"
                                  src={`https://www.youtube.com/embed/mG7O1WKxrdg`}
                                  frameBorder="0"
                                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                  allowFullScreen
                                  title="Embedded youtube"
                                />
                            </div>
                            
                            <div style={{display:'flex', marginLeft:1, flexDirection:'column', textAlign:'left'}}> 
                              <span>Comptabilité : Les bases</span>
                            </div> 
                          </div>

                           {/* Video */}
                           <div style={{display:'flex', color:'#000000',fontWeight:'bold', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', backgroundColor:'#fff', width:'80%', flexDirection:'column'}}>
                            
                            <div style={{display:'flex', height:'100%', alignItems:'center'}}>
                              <iframe
                                  width="853"
                                  height="auto"
                                  src={`https://www.youtube.com/embed/oXi5E0mDQOY`}
                                  frameBorder="0"
                                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                  allowFullScreen
                                  title="Embedded youtube"
                                />
                            </div>
                            <div style={{display:'flex', marginLeft:1, flexDirection:'column', textAlign:'left'}}> 
                              <span>Le bilan comptable</span>
                            </div> 
                          </div>
                          

                          {/* Video */}
                          <div style={{display:'flex', color:'#000000',fontWeight:'bold', fontSize: 14, fontFamily:'Poppins', marginTop:5, padding:'8px 4px', border:'solid 0.5px gainsboro', backgroundColor:'#fff', width:'80%', flexDirection:'column'}}>

                            <div style={{display:'flex', height:'100%', alignItems:'center'}}>
                              <iframe
                                  width="853"
                                  height="auto"
                                  src={`https://www.youtube.com/embed/pMXHCVl2MNg`}
                                  frameBorder="0"
                                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                  allowFullScreen
                                  title="Embedded youtube"
                                />
                            </div>
                            <div style={{display:'flex', marginLeft:1, flexDirection:'column', textAlign:'left'}}> 
                              <span>Organisation comptable</span>
                            </div> 
                          </div>

                          
                      </div>



                  </div>
                
                </div>
              </div>
 
          </div>
          
        {/* RIGHT BLOC */}

        


        </div>


    {/* -------------  MODAL SETTINGS ---------------------- */}


    <Modal
      onClose={() => this.toggleSettingsModal(false)}
      onOpen={() => this.toggleSettingsModal(true)}
      open={this.state.showSettingsModal}
    >
      <Modal.Header>PARAMETRES DE FACTURATION</Modal.Header>
      <Modal.Content style={{width:'95%', paddingBottom: 20}}>

        <BillSettingsScreen/>

      </Modal.Content>
      <Modal.Actions style={{paddingBottom:100}}>
        <Button color='black' onClick={() => this.toggleSettingsModal(false)}>
          Fermer
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}



    {/* -------------  MODAL CHANGE PACKS ---------------------- */}
    
    <Modal
      onClose={() => this.toggleChangePackModal(false)}
      onOpen={() => this.toggleChangePackModal(true)}
      open={this.state.showChangePackModal}
    >
      <Modal.Header>CHANGER DE FORFAIT
        {/* <Button floated='right' style={{backgroundColor:'#ab3134', color:"#ffffff", border:'solid 1px #ab3134'}} onClick={() => this.toggleValidatePackModal(true)}>
          ACTIVER MA LICENCE <Icon style={{marginLeft:10}} name="key" />
        </Button> */}
      </Modal.Header>
       
      <Modal.Content style={{width:'95%'}}>
       
        <Card.Group style={{justifyContent:'center'}} >

          {/* {packsList} */}
          <UpgradePartial />

        </Card.Group>


      </Modal.Content>
      <Modal.Actions>
        <Button color='black' onClick={() => this.toggleChangePackModal(false)}>
          Fermer
        </Button>
      </Modal.Actions>
    </Modal>

    {/* --------------------------------------------------- */}


    {/* -------------  MODAL VALIDATE LICENSE ---------------------- */}

    <Modal
      onClose={() => this.toggleValidatePackModal(false)}
      onOpen={() => this.toggleValidatePackModal(true)}
      open={this.state.showValidatePackModal}
    >
      <Modal.Header>ACTIVATION DE VOTRE LICENCE</Modal.Header>
      <Modal.Content style={{width:'95%', paddingBottom: 20}}>

        <Form loading={this.state.license_checking}>
          <Form.Input style={{textAlign:'center' }} error={this.state.input_license_key_error} label="Clé d'activation" placeholder='604C-068B-29FA-1700-5188-E449' onChange={(e) => this.handleLicenseActivationKey(e.target.value)} />
          <div style={{display:'flex', justifyContent:'center'}}>
            <Button style={{backgroundColor:'#ab3134', color:"#ffffff", border:'solid 1px #ab3134'}} onClick={() => this.activateLicense()} >ACTIVER</Button>
          </div>
        </Form>

      </Modal.Content>
    </Modal>

    {/* --------------------------------------------------- */}



      </div>
 
  );
  
 }
}

function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
