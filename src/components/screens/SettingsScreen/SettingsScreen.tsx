import React, { Component } from 'react';
import globalStyles from '../../global.module.css';
import styles from './style.module.css';

// import "tabler-react/dist/Tabler.module.css";

import { Nav, Card, Button } from "tabler-react";

import { motion } from "framer-motion";
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';

 
// Partials 
import SubHeaderPartial from '../../partials/SubHeaderPartial/SubHeaderPartial';
 
// import Files_mocked from '../../../services/mocks/Files_mocked';

import Dashboard from '../../Dashboard/Dashboard';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


// Data
import TesseractServices from '../../../services/TesseractServices';
import SettingsPartial from './SettingsPartial';

// const height = window.innerHeight;


class SettingsScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {

      isLoading : false,
      subheader_title : "Paramétres",
      toolbar_context_title : "Factures Clients",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      details_visibility : false,
      current_data_type : "customer",
      data_loading  : false,
      submenu : [
        {
          title : "Factures Clients",
          route : "customer"
        },
        {
          title : "Factures Fournisseurs",
          route : "provider"
        },
        // {
        //   title : "Documents Compta",
        //   route : "route"
        // },
        // {
        //   title : "Dossier XY",
        //   route : "route"
        // }
      ],
      toolbar_menu : [
        {
          title : "Editer",
        },
        {
          title : "Supprimer",
        }
      ],
      activities : [ ],

      global_files : [],
      files : []


    };


    this.onOneFileClicked = this.onOneFileClicked.bind(this);
    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);
 
  }
  

  componentDidMount()
  {
   
    // if (this.props.mainReduxState.files.length > 0 ) 
    // {
    //   this.setDataType(this.state.current_data_type);
    // }
    // else
    // {
    //   this._getBills()
    // }
    //console.log(this.props.mainReduxState.created_company);
  }


  _getBills()
  {
      var user = this.props.mainReduxState.user;
     
      this.setState({data_loading : true});
      
      TesseractServices._getBills(user.companyId).then(resp =>{

        this.setState({data_loading : false});
        // this.setState({global_files : resp.data, data_loading : false});
        this.props.mainReduxActions.add_files(resp.data);
        this.setDataType(this.state.current_data_type);
      
        }).catch((err)=>{
        
          console.log(err);
          this.setState({data_loading : false});

        });
  }

  setDataType(type : string)
  {
    var files = this.props.mainReduxState.files.filter((e)=>e.type == type);
    files.sort((a) => (a.status != "none") ? 1 : -1)
    this.setState({files : files});
  }

  onSwitchToTypeData(type : string)
  {
    this.setDataType(type);
  }

  onOneFileClicked(fileIndex : number) {
    //  Get File Data to display (Preview) + (Details Preview) 
    //console.log("File clicked : " + fileIndex)
  }
 
  onFilesAdded() {
    //  Get File Data to display
    // console.log("Files Added");
    this.setDataType(this.state.current_data_type);
  }
 
 
  onSubmenuClicked(index : number) {

    var current_data_type = this.state.submenu[index].route;
    this.setState({
        toolbar_context_title : this.state.submenu[index].title,
        current_data_type : current_data_type
    });
    //Switch content
    this.onSwitchToTypeData(current_data_type);
  }


 render() {

  // ANIMATIONS PROPS

  return (
    
      <div className={globalStyles.appContentContainer} style={{overflowY:'scroll'}}>

        <SubHeaderPartial title={this.state.subheader_title}  display_hmenu={false}  display_searchbar={false}/>

        <div className={globalStyles.appInnerBody}>
         
          <Dashboard />
        
        </div>
      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
