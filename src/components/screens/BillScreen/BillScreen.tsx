import React, { Component, useRef } from 'react';
import globalStyles from '../../global.module.css';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';

//const history = useHistory();


// Partials
import SubHeaderPartial from '../../partials/SubHeaderPartial/SubHeaderPartial';
import SubmenuPartial from '../../partials/SubmenuPartial/SubmenuPartial';
import BillViewPartial from "../../partials/BillViewPartial/BillViewPartial";


// Data
import TesseractServices from '../../../services/TesseractServices';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';
const height = window.innerHeight;

var refreshInterval: NodeJS.Timeout;
// const _tableNode = document.getElementById("table");
class BillScreen extends Component<any, any> {
  
  constructor(props:any)
  {
    super(props)
    
    this.state = {

      isLoading : false,
      subheader_title : "FACTURATION",
      toolbar_context_title : "Factures Clients",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      leftmenu_visibility : false,
      details_visibility : false,
      display_process_notif : false,
      display_results : false,
      current_data_type : "customer",
      data_loading : false,
      tabs : [
         "bills",
         "quotes",
        //  "customers"
      ],
      current_screen : "bills",
      submenu : [
        {
          title : "Mes Factures",
          route : "customer"
        },
        {
          title : "Mes Devis",
          route : "provider"
        } 
        // ,
        // {
        //   title : "Mes Clients",
        //   route : "provider"
        // } 
      ],
      toolbar_menu : [
        {
          title : "Traiter"
        },
        {
          title : "Visualiser"
        },
        // {
        //   title : "Editer"
        // },
        // {
        //   title : "Supprimer"
        // },
        {
          title : "Générer"
        }
      ],
  
      global_files : [],
      files : [
        {
            title : "File Box No 1",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Box No 2",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Box No 3",
            route : "route",
            status: "none",
            type  : "provider"
        }
    ],
      processing : false, // Will help for Interval api call to checkout if we have all our files processed "success" or "failed"
      exportVisibility : "visible"

    };


    this.onOneFileClicked = this.onOneFileClicked.bind(this);
    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
 
  }
  


  componentDidMount()
  {
      // this._getBills();
  }

  _getBills()
  {
      var user = this.props.mainReduxState.user;
      if (this.props.mainReduxState.files.length > 0 ) 
      {
        this.setDataType(this.state.current_data_type);
      }
      else //Only when empty : Not passed by BOX before
      { 
      
        this.setState({data_loading : true});
        
        TesseractServices._getBills(user.companyId).then(resp =>{

          this.setState({data_loading : false});
          // this.setState({global_files : resp.data, data_loading : false});
          this.props.mainReduxActions.add_files(resp.data);
          this.setDataType(this.state.current_data_type);
        
          }).catch((err)=>{
          
            console.log(err);
            this.setState({data_loading : false});

          });
      }
  }

  setDataType(type : string)
  {
    var files = this.props.mainReduxState.files.filter((e)=>e.type == type  && ( e.is_exported == false || e.is_exported == undefined));
    // console.log(files)
    this.setState({files : files});
    // console.log(this.state.files);
  }


  onOneFileClicked(fileIndex : number) {
    //  Get File Data to display (Preview) + (Details Preview) 
    // localStorage.setItem('selected_file', JSON.stringify(this.state.files[fileIndex]));
  
    // console.log("File clicked : " + fileIndex)
  }
 
  onSubmenuClicked(index : number) {

    var screen_name = this.state.tabs[index];

    this.setState({current_screen : screen_name})

  }

 render() {

  // ANIMATIONS PROPS


  return (
    
      <div className={globalStyles.appContentContainer} style={{overflowY:'scroll'}}>

        <SubHeaderPartial title={this.state.subheader_title} display_bill_hmenu={true} display_searchbar={this.state.display_searchbar}
        onSubmenuClicked={this.onSubmenuClicked} />

        <div className={globalStyles.appInnerBody}>
        {/* LEFT BLOC */}
        
          {this.state.leftmenu_visibility &&
            <div className={globalStyles.appContentContainerLeft}>

            <SubmenuPartial submenu={this.state.submenu}/>

            {/* <ActivitiesPartial activities={this.state.activities} /> */}

          </div>
          }
        
        {/*MIDDLE BLOC */}

          <div className={globalStyles.appContentContainerCenter}>
              
              {/* --------- INNER MIDDLE ---------- */}
 
              {this.state.current_screen == "bills" &&
                <BillViewPartial tab="bills" />
              }
 
              {this.state.current_screen == "quotes" &&
                <BillViewPartial tab="quotes" />
              }


          </div>
          
        </div>
         
      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BillScreen);
