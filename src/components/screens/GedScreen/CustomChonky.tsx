import React, { } from 'react';
import {
    ChonkyActions,
    ChonkyFileActionData,
    FileArray,
    FileBrowserProps,
    I18nConfig,
    I18nNamespace,
    FileData,
    FileHelper,
    FullFileBrowser,
    setChonkyDefaults
} from 'chonky';

import {reactLocalStorage} from 'reactjs-localstorage';
import { Position, Toaster, Intent } from "@blueprintjs/core";
import { ChonkyIconFA } from 'chonky-icon-fontawesome';
import TesseractServices from '../../../services/TesseractServices';
import UserServices from '../../../services/UserServices';
import Emitter from '../../../services/emitter';
import { IntlShape, IntlConfig } from 'react-intl';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';


interface CustomFileData extends FileData {
    parentId?: string;
    childrenIds?: string[];
}
interface CustomFileMap {
    [fileId: string]: CustomFileData;
}

setChonkyDefaults({ iconComponent: ChonkyIconFA });


interface inFr extends IntlConfig {

}

const fr : I18nConfig = {
    // locale:'fr',
    // messages : {},
    // formats : {},
    // defaultFormats:{},
    // defaultLocale:"fr",
    // onError : function(){}
}


class CustomChonky extends React.Component<any, any> {
    constructor(props:any)
    {
      super(props)
      
      this.state = {

        files:[null, null, null],
        idCounter :0,
        fileMap:[null],
        fileMapId: "",
        currentFolderId:"",

        baseFileMap:[null],
        rootFolderId:"",

        currentFolderIdRef:null,

        folderChain : []
      };
   
    }

    componentDidMount(){

        // Init
        this.prepareCustomFileMap();

    }



    setCurrentFolderId = (folderId:string) => {

        this.setState({currentFolderId : folderId, currentFolderIdRef : folderId});

        // Update files chain
        this.useFiles(this.state.fileMap, folderId);

    }

    prepareCustomFileMap = () => {

        // var DemoFsMap = 
        // {"rootFolderId":"qwerty123456","fileMap":{"qwerty123456":{"id":"qwerty123456","name":"Archivage","isDir":true,"childrenIds":["Factures Clients", "Factures Fournisseurs", "Devis"],"childrenCount":3},
        //     "Factures Clients":{"id":"Factures Clients","color": "#c29399", "name":"Factures Clients","isDir":true,"modDate":"2020-10-24T17:48:39.866Z","childrenIds":[],"childrenCount":0,"parentId":"qwerty123456"},
        //     "Factures Fournisseurs":{"id":"Factures Fournisseurs","color": "#c29399", "name":"Factures Fournisseurs","isDir":true,"modDate":"2020-10-24T17:48:39.866Z","childrenIds":[],"childrenCount":0,"parentId":"qwerty123456"},
        //     "Devis":{"id":"Devis","color": "#c29399", "name":"Devis","isDir":true,"modDate":"2020-10-24T17:48:39.866Z","childrenIds":[],"childrenCount":0,"parentId":"qwerty123456"}
        // }}

        // var loggedUser = reactLocalStorage.getObject('ls_loggedUser');
        // var companyId = loggedUser.companyId;
        
        var selectedCompany = this.props.mainReduxState.selected_company;
        var companyId = selectedCompany.id;
    
        UserServices._getCompanyGed(companyId).then((res)=>{
            
            // console.log("gaymaaji");
            var fsMap = res.data[0].files; //Where Request type
            var fsMapId = res.data[0].id;

            const baseFileMap = (fsMap.fileMap as unknown) as CustomFileMap;
            const rootFolderId = fsMap.rootFolderId;
            
            this.setState({ fileMapId : fsMapId, baseFileMap : baseFileMap, fileMap : baseFileMap, rootFolderId : rootFolderId, currentFolderIdRef:rootFolderId });

            // USE FILES
            this.useFiles(baseFileMap, rootFolderId);
            this.setCurrentFolderId(rootFolderId);

        })
        .catch((err)=>{
            console.log(err);
        });
           
    };

    useFiles = (fileMap: CustomFileMap, currentFolderId: string) => {
 
        const currentFolder = fileMap[currentFolderId];
        const childrenIds = currentFolder.childrenIds!;
        const files = childrenIds.map((fileId: string) => fileMap[fileId]);
        // console.log(files);
        this.setState({files : files})

        this.useFolderChain(fileMap, currentFolderId);
    }

    updateRemoteFilemap = (fileMap:any)  => {

        console.log("Updating...")
        
        const AppToaster = Toaster.create({
            className: "recipe-toaster",
            position: Position.TOP,
            });

        var fsMap = {"rootFolderId":this.state.rootFolderId ,"fileMap": fileMap};

        UserServices._updateGed(this.state.fileMapId, fsMap).then((res)=>{
            
            AppToaster.show({ intent:Intent.SUCCESS, message: "GED mis à jour !" });

        })
        .catch((err)=>{

            AppToaster.show({ intent:Intent.DANGER, message: "Erreur lors de la sauvegarde !" });

        });
    }

    useFolderChain = (
        fileMap: CustomFileMap,
        currentFolderId: string
    ) => {
        
        const currentFolder = fileMap[currentFolderId];

        const folderChain = [currentFolder];

        let parentId = currentFolder.parentId;
        while (parentId) {
            const parentFile = fileMap[parentId];
            if (parentFile) {
                folderChain.unshift(parentFile);
                parentId = parentFile.parentId;
            } else {
                break;
            }
        }

        this.setState({folderChain : folderChain});
    }



    resetFileMap = () => {
        this.setState({fileMap : this.state.baseFileMap })
        this.setState({currentFolderId :  this.state.rootFolderId })
    }

    deleteFiles = (files: CustomFileData[]) => {
         
            // Create a copy of the file map to make sure we don't mutate it.
            const newFileMap = { ...this.state.fileMap };

            files.forEach((file) => {
                // Delete file from the file map.
                delete newFileMap[file.id];

                // Update the parent folder to make sure it doesn't try to load the
                // file we just deleted.
                if (file.parentId) {
                    const parent = newFileMap[file.parentId]!;
                    const newChildrenIds = parent.childrenIds!.filter(
                        (id) => id !== file.id
                    );
                    newFileMap[file.parentId] = {
                        ...parent,
                        childrenIds: newChildrenIds,
                        childrenCount: newChildrenIds.length,
                    };
                }
            });
 
            this.setState({fileMap : newFileMap })
            this.useFiles(this.state.fileMap, this.state.currentFolderIdRef);
            this.updateRemoteFilemap(newFileMap);
            
    };

    
    downloadFiles = (selectedFiles : any[]) => {

        // console.log('Downloaded')
        console.log(selectedFiles)
    }

    openFile = (selectedFile : any) => {

        console.log(selectedFile)
        Emitter.emit(Emitter.types.DISPLAY_GED_FILE, selectedFile);
    }


    moveFiles = (
        files: CustomFileData[],
        source: CustomFileData,
        destination: CustomFileData) => {

            const newFileMap = { ...this.state.fileMap };
            const moveFileIds = new Set(files.map((f) => f.id));

            // Delete files from their source folder.
            const newSourceChildrenIds = source.childrenIds!.filter(
                (id) => !moveFileIds.has(id)
            );
            newFileMap[source.id] = {
                ...source,
                childrenIds: newSourceChildrenIds,
                childrenCount: newSourceChildrenIds.length,
            };

            // Add the files to their destination folder.
            const newDestinationChildrenIds = [
                ...destination.childrenIds!,
                ...files.map((f) => f.id),
            ];
            newFileMap[destination.id] = {
                ...destination,
                childrenIds: newDestinationChildrenIds,
                childrenCount: newDestinationChildrenIds.length,
            };

            // Finally, update the parent folder ID on the files from source folder
            // ID to the destination folder ID.
            files.forEach((file) => {
                newFileMap[file.id] = {
                    ...file,
                    parentId: destination.id,
                };
            });

        this.setState({fileMap : newFileMap })
        this.useFiles(this.state.fileMap, this.state.currentFolderIdRef);
        this.updateRemoteFilemap(newFileMap);
        

    }

    createFolder = (folderName: string) => {

        const newFileMap = { ...this.state.fileMap };
        // var idCounter = this.state.idCounter;

        // Create the new folder

        // Get Here current folder childcount
        var thisId = newFileMap[this.state.currentFolderIdRef].id;
        var childCount = newFileMap[this.state.currentFolderIdRef].childrenCount;
        var newItemId = "";

        var newChildCount = childCount + 1;

        if(childCount == 0)
        {
            newItemId = `${thisId}-new-folder-${childCount}`;
        }
        else
        {
            var lastChildIndex = newFileMap[this.state.currentFolderIdRef].childrenIds.length - 1;
            var lastChildId = newFileMap[this.state.currentFolderIdRef].childrenIds[lastChildIndex];
            var lastCountString = lastChildId.split("-").pop();
            var lastCountInt = parseInt(lastCountString);
            
            lastCountInt = lastCountInt + 1;

            newItemId = `${thisId}-new-folder-${lastCountInt}`;
 
        }
         

        newFileMap[newItemId] = {
            id: newItemId,
            name: folderName,
            color: "#c29399",
            isDir: true,
            modDate: new Date(),
            parentId: this.state.currentFolderIdRef,
            childrenIds: [],
            childrenCount: 0,
        };


        // Update parent folder to reference the new folder.
        var parent = newFileMap[this.state.currentFolderIdRef];
        newFileMap[this.state.currentFolderIdRef] = {
            ...parent,
            childrenIds: [...parent.childrenIds, newItemId],
            childrenCount : newChildCount
        };

        this.setState({fileMap : newFileMap}) //, currentFolderIdRef : newFolderId
        this.useFiles(this.state.fileMap, this.state.currentFolderIdRef);
        this.updateRemoteFilemap(newFileMap);
        
        // setTimeout(() => {
        //     console.log(this.state.fileMap);
        // }, 250);
        

    }


    uploadFile = () => {
            
        var input = document.createElement('input');
        input.type = 'file';
        input.name = 'file';

                    
        input.onchange = (e) => { 
            //@ts-ignore
            var file = e.target.files[0]; 
           
            var formData = new FormData();
            formData.append('file',file);

            const AppToaster = Toaster.create({
                className: "recipe-toaster",
                position: Position.TOP,
                });
            // console.log(file);
            TesseractServices._uploadFile(formData).then(resp =>{

                var savedFile = resp.data.files[0];
                var fileName = savedFile.filename;
                
                // UPDATE FIE /////////////////////////////

                    const newFileMap = { ...this.state.fileMap };

                    var thisId = newFileMap[this.state.currentFolderIdRef].id;
                    var childCount = newFileMap[this.state.currentFolderIdRef].childrenCount;
                    var newItemId = "";
                    var newChildCount = childCount + 1;
                    if(childCount == 0)
                    {
                        newItemId = `${thisId}-new-file-${childCount}`;
                    }
                    else
                    {
                        var lastChildIndex = newFileMap[this.state.currentFolderIdRef].childrenIds.length - 1;
                        var lastChildId = newFileMap[this.state.currentFolderIdRef].childrenIds[lastChildIndex];
                        var lastCountString = lastChildId.split("-").pop();
                        var lastCountInt = parseInt(lastCountString);
             
                        lastCountInt = lastCountInt + 1;
                        newItemId = `${thisId}-new-file-${lastCountInt}`;
                         
                    }

                    // Create the new file
                    // const fileBase = "https://dscompta-api.herokuapp.com/files/";
                    newFileMap[newItemId] = {
                        id: newItemId,
                        name: fileName,
                        isDir: false,
                        modDate: new Date(),
                        parentId: this.state.currentFolderIdRef,
                        childrenIds: [],
                        childrenCount: 0,
                    };
        
                    // Update parent folder to reference the new folder.
                    const parent = newFileMap[this.state.currentFolderIdRef];
                    newFileMap[this.state.currentFolderIdRef] = {
                        ...parent,
                        childrenIds: [...parent.childrenIds!, newItemId],
                        childrenCount : newChildCount

                    };
        
                    AppToaster.show({ intent:Intent.SUCCESS, message: "Fichier archivé avec succés !" });

                //     return newFileMap;
                this.setState({fileMap : newFileMap })
                this.useFiles(this.state.fileMap, this.state.currentFolderIdRef);
                this.updateRemoteFilemap(newFileMap);
                

                // UPDATE FILE ////////////////////////////////

                }).catch((err)=>{
                
                console.log(err);
    
                });
        }

        input.click();


    }

    

    render() {
       
        
        

        const russ : I18nConfig = {
            // locale:'fr',
            // formats : {},
            // defaultFormats:{},
            // defaultLocale:"fr",
            // onError : function(){},
             
                // formatFileModDate: (intl: IntlShape, file: FileData | null) => {
                //     const safeModDate = FileHelper.getModDate(file);
                //     if (safeModDate) {
                //         return `${intl.formatDate(safeModDate)}, ${intl.formatTime(
                //             safeModDate
                //         )}`;
                //     } else {
                //         return null;
                //     }
                // },
                // formatFileSize: (intl: IntlShape, file: FileData | null) => {
                //     if (!file || typeof file.size !== 'number') return null;
                //     return `Размер: ${file.size}`;
                // },

            // messages: {
            //     // Chonky UI translation strings. String IDs hardcoded into Chonky's source code.
            //     'chonky.toolbar.searchPlaceholder': 'Поиск',
            //     'chonky.toolbar.visibleFileCount': `{fileCount, plural,
            //         one {# файл}
            //         few {# файла}
            //         many {# файлов}
            //     }`,
            //     'chonky.toolbar.selectedFileCount': `{fileCount, plural,
            //         =0 {}
            //         one {# выделен}
            //         other {# выделено}
            //     }`,
            //     'chonky.toolbar.hiddenFileCount': `{fileCount, plural,
            //         =0 {}
            //         one {# скрыт}
            //         other {# скрыто}
            //     }`,
            //     'chonky.fileList.nothingToShow': 'Здесь пусто!',
            //     'chonky.contextMenu.browserMenuShortcut': 'Меню браузера: {shortcut}',

            //     // File action translation strings. These depend on which actions you have
            //     // enabled in Chonky.
            //     [`chonky.actionGroups.Actions`]: 'Действия',
            //     [`chonky.actionGroups.Options`]: 'Опции',
            //     [`chonky.actions.${ChonkyActions.OpenParentFolder.id}.button.name`]: 'Открыть родительскую папку',
            //     [`chonky.actions.${ChonkyActions.CreateFolder.id}.button.name`]: 'Новая папка',
            //     [`chonky.actions.${ChonkyActions.CreateFolder.id}.button.tooltip`]: 'Создать новую папку',
            //     [`chonky.actions.${ChonkyActions.DeleteFiles.id}.button.name`]: 'Удалить файлы',
            //     [`chonky.actions.${ChonkyActions.OpenSelection.id}.button.name`]: 'Открыть выделение',
            //     [`chonky.actions.${ChonkyActions.SelectAllFiles.id}.button.name`]: 'Выделить все',
            //     [`chonky.actions.${ChonkyActions.ClearSelection.id}.button.name`]: 'Сбросить выделение',
            //     [`chonky.actions.${ChonkyActions.EnableListView.id}.button.name`]: 'Показать список',
            //     [`chonky.actions.${ChonkyActions.EnableGridView.id}.button.name`]: 'Показать иконки',
            //     [`chonky.actions.${ChonkyActions.SortFilesByName.id}.button.name`]: 'Сорт. по имени',
            //     [`chonky.actions.${ChonkyActions.SortFilesBySize.id}.button.name`]: 'Сорт. по размеру',
            //     [`chonky.actions.${ChonkyActions.SortFilesByDate.id}.button.name`]: 'Сорт. по дате',
            //     [`chonky.actions.${ChonkyActions.ToggleHiddenFiles.id}.button.name`]: 'Скрытые файлы',
            //     [`chonky.actions.${ChonkyActions.ToggleShowFoldersFirst.id}.button.name`]: 'Папки в начале',
            // },
        };
       
        const fileActions = [
            ChonkyActions.CreateFolder,
            ChonkyActions.DeleteFiles,
            ChonkyActions.UploadFiles,
            ChonkyActions.DownloadFiles,
        ];
    

        const useFileActionHandler = (
            setCurrentFolderId: (folderId: string) => void,
            deleteFiles: (files: CustomFileData[]) => void,
            moveFiles: (files: FileData[], source: FileData, destination: FileData) => void,
            createFolder: (folderName: string) => void,
            uploadFile: (fileName: string) => void,
            downloadFiles: (selectedFiles:any[]) => void,
            openFile: (selectedFile:any) => void,
        ) => {
                return (data: ChonkyFileActionData) => {
                    if (data.id === ChonkyActions.OpenFiles.id) {
                        const { targetFile, files } = data.payload;
                        const fileToOpen = targetFile ?? files[0];
                        if (fileToOpen && FileHelper.isDirectory(fileToOpen)) {
                            setCurrentFolderId(fileToOpen.id);
                            return;
                        }
                        else if (fileToOpen && !FileHelper.isDirectory(fileToOpen)) {
                            console.log("Display FILE");
                            openFile(fileToOpen);
                            return;
                        }
                    } else if (data.id === ChonkyActions.DeleteFiles.id) {
                        deleteFiles(data.state.selectedFilesForAction!);
                    } else if (data.id === ChonkyActions.MoveFiles.id) {
                        moveFiles(
                            data.payload.files,
                            data.payload.source!,
                            data.payload.destination
                        );
                    }
                    else if (data.id === ChonkyActions.UploadFiles.id) {
                        uploadFile("filename");
                    }
                    else if (data.id === ChonkyActions.DownloadFiles.id) {
                        const selectedFiles = data.state.selectedFiles;
                        downloadFiles(selectedFiles);
                    }
                    else if (data.id === ChonkyActions.CreateFolder.id) {
                        const folderName = prompt('Saisir le nom du dossier :');
                        if (folderName) createFolder(folderName);
                    }
        
                    // showActionNotification(data);
                }
        };
 

        const handleFileAction = useFileActionHandler(
            this.setCurrentFolderId,
            this.deleteFiles,
            this.moveFiles,
            this.createFolder,
            this.uploadFile,
            this.downloadFiles,
            this.openFile,
        );
 
        const thumbnailGenerator = (file: FileData) =>
                file.thumbnailUrl ? `https://chonky.io${file.thumbnailUrl}` : null
         
        
        return (
            <FullFileBrowser 
                files={this.state.files} 
                folderChain={this.state.folderChain}
                onFileAction={handleFileAction}
                fileActions={fileActions}
                thumbnailGenerator={thumbnailGenerator}
                 />
        );

    }


};

function mapStateToProps(state:any) {
    return {
      mainReduxState: state.mainReduxState
    }
  }
  
  function mapDispatchToProps(dispatch:any) {
    return {
      mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(CustomChonky);
  
