import React, { Component } from 'react';
import globalStyles from '../../global.module.css';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';

//const history = useHistory();
import { Table } from 'semantic-ui-react';
import { Button as CBTN } from "@blueprintjs/core";


// import { Link } from 'react-router-dom';
// import routes from '../../../constants/routes.json';
// import { history, configuredStore } from '../../../store';

// Partials
import ToolbarPartial from '../../partials/ToolbarPartial/ToolbarPartial';
import SubHeaderPartial from '../../partials/SubHeaderPartial/SubHeaderPartial';
// import Files_mocked from '../../../services/mocks/Files_mocked';


import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';

// Data
import TesseractServices from '../../../services/TesseractServices';
import CustomChonky from "./CustomChonky";
import Emitter from '../../../services/emitter';
import FileViewer from 'react-file-viewer';

class GedScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {

      isLoading : false,
      subheader_title : "GED",
      toolbar_context_title : "",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      preview_right_pos : -110,
      displayedFile : "",
      displayedFileType : "",
      openedFile : {csv_data : {"rows" : []}},
      display : false,
      mediahost: "https://dscompta-api.herokuapp.com/files/",


      details_visibility : false,
      current_view : "customer",
      data_loading  : false,
      submenu : [
        {
          title : "Factures Clients",
          route : "customer"
        },
        {
          title : "Factures Fournisseurs",
          route : "provider"
        },
        {
          title : "Documents Compta",
          route : "route"
        },
        // {
        //   title : "Dossier XY",
        //   route : "route"
        // }
      ],
      toolbar_menu : [
        {
          title : "Editer",
        },
        {
          title : "Supprimer",
        }
      ],

      global_files : [],
      files : [],
 
    };

    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
 
  }
  

  componentDidMount()
  {

    Emitter.on(Emitter.types.DISPLAY_GED_FILE, (file) => this.displayPreview(file));

  }

  onSubmenuClicked(index : number) {

    var current_view = this.state.submenu[index].route;
    this.setState({
        toolbar_context_title : this.state.submenu[index].title,
        current_view : current_view
    });
    
  }

  displayPreview(file:any)
  {
   
    var preview_pos = 0;
    var filename = this.state.mediahost + file.name /*"1610285264880_Facture_FA0184_NS_ConsultingJanv2.png" */ ;
    var filetype =  /* "png" */ file.name.split(".").pop();
    this.setState({
      openedFile : file,
      preview_right_pos : preview_pos,
      displayedFile : filename,
      displayedFileType : filetype,
      display : true
    });

    if(!file.has_extra) // If not csv but Image
    {
      setTimeout(() => {
      
        var container = document.getElementById("pg-photo-container");
        var pageViewer = document.getElementsByClassName("photo");
      
        var img = pageViewer[0];
        
        //@ts-ignore
        container.style.width = "100%";
        //@ts-ignore
        container.style.height = "auto";

        //@ts-ignore
        img.style.width = "100%";
        //@ts-ignore
        img.style.height = "auto";

      }, 500);

    }
    // var display = this.state.preview_right_pos == -110 ? true : false;
    
  }

  hidePreview()
  {
   
    var preview_pos = -110;
    this.setState({
      preview_right_pos : preview_pos,
      display : false
    });
    
    // var display = this.state.preview_right_pos == -110 ? true : false;
  }

  
  downloadExcel = () => 
  {
    let t2e = new Table2Excel('#statement_table', 'exported_to_excel');
    t2e.export();
  }

 render() {

  // ANIMATIONS PROPS

    const statementHeader = this.state.openedFile.csv_data.rows.map((item :any, index : number) =>{
      if(index == 0) // Only the header
        return(
        <Table.Row >
          <Table.HeaderCell>N°</Table.HeaderCell>
            {
              item.map((item2 :any, index2 : number) => {
                return (
                  <Table.HeaderCell>{item2}</Table.HeaderCell>
                );
            })}

        </Table.Row>
        
        )
      }
    );
    
    const statementOps = this.state.openedFile.csv_data.rows.map((item :any, index : number) =>{
      if(index != 0) // All except the header
        return(
        <Table.Row >
          <Table.Cell > {index} </Table.Cell>
            {
              item.map((item2 :any, index2 : number) => {
                return (
                  <Table.Cell>{item2}</Table.Cell>
                );
            })}

        </Table.Row>
        
        )
      }
    );

  return (
    
      <div className={globalStyles.appContentContainer} style={{overflowY:'scroll'}}>

        <SubHeaderPartial title={this.state.subheader_title}  display_hmenu={false}  display_searchbar={false}/>

        <div className={globalStyles.appInnerBody} style={{position:'relative', overflow:'hidden'}}>
        {/* LEFT BLOC */}
          {/* <div className={globalStyles.appContentContainerLeft}>

            <SubmenuPartial submenu={this.state.submenu} denyContext={true} onSubmenuClicked={this.onSubmenuClicked} />

            <ActivitiesPartial activities={this.state.activities} />

          </div> */}
        
        {/*MIDDLE BLOC */}

          <div className={globalStyles.appContentContainerCenter}>
              
              <ToolbarPartial title={this.state.subheader_title} context_title={this.state.toolbar_context_title} menu={this.state.toolbar_menu} files={this.state.files} display_stats={false} />

              {/* <FilterbarPartial/> */}



              {/* --------- INNER MIDDLE ---------- */}
              {/* <FilesContainerPartial data_loading={this.state.data_loading} files={this.state.files} onOneFileClicked={this.onOneFileClicked} /> */}
              
              {/* <div className={globalStyles.appInnerMiddleContainer} style={{overflowY: 'scroll'}}>

                <div style={{display:'flex', flexDirection:'column', alignItems:'center', marginTop:'10%'}}>
                  <img src={AppIcons.ds_ged_icon} style={{width:80, height:"auto", aspectRatio:"auto", opacity:0.5}} alt="" />
                  <span style={{fontFamily:'Poppins',color:'#000000', marginTop:10 }} >Aucun dossier n'est encore archivé</span>
                </div>
                
              </div> */}

             <div style={{ height: 480 }}>

                <CustomChonky/>
                {/* <ChonkyComponent instanceId="GED" /> */}
               
            </div> 



          </div>
          

        {/* RIGHT BLOC */}

        <motion.div animate={{right : this.state.preview_right_pos+'%'}} 
                      transition={{ ease: "easeOut"}}
              className={styles.appInnerMiddlePreviewContainer}>
              <div style={{position:"absolute", left:-50, top:0}}>
                <img src={AppIcons.ds_preview_folder_banner} style={{width:56, height:"auto"}} alt="" />
                <img  src={AppIcons.ds_preview_folder_icon} 
                style={{  position: "absolute",
                          width: 34,
                          left: "20%",
                          top: "18%",
                          height: "auto", cursor:"pointer"}}
                          onClick={()=>{this.hidePreview()}}
                          alt="" />
              </div>
             
                  {this.state.display && !this.state.openedFile.has_extra &&
                    <div id="preview_file_container" className={styles.previewFileContainer}>
                  
                      <FileViewer
                      
                        fileType={this.state.displayedFileType}
                        filePath={this.state.displayedFile}
                        
                        />
                    </div>
                  }
                  {this.state.display && this.state.openedFile.has_extra &&
                  
                    <div id="preview_file_container" className={styles.previewFileContainer}>
                      
                        <CBTN rightIcon="download" intent="success" color="#ffffff" text="VERS EXCEL" style={{position:'absolute', top:5, right:5, backgroundColor : '#ab3134'}} onClick={this.downloadExcel} />
                        
                        <div id="pdf_preview" style={{display:'flex', flexDirection:'column', width:'100%', margin:'auto', marginTop:25}}>
                            <Table celled striped id="statement_table">
                              <Table.Header>
                                {statementHeader}
                              </Table.Header>
                              <Table.Body style={{height:100}}>
                                {statementOps}
                              </Table.Body>
                            </Table>
                        </div>
                    </div>
                  }

             
          </motion.div>

        </div>

      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GedScreen);
