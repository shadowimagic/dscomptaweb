import React, { Component } from 'react';
import globalStyles from '../../global.module.css';
import styles from './style.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';


//const history = useHistory();


// Partials 
import Files_mocked from '../../../services/mocks/Files_mocked';
import FilterbarPartial from '../../partials/FilterbarPartial/FilterbarPartial';
import TesseractServices from '../../../services/TesseractServices';
import Emitter from '../../../services/emitter';


// Data
const height = window.innerHeight;
 
class ProcessResultScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    this.state = {

      isLoading : false,
      subheader_title : "BOX",
      toolbar_context_title : "Factures Clients",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      details_visibility : false,
      preview_right_pos : -110,
      current_data_type : "customer",
      journal_type : "customer",
      active_month : new Date().getMonth(),
      mediahost: "https://dscompta-api.herokuapp.com/files/",

      submenu : [
        {
          title : "Factures Clients",
          route : "customer"
        },
        {
          title : "Factures Fournisseurs",
          route : "provider"
        } 
      ],
      toolbar_menu : [
        {
          title : "Editer",
        },
        {
          title : "Supprimer",
        }
      ],
      activities : [ ],

      selected_file : {},
      global_files : [],
      files : [],
      current_file_uri : AppIcons.ds_facture_model,
      current_file_index : 0


    };


    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
 
  }
  

  componentDidMount()
  {
    this.setDataType(this.state.current_data_type);
  
    Emitter.on(Emitter.types.MONTH_CHANGED, (newMonth) => this.onMonthChanged(newMonth));
  
    setTimeout(() => {
      this.onMonthChanged(this.state.active_month)
    }, 150);
  }


  setDataType(type : string)
  {
    var files = this.props.files.filter((e)=>e.type == type && e.status == "success"  && this.state.active_month == new Date(e.date).getMonth() && new Date(e.date).getFullYear() == new Date().getFullYear());
    this.setState({files : files});
    // console.log(files);
  }

  onSwitchToTypeData(type : string)
  {
    this.setDataType(type);
  }

  onMonthChanged(month:number)
  {
    // Send this to processing screen 
    this.props.onMonthChanged(month);
   
  }  
 
  onSubmenuClicked(index : number) {

    var current_data_type = this.state.submenu[index].route;
    this.setState({
        toolbar_context_title : this.state.submenu[index].title,
        current_data_type : current_data_type
    });
    //Switch content
    this.onSwitchToTypeData(current_data_type);
  }

  togglePreview(fileIndex:number, uri:string)
  {
    this.setState({current_file_uri : uri, current_file_index : fileIndex})
    var preview_pos = this.state.preview_right_pos == -110 ? 0 : -110;
    this.setState({preview_right_pos : preview_pos});
  }
  
  formatDate(str:string)
  {
    return new Date(str).toLocaleDateString();
  }

  getTVAValue(item:any)
  {
    return Math.round((item.ttc - item.ht));
  }

  setupOtion(type:string)
  {
    switch (type) {
      case "edit":
        
        break;
    
      default:
        break;
    }
  }

  changeJournalType(type : string){
    this.setState({journal_type : type});   
    this.setDataType(type);
  }

   
 render() {

  // ANIMATIONS PROPS

  const tr_items = this.props.files.map((item :any, index : number) =>
    {      
       return (
         [1,2,3].map((_item : number, _index:number ) =>
         
            <tr key={(index * 3) + _item}  className={(index%2==0 ? styles.row_bg_1 : styles.row_bg_2)} >
                <td>{(index * 3) + _item}</td>
                <td contentEditable suppressContentEditableWarning={true} >{this.formatDate(item.date)}</td>
                <td contentEditable suppressContentEditableWarning={true} >{item.no_piece}</td>
                <td contentEditable suppressContentEditableWarning={true}>{item.no_compte}</td>
                <td contentEditable suppressContentEditableWarning={true}>{item.libelle}</td>
                {/* +++++++++++++++++ VTE ++++++++++++++++ */}
                    {/*------First Line and VTE -------*/}
                    {_item == 1 &&  item.type == "customer" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}>{item.ttc}</td>
                    }
                    {_item == 1 &&  item.type == "customer" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}></td>
                    }
                    {/* ------------------------------ */}
                    {/*------Second Line and VTE -------*/}
                    {_item == 2 &&  item.type == "customer" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}></td>
                    }
                    {_item == 2 &&  item.type == "customer" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}>{item.ht}</td>
                    }
                    {/* ------------------------------ */}
                    {/*------Third Line and VTE -------*/}
                    {_item == 3 &&  item.type == "customer" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}></td>
                    }
                    {_item == 3 &&  item.type == "customer" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}>{this.getTVAValue(item)}</td>
                    }
                    {/* ------------------------------ */}
                {/* +++++++++++++++++++++++++++++++++++++++ */}


                {/* +++++++++++++++++ HA ++++++++++++++++ */}
                    {/*------First Line and HA -------*/}
                    {_item == 1 &&  item.type == "provider" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}></td>
                    }
                    {_item == 1 &&  item.type == "provider" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}>{item.ttc}</td>
                    }
                    {/* ------------------------------ */}
                    {/*------Second Line and HA -------*/}
                    {_item == 2 &&  item.type == "provider" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}>{item.ht}</td>
                    }
                    {_item == 2 &&  item.type == "provider" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}></td>
                    }
                    {/* ------------------------------ */}
                    {/*------Third Line and HA -------*/}
                    {_item == 3 &&  item.type == "provider" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}>{this.getTVAValue(item)}</td>
                    }
                    {_item == 3 &&  item.type == "provider" &&   
                      <td contentEditable suppressContentEditableWarning={true} style={{fontSize : 13}}></td>
                    }
                    {/* ------------------------------ */}
                {/* +++++++++++++++++++++++++++++++++++++++ */}


              <td>
                <img src={AppIcons.ds_doc_icon} style={{width:18, marginLeft: "30%", height:"auto", cursor:"pointer"}} alt="" 
                onClick={()=>this.togglePreview(index, item.uri)}
                />
              </td>
            </tr>
         
         )   
       
        )
    }
  ); 

  // console.log(table_items)


  return (
    
    <div className={styles.appContentContainer} style={{overflowY:'scroll', visibility : this.props.exportVisibility}}>
      
      {/* HEADER BAR */}

      <div className={styles.appHeader}>
                
              <div className={styles.appHeaderTitlebox}>
                <span>TRAITEMENT</span>
              </div>

              <div className={styles.appHeaderJournal}>
                <span>Journal</span>
                <select name="" id="" value={this.state.journal_type} onChange={(e:any)=>this.changeJournalType(e.target.value)}>
                  <option defaultValue="customer" value="customer">VTE</option>
                  <option value="provider">HA</option>
                </select>
              </div>

              <div className={styles.appHeaderPeriod}>
                 <FilterbarPartial hide_filter={true} />
              </div>
              
              <div className={styles.appHeaderDisplay}>
                <span>Affichage</span>
                <select name="" id="">
                  <option value="200+">200 Derniers mouvmts</option>
                  <option value="200-">200 Premiers mouvmts</option>
                </select>
              </div>

              <div className={styles.appHeaderSearchbox}>
                <input placeholder="Rechercher un document..." type="search" name="" id=""/>
                <button>
                  <img src={AppIcons.ds_search_icon} alt=""/>
                </button>
              </div>

          </div>

        {/* HEADER BAR - END */}
 
        <div className={styles.appInnerBody}>
        {/* LEFT BLOC */}
          <div className={styles.appToolbarOpts}>
            
             {/* <span onClick={()=>this.setupOtion("edit")}>Editer</span> */}
             <span onClick={()=>this.setupOtion("delete")}>Supprimer</span> 
             {/* <span onClick={()=>this.setupOtion("save")}>Enregistrer</span>
             <span onClick={()=>this.setupOtion("validate")}>Valider</span>  */}
            
          </div>
          <div className={styles.appContentContainerCenter}>
              
              {/* --------- FILE LISTER ---------- */}

                <table id="gentable" className={styles.fixed_header}>
                  <thead>
                    <tr>
                      <th>N°</th>
                      <th>Date</th>
                      <th>N° Piéce</th>
                      <th>N° Compte</th>
                      <th>Libellé</th>
                      <th>Débit</th>
                      <th>Crédit</th>
                      <th>Doc</th>
                    </tr>
                  </thead>
                  <tbody className={styles.fixed_header_tbody_main}>
                    {tr_items}
                  </tbody>
                </table>



                <motion.div animate={{right : this.state.preview_right_pos+'%'}} 
                      transition={{ ease: "easeOut"}}
                    className={styles.appInnerMiddlePreviewContainer}>
                    <div style={{position:"absolute", left:-50, top:0}}>
                      <img src={AppIcons.ds_preview_folder_banner} style={{width:56, height:"auto"}} alt="" />
                      <img  src={AppIcons.ds_preview_folder_icon} 
                      style={{  position: "absolute",
                                width: 34,
                                left: "20%",
                                top: "18%",
                                height: "auto", cursor:"pointer"}}
                                onClick={()=>{this.togglePreview(this.state.current_file_index, this.state.current_file_uri)}}
                                alt="" />
                    </div>
                    <div className={styles.previewFileContainer}>
                      <img src={this.state.mediahost+this.state.current_file_uri} style={{width:"100%", height:"auto"}} alt=""/>
                    </div>
                </motion.div>

                 

          </div>
          
        
        </div>

      </div>
 
  );
  
 }
}

export default ProcessResultScreen;
