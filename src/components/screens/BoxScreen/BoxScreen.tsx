import React, { Component } from 'react';
import styles from '../../global.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
import AppIcons from '../../../icons';
import {history} from '../../../redux/store';

//const history = useHistory();


// import { Link } from 'react-router-dom';
// import routes from '../../../constants/routes.json';
// import { history, configuredStore } from '../../../store';

// Partials
import ToolbarPartial from '../../partials/ToolbarPartial/ToolbarPartial';
import FilterbarPartial from '../../partials/FilterbarPartial/FilterbarPartial';
import SubHeaderPartial from '../../partials/SubHeaderPartial/SubHeaderPartial';
import SubmenuPartial from '../../partials/SubmenuPartial/SubmenuPartial';
import ActivitiesPartial from '../../partials/ActivitiesPartial/ActivitiesPartial';
import DetailsPanelPartial from "../../partials/DetailsPanelPartial/DetailsPanelPartial";
import FilesContainerPartial from "../../partials/FilesContainerPartial/FilesContainerPartial";
// import Files_mocked from '../../../services/mocks/Files_mocked';

import { Icon, Intent, Label, Position, Toaster } from "@blueprintjs/core";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../../redux/mainRedux/mainReduxActions';

// Data
import TesseractServices from '../../../services/TesseractServices';
import Emitter from '../../../services/emitter';
import {reactLocalStorage} from 'reactjs-localstorage';
 
const MAX_FILES = 15;

class BoxScreen extends Component<any, any> {
  _isMounted = false;
  
  constructor(props:any)
  {
    super(props)
    this._isMounted = false;
    this.state = {

      isLoading : false,
      subheader_title : "BOX",
      can_load_more : false,
      toolbar_context_title : "Factures Clients",
      display_searchbar : true,
      menubar_focuser_top :5,
      menubar_index : 0,
      details_visibility : false,
      current_data_type : "customer",
      has_more : true,
      active_month:this.props.mainReduxState.active_month,
      data_loading  : false,
      submenu : [
        {
          title : "Factures Clients",
          route : "customer"
        },
        {
          title : "Factures Fournisseurs",
          route : "provider"
        },
        // {
        //   title : "Documents Compta",
        //   route : "route"
        // },
        // {
        //   title : "Dossier XY",
        //   route : "route"
        // }
      ],
      toolbar_menu : [
        // {
        //   title : "Editer",
        // },
        {
          title : "Supprimer",
        }
      ],
      activities : [ ],

      files : [],

    };


    this.onOneFileClicked = this.onOneFileClicked.bind(this);
    this.onSubmenuClicked = this.onSubmenuClicked.bind(this);
    this.onFilesAdded = this.onFilesAdded.bind(this);
 
  }
  

componentDidMount()
{

  // Reset local data
  this._resetLocalData();
  
  this._isMounted = true;
  //nl
  this._loadMore();

  //nl
  Emitter.on(Emitter.types.FILES_TYPE_CHANGED, (type) => this._onBillsTypeChanged(type));
  Emitter.on(Emitter.types.MONTH_CHANGED, (newMonth) => this.onMonthChanged(newMonth));
  Emitter.on(Emitter.types.FILES_UPDATED, (newValue) => this.onFilesUpdated());

}

componentWillUnmount(){
  this._isMounted = false;
}

//nl
onMonthChanged(month:number)
{

  Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, true);

  // Reset local data
  this._resetLocalData();

  if(this._isMounted)
    this.setState({active_month : month});
  setTimeout(() => {
    
    this._loadMore();
    
  }, 500);
}

// Reset current data
_resetLocalData()
{
  var resetedBills = {files : [], page : 1 };
       
  switch (this.state.current_data_type) {
    case "customer":
      this.props.mainReduxActions.update_customers_bills(resetedBills);
      // setTimeout(() => {
      if(this._isMounted)//   
        this.setState({files : this.props.mainReduxState.customers_bills.files});
      // }, 100);
      break;

    case "provider":
      this.props.mainReduxActions.update_providers_bills(resetedBills);
      // setTimeout(() => {
      if(this._isMounted)//   
        this.setState({files : this.props.mainReduxState.providers_bills.files});
      // }, 100);
      break;      
  }
 

  
}

// nl
_loadMore = (fromUpdate : boolean = false, fromScroll: boolean = false )=>{ // fromUpdate : if e must refresh last fetched data

    this._loadLocal();

    switch (this.state.current_data_type) {
      case "customer":
        this._getCustomersBills(fromUpdate, fromScroll);
        break;

      case "provider":
        this._getProvidersBills(fromUpdate, fromScroll);
        break;      
    }

}

// nl
_loadLocal = ()=>{


  switch (this.state.current_data_type) {
    case "customer":
    if(this._isMounted)  
      this.setState({files : this.props.mainReduxState.customers_bills.files});
      break;

    case "provider":
    if(this._isMounted)  
      this.setState({files : this.props.mainReduxState.providers_bills.files});
      break;      
  }

}

//nl
_getCustomersBills = (fromUpdate : boolean, fromScroll : boolean)=>{


    //Define from scroll : that, we don't set has_more to true  

    if(this._isMounted)
      this.setState({has_more : true});

    // Get current customers bills settings
    var customersBills = this.props.mainReduxState.customers_bills;
    if(fromUpdate)
      this._removePreviousFetchedData(customersBills);
    var currentPage = customersBills.page;
    // Later set companyId to the selected one : TO IMPROVE
    // var user = this.props.mainReduxState.user;
    var selectedCompany = this.props.mainReduxState.selected_company;
    // Get lowest date of the current year : 01/01/currentYear
    // var currentYear = new Date().getFullYear();
    var currentYear = selectedCompany.exercise_year == 0 || !selectedCompany.exercise_year ? new Date().getFullYear() : selectedCompany.exercise_year;
    var realMonth = this.props.mainReduxState.active_month + 1;
    var realMonthString = realMonth.toString();
     

    if(realMonthString.length == 1) realMonthString = "0"+realMonthString;
    var dateStringMin = realMonthString+"/01/"+currentYear.toString();
    var febDays = new Date(currentYear, 2, 0).getDate();
    var monthsEnd = [31,febDays,31,30,31,30,31,31,30,31,30,31];
    var dateStringMax = realMonthString+"/"+monthsEnd[this.props.mainReduxState.active_month]+"/"+currentYear.toString(); // 
    var minDate = new Date(dateStringMin).toISOString();
    var maxDate = new Date(dateStringMax).toISOString();

    var d = {

      companyId : selectedCompany.id,
      type      : this.state.current_data_type,
      minDate   : minDate,
      maxDate   : maxDate,
      page      : currentPage
    };

    // console.log(d);

    TesseractServices._getCompanyBills(selectedCompany.id, this.state.current_data_type, minDate, maxDate ,currentPage).then(resp =>{

      var newCustomersBills = {...customersBills};
      var newFiles = resp.data;
      var newPage = newCustomersBills.page;
      if(resp.data.length > 0) newPage = newPage + 1;

      // console.log(newFiles);
      // Set loader status
      if(newFiles.length < MAX_FILES)
      {
        if(this._isMounted)
          this.setState({has_more : false});
      }

      newCustomersBills = {...newCustomersBills, files : [...newCustomersBills.files, ...newFiles], page : newPage };
       
      this.props.mainReduxActions.update_customers_bills(newCustomersBills);
      
      if(this._isMounted)
        this.setState({files : newCustomersBills.files});

      // Reset from month changed
      setTimeout(() => {
        Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, false);
      }, 300);
  
      }).catch((err)=>{
        
        this._loadingError();
        if(this._isMounted)
          this.setState({has_more : false});
        console.log(err);
      });

}

 
//nl
_getProvidersBills = (fromUpdate : boolean, fromScroll : boolean)=>{

  if(this._isMounted && !fromScroll)  
    this.setState({has_more : true});
    // Get current customers bills settings
    var providersBills = this.props.mainReduxState.providers_bills;
    if(fromUpdate)
      this._removePreviousFetchedData(providersBills);
    var currentPage = providersBills.page;
    // Later set companyId to the selected one : TO IMPROVE
    // var user = this.props.mainReduxState.user;
    var selectedCompany = this.props.mainReduxState.selected_company;

    // Get lowest date of the current year : 01/01/currentYear
    // var currentYear = new Date().getFullYear();
    var currentYear = selectedCompany.exercise_year == 0 || !selectedCompany.exercise_year ? new Date().getFullYear() : selectedCompany.exercise_year;
    var realMonth = this.props.mainReduxState.active_month + 1;
    var realMonthString = realMonth.toString();
     
    if(realMonthString.length == 1) realMonthString = "0"+realMonthString;
    var dateStringMin = realMonthString+"/01/"+currentYear.toString();
    var febDays = new Date(currentYear, 2, 0).getDate();
    var monthsEnd = [31,febDays,31,30,31,30,31,31,30,31,30,31];
    var dateStringMax = realMonthString+"/"+monthsEnd[this.props.mainReduxState.active_month]+"/"+currentYear.toString(); // 
    var minDate = new Date(dateStringMin).toISOString();
    var maxDate = new Date(dateStringMax).toISOString();


    TesseractServices._getCompanyBills(selectedCompany.id, this.state.current_data_type, minDate, maxDate ,currentPage).then(resp =>{

      var newProvidersBills = {...providersBills};
      var newFiles = resp.data;
      var newPage = newProvidersBills.page;
      if(resp.data.length > 0) newPage = newPage + 1;

      // Set loader status
      if(newFiles.length < MAX_FILES)
      {
        if(this._isMounted)
          this.setState({has_more : false});
      }

      newProvidersBills = {...newProvidersBills, files : [...newProvidersBills.files, ...newFiles], page : newPage };
      
      if(this._isMounted)// 
        this.setState({data_loading : false});
      if(this._isMounted)// 
        this.setState({global_files : resp.data, data_loading : false});
      this.props.mainReduxActions.update_providers_bills(newProvidersBills);
      
      if(this._isMounted)
        this.setState({files : newProvidersBills.files});

      // Reset from month changed
      setTimeout(() => {
        Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, false);
      }, 300);


      }).catch((err)=>{
      
        this._loadingError();
        if(this._isMounted)
          this.setState({has_more : false});
        console.log(err);
      });

}


//nl
_removePreviousFetchedData(data : any){

  var files : any[] = data.files;
  var page = data.page;
  // >
  if(files.length < (page * MAX_FILES))
  {
    var delStartIndex = ((page - 1) * MAX_FILES);
    var itemsToDel = files.length - delStartIndex;
    files.splice(delStartIndex, itemsToDel);
  }

  data.files = files;
}

//nl
_loadingError()
  {
    const AppToaster = Toaster.create({
      className: "recipe-toaster",
      position: Position.TOP,
    });
    AppToaster.show({ intent:Intent.WARNING, message: "Erreur de chargement" });
  }

  setDataType(type : string)
  {
    var files = this.props.mainReduxState.files.filter((e)=>e.type == type);
    files.sort((a) => (a.status != "none") ? 1 : -1)
    if(this._isMounted)
      this.setState({files : files});

    // Emitter.emit(Emitter.types.FILES_TYPE_CHANGED, type);

  }

  onSwitchToTypeData(type : string)
  {
    this.setDataType(type);
  }

  //nl
  _onBillsTypeChanged(type :string)
  {

    Emitter.emit(Emitter.types.FROM_MONTH_CHANGED, true);

    // Reset local data
    this._resetLocalData();
  
    // console.log(type);
    // LoadMore
    if(this._isMounted)
      this.setState({current_data_type : type});
    setTimeout(() => {
      this._loadMore();
    }, 500);
  }

  onOneFileClicked(fileIndex : number) {
    //  Get File Data to display (Preview) + (Details Preview) 
    // console.log("File clicked : " + fileIndex)
  }
 
  onFilesAdded() {
    
    // Let's update current type bills page number (decrement)

    switch (this.state.current_data_type) {
      case "customer":

        // BOX
        var customersUpdatedBills = {...this.props.mainReduxState.customers_bills};
        if(customersUpdatedBills.page > 1)
          customersUpdatedBills.page = customersUpdatedBills.page - 1;

        this.props.mainReduxActions.update_customers_bills(customersUpdatedBills);

        // PROCESSING
        var customersUpdatedProcessableBills = {...this.props.mainReduxState.customers_processable_bills};
        if(customersUpdatedProcessableBills.page > 1)
          customersUpdatedProcessableBills.page = customersUpdatedProcessableBills.page - 1;

        // Additional line
        this._removePreviousFetchedData(customersUpdatedProcessableBills);

        this.props.mainReduxActions.update_customers_processable_bills(customersUpdatedProcessableBills);

        break;
  
      case "provider":

        // BOX
        var providersUpdatedBills = {...this.props.mainReduxState.providers_bills};
        if(providersUpdatedBills.page > 1)
          providersUpdatedBills.page = providersUpdatedBills.page - 1;

        this.props.mainReduxActions.update_providers_bills(providersUpdatedBills);

        // PROCESSING
        var providersUpdatedProcessableBills = {...this.props.mainReduxState.providers_processable_bills};
        if(providersUpdatedProcessableBills.page > 1)
          providersUpdatedProcessableBills.page = providersUpdatedProcessableBills.page - 1;

        // Additional line
        this._removePreviousFetchedData(providersUpdatedProcessableBills);

        this.props.mainReduxActions.update_providers_processable_bills(providersUpdatedProcessableBills);

        break;      
    }
  
    // Then Load more
    setTimeout(() => {
      this._loadMore(true); //fromUpdate
    }, 500);
  }

  onFilesUpdated() {
    
    // Then Load more
    // console.log("DERT")
    setTimeout(() => {
      this._loadMore();
    }, 500);

  }
 
  onToolbarMenuItemClicked = (option_name : string, type :string) =>
  {
    // Che ckout which method to call
    switch (option_name) {
      case "Editer": // Editer
      this.props.mainReduxActions.edit_box_items();
      break;
      case "Supprimer" : // Supprimer
        this.props.mainReduxActions.remove_box_items();
      break;
     
    }
  }

  
  onSubmenuClicked = (index : number) =>{

    var current_data_type = this.state.submenu[index].route;
    if(this._isMounted)
      this.setState({
        toolbar_context_title : this.state.submenu[index].title,
        current_data_type : current_data_type
    });
    //Switch content
    // this.onSwitchToTypeData(current_data_type);

  }


 render() {

  // ANIMATIONS PROPS

  return (
    
      <div className={styles.appContentContainer} style={{overflowY:'scroll'}}>

        <SubHeaderPartial title={this.state.subheader_title}  display_hmenu={false}  display_searchbar={this.state.display_searchbar}/>

        <div className={styles.appInnerBody}>
        {/* LEFT BLOC */}
          <div className={styles.appContentContainerLeft}>

            <SubmenuPartial submenu={this.state.submenu} onSubmenuClicked={this.onSubmenuClicked} onFilesAdded={this.onFilesAdded} />

            {/* <ActivitiesPartial activities={this.state.activities} /> */}

          </div>
        
        {/*MIDDLE BLOC */}

          <div className={styles.appContentContainerCenter}>
              
              <ToolbarPartial title={this.state.subheader_title} context_title={this.state.toolbar_context_title} menu={this.state.toolbar_menu} files={this.state.files} display_stats={false} display_options={true} display_count={true} 
                onToolbarMenuItemClicked={this.onToolbarMenuItemClicked}  />

              <FilterbarPartial/>

              {/* --------- INNER MIDDLE ---------- */}

              <FilesContainerPartial edit={this.props.mainReduxState.edit_box_items} remove={this.props.mainReduxState.remove_box_items} data_loading={this.state.data_loading} files={this.state.files} has_more={this.state.has_more} onOneFileClicked={this.onOneFileClicked}  _loadMore={this._loadMore} can_load_more={this.state.can_load_more} />

          </div>
          
        {/* RIGHT BLOC */}

        <DetailsPanelPartial display={this.state.details_visibility} />

        </div>

      </div>
 
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BoxScreen);
