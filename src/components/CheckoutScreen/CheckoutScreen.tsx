import React, { Component } from 'react';
import styles from './style.module.css';
import globalStyles from '../global.module.css';
// import {useSpring, animated} from 'react-spring';
import { motion } from "framer-motion";
// import TesseractServices from '../services/TesseractServices';
import AppIcons from '../../icons';

import routes from '../../constants/routes.json';
// import { history } from '../../store';

import UserServices from '../../services/UserServices';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as mainReduxActions from '../../redux/mainRedux/mainReduxActions';

// import { fromBase64, fromBuffer } from "pdf2pic";
import {history} from '../../redux/store';
import {reactLocalStorage} from 'reactjs-localstorage'; 
import { Dimmer, Loader } from 'semantic-ui-react';

import { loadStripe } from '@stripe/stripe-js';
import TesseractServices from '../../services/TesseractServices';
import packs from '../../constants/packs';
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe('pk_test_51Iw7QaHjs6zu68wd4D6DfA1VLzjSwqV6SYPiecrqszgxtAEG9DorNldPe8F6VaSOqXon0KTlQPcjhrFC0jtgZbY6001hzIudEK');

const height = window.innerHeight;
//const history = useHistory();


 
class CheckoutScreen extends Component<any, any> {
  constructor(props:any)
  {
    super(props)
    
    this.state = {

      checkout_status : false,
      checkout_message : "",
      session_id : "",
      pack_id : 0,
      logged_company_id : "",

      isChecking : false,
      
    }

  }
   
 
  componentDidMount()
  {

    
    const href = window.location.href;
    const params = href.split('?')[1];
    const query = new URLSearchParams(params);

    if (query.get("success")) {
      // console.log(query.values());

      var session_id = query.get("session_id");
      var pack_id = query.get("pack_id") || "0";


      this.setState({
        checkout_status : true,
        session_id : session_id,
        pack_id : pack_id
      })


      // Update new subscription
      this.setupSubscription(session_id, parseInt(pack_id));

    }

    if (query.get("canceled")) {
      this.setState({checkout_status : false})
    }



  }


  setupSubscription = (session_id : any, pack_id : any)=>
  {
    // Get company Id
    var loggedCompany = reactLocalStorage.getObject('ls_loggedCompany');
    this.setState({logged_company_id : loggedCompany.id});
    var companyId = loggedCompany.id;

    var currentPack = loggedCompany.pack;

    // Get pack and define a model for api
    var pack = packs[pack_id];
    var definedPack = 
    {
      name: pack.name,
      paymentId: session_id,
      // expiracy: new Date(),
      price: pack.price,
      access: pack.access,
      ds_scan: pack.ds_scan,
      ds_bank: pack.ds_bank,
      ds_archi: pack.ds_archi,
      ds_ai: pack.ds_ai,
      ds_fact: pack.ds_fact
    };


    // Get session if payed (Avoid fraud)
  if(!currentPack.paymentId || currentPack.paymentId != session_id)
  {

    if(session_id) {
  
      this.setState({isChecking : true, checkout_message : "Vérification de l'abonnement..."});
  
      fetch("http://dscompta-api.herokuapp.com/checkout-session/" + session_id)
      .then(function(result){
        return result.json()
      })
      .then((session)=>{
        // 
        
        // console.log(session.payment_status);
        if(session.payment_status == "paid")
        {


          var data = {
            pack: definedPack
          }
      
      
          UserServices._updateCompanyConfig(companyId, data).then((res)=>{
            
            this.setState({isChecking : false, checkout_message : "Votre nouvel abonnement a été activé avec succés !"});

            // console.log("Updated");
            var newCompany = loggedCompany;
            newCompany.pack = data.pack;
  
            // Update current redux state + localstorage
            this.props.mainReduxActions.update_company(newCompany);
            
            }).catch((err)=>{
            console.log(err);
            
            this.setState({isChecking : false, checkout_message : "Erreur lors de la vérification.Veuillez réessayer svp !"});
            

          })

        }
      })
      .catch((err)=>{
        console.log('Error when fetching Checkout session', err);
        this.setState({isChecking : false, checkout_message : "Erreur lors de la vérification.Veuillez réessayer svp !"});
        
      });

    }
  }
  else
  {
    this.setState({isChecking : false, checkout_message : "Abonnement déja activé !"});
  }
  





  }
 

  

  getSession = async(session_id : string)=>
  {

    const stripe = require('stripe')("sk_test_51Iw7QaHjs6zu68wd3Z8ip1qXYyflRIJkTNV9TBzWO3fFSAJWcW9PuS8CJkZ0lAslnBX9xTeyQ5dYe5LTzkGTZOrj00p9Iu8cZR", {
      apiVersion: '2020-08-27',
      appInfo: { // For sample support and debugging, not required for production:
        name: "Dokcompta subscriptions",
        version: "0.0.1",
        url: "https://app.dokcompta.com"
      }
    });


    const session = await stripe.checkout.sessions.retrieve(
      session_id
    );

    return session;
  }

  gotoSignup = ()=>
  {
    history.replace(routes.SIGNUP)
  }
 

 render() {

// ANIMATIONS PROPS


  return (
    <div className={styles.container} data-tid="container" style={{height : (height ) , backgroundImage: `url(${AppIcons.ds_backg_2})`, backgroundSize: "cover", backgroundRepeat:"no-repeat" }}>
       
       <Dimmer active={this.state.isChecking}>
        <Loader content='Validation...' />
      </Dimmer>

       <div style={{display : "flex", flexDirection:"column", alignItems:"center", width:"100%", marginTop:"0%"}}>
          <img src={AppIcons.ds_logo_big} style={{width : 80, height : "auto"}}  alt="" />
          <span style={{fontSize : 22, color : "#ffffff",fontWeight:"bolder" }}>Dok Compta</span>
       </div>

       <div style={{display : "flex",width: "100%",
          flex: 2,
          justifyContent: "center",
          alignSelf: "center",
          alignItems: "flex-start",
          justifySelf: "flex-end",
          marginTop: 100}}>
          
          <div style={{display:'flex', paddingBottom:10, flexDirection:'column', width:'40%', backgroundColor : "#ffffff", borderRadius:4, textAlign:'center'}} >
            
            <p style={{marginTop:10}}>ABONNEMENT</p>

            {this.state.checkout_status == true && 

              <span style={{fontSize:14, fontFamily:'Poppins', padding:'10px 0px'}}>{this.state.checkout_message}</span>

            }

            {this.state.checkout_status == false && 

            <span style={{fontSize:14, fontFamily:'Poppins', padding:'10px 0px'}}>Requête annulée avec succés !</span>

            }

            <span style={{color : "#111111",fontFamily:"Poppins", fontSize:14, fontWeight : "bolder", marginTop:30}}><span onClick={()=>{history.replace(routes.HOME) }} style={{color:"#AB3134", cursor:"pointer"}}> Retourner à l'accueil</span> </span> 

          </div>

       </div>
      

    </div>
  );
  
 }
}


function mapStateToProps(state:any) {
  return {
    mainReduxState: state.mainReduxState
  }
}

function mapDispatchToProps(dispatch:any) {
  return {
    mainReduxActions: bindActionCreators(mainReduxActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutScreen);
