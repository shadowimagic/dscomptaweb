import axios, { AxiosRequestConfig } from 'axios';

let config : AxiosRequestConfig = {
  headers: {  
            "Access-Control-Allow-Origin" : "*",
            Accept: 'application/json', 
            'Content-Type': 'application/json' 
          },
  responseType: 'json'
};


// let baseUrl = 'https://dscompta-api.herokuapp.com';
let baseUrl = 'https://dscompta-api.herokuapp.com';
// let baseUrl = 'http://localhost:3000';


async function _createUser(_data:any) {

  return await axios.post(baseUrl+'/users', _data);

}

async function _updateUser(id: string, _data:any) {

  return await axios.patch(baseUrl+'/users/'+id, _data, config );

}

async function _deleteUser(id: string) {

  return await axios.delete(baseUrl+'/users/'+id, config );

}

async function _sendMail(data:any) {

  return await axios.post(baseUrl+'/send_user_mail', data);

}

async function _createCompany(data : any) {

  return await axios.post(baseUrl+'/companies', data);
}

async function _checkLicense(license : string) {

  let filter = JSON.stringify({"where" : {"id" : license}}) ;

  return await axios.get(baseUrl+'/licenses?filter='+filter)
}

async function _getCompanyByLicense(license : string) {

  let filter = JSON.stringify({"where" : {"license" : license}}) ;

  return await axios.get(baseUrl+'/companies?filter='+filter)
}

async function _getCompanyById(id : string) {

  return await axios.get(baseUrl+'/companies/'+id);
}

async function _useLicense(license : string) {

  let data = {"is_used" : true};
  return await axios.put(baseUrl+'/licenses/'+license, data, config);

}

async function _setCompanyOwnership(id : string) {

  let data = {"has_owner" : true};
  return await axios.put(baseUrl+'/companies/'+id, data, config);

}

async function _getCompanyAffiliatedCompanies(companyId : string) {

  let filter = JSON.stringify({"where" : {"affiliatedId" : companyId}}) ;

  return await axios.get(baseUrl+'/companies?filter='+filter)
}



//@Gets feed Ads
async function _signup(data : any) {

    return await axios.post(baseUrl+'/users', data);

}

async function _signin(email : string, password : string) {

  let filter = JSON.stringify({"where" : {"email" : email, "password" : password}}) ;

  return await axios.get(baseUrl+'/users?filter='+filter)

}
 

// Bills Section

async function _createCustomerCard(data : any) {

  return await axios.post(baseUrl+'/customer-cards', data);
}

async function _updateCustomerCard(id: string, _data:any) {

  let data = _data;

  return await axios.patch(baseUrl+'/customer-cards/'+id, data, config );

}

async function _checkCustomerCardByName(name: string) {

  let filter = JSON.stringify({"where" : {"name": name.toLowerCase() }});
  return await axios.get(baseUrl+'/customer-cards?filter='+filter);

}

async function _getCompanyCustomerCards(companyId: string) {

  let filter = JSON.stringify({"where" : {"companyId": companyId }});
  return await axios.get(baseUrl+'/customer-cards?filter='+filter);

}

async function _createComputedBill(data : any) {

  return await axios.post(baseUrl+'/computed-bills', data);
}

async function _createComputedBillItem(data : any) {

  return await axios.post(baseUrl+'/computed-bill-items', data);
}

async function _getComputedBills(companyId : string, type : string ,status : string, page : number) {
  
  let _limit = 30;
  let _skip = _limit * (page-1); 
  let _type = "bill";
  if(type == "quotes") _type = "quote"; 

  let filter;
  filter = JSON.stringify({"where" : {"companyId" : companyId, "type" : _type}, "limit": _limit, "skip": _skip});
  if(status != "all")
    filter = JSON.stringify({"where" : {"companyId" : companyId , "type" : _type, "status" : status }, "limit": _limit, "skip": _skip});

  return await axios.get(baseUrl+'/computed-bills?filter='+filter)
}


async function _updateComputedBill(id: string, data:any) {

  return await axios.patch(baseUrl+'/computed-bills/'+id, data, config );

}

async function _deleteComputedBill(id: string) {

  return await axios.delete(baseUrl+'/computed-bills/'+id);

}


// Archive Section

async function _initGed(data:any) {

  return await axios.post(baseUrl+'/archives', data);

}

async function _updateGed(id: string, _data:any) {

  let data = {"files" : _data};

  return await axios.patch(baseUrl+'/archives/'+id, data, config );

}


async function _getCompanyGed(companyId: string) {

  let filter = JSON.stringify({"where" : {"companyId" : companyId}});
  return await axios.get(baseUrl+'/archives?filter='+filter);

}

async function _checkCompanyByName(name: string) {

  let filter = JSON.stringify({"where" : {"name" : {"like": name,"options": "i" } }});
  return await axios.get(baseUrl+'/companies?filter='+filter);

}


async function _getCompanyUsers(companyId: string) {

  var path = `/companies/${companyId}/users`;
  return await axios.get(baseUrl+path);

}

async function _chonkize() {

  return await axios.get(baseUrl+'/users');

}

/////////// COMPANY ////////////////////

async function _getCompanyData(companyId: string) {

  return await axios.get(baseUrl+'/companies/'+companyId);

}

async function _updateCompanyConfig(id: string, data:any) {

  return await axios.patch(baseUrl+'/companies/'+id, data, config );

}

async function _updateCompany(id: string, data:any) {

  return await axios.patch(baseUrl+'/companies/'+id, data, config );

}


async function _deleteCompany(id: string) {

  return await axios.delete(baseUrl+'/companies/'+id, config );

}
//////////////////////////////


//////////////// BANK ////////////////////

async function _getBankCards(companyId: string) {

  let filter = JSON.stringify({"where" : {"companyId": companyId }});
  return await axios.get(baseUrl+'/bank-cards?filter='+filter);

}

async function _createBankCard(data : any) {

  return await axios.post(baseUrl+'/bank-cards', data);
}

async function _updateBankCard(id: string, data:any) {

  return await axios.patch(baseUrl+'/bank-cards/'+id, data, config );

}

async function _deleteBankCard(id: string) {

  return await axios.delete(baseUrl+'/bank-cards/'+id);

}



async function _createBankStatement(data : any) {

  return await axios.post(baseUrl+'/bank-statements', data);
}

async function _getBankStatements(companyId: string, month : string, year : string) {

  let filter = JSON.stringify({"where" : {"companyId": companyId, "month" : month, "year" : year }});
  return await axios.get(baseUrl+'/bank-statements?filter='+filter);

}


async function _getAllBankStatements(companyId: string, year : string) {

  let filter = JSON.stringify({"where" : {"companyId": companyId, "year" : year }});
  return await axios.get(baseUrl+'/bank-statements?filter='+filter);

}

//////////////////////////////////////////
  
const UserServices = {

  // Users

  _updateUser,
  _deleteUser,
  _createUser,
  _sendMail,

  _checkLicense,
  _useLicense,
  _createCompany,
  _updateCompany,
  _deleteCompany,
  _setCompanyOwnership,
  _getCompanyByLicense,
  _getCompanyById,
  _checkCompanyByName,
  _signup,
  _signin,
  _getCompanyAffiliatedCompanies,


  ///BILLS
  _createCustomerCard,
  _updateCustomerCard,
  _checkCustomerCardByName,
  _getCompanyCustomerCards,


  _createComputedBill,
  _createComputedBillItem,
  _getComputedBills,
  _updateComputedBill,
  _deleteComputedBill,

  ////
  ///GED
  _initGed,
  _updateGed,
  _getCompanyGed,

  _chonkize,

  /// COMPANY
  _getCompanyData,
  _updateCompanyConfig,
  _getCompanyUsers,


  /// BANK
  _createBankCard,
  _updateBankCard,
  _deleteBankCard,
  _getBankCards,

  _createBankStatement,
  _getBankStatements,
  _getAllBankStatements,

}

export default UserServices;
