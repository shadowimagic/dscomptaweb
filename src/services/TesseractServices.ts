import axios, { AxiosRequestConfig } from 'axios';

let config : AxiosRequestConfig = {
  headers: {  
              "Access-Control-Allow-Origin" : "*",
              Accept: 'application/json', 
             'Content-Type': 'application/json' 
          },
  responseType: 'json'
};




// let baseUrl = 'https://dscompta-api.herokuapp.com';
let baseUrl = 'https://dscompta-api.herokuapp.com';
// let baseUrl = 'http://localhost:3000';

//@Gets feed Ads
async function _getFileResult(uri : string) {

    var data = { uri : uri };

    return await axios.post(baseUrl+'/tess', data);

}


async function _getCompanyBills(companyId : string, billsType: string, minDate: string , maxDate: string, page : number) {
  
  let _limit = 15;
  let _skip = _limit * (page-1);
  
  let filter = JSON.stringify({"where" : {"and" : [{"is_exported" : false}, {"type" : billsType}, {"date" : {"between":[minDate,maxDate]}}  ]},  "limit": _limit, "skip": _skip });
 
  return await axios.get(baseUrl+'/companies/'+companyId+'/bills?filter=' + filter);

}


async function _getCompanyExerciseBills(companyId : string, minDate: string , maxDate: string) {
  
  
  let filter = JSON.stringify({"where" : {"and" : [{"is_exported" : false}, {"date" : {"between":[minDate,maxDate]}}  ]} });
 
  return await axios.get(baseUrl+'/companies/'+companyId+'/bills?filter=' + filter);

}

async function _getBills(companyId : string) {
  
  return await axios.get(baseUrl+'/companies/'+companyId+'/bills');

}


async function _getProcessableBills(companyId : string) {
  
  let filter = JSON.stringify({"where" : {"or" : [{"is_exported" : false}, {"is_exported" : undefined}]}}) ;
 
  return await axios.get(baseUrl+'/companies/'+companyId+'/bills?filter=' + filter);

}

async function _getProcessingBills(processableBills : any[]) {
  
  var idFilters = [];
  for (let i = 0; i < processableBills.length; i++) 
  {
    const el = processableBills[i];
    //@ts-ignore
    idFilters.push({"id" : el.id});
  }

  let filter = JSON.stringify({"where" : {"or" : idFilters }}) ;
 
  return await axios.get(baseUrl+'/bills?filter=' + filter);

}
 

async function _initBillsProcessing(files : any[]) {

  // var data = { status : "progress" };
  // let filter = JSON.stringify({"where" :{"status" : "none"}}) ;

  // return await axios.patch(baseUrl+'/bills?filter=' + filter , data );
    return await axios.post(baseUrl+'/tess' , files );

}

//@
async function _uploadBill(data : any) {

  return await axios.post(baseUrl+'/bills', data);

}
 
//@
async function _uploadFile(data : any) {

  return await axios.post(baseUrl+'/files', data, {
    headers: {
        'content-type': 'multipart/form-data'
    }});

}
 
//@ Remove bill
async function _removeBill(id : string) {

  return await axios.delete(baseUrl+'/bills/'+id);

}





async function _upgradePack(data : any) {

  return await axios.post('http://localhost:3000/upgrade', data);

}

 
 

const TesseractServices = {
  _getBills,
  _getProcessableBills,
  _getProcessingBills,
  _uploadBill,
  _uploadFile,
  _getFileResult,

  _getCompanyBills,
  _getCompanyExerciseBills,

  _removeBill,
  _initBillsProcessing,




  _upgradePack
}

export default TesseractServices;
