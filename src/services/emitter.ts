import EventEmitter from 'eventemitter3';

const eventEmitter = new EventEmitter();

const EmitterTypes = {
    CLOSE_POPOVER : "CLOSE_POPOVER",
    FILES_UPDATED : "FILES_UPDATED",
    FILES_TYPE_CHANGED : "FILES_TYPE_CHANGED",
    MONTH_CHANGED : "MONTH_CHANGED",
    FROM_MONTH_CHANGED : "FROM_MONTH_CHANGED",
    FILES_COUNT : "FILES_COUNT",
    FILE_TOGGLED : "FILE_TOGGLED",
    CREATE_BILL : "CREATE_BILL",
    CHANGE_BILL_VIEW : "CHANGE_BILL_VIEW",
    DISPLAY_GED_FILE : "DISPLAY_GED_FILE",

    //COMPUTED BILLS
    UPDATE_COMPUTED_BILLS_TAB : "UPDATE_COMPUTED_BILLS_TAB",

    CHANGE_EXERCISE_YEAR : "CHANGE_EXERCISE_YEAR",

}

const Emitter = {
  on: (event, fn) => eventEmitter.on(event, fn),
  once: (event, fn) => eventEmitter.once(event, fn),
  off: (event, fn) => eventEmitter.off(event, fn),
  emit: (event, payload) => eventEmitter.emit(event, payload),
  types : EmitterTypes
}


Object.freeze(Emitter);

export default Emitter;