
const Files_mocked = {
  
    box_files : [
          
        {
            title : "File Box No 1",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Box No 2",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Box No 3",
            route : "route",
            status: "none",
            type  : "provider"
        },
        {
            title : "File Box No 4",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Box No 5",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Box No 6",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 7",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Box No 8",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 9",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 10",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Box No 11",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Box No 12",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 13",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 14",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 15",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Box No 16",
            route : "route",
            status: "success",
            type  : "customer"
        }
    ],

    processing_files : [
          
        {
            title : "File Process No 1",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Process No 2",
            route : "route",
            status: "none",
            type  : "provider"
        },
        {
            title : "File Process No 3",
            route : "route",
            status: "none",
            type  : "customer"
        },
        {
            title : "File Process No 4",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Process No 5",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 6",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Process No 7",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Process No 8",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 9",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 10",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 11",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 12",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 13",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 14",
            route : "route",
            status: "success",
            type  : "provider"
        },
        {
            title : "File Process No 15",
            route : "route",
            status: "success",
            type  : "customer"
        },
        {
            title : "File Process No 16",
            route : "route",
            status: "success",
            type  : "customer"
        }
    ]

}

export default Files_mocked;
