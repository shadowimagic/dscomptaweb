
import ds_logo_big from './assets/images/ds_logo_big.png';
import ds_logo_min from './assets/images/ds_logo_min.png';
import ds_home_icon from './assets/images/ds_home_icon.png';
import ds_box_icon from './assets/images/ds_box_icon.png';
import ds_ged_icon from './assets/images/ds_ged_icon.png';
import ds_bank_icon from './assets/images/ds_bank_icon.png';
import ds_processing_icon from './assets/images/ds_processing_icon.png';
import ds_stats_icon from './assets/images/ds_stats_icon.png';
import ds_devis_icon from './assets/images/ds_devis_icon.png';
import ds_gear_icon from './assets/images/ds_gear_icon.png';

import ds_plus_green_icon from './assets/images/ds_plus_green_icon.png';
import ds_plus_gray_icon from './assets/images/ds_plus_gray_icon.png';

import ds_service_icon from './assets/images/ds_service_icon.png';


import ds_fileitem_icon from './assets/images/ds_fileitem_icon.png';
import ds_preview_folder_banner from './assets/images/ds_preview_folder_banner.png';
import ds_preview_folder_icon from './assets/images/ds_preview_folder_icon.png';
import ds_filter_dropper_icon from './assets/images/ds_filter_dropper_icon.png';
import ds_grid_display_icon from './assets/images/ds_grid_display_icon.png';


import ds_search_icon from './assets/images/ds_search_icon.png';
import ds_stat_success_icon from './assets/images/ds_stat_success_icon.png';
import ds_stat_pending_icon from './assets/images/ds_stat_pending_icon.png';
import ds_stat_failed_icon from './assets/images/ds_stat_failed_icon.png';

import ds_doc_icon from './assets/images/ds_doc_icon.png';
import ds_statements_icon from './assets/images/ds_statements_icon.png';
import ds_bank_bicig_icon from './assets/images/ds_bank_bicig_icon.jpg';
import ds_bank_orabank_icon from './assets/images/ds_bank_orabank_icon.jpg';
import ds_bank_sg_icon from './assets/images/ds_bank_sg_icon.png';

import ds_company_icon from './assets/images/ds_company_icon.png';
import ds_pack_1_icon from './assets/images/ds_pack_1_icon.png';
import ds_pack_2_icon from './assets/images/ds_pack_2_icon.png';
import ds_pack_3_icon from './assets/images/ds_pack_3_icon.png';

import ds_facture_model from './assets/images/facture_model.png';

import ds_license_expired from './assets/images/ds_license_expired.png';


import ds_menubar_focuser_icon from './assets/images/ds_menubar_focuser.png';
import ds_sync_icon from './assets/images/ds_sync_icon.png';
import ds_cross_icon from './assets/images/ds_cross_icon.png';
import ds_signup_banner from './assets/images/ds_signup_banner.png';
import ds_signup_loading from './assets/images/signup_loading.gif';
import ds_spinner_icon from './assets/images/spinner_1.gif';

import ds_signup_select_icon from './assets/images/signup_select_icon.png';
import ds_signin_select_icon from './assets/images/signin_select_icon.png';
import ds_bill_logo_selector from './assets/images/ds_bill_logo_selector.png';
import ds_signing_logo from './assets/images/ds_signing_logo.png';
import ds_exercise_icon from './assets/images/ds_exercise_icon.png';
// backgs
import ds_backg_1 from './assets/images/ds_backg_1.jpg';
import ds_backg_2 from './assets/images/ds_backg_2.jpg';
import ds_actu_1 from './assets/images/ds_actu_1.jpg';
import ds_actu_2 from './assets/images/ds_actu_2.jpg';
import ds_actu_3 from './assets/images/ds_actu_3.jpg';


 

const AppIcons = {
    ds_logo_big,
    ds_logo_min,
    ds_home_icon,
    ds_box_icon,
    ds_bank_icon,
    ds_ged_icon,
    ds_processing_icon,
    ds_stats_icon,
    ds_devis_icon,
    ds_gear_icon,
    ds_fileitem_icon,
    ds_filter_dropper_icon,
    ds_grid_display_icon,
    ds_preview_folder_banner,
    ds_preview_folder_icon,
    ds_search_icon,
    ds_stat_success_icon,
    ds_stat_pending_icon,
    ds_stat_failed_icon,
    ds_doc_icon,
    ds_facture_model,

    ds_bank_bicig_icon,
    ds_bank_orabank_icon,
    ds_bank_sg_icon,

    ds_cross_icon,
    ds_pack_1_icon,
    ds_pack_2_icon,
    ds_pack_3_icon,

    ds_license_expired,

    ds_bill_logo_selector,
    ds_signing_logo,

    ds_plus_green_icon,
    ds_plus_gray_icon,

    ds_menubar_focuser_icon,
    ds_sync_icon,

    ds_statements_icon,

    ds_service_icon,
    ds_company_icon,

    ds_signup_banner,
    ds_signup_loading,
    ds_spinner_icon,
    ds_exercise_icon,

    ds_signup_select_icon,
    ds_signin_select_icon,

    ds_backg_1,
    ds_backg_2,
    ds_actu_1,
    ds_actu_2,
    ds_actu_3,
 
}

export default AppIcons;