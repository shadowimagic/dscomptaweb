 
const packs = [

        {
            index : 0,
            name: "Trial",
            priceId: "",
            price : 0,
            access : 1,
            ds_scan : true,
            ds_bank : true,
            ds_archi : true,
            ds_ai : true,
            ds_fact : true
        },
        {
            index : 1,
            name: "Standard",
            priceId: "price_1IwXvtHjs6zu68wd5wNGavPA",
            price : 26290,
            access : 5,
            ds_scan : true,
            ds_bank : true,
            ds_archi : true,
            ds_ai : false,
            // ds_fact : {dafault :false, price : 6500}
            ds_fact : false
        },
        {
            index : 2,
            name: "Premium",
            priceId: "price_1IwXwQHjs6zu68wdNxdvmkC4",
            price : 46013,
            access : 10,
            ds_scan : true,
            ds_bank : true,
            ds_archi : true,
            ds_ai : true,
            ds_fact : true
        },
        {
            index : 3,
            name: "Entreprise",
            priceId: "",
            price : null,
            access : null,
            ds_scan : true,
            ds_bank : true,
            ds_archi : true,
            ds_ai : true,
            // ds_fact : {dafault :false, price : 6500}
            ds_fact : true
        } 
         
        

]


export default packs;